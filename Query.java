/*Constraints joined by AND's form AND blocks. AND blocks are joined by OR's to form a complete query.
 *This structure may seem limiting, but with some simple logic, the user can make up for it.
 * 
 *Example: To obtain (A and (B or C)), the user can just enter as A and B or A and C, which Query.java will
 *interpret as (A and B) or (A and C), which is logically equivalent.
 * 
 *Similarly, to obtain ((A or B) and C), the user can just enter A and C or B and C, which Query.java will
 *interpret as (A and C) or (B and C), which is logically equivalent.
 */
public class Query{
  private String query[][] = new String[16][16]; //An array of AND blocks, each of which is an array of constraints joined by AND's
  private int iter[] = new int[16]; //Indicates the next position within each AND block.
  private int blockIdx = 0; //Indicates the current AND block index.
  private String sort = null;
  private boolean ascending = true;
  private String source = null;
  public Query(){
    for (int i=0; i<iter.length; i++){
      iter[i]=0;
    }
  }
  public void addAndConstraint(String field, String bool){
    String str = field + " is " + bool;
    addAndConstraint(str);
  }
  public void addAndConstraint(String field, String op, String constraint){
    String str = field + " " + op + " " + constraint;
    addAndConstraint(str);
  }
  private void addAndConstraint(String constraint){
    query[blockIdx][iter[blockIdx]]=constraint;
    iter[blockIdx]++;
    if (iter[blockIdx]==query[blockIdx].length){
      String[] temp = new String[query[blockIdx].length*2];
      for (int i=0; i<query[blockIdx].length; i++){
        temp[i] = query[blockIdx][i];
      }
      query[blockIdx]=temp;
    }
  }
  public void addOrConstraint(String field, String bool){
    String str = field + " is " + bool;
    addOrConstraint(str);
  }
  public void addOrConstraint(String field, String op, String constraint){
    String str = field + " " + op + " " + constraint;
    addOrConstraint(str);
  }
  private void addOrConstraint(String constraint){
    if (blockIdx==0&&iter[0]==0){
      addAndConstraint(constraint);
      return;
    }
    blockIdx++;
    if (blockIdx==query.length){
      String temp[][] = new String[query.length*2][];
      for (int i=0; i<query.length; i++){
        temp[i]=query[i];
      }
      for (int i=query.length; i<query.length*2; i++){
        temp[i] = new String[16];
      }
      query = temp;
      int iTemp[] = new int[iter.length*2];
      for (int i=0; i<iter.length; i++){
        iTemp[i] = iter[i];
      }
      iter = iTemp;
    }
    query[blockIdx][0]=constraint;
    iter[blockIdx]++;

  }
  public void removeConstraint(){
    if (iter[0]==0) return;
    if (iter[blockIdx]<=1){
      query[blockIdx][0]=null;
      iter[blockIdx]=0;
      if (blockIdx!=0) blockIdx--;
    }else{
      query[blockIdx][iter[blockIdx]-1]=null;
      iter[blockIdx]--;
    }
  }
  public void clearConstraints(){
    query = new String[16][16];
    iter = new int[16];
    blockIdx=0;
    for (int i=0; i<iter.length; i++){
      iter[i]=0;
    }
  }
  public boolean ascendingOrder(){
    if (ascending) return true;
    return false;
  }
  public void setSort(String str){
    sort = str;    
  }
  public String getSort(){
    return sort;
  }
  public void setSource(String str){
    source = str;
  }
  public String getSource(){
    return source;
  }
  public String[][] getQuery(){
    return query;
  }
  public String getText(){
    String str = "";
    if (iter[blockIdx]==0) return "";
    for (int i=0; i<=blockIdx; i++){
      if (i!=0) str+=" OR ";
      str+="( ";
      for (int j=0; j<iter[i]; j++){
        if (j!=0) str+=" AND ";
        str+=query[i][j];
      }
      str+=" )";
    }
    return str;
  }
  public static void main (String[] args){
    Query qry = new Query();
    qry.addAndConstraint("A");
    qry.addAndConstraint("C");
    qry.addOrConstraint("B");
    qry.addAndConstraint("C");
    System.out.println(qry.getText());
  }
}