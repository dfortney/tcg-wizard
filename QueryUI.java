import java.awt.*;

import javax.swing.*;

import java.awt.Dimension;
import javax.swing.table.*;

import java.util.*;
import java.awt.event.*;

public class QueryUI extends GenericUI implements KeyListener{
 private static final long serialVersionUID = 1L;
private Query query;
 private JMenuItem mnuExport, mnuExportToDeck;
 private JTable table;
 private JScrollPane scrollpane;
 private JTextArea txtQuery;
 private JButton btnSearch;
 private int matches;
 private JComboBox cbxCardType; //Lets user select a card type, should lock selection once user does anything dependent on card type.
 private JComboBox cbxSourceType;
 private JComboBox cbxSource;
 private JComboBox cbxField;
 private JComboBox cbxOp;
 private JComboBox cbxSortBy;
 private JComboBox cbxOrder;
 private JTextField txtConstraint;
 private JComboBox cbxJoin;
 private JButton btnAdd, btnUndo, btnReset;
// private JPanel pnl; //Main panel, split into pnlParams and pnlResults
 private JPanel pnlParams; 
// private JPanel pnlResults;
 private JPanel pnlCardType; //Contains cbxCardType and label
 private JPanel pnlSource; //Contains cbxSourceType, cbxSource, labels.
 private JPanel pnlSearch; //Contains btnSearch and query.
 private JPanel pnlSubParams;
 private JPanel pnlButtons;
 private String cbxForceExpand = "                                                ";
 private String lblForceExpand = "                                                ";
 private String btnForceExpand = "                                      ";
 private int columns = 10;
 private String[][] queryData;
 QueryUI(){
  super (0,0, "Query");
  pnlSearch = new JPanel(new GridBagLayout());
  pnlParams = new JPanel(new GridBagLayout());
  pnlCardType = new JPanel(new GridBagLayout());
  pnlSource = new JPanel(new GridBagLayout());
  pnlSubParams = new JPanel(new GridBagLayout());
  pnlButtons = new JPanel(new GridBagLayout());
  matches = 10;
  txtQuery = new JTextArea(lblForceExpand);
  txtQuery.setEditable(false);
  txtConstraint = new JTextField();
  btnSearch = new JButton("Search");
  btnSearch.addActionListener(this);
  txtQuery.addKeyListener(this);
  btnSearch.setFocusable(true);
  txtQuery.setFocusable(true);
  txtConstraint.setFocusable(true);
  GridBagConstraints c = new GridBagConstraints();
  GridBagConstraints c2 = new GridBagConstraints();
  table = new JTable(dataModel);
  table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
  scrollpane = new JScrollPane(table);
  scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
  scrollpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
  table.addMouseListener(this);
  TableRowSorter<DefaultTableModel> sort = new TableRowSorter<DefaultTableModel>(dataModel);
  table.setRowSorter(sort);
  table.getTableHeader().setReorderingAllowed(false);  
  table.setDefaultRenderer( Object.class, new BorderLessTableCellRenderer() );
  table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  txtConstraint.setText(cbxForceExpand);
  cbxCardType = new JComboBox(new String[] {cbxForceExpand,"Basic Magic", "Extended Magic", "Pokemon"});
  cbxSourceType = new JComboBox(new String[] {cbxForceExpand,"Collection", "Deck", "All decks in collection", "All collections", "All decks"}); //Options are static
  cbxSource = new JComboBox(new String[] {cbxForceExpand, "Magic", "Pokemon", "Imaginary", "Game", "Names"});//Options depend on sourceType and cardType selections
  cbxField = new JComboBox(new String[] {cbxForceExpand, "Card ID", "Name","Quantity", "Power", "Toughness", "Image", "Notes"}); //Options depend on cardType selection
  cbxOp = new JComboBox(new String[] {cbxForceExpand, "==","!=","<","<=",">",">="}); //Options depend on field selection
  cbxJoin = new JComboBox(new String[] {cbxForceExpand, "AND", "OR"}); //Options are static
  cbxSortBy = new JComboBox(new String[] {"", "Card ID", "Name","Quantity", "Power", "Toughness", "Image", "Notes"}); //Items should always correspond with those in cbxField
  cbxOrder = new JComboBox(new String[] {"", "Ascending", "Descending"});
  btnAdd = new JButton(lblForceExpand+cbxForceExpand);
  btnReset = new JButton(btnForceExpand);
  btnUndo = new JButton(btnForceExpand);
  txtConstraint.setText(cbxForceExpand);
  JLabel lblField = new JLabel(lblForceExpand);
  JLabel lblConstraint = new JLabel(lblForceExpand);
  JLabel lblOp = new JLabel(lblForceExpand);
  JLabel lblJoin = new JLabel(lblForceExpand);
  JLabel lblSortBy = new JLabel("Sort by: ");
  JLabel lblOrder = new JLabel("Sort by: ");  
  JLabel lblCardType = new JLabel(lblForceExpand);
  mnuExport = new JMenuItem("Export Query Results to Make Deck");
  mnuExportToDeck = new JMenuItem("Export Query Results as Deck");
  btnAdd.addActionListener(this);
  btnUndo.addActionListener(this);
  btnReset.addActionListener(this);
  mnuExport.addActionListener(this);
  mnuExportToDeck.addActionListener(this);
  //file.add(mnuExportToDeck);
  //file.add(mnuExport);
  file.add(exit);
  JScrollPane scrollQuery = new JScrollPane(txtQuery, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
  c.gridwidth = 1;
  c.gridy = 0;
  c.gridheight = 3;
  c.insets = new Insets(5,5,5,5);
  pnlSearch.add(scrollQuery,c);
  
  c.gridwidth = 1;
  c.gridx = 1;
  c.gridheight = 1;
  pnlSearch.add(lblSortBy, c);
  c.gridx = 2;
  pnlSearch.add(cbxSortBy, c);
  c.gridy = 1;
  pnlSearch.add(cbxOrder, c);
  c.gridx = 1;
  pnlSearch.add(lblOrder, c);
  c.gridwidth = 2;
  c.gridy=2;
  pnlSearch.add(btnSearch, c);
  
  c2.anchor = GridBagConstraints.FIRST_LINE_START;
  c2.gridheight=3;
  c2.insets = new Insets(0,0,0,0);
  pnlParams.add(pnlSearch, c2);
  
  c.ipady = 0;
  c.ipadx = 0;
  c.gridx =0;
  c.gridy = 0;
  c.gridwidth = 1;
  c.gridheight = 1;
  c.anchor = GridBagConstraints.FIRST_LINE_START;
  pnlCardType.add(lblCardType,c);
  
  c.gridx = 1;
  c.ipady = 0;
  //c.anchor = GridBagConstraints.FIRST_LINE_END;
  pnlCardType.add(cbxCardType,c);
  
  c.gridx = 0;
  c.gridy = 0;
  JLabel lblSourceType = new JLabel(lblForceExpand);
  JLabel lblSource = new JLabel(lblForceExpand);
  pnlSource.add(lblSourceType,c);
  c.gridx=1;
  pnlSource.add(cbxSourceType,c);
  c.gridy=1;
  pnlSource.add(cbxSource,c);
  c.gridx=0;
  pnlSource.add(lblSource,c);

  c.gridx=0;
  c.gridy=0;
  pnlSubParams.add(lblField,c);
  c.gridx=1;
  pnlSubParams.add(cbxField,c);
  c.gridy=1;
  pnlSubParams.add(cbxOp, c);
  c.gridy=2;
  pnlSubParams.add(txtConstraint, c);
  c.gridy=3;
  pnlSubParams.add(cbxJoin,c);
  c.gridy=4;
  //pnlSubParams.add(cbxSortBy,c);
  c.gridy=5;
  //pnlSubParams.add(cbxOrder,c);
  c.gridx=0;
  //pnlSubParams.add(lblOrder,c);
  c.gridy=4;
  //pnlSubParams.add(lblSortBy,c);
  c.gridy=3;
  pnlSubParams.add(lblJoin,c);
  c.gridy=2;
  pnlSubParams.add(lblConstraint,c);
  c.gridy=1;
  pnlSubParams.add(lblOp,c);
  c.gridy=6;
  c.gridwidth=2;
  c.insets = new Insets(10,5,5,5);
  c.anchor = GridBagConstraints.CENTER;
  pnlSubParams.add(btnAdd,c);
  c.gridwidth=1;
  
  GridBagConstraints c3 = new GridBagConstraints();
  c3.anchor = GridBagConstraints.CENTER;
  c3.insets = new Insets(0,5,10,5);
  pnlButtons.add(btnUndo, c3);
  c3.gridx=1;
  pnlButtons.add(btnReset,c3);
  c.gridy=7;
  c.gridwidth=2;
  c.anchor = GridBagConstraints.FIRST_LINE_START;
  pnlSubParams.add(pnlButtons,c);
    
  c2.gridy=3;
  c2.gridx=0;
  c2.gridheight=1;
  pnlParams.add(pnlCardType, c2);
  c2.gridheight=2;
  c2.gridy=4;
  pnlParams.add(pnlSource, c2);
  c2.gridheight=8;
  c2.gridy=6;
  pnlParams.add(pnlSubParams,c2);
  
  c2.gridy=0;
  c2.gridx=0;
  this.add(pnlParams, c2);
  c2.gridx=1;
  this.add(scrollpane,c2);

  /*c.gridwidth = 3;
  c.gridy = 2;
  c.insets = new Insets(0,10,10,10);
  c.anchor = GridBagConstraints.PAGE_START;
  c.gridheight = 2;
  c.fill = GridBagConstraints.VERTICAL;
  add(scrollpane, c);*/
  
  pnlSource.setPreferredSize(new Dimension(pnlSource.getPreferredSize().width, pnlSource.getPreferredSize().height));
  pack();
  Dimension d0 = new Dimension(txtQuery.getSize().width,100);
  txtQuery.setMinimumSize(d0);
  txtQuery.setSize(d0);
  txtQuery.setLineWrap(true);
  scrollQuery.setPreferredSize(d0);
  scrollQuery.setMinimumSize(d0);
  scrollQuery.setSize(d0);
  Dimension dim = lblCardType.getPreferredSize();
  Dimension d = new Dimension(dim.width,dim.height);
  
  lblCardType.setText("Card type:");
  setLabelSize(lblCardType, d);
  lblSourceType.setText("Parameter source type:");
  setLabelSize(lblSourceType, d);
  
  lblSource.setText("Parameter source:");
  setLabelSize(lblSource, d);
  
  lblField.setText("Field:");
  setLabelSize(lblField, d);
  
  lblOp.setText("Operation:");
  setLabelSize(lblOp, d);
  
  lblConstraint.setText("Constraint:");
  setLabelSize(lblConstraint, d);
  
  lblJoin.setText("Join:");
  setLabelSize(lblJoin, d);
  
  Dimension d4 = lblSortBy.getPreferredSize();
  setLabelSize(lblSortBy, d4);
  
  lblOrder.setText("Order:");
  setLabelSize(lblOrder, d4);
  
  Dimension d2 = btnAdd.getPreferredSize();
  Dimension d3 = btnAdd.getPreferredSize();
  d3.width/=2;
  d3.width-=6;
  btnAdd.setText("Add");
  setBtnSize(btnAdd,d2);
  btnReset.setText("Reset");
  btnUndo.setText("Undo");
  setBtnSize(btnReset,d3);
  setBtnSize(btnUndo,d3);
  
  txtConstraint.setText("");
  dim = cbxCardType.getPreferredSize();
  txtConstraint.setMinimumSize(dim);
  txtConstraint.setMaximumSize(dim);
  txtConstraint.setPreferredSize(dim);
  txtConstraint.setSize(dim);
  setCbxSize(cbxCardType, dim);
  setCbxSize(cbxField, dim);
  setCbxSize(cbxSourceType, dim);
  setCbxSize(cbxSource, dim);
  setCbxSize(cbxOp, dim);
  setCbxSize(cbxJoin, dim);
  setBtnSize(btnSearch, new Dimension(dim.width-5,dim.height-5));
  Dimension d5 = (new Dimension(dim.width - d4.width - 10, dim.height));
  setCbxSize(cbxSortBy, d5);
  setCbxSize(cbxOrder, d5);
  cbxCardType.addActionListener(this);
  cbxField.addActionListener(this);
  cbxSourceType.addActionListener(this);
  
  cbxCardType.removeAllItems();
  String cardTypes[] = Core.getCardTypes();
  for (int i=0; i<cardTypes.length; i++){
    cbxCardType.addItem(cardTypes[i]);
  }
  cbxSourceType.removeItemAt(0);
  cbxOp.removeItemAt(0);
  cbxJoin.removeItemAt(0);
  cbxOrder.removeItemAt(0);
  refreshCbxField();
  refreshCbxSource();
  query = new Query();
  query.setSort((String)cbxSortBy.getSelectedItem());
  query.setSource((String)cbxSource.getSelectedItem());
  this.pack();
 }
 private void setBtnSize(JButton btn, Dimension d){
   btn.setMinimumSize(d);
   btn.setPreferredSize(d);
   btn.setMaximumSize(d);
   btn.setSize(d);
 }
 private void setCbxSize(JComboBox cbx, Dimension d){
   cbx.setMinimumSize(d);
   cbx.setPreferredSize(d);
   cbx.setMaximumSize(d);
   cbx.setSize(d);
 }
 private void setLabelSize(JLabel lbl, Dimension d){
   lbl.setMinimumSize(d);
   lbl.setPreferredSize(d);
   lbl.setMaximumSize(d);
   lbl.setSize(d);
 }
 DefaultTableModel dataModel = reInitDataModel();
 private DefaultTableModel reInitDataModel(){
   return new DefaultTableModel() {
     /**
  * 
  */
 private static final long serialVersionUID = 1L;
 @Override
     public boolean isCellEditable(int row, int column){
       return false; 
     }
     public int getColumnCount() {
       return columns;
     }
     public int getRowCount() {
       return matches;
       //Call number of items matching search
     }
     /*public Class getColumnClass(int col){
      return Integer.class; //Actually column classes
      }*/
     public Object getValueAt(int row, int col) {
       if (queryData==null) return "";
       if (queryData.length<=row) return "";
       if (queryData[row].length<=col) return "";
       if (queryData[row][col]==null) return "";
       return queryData[row][col];
     }
     public String getColumnName(int col){
       //System.out.println("Here");
       try{
         String fields[] = Core.getFields((String)cbxCardType.getSelectedItem());
         //System.out.println("Here 2: Source");
         if (col==0) return "Source";
         //System.out.println("Here 3: "+fields[col-1]);
         if (col==fields.length) return fields[col-2];
         if (col==fields.length-1) return fields[col];
         if (col<=fields.length) return fields[col-1];
         //System.out.println("Here 4: ");
       }catch (Exception ex){
       }
       //System.out.println("Here 5: ");
       return " ";
     }
   };
 }
 public static void main(String[] args){
   @SuppressWarnings("unused")
QueryUI test = new QueryUI();
 }
 public void keyPressed(KeyEvent e) {
   if(e.getSource() == txtQuery){
     if(e.getKeyCode() == KeyEvent.VK_ENTER){
       search();
     }
   }
   
 }
 private void reorderColumns(){
   //System.out.println("printing at reorder columns");
   String temp[][] = new String[queryData.length][queryData[0].length];
   for (int i=0; i<queryData.length; i++){
     for (int j=0; j<queryData[0].length; j++){
       //System.out.println(temp[i][j]);
       if (j==1)
         temp[i][j]=queryData[i][2];
       else if (j==2)
         temp[i][j]=queryData[i][1];
       else if (j==queryData[0].length-2)
         temp[i][j]=queryData[i][5];
       else if (j==queryData[0].length-1)
         temp[i][j]=queryData[i][4];
       else if (j==3 || j==0)
         temp[i][j]=queryData[i][j];
       else
         temp[i][j]=queryData[i][j+2];
     }
   }
   queryData = temp;
 }
 private void refreshCbxSource(){
   //Can't handle "All decks in collection" just yet
   if (cbxSourceType.getItemCount()==0) return;
//   String originalField = (String)cbxSource.getSelectedItem();
   String type = (String)cbxSourceType.getSelectedItem();
   cbxSource.removeAllItems();
   if (type.equals("All collections")){
     cbxSource.addItem("All collections");
   }else if (type.equals("All decks")){
     cbxSource.addItem("All decks");
   }else if (type.equals("Collection")){
     Object[] items = Core.getCollNamesArray();
     for (int i=0; i<items.length; i++){
       String ct = Core.getCollCardType((String)items[i]);
       String selected = (String)cbxCardType.getSelectedItem();
       if (ct.equals(selected)){
         cbxSource.addItem((String)items[i]);
       }
     }
   }else if (type.equals("Deck")){
     //As for collections, should only include decks of a matching card type.
     String[] items = Core.getDeckNamesArray();
     for (int i=0; i<items.length; i++){
       String ct = Core.getCollCardType(Core.getDeckCollection((String)items[i]));
       String selected = (String)cbxCardType.getSelectedItem();
       if (ct.equals(selected)){
         cbxSource.addItem(items[i]);
       }
     }
   }
 }
 private void refreshCbxField(){ //Actually refreshes cbxField and cbxSortBy both.
   String originalField = (String)cbxField.getSelectedItem();
   if (cbxCardType.getItemCount()>0){
     String fields[] = Core.getFields((String)cbxCardType.getSelectedItem());
     cbxField.removeAllItems();
     //cbxField.addItem("");
     for (int i=0; i<fields.length; i++){
       cbxField.addItem(fields[i]);
     }
   }
   cbxField.setSelectedItem(originalField);  //Does nothing if originalField isn't there
   originalField = (String)cbxSortBy.getSelectedItem();
   if (cbxCardType.getItemCount()>0){
     String fields[] = Core.getFields((String)cbxCardType.getSelectedItem());
     cbxSortBy.removeAllItems();
     //cbxSortBy.addItem("");
     for (int i=0; i<fields.length; i++){
       cbxSortBy.addItem(fields[i]);
     }
   }
   cbxSortBy.setSelectedItem(originalField);  //Does nothing if originalField isn't there
 }
 private void refreshCbxOp(){
   //Display the initial arithmetic comparison operators if int or float; contains, equals, startsWith and endsWith if String; true or false if boolean.
   String originalOp = (String)cbxOp.getSelectedItem();
   int type = getSelectedDataType();
   if (type<0) return;
   cbxOp.removeAllItems();
   if (type==0){ //boolean
     cbxOp.addItem("TRUE");
     cbxOp.addItem("FALSE");
   }else if (type==1){ //String
     cbxOp.addItem("contains");
     cbxOp.addItem("equals");
     cbxOp.addItem("starts with");
     cbxOp.addItem("ends with");
     cbxOp.addItem("does not contain");
     cbxOp.addItem("does not equal");
     cbxOp.addItem("does not start with");
     cbxOp.addItem("does not end with");
   }else{ //int, char, float
     //"==","!=","<","<=",">",">="
     cbxOp.addItem("==");
     cbxOp.addItem("!=");
     cbxOp.addItem("<");
     cbxOp.addItem("<=");
     cbxOp.addItem(">");
     cbxOp.addItem(">=");
   }
   cbxOp.setSelectedItem(originalOp);
 }
 private int getSelectedDataType(){
   String field = (String)cbxField.getSelectedItem();
   String fields[] = Core.getFields((String)cbxCardType.getSelectedItem());
   if (fields == null) return -1;
   int idx=-1;
   for (int i=0; i<fields.length; i++){
     if (fields[i].equals(field)){
       idx = i;
       break;
     }
   }
   if (idx==-1) return -1;
   int types[] = Core.getFieldTypes((String)cbxCardType.getSelectedItem());
   int type = types[idx];
   return type;
 }
 private static boolean isNumeric(String str) {  
   try {  
     Double.parseDouble(str);  
   } catch(NumberFormatException e) {  
     return false;  
   }  
   return true;
 }
 private void addConstraint(){
   int type = getSelectedDataType();
   if (type<0){
     JFrame frame = new JFrame("Error");
     JOptionPane.showMessageDialog(frame, "Unrecognized data type in \"Field\".");
     return;
   }
   if (txtConstraint.getText().equals("")&&type!=0){
     JFrame frame = new JFrame("Error");
     JOptionPane.showMessageDialog(frame, "You must provide a constraint.");
     return;
   }
   if (type==2||type==4){
     if (!isNumeric(txtConstraint.getText())){
       JFrame frame = new JFrame("Error");
       JOptionPane.showMessageDialog(frame, "You must enter a number.");
       return;
     }
     //Should also return if number is above the maximum size.
   }
   if (type==3){
     if (txtConstraint.getText().length()!=1){
       JFrame frame = new JFrame("Error");
       JOptionPane.showMessageDialog(frame, "You must enter a single character.");
       return;
     }
   }
   
   if (type==0){
     if (((String)(cbxJoin.getSelectedItem())).equals("AND")){
       query.addAndConstraint((String)cbxField.getSelectedItem(), (String)cbxOp.getSelectedItem());
     }else{
       query.addOrConstraint((String)cbxField.getSelectedItem(), (String)cbxOp.getSelectedItem());
     }
   }else{
     if (((String)(cbxJoin.getSelectedItem())).equals("AND")){
       query.addAndConstraint((String)cbxField.getSelectedItem(), (String)cbxOp.getSelectedItem(), txtConstraint.getText());
     }else{
       query.addOrConstraint((String)cbxField.getSelectedItem(), (String)cbxOp.getSelectedItem(), txtConstraint.getText());
     }
   }
   txtQuery.setText(query.getText());
   cbxSource.setEnabled(false);
   cbxSourceType.setEnabled(false);
   cbxCardType.setEnabled(false);
 }
 public void extendedActionPerformed(ActionEvent e){
   if(e.getSource() == btnSearch){
     search();
   }else if (e.getSource() == cbxCardType){
     //Refresh cbxSource, cbxField
     refreshCbxSource();
     refreshCbxField();
   }else if (e.getSource() == cbxSourceType){
     //Refresh cbxSource (try to maintain selected item on refreshes)
     refreshCbxSource();
   }else if (e.getSource() == cbxField){
     //Refresh cbxOp
     refreshCbxOp();
   }else if (e.getSource() == btnAdd){
     addConstraint();
   }else if (e.getSource() == cbxSource){
     query.setSource((String)cbxSource.getSelectedItem());
   }else if (e.getSource() == cbxSortBy){
     query.setSort((String)cbxSortBy.getSelectedItem());
   }else if (e.getSource() == btnUndo){
     query.removeConstraint();
     txtQuery.setText(query.getText());
     if (txtQuery.getText().equals("")){
       cbxSource.setEnabled(true);
       cbxSourceType.setEnabled(true);
       cbxCardType.setEnabled(true);
     }
   }else if (e.getSource() == btnReset){
     query.clearConstraints();
     txtQuery.setText(query.getText());
     cbxSource.setEnabled(true);
     cbxSourceType.setEnabled(true);
     cbxCardType.setEnabled(true);
   }else if (e.getSource() == mnuExport){
     if ((String)(cbxSourceType.getSelectedItem())!="Collection"){
       JFrame frame = new JFrame("Error");
       JOptionPane.showMessageDialog(frame, "Source type must be Collection");
       return; 
     }
     //Recall: public DeckEditorUI(String deckName, String collection, String fields[], Object argCollectionData[][]){
     Object[][] temp = new Object[queryData.length][queryData[0].length-1];
     for (int i=0; i<queryData.length; i++){
       for (int j=0; j<queryData[0].length-1; j++){
         /*if (j==queryData[0].length-3)
           temp[i][j]=queryData[i][j+2];
         else if (j==queryData[0].length-2)
           temp[i][j]=queryData[i][j];*/
         //else 
           temp[i][j]=queryData[i][j+1];
       }
     }
     String fields[] = Core.getFields((String)cbxCardType.getSelectedItem());
     String fTemp = fields[fields.length-1];
     fields[fields.length-1]=fields[fields.length-2];
     fields[fields.length-2]=fTemp;
     new DeckEditorUI("Deck 1", (String)cbxSource.getSelectedItem(), fields, temp);
   }else if (e.getSource() == mnuExportToDeck){
     if ((String)(cbxSourceType.getSelectedItem())!="Collection"){
       JFrame frame = new JFrame("Error");
       JOptionPane.showMessageDialog(frame, "Source type must be Collection");
       return; 
     }
     /*Recall:         public DeckEditorUI(String deckName, String collection, String loadSource,
                        String argFields[], Object argCollectionData[][],
                        Object argDeckData[][], boolean virtual) {*/
     Object[][] temp = new Object[queryData.length][queryData[0].length-1];
     for (int i=0; i<queryData.length; i++){
       for (int j=0; j<queryData[0].length-1; j++){
         /*if (j==queryData[0].length-3)
          temp[i][j]=queryData[i][j+2];
          else if (j==queryData[0].length-2)
          temp[i][j]=queryData[i][j];*/
         //else 
         temp[i][j]=queryData[i][j+1];
       }
     }
     String fields[] = Core.getFields((String)cbxCardType.getSelectedItem());
     String fTemp = fields[fields.length-1];
     fields[fields.length-1]=fields[fields.length-2];
     fields[fields.length-2]=fTemp;
     new DeckEditorUI("Deck 1", (String)cbxSource.getSelectedItem(), (String)cbxSource.getSelectedItem(), fields, temp, temp, false);
   }
 }
 public void mouseClicked(MouseEvent e){
  if(e.getSource() == txtQuery){
   txtQuery.requestFocusInWindow();
  }
  
 }
 private void search(){
  String search = txtQuery.getText().trim();
  if(search.isEmpty())
   return;
  String ctName = (String) cbxCardType.getSelectedItem();
  int count = Core.getFields(ctName).length;
  if (count >= 10)
    columns = count+1;
  else
    columns = 10;
  dataModel = reInitDataModel();
  
  //Reinitialize the table
  table = new JTable(dataModel);
  table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
  scrollpane.setViewportView(table);
  table.getTableHeader().setReorderingAllowed(false);  
  table.setDefaultRenderer( Object.class, new BorderLessTableCellRenderer() );
  table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  
  //Insert search function here
  String where = "collection"; // collection or deck
  String name = null; // name of collection/deck can leave null if all is true;
  String type = (String) cbxCardType.getSelectedItem();  // card type can leave null if all is false;
  String quer = query.getText();  //getText
  boolean all = true;  // true if checking all of collection or deck by type, false if only checking one
  if (((String)cbxSourceType.getSelectedItem()).equals("Collection")){
    all = false;
    name = (String)cbxSource.getSelectedItem();
  }else if (((String) cbxSourceType.getSelectedItem()).equals("All decks")){
    where = "deck";
  }else if (((String) cbxSourceType.getSelectedItem()).equals("Deck")){
    where = "deck";
    all = false;
    name = (String)cbxSource.getSelectedItem();
  }
  //"All decks in collection" not yet implemented
  
  String result[][] = Core.search(where, name, type, quer, all);
  /*String result[][] = new String[4][4];
  for (int i=0; i<result.length; i++){
    for (int j=0; j<result[0].length; j++){
      result[i][j] = ""+(((i+j+1)*(i+1)*(j+1))%20);
    }
  }*/
  queryData = result;
  if (queryData==null){
    queryData=new String[1][columns];
    for (int i=0;i<columns; i++){
      queryData[0][i]="";
    }
  }
  matches = queryData.length;
  
  table.setSize((int) table.getPreferredSize().getWidth(),16*matches);
  if(table.getSize().getHeight()+33<scrollpane.getPreferredSize().getHeight()){
    scrollpane.setSize((int)scrollpane.getSize().getWidth(),(int)table.getSize().getHeight()+33);
  }
  else{
    scrollpane.setSize(scrollpane.getPreferredSize());
  }
  dataModel.fireTableDataChanged();
  
  reorderColumns();
  //Update column names
  table.getColumnModel().getColumn(0).setHeaderValue(dataModel.getColumnName(0));
  for (int i=1; i<columns; i++){
    table.getColumnModel().getColumn(i).setHeaderValue(dataModel.getColumnName(i));
  }
  table.getTableHeader().repaint();
  
  //Add sorters
  TableRowSorter<DefaultTableModel> sort = new TableRowSorter<DefaultTableModel>(dataModel);
  int types[] = Core.getFieldTypes((String)cbxCardType.getSelectedItem());
  for (int i=0; i<types.length; i++){
    if (types[i]==2)
      sort.setComparator(i+1, new IntComparator());
    else if (types[i]==1||types[i]==3)
      sort.setComparator(i+1, new StringComparator());
    else if (types[i]==4)
      sort.setComparator(i+1, new FloatComparator());
    else
      sort.setComparator(i+1, new BoolComparator());
  }
  table.setRowSorter(sort);  
  dataModel.fireTableDataChanged();
  
  String sortBy = (String)cbxSortBy.getSelectedItem();
  int sortIdx = -1;
  for (int i=0; i<columns; i++){
    if (sortBy.equals((String)table.getColumnModel().getColumn(i).getHeaderValue())){
      sortIdx=i;
      break;
    }
  }
  if (sortIdx!=-1){
    sort.toggleSortOrder(sortIdx);
    if (((String)cbxOrder.getSelectedItem()).equals("Descending")){
      sort.toggleSortOrder(sortIdx);
    }
  }else{
    System.out.println("Invalid \"Sort by\" selection");
  }
 }
 @Override
 public void keyReleased(KeyEvent e) {
  // TODO Auto-generated method stub
  
 }
 @Override
 public void keyTyped(KeyEvent e) {
  // TODO Auto-generated method stub

 }
 
class StringComparator implements Comparator<Object>{
  public int compare (Object obj1, Object obj2){
    try{
      if ((((String) obj1)==""||((String) obj1)==null)&&(((String) obj2)==""||((String) obj2)==null)) return 0;
      if (((String) obj1)==""||((String) obj1)==null) return 0;
      if (((String) obj2)==""||((String) obj2)==null) return 0;
    }catch(Exception ex){
      return 0;
    }
    String str1 = (String) obj1;
    String str2 = (String) obj2;
    return str1.compareTo(str2);
  }
}

//int comparator
class IntComparator implements Comparator<Object>{
  public int compare (Object obj1, Object obj2){
    try{
      if ((((String) obj1)==""||((String) obj1)==null)&&(((String) obj2)==""||((String) obj2)==null)) return 0;
      if (((String) obj1)==""||((String) obj1)==null) return 0;
      if (((String) obj2)==""||((String) obj2)==null) return 0;
    }catch(Exception ex){
      return 0;
    }
    int int1 = Integer.parseInt((String) obj1);
    int int2 = Integer.parseInt((String) obj2);
    if (int1<int2) return -1;
    if (int1==int2) return 0;
    return 1;
  }
}
class BoolComparator implements Comparator<Object>{
  public int compare (Object obj1, Object obj2){
    try{
      if (!obj1.equals("true")&&!obj1.equals("false")) return 0;
      if (!obj2.equals("true")&&!obj2.equals("false")) return 0;
      if ((obj2==null)||(obj1==null)) return 0;
    }catch(Exception ex){
      return 0;
    }
    if (obj1.equals(obj2)) return 0;
    if (obj1.equals("true")) return 1;
    return -1;
  }
}
class FloatComparator implements Comparator<Object>{
    public int compare (Object obj1, Object obj2){
      try{
        if (((String.valueOf(obj1))==""||(obj1)==null)||((String.valueOf(obj2))==""||(obj2)==null)) return 0;
      }catch(Exception ex){
        return 0;
      }
    float int1 = Float.parseFloat((String)obj1);
    float int2 = Float.parseFloat((String)obj2);
    if (int1<int2) return -1;
    if (int1==int2) return 0;
    return 1;
  }
}
}
