import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;

public class GenericUI extends JFrame implements MenuListener, ActionListener, MouseListener, WindowListener{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public JMenuBar bar;
    public JMenu file;
    protected JMenuItem exit;

    public JMenu games;
    public JMenuItem openLobby;
    protected Vector<JMenuItem> collections;
    private boolean fullScreen;
    public GenericUI(double x, double y, boolean enableMenu){
     this(x,y,"",enableMenu);
    }
    public GenericUI(String title){
      this(0,0,title,true); 
    }
    public GenericUI(String title, boolean enableMenu){
     this(0,0,title,enableMenu);
    }
    public GenericUI(boolean enableMenu){
     this(0,0,"",enableMenu);
    }
    public GenericUI(){
     this(0,0,"",true);
    }
    public GenericUI(double x, double y){
     this(x,y,"",true);
    }
    public GenericUI(double x, double y, String title){
     this(x,y,title,true);
    }
    public GenericUI(double x, double y, String title, boolean enableMenu){
      this.setVisible(false);
      addWindowListener(this);
     if (x==y && y==0){
      fullScreen=true;
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      x = screenSize.getWidth();
      y = screenSize.getHeight();
     }
     if (enableMenu){
      file = new JMenu("File");

      games = new JMenu("Games");
      openLobby = new JMenuItem("Create/Join a Game");
      exit = new JMenuItem("Exit");


      exit.addActionListener(this);
      openLobby.addActionListener(this);
         exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
         

         
         games.add(openLobby);
         bar = new JMenuBar();
         bar.add(file);
         bar.add(games);
         setJMenuBar(bar);
     }
        setLayout(new GridBagLayout());
        
        // Correctly close when clicking the X button
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        
        this.setSize((int)x, (int)y);
        if (fullScreen){
         setLocation(0,0);
        }else{
         setLocation(100,100);
        }
        
        // Disable resizing of the window
        setResizable(false);
        this.setTitle(title);
        setVisible(true);
        
        //Fill in load collection submenu.
        
        Core.addGUI(this);
    }
    public void menuCanceled(MenuEvent e){
    }
    public void menuDeselected(MenuEvent e){
    }
    public void menuSelected(MenuEvent e){
    }
    public void mousePressed(MouseEvent e) {
    }
    public void mouseReleased(MouseEvent e) {
    }
    public void mouseEntered(MouseEvent e) {
    }
    public void mouseExited(MouseEvent e) {
    }
    public void mouseClicked(MouseEvent e) {
    }
    public void windowOpened(WindowEvent e){
    }
    public void windowClosing(WindowEvent e){
      boolean proceed = this.close();
      if(proceed) this.dispose();
    }
    public void windowClosed(WindowEvent e){
      Core.removeGUI(this);
    }
    public void windowIconified(WindowEvent e){
    }
    public void windowDeiconified(WindowEvent e){
    }
    public void windowActivated(WindowEvent e){
    }
    public void windowDeactivated(WindowEvent e){
    }
    public void actionPerformed(ActionEvent e){
     //if(e.getSource()==newCollectionJM){
      //Handled in CollectionEditorUI as the DBM was needed
     //}

     if(e.getSource()==exit){
      if(this.close()) this.dispose();
     }else if (e.getSource()==openLobby){
       if (Core.canOpenConnectionUI())
         new ConnectionUI();
       else
         JOptionPane.showMessageDialog(this,"You're already in the process of starting/playing a game.");
     }else{
       extendedActionPerformed(e);
     }
    }
  //An overridable method that allows each class to respond to closing. Returns true if it's okay to close.
    public boolean close(){
     return true;
    }
    
    //An overridable method that allows each class to respond to other ActionEvents.
    public void extendedActionPerformed(ActionEvent e){
    }
    /*public static void main (String[] args){
     GenericUI gui = new GenericUI("My title", false);
    }*/
}
