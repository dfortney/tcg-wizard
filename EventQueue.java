import java.util.Collections;
import java.util.Vector;

public class EventQueue {
	private Vector<Event> entries;
	Boolean sort;

	public synchronized void append(Event e) {
		entries.add(e);
		if(sort){
			Collections.sort(entries);
		}
		notifyAll();
	}
	
	public synchronized void append(String s) {
		append(s, "", Event.EVENT, "");
	}

	public synchronized void append(String s, String user, int type) {
		append(s, user, type, "");
	}
	
	public synchronized void append(String s, String user, int type, String notes) {
		entries.add(new Event(s, user, type, notes));
		if(sort){
			Collections.sort(entries);
		}
		notifyAll();
	}
	
	public synchronized void offer(String s, String user, int type){
		append(s,user, type);
	}
	
	public synchronized Event get(int i){
		while (entries.size() <= i) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
		return entries.get(i);
	}
	
	public synchronized Event poll(){
		while (entries.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
		Event e =  entries.get(0);
		entries.remove(0);
		return e;
	}

	public EventQueue(Boolean sorted) {
		this.sort = sorted;
		entries = new Vector<Event>();
	}
	
	public EventQueue() {
		this.sort = false;
		entries = new Vector<Event>();
	}
}
