/**
 * 
 * The Protocol Abstract class can be extended to create new functions for the
 * SimpleServer class. Each server is given a protocol that is calls on every
 * string of input it receives, and the implementation of the protocol will
 * determine the actions of the server. The protocol can make calls to Core if
 * it needs to communicate with other parts of the program.
 * 
 * Implementation note: processInput is called with a null string before the
 * server loop begins. If you need to prepare state variables in the protocol,
 * do it then.
 * 
 * If using a multi-threaded server, be aware that the single protocol will be
 * called in all threads to handle input. If your protocol uses states, be sure
 * it is synchronized to avoid race conditions
 * 
 * See KnockKnockProtocol.j for an example of a protocol
 * 
 * @author ktheller
 * 
 */
public abstract class Protocol {
	private String terminator = null;

	/**
	 * processInput handles the actions of a server after receiving data from a
	 * client.
	 * 
	 * @param s
	 *            One line of data from the client
	 * @return The string to send back to the client.
	 */
	public abstract String processInput(String s);

	/**
	 * used by a multi-threaded server to clone the protocol for each thread
	 * could just return "this" if you want all of them to share one protocol.
	 */
	public Protocol clone(){
		return this;
	}
	
	/**
	 * Returns the terminating character for this protocol. Used by the server
	 * to recognize an end-of-transmission message
	 * 
	 * @return
	 */
	public String getTerminator() {
		return terminator;
	}

	/**
	 * Sole constructor for the basic Protocol. creates the terminator
	 * 
	 * @param terminator
	 *            The string that will terminate the connection
	 */
	public Protocol(String terminator) {
		this.terminator = terminator;
	}

	/**
	 * The close function is called after the client of server finishes using
	 * the protocol. Can be used to clean up resources
	 */
	public void close() {
	}
	

}
