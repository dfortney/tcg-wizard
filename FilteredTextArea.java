import java.util.Collections;
import java.util.Vector;

import javax.swing.JTextArea;

public class FilteredTextArea extends JTextArea {
 private static final long serialVersionUID = -2589438968137148323L;
 private Vector<FilteredChatEntry> entries;
 private int mode;
 public static final int CHAT = 1;
 public static final int LOG = 2;
 public static final int BOTH = 3;
 
 
 public synchronized void append(String s) {
  append(s, "", FilteredChatEntry.CHAT);
 }
 
 public synchronized void append(String s, String user, int type) {
  if(s.equals("")){
   return;
  }
  
  entries.add(new FilteredChatEntry(s, user, type));

  Collections.sort(entries);

  super.setText("");
  for (int i = 0; i < entries.size(); i++) {
   if((entries.get(i).getType() & mode) != 0){
    super.append(entries.get(i) + "\n");
   }
  }
 }
 
 public synchronized void append(String s, int type) {
  append(s, "", type);
 }
 
 public synchronized void setMode(int newMode){
  if(newMode != CHAT && newMode != LOG && newMode != BOTH){
   mode = CHAT;
  } else {
   mode = newMode;
  }
  super.setText("");
  if(entries.size() > 0){
   for (int i = 0; i < entries.size(); i++) {
    if((entries.get(i).getType() & mode) != 0){
     super.append(entries.get(i) + "\n");
    }
   }
  }
 }
 
 public FilteredTextArea() {
  super();
  entries = new Vector<FilteredChatEntry>();
  mode = CHAT;
  setLineWrap(true);
  setWrapStyleWord(true);
 }

 public FilteredTextArea(int mode) {
  super();
  entries = new Vector<FilteredChatEntry>();
  this.mode = mode;
  setLineWrap(true);
  setWrapStyleWord(true);
 }
}
