import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.*;
import java.util.ArrayList;

public class CollectionDBM {

 private final String delimiter = "`";
 private static Connection con;
 private int counter; // used to keep track of how many values there are in a

 // table

 /**
  * An internal method to join several ints into a single string using a
  * connecting element
  * 
  * The input ints can be in an array or passed in as separate arguments
  * 
  * As ints are not Objects, this must be a separate join method
  * 
  * @param glue
  *            a string to serve as the connective element
  * @param s
  *            the input Object(s) to join
  * @return the joined string
  */
 private static String join(String glue, int... s) {
  int n = s.length;
  if (n == 0) {
   return null;
  }
  StringBuilder out = new StringBuilder();
  out.append("" + s[0]);
  for (int i = 1; i < n; i++) {
   out.append(glue).append("" + s[i]);
  }
  return out.toString();
 }

 /**
  * An internal method to join several Objects into a single string using a
  * connecting element
  * 
  * The input Objects can be in an array or passed in as separate arguments
  * 
  * @param glue
  *            a string to serve as the connective element
  * @param s
  *            the input Object(s) to join
  * @return the joined string
  */
 private static String join(String glue, Object... s) {
  int n = s.length;
  if (n == 0) {
   return null;
  }
  StringBuilder out = new StringBuilder();
  out.append("" + s[0]);
  for (int i = 1; i < n; i++) {
   out.append(glue).append(s[i].toString());
  }
  return out.toString();
 }

 public CollectionDBM(String db) throws SQLException, ClassNotFoundException {

  counter = 5;

  Class.forName("org.sqlite.JDBC");

  // db is the name of the database file that you are wanting to access
  // be sure to use the full file name, eg. test.db
  con = DriverManager.getConnection("jdbc:sqlite:" + db);
  Statement stat = con.createStatement();
  stat.executeUpdate("attach '' as TMP;");
  stat.executeUpdate("create table if not exists m_coll (name string not null primary key, cardtype string, fields string, types string)");
  stat.executeUpdate("create table TMP.m_coll as select * from m_coll;");
  ArrayList<String> colls = getCollections();
  for (int i = 0; i < colls.size(); i++) {
   // System.out.println(colls.get(i) + "loaded");
   stat.executeUpdate("create table TMP." + colls.get(i)
     + " as select * from " + colls.get(i) + ";");
  }
  stat.close();
 }

 public static void main(String[] args) {
  try {
   CollectionDBM c = new CollectionDBM("collections");
   c.drop("name");
   c.create("magic", "name");
   Datapacket d = new Datapacket("Awesome", "1", "1", "all", "in");
   d.addNewField("Cloudy");
   d.addNewField("with");
   d.addNewField("Meatballs");
   d.addNewField("1");
   d.addNewField("2");
   c.add(d, "name");
   c.save("name");

  } catch (Exception e) {
   e.printStackTrace();
  }
 }

 public void create(String type, String name) throws SQLException {

  int i = 0;
  int[] types;
  String[] fields;
  String temp_fields = "";

  // System.out.println(type);
  // CardTypeDBM ctDBM = new CardTypeDBM();
  fields = Core.ctDBM.getFields(type);
  types = Core.ctDBM.getTypes(type);
  // ctDBM.close();

  /*
   * Types 0 = Boolean 1 = String 2 = int 3 = char 4 = float
   */

  // uses the card type DBM to get any additional fields a certain card
  // would have
  // and stores it in a string so we can append it to the create sql
  // statement
  while (i < fields.length && fields[i] != null) {
   // System.out.println(types[i]);
   if (types[i] == 0) {
    temp_fields += ", " + fields[i] + " BOOLEAN";
   } else if (types[i] == 1) {
    temp_fields += ", " + fields[i] + " varchar(50)";
   } else if (types[i] == 2) {
    temp_fields += ", " + fields[i] + " INT";
   } else if (types[i] == 3) {
    temp_fields += ", " + fields[i] + " char";
   } else if (types[i] == 4) {
    temp_fields += ", " + fields[i] + " float";
   }
   i++;
   counter++;
  }

  Statement stat = con.createStatement();

  // System.out.println("(name varchar(50), id INT, quantity INT, image varchar(50), notes varchar(200)"
  // + temp_fields + ");");
  // If there is a table by that name in the database, remove it and then
  // make a new one
  stat.executeUpdate("drop table if exists TMP." + name + ";");
  stat.executeUpdate("create table TMP."
    + name
    + "(name varchar(50), id INT, quantity INT, image varchar(50), notes varchar(200)"
    + temp_fields + ");");
  stat.executeUpdate("create table if not exists " + name
    + " as select * from TMP." + name);
  PreparedStatement prep = con
    .prepareStatement("insert into TMP.m_coll values(?,?,?,?);");
  prep.setString(1, name);
  prep.setString(2, type);
  prep.setString(
    3,
    join(",", "name,id,quantity,image,notes",
      join(",", (Object[]) fields)));
  prep.setString(4, join(",", "1,2,2,1,1", join(",", types)));
  prep.execute();
  prep.close();
  stat.close();
  return;
 }

 public void add(Datapacket data, String name) throws SQLException {

  // note: name is the variable for the name of the table you want to add
  // to.

  String values = "";
  int i = 5;

  // creating a string that keeps the additional ",?"'s we need for the
  // insert statement
  // that correspond to the addtional fields
  while (i < counter) {

   values += ",?";
   i++;
  }

  PreparedStatement prep = con.prepareStatement("insert into TMP." + name
    + " values(?,?,?,?,?" + values + ");");

  i = 0;
  // getting all the fields from the Datapacket object that was passed
  // side note, the first "?" in the value statement is 1
  while (i < counter) {
   // System.out.println("Adding: " + data.getField(i));
   prep.setString(i + 1, data.getField(i));
   i++;
  }

  prep.execute();
  prep.close();
 }

 public int remove(String name, String id) throws SQLException {

  // note: name is the variable for the name of the table you want to
  // delete from
  // deletes the row the contains the id the was passed in
  PreparedStatement prep = con.prepareStatement("delete from TMP." + name
    + " where id = '" + id + "';");

  prep.execute();
  prep.close();
  return 1;
 }

 public ArrayList<Object> getCard(int id, String name) throws SQLException {
  // note: name is the variable for the name of the table you want to add
  // to and db is the name of
  // the database file to are wanting to access
  ResultSet query;
  // Datapacket data;
  // int i = 5;

  // connects to the database file that was passed in
  // con = DriverManager.getConnection("jdbc:sqlite:" + db);

  // gets the row that contains the id
  PreparedStatement prep = con.prepareStatement("select * from " + name
    + " where id = '" + id + "';");
  query = prep.executeQuery();
  String[] fields = getFields(name);
  int[] types = getTypes(name);
  query.next();
  ArrayList<Object> o = new ArrayList<Object>();

  for (int i = 0; i < fields.length; i++) {
   // System.out.println("i:" + i);
   // System.out.println("T:" + types[i] + "; F:"+fields[i]);
   // System.out.println("IS: " + rs.getString(fields[i]));
   switch (types[i]) {
   case 0:
    o.add(query.getBoolean(fields[i]));
    break;
   case 1:
    o.add(query.getString(fields[i]));
    break;
   case 2:
    o.add(query.getInt(fields[i]));
    break;
   case 3:
    o.add(query.getString(fields[i]));
    break;
   case 4:
    o.add(query.getFloat(fields[i]));
    break;
   default:
    o.add(query.getString(fields[i]));
   }
  }

  // Getting the values that are under the noted columns and storing them
  // in a new datapacket
  // note: column 1 is the very first column in the row
  /*
   * data = new Datapacket(query.getString(1), query.getString(2),
   * query.getString(3), query.getString(4), query.getString(5)); while (i
   * < counter) { data.addNewField(query.getString(i + 1)); i++; }
   */
  prep.close();
  // return data;
  return o;
 }

 public ArrayList<String> getCollections() {
  ArrayList<String> outList = new ArrayList<String>();
  Statement statement;
  try {
   statement = con.createStatement();
   ResultSet rs = statement.executeQuery("select * from TMP.m_coll");

   while (rs.next()) {
    // print the result set
    outList.add(rs.getString("name"));
    // System.out.println("deck name = " + rs.getString("name"));
   }
   statement.close();
  } catch (SQLException e) {
   e.printStackTrace();
  }

  return outList;
 }

 public ArrayList<Object[]> getCollectionData(String collName) throws SQLException{
   Statement statement = con.createStatement();
   ResultSet rs = statement.executeQuery("select * from TMP." + collName);
   ArrayList<Object[]> out = new ArrayList<Object[]>();
   int[] types = getTypes(collName);
   String[] fields = getFields(collName);
   
   while (rs.next()) {
     ArrayList<Object> o = new ArrayList<Object>();
     
     for (int i = 0; i < fields.length; i++) {
       // System.out.println("i:" + i);
       // System.out.println("T:" + types[i] + "; F:"+fields[i]);
       // System.out.println("IS: " + rs.getString(fields[i]));
       switch (types[i]) {
         case 0:
           o.add(Boolean.parseBoolean(rs.getString(fields[i])));
           break;
         case 1:
           o.add(rs.getString(fields[i]));
           break;
         case 2:
           o.add(rs.getInt(fields[i]));
           break;
         case 3:
           o.add(rs.getString(fields[i]));
           break;
         case 4:
           o.add(rs.getFloat(fields[i]));
           break;
         default:
           o.add(rs.getString(fields[i]));
       }
     }
     
     out.add(o.toArray());
   }
   statement.close();
   return out;
 }

 public void setCollectionData(String collName, ArrayList<Object[]> data)
   throws SQLException {
  Statement stat = con.createStatement();
  // ResultSet res
  // =stat.executeQuery("select sql from sqlite_master where tbl_name='" +
  // collName + "' AND type='table'");
  // System.out.println(res.getString("sql"));
  stat.executeUpdate("delete from TMP." + collName);

  // int[] types = getTypes(collName);
  String[] fields = getFields(collName);

  String values = "";
  int i = 5;
  while (i < fields.length) {

   values += ",?";
   i++;
  }

  for (int j = 0; j < data.size(); j++) {
   PreparedStatement prep = con.prepareStatement("insert into TMP."
     + collName + " values(?,?,?,?,?" + values + ");");
   i = 0;
   while (i < fields.length) {
    prep.setString(i + 1, data.get(j)[i].toString());
    i++;
   }
   prep.execute();
   prep.close();
  }
  stat.close();
 }

 public String[] getFields(String name) {
  /*
   * Goes to the element with the specified name and returns the
   * corresponding String array fields[], specified upon initialization
   */
  if (name.trim().equals(""))
   return null;
  try {
   Statement stat = con.createStatement();
   // ResultSet resx = stat
   // .executeQuery("select sql from sqlite_master where tbl_name='m_coll' AND type='table'");
   // System.out.println(resx.getString("sql"));
   // String t = res.getString("sql").replaceAll(".*?\\(", "")
   // .replaceAll("\\).*", "");
   // t = t.replaceAll(" [A-Z]*?(,|)\\n", "");
   // t = t.replaceAll("^\\s*", "");
   // return t.split("\\s+");
   ResultSet res = stat
     .executeQuery("select fields from TMP.m_coll where name ='"
       + name + "'");
   // System.out.println("Fields:"+res.getString("fields"));
   String s = res.getString("fields");
   stat.close();
   return s.split(",");
  } catch (SQLException e) {
   System.out.println("ERROR: CollectionDBM.getFields failed\n"
     + e.getMessage());
   e.printStackTrace();
  }
  return null;
 }

// private int sqlToClass(String sql) {
//  if (sql.equals("TEXT")) {
//   return 1;
//  } else if (sql.equals("INT")) {
//   return 2;
//  } else if (sql.equals("NUM")) {
//   return 0;
//  } else if (sql.equals("REAL")) {
//   return 4;
//  }
//
//  return 1;
// }

 public int[] getTypes(String name) {

  /*
   * Goes to the element with the specified name and returns the
   * corresponding String array fields[], specified upon initialization
   */
  try {
   /*
    * Statement stat = con.createStatement(); ResultSet res =
    * stat.executeQuery
    * ("select sql from sqlite_master where tbl_name='" + name +
    * "' AND type='table'"); //
    * System.out.println(res.getString("sql")); String t =
    * res.getString("sql").replaceAll(".*?\\(", "")
    * .replaceAll("\\).*", ""); t = t.replaceAll("  [a-z,A-Z]*? ", "");
    * t = t.replaceAll("\\n", "");
    */

   Statement stat = con.createStatement();
   ResultSet res = stat
     .executeQuery("select types from TMP.m_coll where name ='"
       + name + "'");

   String[] list = res.getString("types").split(",");
   // System.out.println("Types: " + res.getString("types"));
   int[] out = new int[list.length];
   for (int i = 0; i < list.length; i++) {
    out[i] = Integer.parseInt(list[i]);
   }
   // System.out.println("\n");
   // System.out.println(t);
   stat.close();
   return out;
  } catch (SQLException e) {
   System.out.println("ERROR: CollectionDBM.getTypes failed\n"
     + e.getMessage());
   e.printStackTrace();
  }
  return null;
 }

 @SuppressWarnings("rawtypes")
public Class[] getClasses(String collName) {
  int[] types = getTypes(collName);
  Class[] out = new Class[types.length];
  for (int i = 0; i < types.length; i++) {
   out[i] = Core.typeConversion(types[i]);
  }
  return out;
 }

 public String getCardType(String collName) throws SQLException {
  Statement statement = con.createStatement();
  ResultSet rs = statement
    .executeQuery("select * from TMP.m_coll where name='"
      + collName + "'");
  String ct = rs.getString("cardtype");
  statement.close();
  return ct;
 }

 public void save(String name) throws SQLException {
  Statement stat = con.createStatement();

  // creating table
  stat.executeUpdate("delete from " + name);
  stat.executeUpdate("replace into " + name + " select * from TMP."
    + name + ";");
  stat.executeUpdate("replace into m_coll select * from TMP.m_coll where name='"
    + name + "';");
  stat.close();
 }

 public void revert(String name) throws SQLException {
  Statement stat = con.createStatement();
  // stat.executeUpdate("drop table if exists user");

  // creating table
  stat.executeUpdate("drop table if exists TMP." + name);
  stat.executeUpdate("create table TMP." + name + " as select * from "
    + name + ";");
  stat.executeUpdate("drop table if exists TMP.m_coll");
  stat.executeUpdate("create table TMP.m_coll as select * from m_coll;");
  stat.close();
 }

 public void drop(String name) throws SQLException {
  Statement stat = con.createStatement();
  // stat.executeUpdate("drop table if exists user");

  // dropping table
  stat.executeUpdate("drop table if exists TMP." + name);
  stat.executeUpdate("drop table if exists " + name);
  stat.executeUpdate("delete from m_coll where name='" + name + "'");
  stat.executeUpdate("delete from TMP.m_coll where name='" + name + "'");
  stat.close();
 }

 public void clear(String name) throws SQLException {
  Statement stat = con.createStatement();
  // empty table
  stat.executeUpdate("delete from TMP." + name);
  stat.close();
 }

 public void saveToFile(String collName, String filePath)
   throws SQLException, IOException {
  String[] fields = getFields(collName);

  PreparedStatement prep = con.prepareStatement("select * from "
    + collName);
  ResultSet query = prep.executeQuery();

  FileOutputStream fos = new FileOutputStream(filePath);
  OutputStreamWriter out = new OutputStreamWriter(fos, "UTF-8");

  while (query.next()) {
   String[] temp = new String[fields.length];
   for (int i = 0; i < fields.length; i++) {
    temp[i] = query.getString(i + 1);
    // System.out.println(query.getString(i + 1));
   }
   out.append(join(delimiter, (Object[])temp) + "\n");
  }
  out.close();
  fos.close();
  query.close();
  prep.close();
 }

 public void loadFromFile(String collName, String filePath)
   throws SQLException, IOException {
  String[] fields = getFields(collName);
  for (int i = 0; i < fields.length; i++)
   System.out.println("'" + fields[i] + "'");
  int[] types = getTypes(collName);
  for (int i = 0; i < types.length; i++)
   System.out.println("'" + types[i] + "'");
  clear(collName);

  FileInputStream fis = new FileInputStream(filePath);
  InputStreamReader in = new InputStreamReader(fis, "UTF-8");

  StringBuilder outStr = new StringBuilder();

  int c = in.read();
  while (c != -1) {
   outStr.append("" + ((char) c));
   c = in.read();
  }
  in.close();
  fis.close();

  // System.out.println(outStr.toString());
  System.out.println("made it this far");
  ArrayList<Object[]> values = new ArrayList<Object[]>();
  String[] lines = outStr.toString().split("\n");
  Statement stat = con.createStatement();
//  int idx = 0;
  for (String line : lines) {

    //System.out.println("line "+line);
   Object[] temp = new Object[types.length];

   if(line.substring(line.length() - 1).equals(delimiter)){
     line += delimiter;
   }
   
   String[] elements = line.split(delimiter);
   if (elements.length <5) continue; //Fixes exceptions that can occur if a .csv file is manually edited.
//   idx++;
//   if (false) {System.out.println("Index "+idx);
//   System.out.println("ID "+elements[1]);
//   System.out.println(elements.length);
//   System.out.println(line);}
   if (elements.length<types.length) continue;
   for (int i = 0; i < types.length; i++) {
    switch (types[i]) {
    case 0:
     temp[i] = Boolean.parseBoolean(elements[i]);
     break;
    case 1:
     temp[i] = elements[i];
     break;
    case 2:
     temp[i] = Integer.parseInt(elements[i]);
     break;
    case 3:
     temp[i] = elements[i];
     break;
    case 4:
     temp[i] = Float.parseFloat(elements[i]);
     break;
    default:
     temp[i] = elements[i];
    }
   }
   values.add(temp);
  }
  stat.close();

  /*
   * for(int i = 0 ; i < values.size() ; i++){ for(int j = 0 ; j <
   * values.get(i).length ; j++){ System.out.print("" + values.get(i)[j]);
   * } System.out.print("'"); } System.out.print("\n");
   */

  setCollectionData(collName, values);
 }

 public String[][] search(String which, String typechk, String quer,
   boolean all) {
  quer = quer.replaceAll("Card ID", "id");
  if (all)// if checking all collections of type typechk
  {
   // get all collections of cardtype typechk
   String s = "Select * from m_coll";
   try {
    Statement st = con.createStatement();
    ResultSet rs = st.executeQuery(s);
    int i = 0;
    while (rs.next())
     i++;// get number of collections
    boolean[] b = new boolean[i];
    rs = st.executeQuery(s);// get all collections
    Statement ss = con.createStatement();
    ResultSet r;
    i = 0;
    // System.out.println("rs.name "+rs.getString("name"));
    while (rs.next())// go through list of decks
    {
     System.out.println("rs.name " + rs.getString("name"));
     // check if current collection's type equals asking type
     r = ss.executeQuery("select cardtype from m_coll where name ='"
       + rs.getString("name") + "'");
     while (r.next()) {
      // set boolean array of current deck index
      if (r.getString("cardtype").equals(typechk)) {
       b[i] = true;
      } else {
       b[i] = false;
      }
     }
     i++;

    }
    int j = 0;
    int total = 0;
    String randName = "";
    // get decks again
    rs = st.executeQuery(s);

    while (rs.next()) {
     if (b[j++])// table type matches asking type
     {
      System.out.println("boolean matches at index "
        + (j - 1));
      System.out.println("select * from "
        + rs.getString("name") + " where (" + quer
        + ")");
      r = ss.executeQuery("select * from "
        + rs.getString("name") + " where (" + quer
        + ")");
      while (r.next())
       total++;
      System.out.println("total is " + total);
      randName = rs.getString("name");
     }

    }
    // get actual query results
    String[] fields = getFields(randName); // list of fields for
              // insertion into final
              // array
    String[][] array = new String[total][(fields.length) + 1];

    j = 0;
    i = 0;
    rs = st.executeQuery(s);
    while (rs.next()) {
     if (b[j++])// table type matches asking type
     {
      // get results from current deck against query string
      r = ss.executeQuery("select * from "
        + rs.getString("name") + " where (" + quer
        + ")");
      while (r.next()) {
       array[i][0] = rs.getString("name");
       for (int k = 1; k < fields.length + 1; k++)
        array[i][k] = r.getString(fields[k - 1]);
       i++;
      }
     }

    }
    return array;
   } catch (Exception e) {
    System.out.println(e.getMessage());
    e.printStackTrace();
    return null;
   }

  } else // checking only one deck, String which
  {
   try {
    String s = "select * from " + which + " where (" + quer + ")";
    // String s = "select * from " + which + "Where " + quer;
    Statement statement = con.createStatement();
    ResultSet rs = statement.executeQuery(s);
    String[] type = getFields(which);
    int i = 0;
    while (rs.next())
     i++;
    rs = statement.executeQuery(s);
    String array[][] = new String[i][type.length + 1];
    i = 0;
    while (rs.next()) {
     array[i][0] = which;
     for (int j = 1; j < type.length + 1; j++)
      array[i][j] = rs.getString(type[j - 1]);
     i++;
    }
    return array;
   } catch (Exception e) {
    System.out.println("Collection Query Failed:\n"
      + e.getMessage() + "\n" + e.getStackTrace());
   }
   return null;
  }
 }
}
