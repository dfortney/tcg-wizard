import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
public class FieldStackThread extends Thread{
  ArrayList<FieldCard> cards;
  JLabel[] labels;
  int start, length;
  FieldStack fs;
  Point origin;
  JLayeredPane jlp;
  int offset;
  public FieldStackThread(ArrayList<FieldCard> cards, JLabel[] labels, Point origin, JLayeredPane jlp, int offset, int start, int length){
    this.cards=cards;
    this.labels=labels;
    this.origin=origin;
    this.start=start;
    this.length=length;
    this.jlp=jlp;
    this.offset=offset;
  }
  public void run(){
    Point p = new Point(origin.x, origin.y);
    for (int i = start; i < start+length && i < cards.size(); i++) {
      labels[i] = createCardLabel("Card", cards.get(i).getRaw(), p);
      //labels[i].addMouseListener(new RolloverListener()); FieldStack.java needs to implement this line.
      jlp.add(labels[i], new Integer(i));
      p.y += offset;
    }
  }
  
  private JLabel createCardLabel(String text, BufferedImage color, Point origin) {
    JLabel label = new JLabel();
    label.setVerticalAlignment(JLabel.TOP);
    label.setHorizontalAlignment(JLabel.CENTER);
    label.setOpaque(true);
    label.setIcon(new ImageIcon(((Image) color).getScaledInstance(223, 310,
                                                                  0)));
    label.setForeground(Color.black);
    label.setBorder(BorderFactory.createLineBorder(Color.black));
    label.setBounds(origin.x, origin.y, 223, 310);
    return label;
  }
}