import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * This class is the protocol that will control the client of a joining player.
 * It connects to the host's server, and once the game starts, it sends messages
 * containing the user's chat and actions to the host to distribute to other
 * users.
 * 
 * Messages between server and client are encoded with URL encoding (so special
 * characters are sent as %20, etc) excepting the # used to denote the beginning
 * of a command (this is used to differentiate the language the server and
 * client talk with from other text sent
 * 
 * The process for a connection is as follows (messages follow an S: for server
 * of C: for client):
 * 
 * (client connects)
 * - S: #GREETINGS<name of server user>.
 * - C: #NAME<name of client user>
 * - (server waits until user starts the game)
 * - S: #BEGIN
 * - (server state changes to GAME from GREETINGS)
 * 
 * - (client sends either a chat message or a game event):
 * 
 * - Chat:
 * - C: #CHAT<text message that was sent>
 * - S: #THANKS
 * - (server prints chat to server player's chat box, then puts the chat message
 * on the event queue. The event queue will be read by
 * ConnectionHostClientProtocol)
 * 
 * - Event:
 * - C: #EVENT<description of event to be parsed>
 * - S: #THANKS
 * - (server will then enact the event and put the event message on the event
 * queue to be sent to other users by the ConnectionHostClientProtocol)
 * 
 * 
 * @author ktheller
 * 
 */
public class ConnectionPlayerClientProtocol extends Protocol {
	private EventQueue Q;
	private int pos = 0;
	private String username;
	public String hostname;


	/**
	 * processInput handles the actions of a server after receiving data from a
	 * client.
	 * 
	 * @param s
	 *            One line of data from the client
	 * @return The string to send back to the client.
	 */
	public String processInput(String s) {
		System.out.println("Got: " + s);
		if (s.contains(getTerminator())) {
			return getTerminator();
		}
		if (s.contains("#GREETINGS")) {

			s = s.replace("#GREETINGS", "");

			try {
				s = URLDecoder.decode(s, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			System.err.println(s);
			hostname = s;

			String name = username;

			try {
				name = URLEncoder.encode(name, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			

			return "#NAME" + name;
		} else if(s.contains("#BEGIN")){
			Core.setEventQueue(Q);
		}

		Event newest = Q.get(pos);
		pos++;
		
		String fromUser = newest.toString();
		return fromUser;
	}

	public ConnectionPlayerClientProtocol(String user, EventQueue Q) {
		super("#END");
		if (user == null || user.equals("")) {
			user = "You";
		}
		username = user;
		this.Q = Q;

	}

	/**
	 * The close function is called after the client of server finishes using
	 * the protocol. Can be used to clean up resources
	 */
	public void close() {

	}

	public Protocol clone() {
		return null;
	}

}
