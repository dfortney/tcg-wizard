
public class WaitLock {
	private Boolean condition = false;
	
	public synchronized void setCondition(Boolean cond){
		condition = cond;
		notifyAll();
	}
	
	public synchronized void waitOnLock(){
		while (!condition) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
