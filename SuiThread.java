/*****CLASS NO LONGER IN USE****/

import java.util.*;
public class SuiThread extends Thread{
  ArrayList<FieldCard> cards;
  Object[][] cardData;
  int start, length;
  public SuiThread(ArrayList<FieldCard> cards, Object[][] cardData, int start, int length){
    this.cards=cards;
    this.cardData=cardData;
    this.start=start;
    this.length=length;
  }
  public void run(){
    for(int j = start; j < start+length && j < cardData.length; j++){
      for(int k = 0; k < (Integer) cardData[j][2]; k++){
        FieldCard fc = new FieldCard((String)cardData[j][0],(String) cardData[j][3]);
        synchronized(this){
          cards.add(fc);
        }
      }
    }
  }
}