import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.*;
public class CardTransferHandler extends TransferHandler{
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
Object data[];
  int source, sourceRow, quantity;
  JTabbedPane jtp;
  int mouseTab;
  DeckEditorUI dui;
  
  //Indicates whether the mouse location is a valid drop location.
  public boolean canImport(){
    PointerInfo pi = MouseInfo.getPointerInfo();
    Point p = pi.getLocation();
    SwingUtilities.convertPointFromScreen(p, jtp);
    mouseTab = jtp.indexAtLocation( (int)p.getX(), (int)p.getY() );    
    if ( mouseTab==-1 && (int)p.getX()>0 && (int)p.getY()>0 && (int)p.getX()<jtp.getWidth() && (int)p.getY()<jtp.getHeight() ) mouseTab = jtp.getSelectedIndex();
    SwingUtilities.convertPointToScreen(p, jtp);
    SwingUtilities.convertPointFromScreen(p, dui.tblMain);
    boolean overTblMain = false;
    if ( (int)p.getX()>0 && (int)p.getY()>0 && (int)p.getX()<dui.tblMain.getWidth() && (int)p.getY()<dui.tblMain.getHeight() ){
      overTblMain = true;
    }
    if (mouseTab>0 && jtp.getTitleAt(mouseTab).equals("Sample Hand")) return false;
    if ( mouseTab == -1 && !overTblMain)
      return false;
    else
      return true;
  }
  
  //Called when a "drag" occurs.
  protected Transferable createTransferable(JComponent c){
    JTable table = (JTable)c;
    sourceRow = table.convertRowIndexToModel(table.getSelectedRow());
    //System.out.println("sourceRow "+sourceRow);
    try{
      //System.out.println("table value at (sourceRow, 2) "+table.getValueAt(sourceRow,2));
      quantity = Integer.parseInt((String)table.getValueAt(sourceRow,2));
      //System.out.println("quantity "+quantity);
    }catch(Exception ex){
      System.out.println("Invalid move quantity");
    }
    return new StringSelection("");
  }
  
  //Called when a "drop" occurs.
  protected void exportDone(JComponent c, Transferable t, int action){
    if (canImport() && source!=mouseTab){
      //System.out.println("Original quantity "+quantity);
      quantity = dui.getMoveQuantity(source,mouseTab,quantity);
      System.out.println ("Moving card at row "+sourceRow+" in quantity "+quantity+" to tab "+mouseTab);
      dui.moveCard(source, mouseTab, sourceRow, quantity);      
    }
  }
  public int getSourceActions (JComponent c){
    return COPY_OR_MOVE;
  }
}
