import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.*;

import javax.swing.*;

public class FieldStack extends JFrame implements MouseListener, MouseMotionListener{
 /**
  * 
  */
 private static final long serialVersionUID = 1L;
int stackSize;
 int windowWidth;
 int windowHeight;
 int[] cardSize;
 int[] imageSize;
 int offset;
 private JLayeredPane layeredPane;
 boolean oneClicked;
 JLabel[] labels;
 JScrollPane scroll;
 int first;
 int add;
 ArrayList<FieldCard> cards;
 String stackLabelName = "Stack";
 Stack sta;
 SandboxUI sui;
 int dragIdx = -1;
 private final int threadCount = 8;

 public void updateCards(ArrayList<FieldCard> cards) {
  if(sta == null)return;
  //ArrayList<FieldCard> tempCards = sta.getCards();
  sta.setCards(cards);
  sta.refreshImg();
  Core.sendShuffleInfo(sta.getId(), sta.getDescriptor()+" was updated accordingly.");
 // sta.setCards(tempCards);
  /*System.out.println("Update cards called:");
  for (int i=0; i<cards.size(); i++){
    System.out.println(cards.get(i).getImage());
  }*/
 }

 public FieldStack(ArrayList<FieldCard> cardIn, Stack s, SandboxUI sui) {
   super("Arrange Stack");
  sta = s;
  cards = cardIn;
  add = 0;
  if(sta != null){
    sta.setFieldStack(this);
  }
  this.sui = sui;

  this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
  this.addWindowListener(new WindowAdapter() {
    public void windowClosing(WindowEvent ev) {
      if (sta!=null){
        updateCards(cards);
        Core.printToLog("A player is done looking through "+sta.getDescriptor()+".");
      }
   }
  });
  this.addWindowFocusListener(new WindowFocusListener() {  
      @Override  
      public void windowGainedFocus(WindowEvent e) {}  
      @Override  
      public void windowLostFocus(WindowEvent e) {
          //updateCards(cards);
          //this.dispose();  
      }  
  }); 
  refresh();
 }
 public FieldStack(ArrayList<FieldCard> cardIn, Stack s, SandboxUI sui, String title, String labelName) {
   super(title);
  sta = s;
  cards = cardIn;
  add = 0;
  if(sta != null){
    sta.setFieldStack(this);
  }
  this.sui = sui;
  stackLabelName = labelName;

  this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
  this.addWindowListener(new WindowAdapter() {
   public void windowClosing(WindowEvent ev) {
    updateCards(cards);
    Core.printToLog("A player is done looking through "+sta.getDescriptor()+".");
   }
  });
  refresh();
 }
 public void refresh(){
   JComponent newContentPane = makeFieldStack();
   this.setPreferredSize(new Dimension(275, windowHeight));
   newContentPane.setOpaque(true); // content panes must be opaque
   this.setContentPane(newContentPane);
   
   // Display the window.
   this.pack();
   this.setVisible(true); 
 }

 public JComponent makeFieldStack() {
  first = -1;
  oneClicked = false;
  labels = new JLabel[cards.size()];
  windowWidth = 243;
  Point origin = new Point(10, 20);
  offset = 40;
  if (origin.y + offset * cards.size() + 300 > 700) {
   windowHeight = 700;
   add = 26;
  } else
   windowHeight = origin.y + offset * cards.size() + 340;
  stackSize = cards.size();
  // Create and set up the layered pane.
  layeredPane = new JLayeredPane();
  layeredPane.setPreferredSize(new Dimension(windowWidth, origin.y
    + offset * cards.size() + 300));
  layeredPane.setBorder(BorderFactory.createTitledBorder(stackLabelName));
  // This is the origin of the first label added.

  // This is the offset for computing the origin for the next label.

  // Add overlapping labels to the layered pane
  // using absolute positioning/sizing.
  FieldStackThread threads[] = new FieldStackThread[threadCount];
  //Recall: public FieldStackThread(ArrayList<FieldCard> cards, JLabel[] labels, Point origin, JLayeredPane jlp, int offset, FieldStack fs, int start, int length){
  int index = 0;
  for (int j = 0; j<threadCount; j++){
    threads[j]=new FieldStackThread(cards, labels, new Point(origin.x, origin.y + offset*index), layeredPane, offset, index, 1+cards.size()/threadCount);
    index+=1+cards.size()/threadCount;
  }
  for (int j = 0; j<threadCount; j++){
    threads[j].start();
  }
  for (int j=0; j<threadCount; j++){
    try{
      threads[j].join();
    }catch(Exception ex){
      ex.printStackTrace();
    }
  }
  for (int i = 0; i < cards.size(); i++) {
    //labels[i] = createCardLabel("Card", cards.get(i).getRaw(), origin);
   //if(sta!=null){
    labels[i].addMouseListener(new RolloverListener());
    labels[i].addMouseMotionListener(this);
    labels[i].addMouseListener(this);
   //}
    //layeredPane.add(labels[i], new Integer(i));
    //origin.y += offset;
  }

  // add(Box.createRigidArea(new Dimension(0, 10)));
  // add(Box.createRigidArea(new Dimension(0, 10)));
  JScrollPane scroll = new JScrollPane(layeredPane);
  if (origin.y + 310 > 700) {
   scroll.setPreferredSize(new Dimension(263, 700));
  } else
   scroll.setPreferredSize(new Dimension(263, origin.y + 330));
  return scroll;
 }

 @SuppressWarnings("unused")
private JLabel createCardLabel(String text, BufferedImage color,
   Point origin) {
  JLabel label = new JLabel();
  label.setVerticalAlignment(JLabel.TOP);
  label.setHorizontalAlignment(JLabel.CENTER);
  label.setOpaque(true);
  label.setIcon(new ImageIcon(((Image) color).getScaledInstance(223, 310,
    0)));
  label.setForeground(Color.black);
  label.setBorder(BorderFactory.createLineBorder(Color.black));
  label.setBounds(origin.x, origin.y, 223, 310);
  return label;
 }

 class RolloverListener extends MouseAdapter {
  public boolean clicked = false;

  public void setClicked(Boolean b) {
   clicked = b;
   repaint();
  }

  public void mouseEntered(MouseEvent e) {
   if (!clicked)
    ((JLabel) e.getComponent()).setBorder(BorderFactory
      .createLineBorder(Color.green, 2));
   repaint();
  }

  public void mouseExited(MouseEvent e) {
   if (!clicked) {
    ((JLabel) e.getComponent()).setBorder(BorderFactory
      .createEmptyBorder());
    repaint();
   }
  }

  public void mouseClicked(MouseEvent e) {
   if (clicked)
    clicked = false;
   else
    clicked = true;
   ((JLabel) e.getComponent()).setBorder(BorderFactory
     .createLineBorder(Color.red, 2));
   repaint();
   exchange((JLabel) e.getComponent());
  }
 }

 @SuppressWarnings("static-access")
public void exchange(JLabel f) {
  int j = 0;
  for (int i = 0; i < labels.length; i++) {
   if (f == labels[i])
    j = i;
  }
  if (oneClicked) {
   RolloverListener e = (RolloverListener) labels[j].getMouseListeners()[0];
   FieldCard ctemp = cards.get(first);
   cards.set(first, cards.get(j));
   cards.set(j, ctemp);
   if(sta != null) {
     Core.printToLog("The cards at indices "+first+" and "+j+" of "+sta.getDescriptor()+" were swapped.");
   }
   int temp = layeredPane.getLayer(labels[first]);
   layeredPane
     .setLayer(labels[first], layeredPane.getLayer(labels[j]));
   layeredPane.setLayer(labels[j], temp);
   Point temp2 = new Point(labels[first].getLocation());
   labels[first].setLocation(new Point(labels[j].getLocation()));
   labels[j].setLocation(temp2);
   labels[j].setBorder(BorderFactory.createEmptyBorder());
   labels[first].setBorder(BorderFactory.createEmptyBorder());
   e.setClicked(false);
   e = (RolloverListener) labels[first].getMouseListeners()[0];
   e.setClicked(false);
   oneClicked = false;
   first = -1;
  } else
   oneClicked = true;
  first = j;
 }

 public void what() {

 }
 public void mouseDragged(MouseEvent e){
   for (int i=0; i<labels.length; i++){
     if (e.getSource()==labels[i]){
       dragIdx=i;
     }
   }
 }
 public void mouseMoved(MouseEvent e){
 }
 public void mousePressed(MouseEvent e){
 }
 public void mouseReleased(MouseEvent e){
   if (sui==null) return;
   if(dragIdx==-1)
     return;
   Vector<Stack> images = sui.getRelevantImages();
   PointerInfo pi = MouseInfo.getPointerInfo();
   Point drop = pi.getLocation();
   SwingUtilities.convertPointFromScreen(drop, sui.getSelectedPanel()); //It may be that we want the containing scrollpane, not the pane
   int dropIdx = -1;
   int i;
   for(i=0; i<images.size() && i >= 0; i++){
   JLabel container = images.get(i).getContainer();
   if(container.contains(SwingUtilities.convertPoint(sui.getSelectedPanel(), drop, container))){
    dropIdx = i;
    break;
   }
   else if(i+1 == images.size()){
    i = -2;
   }
  }
  int origDragIdx = dragIdx; //Temp storage of dragIdx, which we'll need to reset to -1 now.
  dragIdx=-1;

  //Handle each of the six cases
  FieldCard card = cards.get(origDragIdx);
  boolean empty = (i == -1);
  if(i!=-1)
   empty = images.get(i).getCards().get(0).getName().equals("empty");
  if(empty){
   SwingUtilities.convertPointToScreen(drop, sui.getSelectedPanel());
   SwingUtilities.convertPointFromScreen(drop, sui);
   int check = sui.spawnStack(card,drop,sta.getPlayer(),this.sta.getId());
   if(check == 0){
    cards.remove(origDragIdx);
    updateCards(cards);
    refresh();
    return;
   }
   else if(check != -2){
    return;
   }
  }
  if(dropIdx < 0)
   return;
  String[] possibleValues = {"Put on top", "Put on bottom", "Shuffle in", "Insert nth from top", "Insert nth from bottom", "Insert manually"};
  String selectedValue = (String) JOptionPane.showInputDialog(null,
                                                                 "Choose an option", "Card Movement",
                                                                 JOptionPane.INFORMATION_MESSAGE, null, possibleValues,
                                                                 possibleValues[0]);
  if (selectedValue == null) return;
  if (selectedValue == "Put on top"){
   images.get(dropIdx).addCardToTop(card);
   images.get(dropIdx).refreshImg();
   Core.printToLog("The card at index "+origDragIdx+" was moved from "+sta.getDescriptor()+" to the top of "+images.get(dropIdx).getDescriptor()+".", "SSMove "+sta.getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 0 T;");
     }else if (selectedValue == "Put on bottom"){
       images.get(dropIdx).addCardToBot(card);
       images.get(dropIdx).refreshImg();
        Core.printToLog("The card at index "+origDragIdx+" was moved from "+sta.getDescriptor()+" to the bottom of "+images.get(dropIdx).getDescriptor()+".", "SSMove "+sta.getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 0 B;");
     }else if (selectedValue == "Shuffle in"){
       images.get(dropIdx).addCardToTop(card);
       images.get(dropIdx).shuffle();
       Core.printToLog("The card at index "+origDragIdx+" was moved from "+sta.getDescriptor()+" to the top of "+images.get(dropIdx).getDescriptor()+".", "SSMove "+sta.getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 0 T;");
       Core.printToLog(images.get(dropIdx).getDescriptor()+" was shuffled.", "Shuffle "+sta.getId()+";");
     }else if (selectedValue == "Insert nth from top"){
       int n;
       String result = JOptionPane.showInputDialog("Enter index from top. (On top is 1, below the top card is 2, etc.)");
       if (result==null) return;
       try { 
         Integer.parseInt(result); 
       } catch(NumberFormatException ex) { 
         JOptionPane.showMessageDialog(this, "Must enter an integer.", "Bad Input", JOptionPane.INFORMATION_MESSAGE);
         return;
       }
       n = Integer.parseInt(result);
       images.get(dropIdx).insertCardFromTop(card, n-1);
       images.get(dropIdx).refreshImg();
       if (n<=1) 
           Core.printToLog("The card at index "+origDragIdx+" was moved from "+sta.getDescriptor()+" to the top of "+images.get(dropIdx).getDescriptor()+".", "SSMove "+sta.getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 0 T;");
       else if (n==2)
        Core.printToLog("The card at index "+origDragIdx+" was moved from "+sta.getDescriptor()+" to "+images.get(dropIdx).getDescriptor()+", 2nd from the top.", "SSMove "+sta.getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 1 T;");
       else if (n==3)
           Core.printToLog("The card at index "+origDragIdx+" was moved from "+sta.getDescriptor()+" to "+images.get(dropIdx).getDescriptor()+", 3rd from the top.", "SSMove "+sta.getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 2 T;");
       else
           Core.printToLog("The card at index "+origDragIdx+" was moved from "+sta.getDescriptor()+" to "+images.get(dropIdx).getDescriptor()+", "+n+"th from the top.", "SSMove "+sta.getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" "+(n-1)+" T;");     
       }else if (selectedValue == "Insert nth from bottom"){
       int n;
       String result = JOptionPane.showInputDialog("Enter index from bottom. (On bottom is 1, above the bottom card is 2, etc.)");
       if (result==null) return;
       try { 
         Integer.parseInt(result); 
       } catch(NumberFormatException ex) { 
         JOptionPane.showMessageDialog(this, "Must enter an integer.", "Bad Input", JOptionPane.INFORMATION_MESSAGE);
         return;
       }
       n = Integer.parseInt(result);
       images.get(dropIdx).insertCard(card, n-1);
       images.get(dropIdx).refreshImg();
       if (n<=1) 
        Core.printToLog("The card at index "+origDragIdx+" was moved from "+sta.getDescriptor()+" to the bottom of "+images.get(dropIdx).getDescriptor()+".", "SSMove "+sta.getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 0 B;");
       else if (n==2)
        Core.printToLog("The card at index "+origDragIdx+" was moved from "+sta.getDescriptor()+" to "+images.get(dropIdx).getDescriptor()+", 2nd from the bottom.", "SSMove "+sta.getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 1 B;");
       else if (n==3)
        Core.printToLog("The card at index "+origDragIdx+" was moved from "+sta.getDescriptor()+" to "+images.get(dropIdx).getDescriptor()+", 3rd from the bottom.", "SSMove "+sta.getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 2 B;");
       else
        Core.printToLog("The card at index "+origDragIdx+" was moved from "+sta.getDescriptor()+" to "+images.get(dropIdx).getDescriptor()+", "+n+"th from the bottom.", "SSMove "+sta.getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" "+(n-1)+" B;");
     }else if (selectedValue == "Insert manually"){
       images.get(dropIdx).addCardToTop(card);
       new FieldStack(images.get(dropIdx).getCards(), images.get(dropIdx), sui);
       Core.printToLog("The card at index "+origDragIdx+" was moved from "+sta.getDescriptor()+" to the top of "+images.get(dropIdx).getDescriptor()+".", "SSMove "+sta.getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 0 T;");
       Core.printToLog("A player is looking through "+images.get(dropIdx).getDescriptor()+".");
     }
  //In any case, remove the card from the fieldStack.
  cards.remove(origDragIdx);
  updateCards(cards);
  refresh();
 }
 public void mouseClicked(MouseEvent e){
 }
 public void mouseEntered(MouseEvent e){
 }
 public void mouseExited(MouseEvent e){
 }
}