import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ChatEntry implements Comparable<ChatEntry> {
 private final Date date;
 private final String entry;
 private final String user;

 public ChatEntry(String entry, String user) {
  date = new Date();
  this.entry = entry;
  this.user = user;
 }

 public Date getDate() {
  return date;
 }

 public String getEntry() {
  return entry;
 }
 
 public String getUser(){
  return user;
 }

 public String toString() {
  DateFormat df = new SimpleDateFormat("HH:mm:ss");
  return user + " [" + df.format(date) + "]: " + entry;
 }

 public int compareTo(ChatEntry c) {
  return date.compareTo(c.getDate());
 }
}
