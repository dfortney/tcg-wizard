import javax.swing.*;
import java.awt.*;

public class CreateGameUI {
  static int bool = 0;
  static String nF;
  static String fF;
     public CreateGameUI() {
      JComboBox ncBox, ccBox, dcBox;
      String[] test = {"","2", "3", "4"};
      //String[] cards = {"52 card", "Magic", "Poke'mon"};
     //String[] decks = {"Boss", "Elite", "God Mode"};
      ccBox = new JComboBox();
      dcBox = new JComboBox(new String[] {"","Collection", "Deck", "All decks in collection", "All collections", "All decks"});
      ccBox.removeAllItems();
      dcBox.removeAllItems();
      String cardTypes[] = Core.getCardTypes();
      for (int i=0; i<cardTypes.length; i++){
        ccBox.addItem(cardTypes[i]);
      }
      String[] items = Core.getDeckNamesArray();
      for (int i=0; i<items.length; i++){
       String ct = Core.getCollCardType(Core.getDeckCollection((String)items[i]));
       String selected = (String)dcBox.getSelectedItem();
       if (ct.equals(selected)){
         dcBox.addItem(items[i]);
        }
       }
     
      
      
      ncBox = new JComboBox(test);
      
      JTextField formatField = new JTextField(5);
      
      JPanel myPanel = new JPanel();
      myPanel.setLayout(new GridLayout(2,2));
      myPanel.add(new JLabel("Number of Players:"));
      myPanel.add(ncBox);
      myPanel.add(Box.createHorizontalStrut(15)); // a spacer
      myPanel.add(new JLabel("Card Type:"));
      myPanel.add(ccBox);
      myPanel.add(new JLabel("Game Format:"));
      myPanel.add(formatField);
      myPanel.add(Box.createHorizontalStrut(15)); // a spacer
      myPanel.add(new JLabel("Deck:"));
      myPanel.add(dcBox);
      
      

      int result = JOptionPane.showConfirmDialog(null, myPanel, 
               "Game Creation Requirements", JOptionPane.OK_CANCEL_OPTION);
      if (result == JOptionPane.OK_OPTION) {
         bool = 1;
         nF = (String) ncBox.getSelectedItem();
         fF = formatField.getText();
         
         
      }
   }
     public static void main(int i) {
       @SuppressWarnings("unused")
       CreateGameUI cgui = new CreateGameUI();
     }
}




     
 