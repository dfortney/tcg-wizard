import java.util.ArrayList;

public class Datapacket {
  //note: the arraylist is structured as such:
  //name of the card, the file path of the image, notes for the car, id of the card, quantity, then
  //any other field that needs to be added
  
  //An arraylist that keeps all the fields that are related to a card
  ArrayList<Object> data;
    
  public Datapacket(String name, String image, String notes, String id, String quantity) {
    
    data = new ArrayList<Object>();
    
    data.add(name);
    data.add(image);
    data.add(notes);
    data.add(id);
    data.add(quantity);
  }
  void addNewField(String field) {
    data.add(field);
  }
  
  String getField(int id) {
    
    return data.get(id).toString();
  }
}