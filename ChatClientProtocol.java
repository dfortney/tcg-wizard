import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.Queue;

public class ChatClientProtocol extends Protocol {
	private Queue<String> queue;
	private ChatTextArea tArea;
	private String username;

	public synchronized void addLine(String line) {
		if (line != null) {
			queue.offer(line);
			notifyAll();
		}
	}

	public synchronized String readLine() {
		// This guard only loops once for each special event, which may not
		// be the event we're waiting for.
		while (queue.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}

		return queue.poll();
	}

	/**
	 * processInput handles the actions of a server after receiving data from a
	 * client.
	 * 
	 * @param s
	 *            One line of data from the client
	 * @return The string to send back to the client.
	 */
	public String processInput(String s) {
		if(s.contains("WELCOME")){
			
			String name = username;
			
			try {
				name = URLEncoder.encode(name, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			return "#NAME" + name;
		}
		
		String fromUser = readLine();

		if (fromUser != null) {
			tArea.append(fromUser, username);
		}

		try {
			fromUser = URLEncoder.encode(fromUser, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return fromUser;
	}

	public ChatClientProtocol(ChatTextArea cta, String user) {
		super("#END");
		if(user==null || user.equals("")){
			user = "You";
		}
		username = user;
		queue = new LinkedList<String>();
		tArea = cta;
	}

	/**
	 * The close function is called after the client of server finishes using
	 * the protocol. Can be used to clean up resources
	 */
	public void close() {
	}
	
	public Protocol clone(){
		return null;
	}

}
