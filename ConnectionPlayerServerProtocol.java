import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.swing.JComboBox;

public class ConnectionPlayerServerProtocol extends Protocol {
 private String username;
 private FilteredTextArea tArea;
 private SimpleServer playerServ;
 private JComboBox deckBox;

 /**
  * processInput handles the actions of a server after receiving data from a
  * client.
  * 
  * @param s
  *            One line of data from the client
  * @return The string to send back to the client.
  */
 public String processInput(String s) {
  System.out.println("Got: " + s);
  if (s == null) {
   return "#WTF";
  }

  if (s.contains(getTerminator())) {
   return getTerminator();
  }
  if (s.contains("#GREETINGS")) {

   String name = username;

   try {
    name = URLEncoder.encode(name, "UTF-8");
   } catch (UnsupportedEncodingException e) {
    e.printStackTrace();
   }

   return "@GREETINGS" + name;
  } else if (s.contains("#PDATA")) {
   int i = s.indexOf("#GRID");
   s = s.replace("#GRID", "");
   String evGrid = s.substring(i);
   String evS = s.substring(0, i);
   
   int j = evS.indexOf("#PDATA");
   evS = evS.replace("#PDATA", "");
   String evData = evS.substring(j);

   
   try {
    evData = URLDecoder.decode(evData, "UTF-8");
    evGrid = URLDecoder.decode(evGrid, "UTF-8");
   } catch (UnsupportedEncodingException e) {
    e.printStackTrace();
   }

   String[] full = evData.split(";");
   //NOW START MULTIPLAYER SANDBOX
   Core.startMultiplayerGame(full, username, (String)deckBox.getSelectedItem());
   tArea = Core.getOnlineGame().getTextArea();
  } else if (s.contains("#EVENT")) {
   int j = s.indexOf("#NOTES");
   String evNotes = "";
   if(j!= -1){
    s = s.replace("#NOTES", "");
    evNotes = s.substring(j);
    s = s.substring(0,j);
   }
   
   int i = s.indexOf("#EVENT");
   s = s.replace("#EVENT", "");
   String evText = s.substring(i);
   String evName = s.substring(0, i);

   try {
    evText = URLDecoder.decode(evText, "UTF-8");
    evName = URLDecoder.decode(evName, "UTF-8");
   } catch (UnsupportedEncodingException e) {
    e.printStackTrace();
   }

   if (!evName.equals(username)) {
     try{
    Core.receiveBlock(evText);
     } catch(NullPointerException e1){e1.printStackTrace();}
     catch(IndexOutOfBoundsException e2){e2.printStackTrace();}
    if(tArea != null)
     tArea.append(evNotes, username, FilteredChatEntry.LOG);
   }

   return "@THANKS";
  } else if (s.contains("#CHAT")) {
   int i = s.indexOf("#CHAT");
   s = s.replace("#CHAT", "");
   String chatText = s.substring(i);
   String chatName = s.substring(0, i);

   try {
    chatText = URLDecoder.decode(chatText, "UTF-8");
    chatName = URLDecoder.decode(chatName, "UTF-8");
   } catch (UnsupportedEncodingException e) {
    e.printStackTrace();
   }

   if (tArea != null && !chatName.equals(username)) {
    tArea.append(chatText, chatName, FilteredChatEntry.CHAT);
   }

   return "@THANKS";
  }
  return "@SORRY";
 }

 public ConnectionPlayerServerProtocol(FilteredTextArea fta, JComboBox deckSelect, String name) {
  super("#END");
  tArea = fta;
  username = name;
  deckBox = deckSelect;
 }
 
 public void setTextArea(FilteredTextArea fta){
  tArea = fta;
 }

 /**
  * The close function is called after the client of server finishes using
  * the protocol. Can be used to clean up resources
  */
 public void close() {
  if (playerServ != null) {
   playerServ.close();
  }
 }

 public Protocol clone() {
  return new ConnectionPlayerServerProtocol(tArea, deckBox, username);
 }
}
