import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.AbstractTableModel;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Pattern;
//http://docs.oracle.com/javase/tutorial/uiswing/components/table.html\
//Uses Core with same end functionality as original

public class CollectionEditorUI extends GenericUI {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private JMenu tools, mnuDeck;
  private JMenuItem runQuery, openSandbox, importHTML, mnuFind, incrementQty, zeroQtys;
  private JMenuItem newDeck;
  private JMenuItem loadDeck;
  private JMenuItem newCollectionJM;
  private JMenuItem saveCollectionJM; 
  private JMenuItem importCollectionJM; 
  private JMenuItem exportCollectionJM;
  private JMenuItem deleteCollectionJM;
  private JMenuItem newCardType;
 private ArrayList<JTable> tables; // Data table for collection
// private JPanel enclosure; // Object holding the array of JLabels
// private JLabel[] attributes; // Used to display the selected card data
 private ArrayList<dataModel> models;
 private GridBagConstraints c;
 private JTabbedPane tabbedPane;

 private int currentCollection = 0;
 // private CollectionDBM collectionDB;
 private ArrayList<String> collections;

// private ArrayList<String> names;
 private JLabel card;
 private int deuiThreadCount = 8;

 private ArrayList<Boolean> changed;

 final private JFileChooser fc = new JFileChooser();

 public CollectionEditorUI(String cardTypeName) {
  super(0, 0, "Collection Editor");
  this.setVisible(false); //If the user is allowed to move the window as its components are being placed, it will usually interfere with the placement.
  models = new ArrayList<dataModel>();
  tables = new ArrayList<JTable>();

  changed = new ArrayList<Boolean>();

  // CardTypeDBM cardTypesDB = new CardTypeDBM();
  // this.fields = cardTypesDB.getFields(cardTypeName);
  // this.types = cardTypesDB.getTypes(cardTypeName);
  // cardTypesDB = null;

  //Add menus
  tools = new JMenu("Tools");
  mnuDeck = new JMenu("Decks");
  runQuery = new JMenuItem("Run Query");
  newDeck = new JMenuItem("New Deck");
  loadDeck = new JMenuItem("Load Deck");
  openSandbox = new JMenuItem("Open Sandbox");
  importHTML = new JMenuItem("Convert HTML to importable");
  mnuFind = new JMenuItem("Find");
  zeroQtys = new JMenuItem("Set Quantities to Zero");
  newCollectionJM = new JMenuItem("New Collection");
  newCardType = new JMenuItem("New Card Type");
  saveCollectionJM = new JMenuItem("Save Collection");
  deleteCollectionJM = new JMenuItem("Delete Collection");
  exportCollectionJM = new JMenuItem("Export Saved Collection");
  importCollectionJM = new JMenuItem("Import Collection");
  newCollectionJM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
  newCardType.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
  exportCollectionJM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
  importCollectionJM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.CTRL_MASK));
  exportCollectionJM.addActionListener(this);
  importCollectionJM.addActionListener(this);
  saveCollectionJM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK));
  deleteCollectionJM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, ActionEvent.CTRL_MASK));
  newCollectionJM.addActionListener(this);
  saveCollectionJM.addActionListener(this);
  deleteCollectionJM.addActionListener(this);
  newCardType.addActionListener(this);
  this.bar.add(mnuDeck);
  this.bar.add(tools);
  this.mnuDeck.add(newDeck);
  this.mnuDeck.add(loadDeck);
  tools.add(runQuery);
  tools.add(openSandbox);
  file.add(newCollectionJM);
  //file.add(loadCollectionJM);
  file.add(saveCollectionJM);
  file.add(deleteCollectionJM);
  file.add(importCollectionJM);
  file.add(exportCollectionJM);
  file.add(newCardType);
  file.add(exit);
  //tools.add(importHTML);
  tools.add(mnuFind);
  tools.add(zeroQtys);
  incrementQty = new JMenuItem("Increment quantity");
  incrementQty.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_SEMICOLON, ActionEvent.CTRL_MASK));
  incrementQty.addActionListener(this);
  tools.add(incrementQty);
  mnuFind.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, ActionEvent.CTRL_MASK));
  newDeck.addActionListener(this);
  loadDeck.addActionListener(this);
  runQuery.addActionListener(this);
  openSandbox.addActionListener(this);
  importHTML.addActionListener(this);
  mnuFind.addActionListener(this);
  zeroQtys.addActionListener(this);
  
  
  c = new GridBagConstraints();
  // Read in default image

  Image newImg = Core.imgMaker("backofamagicthegatheringcard.jpg", null);
  ImageIcon newIcon = null;
  if (newImg != null) {
   try {
    newIcon = new ImageIcon(newImg);
   } catch (Exception ex) {
   }
  }

  card = new JLabel(newIcon);
  card.revalidate();
  Core.imgMaker("backofamagicthegatheringcard.jpg", card);

  // Place image
  c.fill = GridBagConstraints.NONE;
  c.gridx = 1;
  c.anchor = GridBagConstraints.PAGE_START;
  add(card, c);
  c.insets = new Insets(0, 0, 0, 20);
  c.gridx = 0;
  c.gridy = 0;
  c.ipadx = 50;
  c.ipady = 0;
  c.gridheight = 2;
  c.fill = GridBagConstraints.VERTICAL;
  // Create table and scrollpane; then place them
  tabbedPane = new JTabbedPane();
  collections = Core.getCollNames();
  for (int j = 0; j < collections.size(); j++) {
   JScrollPane temp = tableMaker(j);

   tabbedPane.addTab(collections.get(j), null, temp,
     collections.get(j));
  }

  ChangeListener changeListener = new ChangeListener() {
   public void stateChanged(ChangeEvent changeEvent) {
    JTabbedPane sourceTabbedPane = (JTabbedPane) changeEvent
      .getSource();
    int i = sourceTabbedPane.getSelectedIndex();
    if (i >= 0) {
     System.out.println("Tab changed to: "
       + sourceTabbedPane.getTitleAt(i));
     currentCollection = i;
    }
   }
  };
  tabbedPane.addChangeListener(changeListener);
  add(tabbedPane, c);

  // Generate Panel and JLabels and place them
  /*
   * enclosure = new JPanel(); String[] curFields =
   * collectionDB.getFields(collections.get(currentCollection));
   * enclosure.setLayout(new GridLayout(curFields.length, 1, 0, 10));
   * attributes = new JLabel[curFields.length]; for (int i = 0; i <
   * curFields.length; i++) { attributes[i] = new JLabel(curFields[i]);
   * enclosure.add(attributes[i]); }
   */
  c.gridheight = 1;
  c.gridx = 1;
  c.gridy = 1;
  c.anchor = GridBagConstraints.CENTER;
  c.insets = new Insets(5, 0, 10, 10);
  c.fill = GridBagConstraints.NONE;

  // add(enclosure, c);
  this.pack();
  this.setVisible(true);
 }

 // Override the GenericUI method. Prompts user if a collection has been
 // modified but not saved.
 public boolean close() {
  try{
   for (int i = 0; i < collections.size(); i++) {
    if (changed.get(i) == true) {
     int result = JOptionPane
       .showConfirmDialog(
         (Component) null,
         "The collection "+ collections.get(i) +" has been modified. Do you want to save your changes?",
         "TCG Wizard", JOptionPane.YES_NO_CANCEL_OPTION);
     if (result == 2)
      return false;
     if (result == 0) {
      ArrayList<Object[]> o = models.get(i).getData();
      o.remove(o.size() - 1);
      Core.saveCollection(collections.get(i), o);
      changed.set(i, false);
     }
    }
   }
  }
  catch(Exception e){
  }
  return true;
 }

 public static void main(String[] args) {
  @SuppressWarnings("unused")
CollectionEditorUI gui = new CollectionEditorUI("magic");
 }

 //Depending on the system, pop-up triggers may be recognized on mouse press or release. Thus, we handle both.
 public void mousePressed(MouseEvent e){
   mouseReleased(e);
 }
 public void mouseReleased(MouseEvent e) {
  if (e.getSource() == tables.get(currentCollection)) {

   int r = tables.get(currentCollection).rowAtPoint(e.getPoint());
   if (r >= 0 && r < tables.get(currentCollection).getRowCount()) {
    tables.get(currentCollection).setRowSelectionInterval(r, r);
   } else {
    tables.get(currentCollection).clearSelection();
   }
   int rowindex = tables.get(currentCollection).getSelectedRow();
   if (rowindex < 0)
    return;
   System.out.println("isPopupTrigger? "+e.isPopupTrigger());
   if (e.isPopupTrigger() && e.getComponent() instanceof JTable) {
    class RemovePopup extends JPopupMenu {
     /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
	JMenuItem removeItem, extendImageURL, randomQtys, addCopy;

     class popListener implements MouseListener {
      private int row;

      public popListener(int i) {
       row = i;
      }

      public void mouseClicked(MouseEvent e) {
      }

      public void mouseEntered(MouseEvent e) {
      }

      public void mouseExited(MouseEvent e) {
      }

      @Override
      public void mousePressed(MouseEvent e) {
      }

      @Override
      public void mouseReleased(MouseEvent e) {
        if (e.getSource()==removeItem){
         
            int ID = (Integer)models.get(currentCollection).getValueAt(row, 1);
            Vector<String> possibleValues = Core.getDeckNames();
   if (possibleValues != null) {
    int fdIdx = 0;
    String temp[] = new String[possibleValues.size()];
    for (int i = 0; i < possibleValues.size(); i++) {
     if (Core.getDeckCollection(possibleValues.get(i)).equals(
       tabbedPane.getTitleAt(tabbedPane.getSelectedIndex()))) {
      temp[fdIdx] = possibleValues.get(i);
      fdIdx++;
     }
    }
    String finalDecks[] = new String[fdIdx];
    for (int i = 0; i < fdIdx; i++) {
     finalDecks[i] = temp[i];
    }
    Vector<String> error = new Vector<String>();
    for(int i = 0; i < finalDecks.length; i++){
     Vector<Object> data = Core.getDeckIDs(finalDecks[i]);
     for(int j = 0; j < data.size(); j++){
      System.out.println("HWAEL");
      System.out.println(data.get(j));
      try{
       if(Integer.parseInt((String)data.get(j)) == ID){
        error.add(finalDecks[i]);
        break;
       }
      }catch(Exception e2){
       if((Integer)data.get(j) == ID){
        error.add(finalDecks[i]);
        break;
       }
      }
     }
    }
    if(error.size() != 0){
     String list = "";
     for(int i = 0; i < error.size(); i++){
      list += error.get(i);
      if(i + 1 != error.size()){
       list+=", ";
      }
     }
     JOptionPane.showMessageDialog(null, "Cannot Delete this card it is currently in the following Decks: " + list, "Removal Error", JOptionPane.ERROR_MESSAGE);
     return;
    }
   }
   
         models.get(currentCollection).remove(row);
          models.get(currentCollection)
            .fireTableDataChanged();
        }else if (e.getSource()==extendImageURL){
          String start = (String)models.get(currentCollection).getData().get(row)[3];
          //Remove the `number`.jpg at the end.
          int len = start.length();
          int idx = start.lastIndexOf('.');
          int dotIdx = idx;
          String end = start.substring(dotIdx+1, len);
          for (idx-=1;idx>0;idx--){
            char ch = start.charAt(idx);
            boolean digit = Character.isDigit(ch);
            if (!digit) break; //And idx is the index of the last character we want.
          }
          String base = start.substring(0,idx+1);
          String strnum = start.substring(idx+1, dotIdx);
          Integer startnum = Integer.parseInt(strnum);
          String iterations = JOptionPane.showInputDialog("You're about to copy "+base+"*."+end+", where * starts at "+strnum+". How many rows do you wish to iterate over?");
          int iter = 0;
          try{
            iter = Integer.parseInt(iterations);
          }catch(Exception ex){
            System.out.println("Did not enter an int!");
            return;
          }
          for (int i=1; i<=iter; i++){
            String submission = base + Integer.toString(startnum+i) +"."+end;
            ArrayList<Object[]> objData = models.get(currentCollection).getData();
            Object[] objRow = objData.get(row+i);
            objRow[3] = submission;
            objData.set(row+i, objRow);
            models.get(currentCollection).setData(objData);
           // models.get(currentCollection).getData().set(row+i, objRow);
          }
          models.get(currentCollection).fireTableDataChanged();
        }else if (e.getSource()==randomQtys){
          String iterations = JOptionPane.showInputDialog("How many rows do you wish to insert quantities for?");
          int iter = 0;
          try{
            iter = Integer.parseInt(iterations);
          }catch(Exception ex){
            System.out.println("Did not enter an int!");
            return;
          }
          Random generator = new Random();
          for (int i=0; i<iter; i++){
            ArrayList<Object[]> objData = models.get(currentCollection).getData();
            Object[] objRow = objData.get(row+i);
            objRow[2] = generator.nextInt(10);
            objData.set(row+i, objRow);
            models.get(currentCollection).setData(objData);
            // models.get(currentCollection).getData().set(row+i, objRow);
          }
          models.get(currentCollection).fireTableDataChanged();
        }else if (e.getSource()==addCopy){
          ArrayList<Object[]> objData = models.get(currentCollection).getData();
          int types[] = Core.getCollTypes(collections.get(currentCollection));
          Object[] objRow = new Object[objData.get(row).length];
          System.arraycopy(objData.get(row),0,objRow,0,objData.get(row).length);
          objRow[1] = objData.get(objData.size()-1)[1];
          objData.set(objData.size()-1,objRow);
          Object[] blankRow = new Object[objRow.length];
          blankRow[0]="";
          blankRow[1]=models.get(currentCollection).nextId();
          blankRow[2]=0;
          for (int j=3; j<blankRow.length; j++){
            int i = types[j];
            if (i==2||i==4)
              blankRow[j]=0;
            else if (i==0)
              blankRow[j]=(Boolean)true;
            else if (i==3)
              blankRow[j]=' ';
            else
              blankRow[j]="";
          }
          objData.add(blankRow);
          models.get(currentCollection).setData(objData);
          models.get(currentCollection).fireTableDataChanged();
          tables.get(currentCollection).changeSelection(objData.size()-2,0,false,false);
          tables.get(currentCollection).setRowSelectionInterval(objData.size()-2, objData.size()-2);
        }
      }
     }

     public RemovePopup(int rowNum) {
      int r = tables.get(currentCollection)
        .convertRowIndexToModel(rowNum);
      removeItem = new JMenuItem("Remove Card");
      removeItem.addMouseListener(new popListener(r));
      add(removeItem);
      extendImageURL = new JMenuItem("Extend Image Path");
      extendImageURL.addMouseListener(new popListener(r));
      add(extendImageURL);
      randomQtys = new JMenuItem("Fill with random quantities");
      randomQtys.addMouseListener(new popListener(r));
      //add(randomQtys);
      addCopy = new JMenuItem("Copy to bottom");
      addCopy.addMouseListener(new popListener(r));
      add(addCopy);
     }
    }
    JPopupMenu popup = new RemovePopup(rowindex);
    popup.show(e.getComponent(), e.getX(), e.getY());
   }
  } else if (e.isPopupTrigger()) {

  }
 }

public JScrollPane tableMaker(int j) {
  int[] curTypes = Core.getCollTypes(collections.get(j));
  String[] curFields = Core.getCollFields(collections.get(j));
  @SuppressWarnings("rawtypes")
  Class[] cList = new Class[curTypes.length];
  for (int i = 0; i < curTypes.length; i++) {
   cList[i] = Core.typeConversion(curTypes[i]);
  }
  if (collections.size() > 0) {
   try {

    models.add(new dataModel(curFields, cList, Core
      .getCollectionData(collections.get(j))));
   } catch (SQLException e) {
    // TODO Auto-generated catch block
    e.printStackTrace();
    models.add(new dataModel(curFields, cList));
   }
  } else {
   models.add(new dataModel(curFields, cList));
  }

  changed.add(false);
  tables.add(new JTable(models.get(j)));
  tables.get(j).setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

  ListSelectionListener lsListener = new ListSelectionListener() {
   public void valueChanged(ListSelectionEvent e) {
    int[] selectedRow = tables.get(currentCollection)
      .getSelectedRows();
    if (selectedRow.length == 0)
     return;
    // Add an image.
    Object objRow[] = models
      .get(currentCollection)
      .getData()
      .get(tables.get(currentCollection)
        .convertRowIndexToModel(selectedRow[0]));
    //System.out.println((String) objRow[3]);
    String imageFile = (String) objRow[3];
    if(imageFile == null){
      imageFile = "";
    }
    if ( imageFile.equals("")) {
      imageFile = "backofamagicthegatheringcard.jpg";
    }
    Core.imgMaker(imageFile, card);
    // enclosure.removeAll();
//    String[] curFields = Core.getCollFields(collections
//      .get(currentCollection));
    /*
     * attributes = new JLabel[curFields.length]; for (int i = 0; i
     * < curFields.length; i++) { attributes[i] = new
     * JLabel(curFields[i] + ": " +
     * tables.get(currentCollection).getValueAt( selectedRow[0],
     * i)); enclosure.add(attributes[i]); }
     */
    pack();
   }

  };
  tables.get(j).getSelectionModel().addListSelectionListener(lsListener);
  JScrollPane scrollpane = new JScrollPane(tables.get(j));
  scrollpane
    .setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
  scrollpane
    .setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
  tables.get(j).addMouseListener(this);
  SummaryTableSorter<dataModel> sort = new SummaryTableSorter<dataModel>(models.get(j));
   tables.get(j).setRowSorter(sort);
  return scrollpane;
 }

 public ArrayList<Object[]> loadCollection(String name) {
  ArrayList<Object[]> dbData = null;
  try {
   dbData = Core.getCollectionData(name);
   
   
   int tempId = 0;
   for (int i = 0; i < dbData.size(); i++) {
     if(tempId < (Integer)dbData.get(i)[1]){
       tempId = (Integer)dbData.get(i)[1];
     }
   }
  if(dbData.size() == 0){
    models.get(currentCollection).setId(0);
  } else {
    models.get(currentCollection).setId(tempId + 1);
  }
   
   Object[] o = new Object[models.get(currentCollection).getColumnCount()];
   for (int i = 0; i < models.get(currentCollection).getColumnCount(); i++) {
     if (i != 1) {
      o[i] = models.get(currentCollection).defaultValues(Core
        .typeToInt(models.get(currentCollection).getColumnClass(i)));
     } else {
      o[i] = models.get(currentCollection).nextId();
     }
    }
   dbData.add(o);
  } catch (SQLException e) {
   // TODO Auto-generated catch block
   System.out.println("failed");
   e.printStackTrace();
  }

  return dbData;
 }

 /*
  * public void sendCollectionToDB(String name) { if (collectionDB == null ||
  * name == null || name.equals("")) { return; } try { ArrayList<Object[]> o
  * = (ArrayList<Object[]>) models .get(currentCollection).getData().clone();
  * o.remove(models.get(currentCollection).getData().size() - 1);
  * collectionDB.setCollectionData(name, o); } catch (SQLException e) { //
  * TODO Auto-generated catch block e.printStackTrace(); } }
  */

 class dataModel extends AbstractTableModel {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private String[] columnNames;
  @SuppressWarnings("rawtypes")
  private Class[] columnTypes;
  private ArrayList<Object[]> data;
  private int id;

  private Object defaultValues(int t) {
   switch (t) {
   case 0:
    return true;
   case 1:
    return "";
   case 2:
    return 0;
   case 3:
    return ' ';
   case 4:
    return 0;
   default:
    return "";
   }
  }

  private void setId(int i){
    id = i;
  }
  
  private int nextId() {
   return id++;
  }

  @SuppressWarnings("rawtypes")
  public dataModel(String[] colNames, Class[] colTypes) {
   columnNames = colNames;
   columnTypes = colTypes;
   id = 0;
   data = new ArrayList<Object[]>();
   data.add(new Object[getColumnCount()]);
   for (int i = 0; i < getColumnCount(); i++) {
    if (i != 1) {
     data.get(data.size() - 1)[i] = defaultValues(Core
       .typeToInt(getColumnClass(i)));
    } else {
     data.get(data.size() - 1)[i] = nextId();
    }
   }
  }

  @SuppressWarnings("rawtypes")
  public dataModel(String[] colNames, Class[] colTypes,
    ArrayList<Object[]> oldData) {
   columnNames = colNames;
   columnTypes = colTypes;
   id = 0;
   data = new ArrayList<Object[]>();
   for (int i = 0; i < oldData.size(); i++) {
    for (int j = 0; j < oldData.get(i).length; j++) {
     if (oldData.get(i)[j] == null) {
      oldData.get(i)[j] = "";
     }
    }
    data.add(oldData.get(i).clone());
    id = (Integer) oldData.get(i)[1] + 1;
   }
   data.add(new Object[getColumnCount()]);
   for (int i = 0; i < getColumnCount(); i++) {
    if (i != 1) {
     data.get(data.size() - 1)[i] = defaultValues(Core
       .typeToInt(getColumnClass(i)));
    } else {
     data.get(data.size() - 1)[i] = nextId();
    }
   }
  }

  public ArrayList<Object[]> getData() {
   ArrayList<Object[]> out = new ArrayList<Object[]>();
   for (int i = 0; i < data.size(); i++) {
    Object[] o = data.get(i).clone();
    out.add(o);
   }
   return out;
  }

  public void setData(ArrayList<Object[]> a) {
   ArrayList<Object[]> newData = new ArrayList<Object[]>();
   for (int i = 0; i < a.size(); i++) {
    Object[] o = a.get(i).clone();
    newData.add(o);
   }
   data = newData;
  }

  public int getColumnCount() {
   return columnNames.length;
  }

  public int getRowCount() {
   return data.size();
   // Call number of items in collection
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public Class getColumnClass(int col) {
   if (col < columnTypes.length) {
    return columnTypes[col];
   }
   return String.class;
  }

  public Object getValueAt(int row, int col) {
   if (row < data.size()) {
    return data.get(row)[col];
   }
   return null;
  }

  public void setValueAt(Object value, int row, int col) {
   /*if(value.getClass().equals(String.class))
   {
    System.out.println("Its a string!!");
    Pattern p = Pattern.compile("[^a-zA-Z0-9. ]");
    boolean hasSpecialChar = p.matcher((CharSequence) value).find();
    if (hasSpecialChar)
    {
     JOptionPane.showMessageDialog(Core.GUIs.get(0), "This is a non-allowed character for this field", "Illegal Character", JOptionPane.ERROR_MESSAGE);
     return;
    }
   }*/
   if(value.getClass().equals(Integer.class))
   {
    System.out.println("Its an integer");
    Pattern p = Pattern.compile("[^0-9]");
    boolean hasSpecialChar = p.matcher((CharSequence) Integer.toString((Integer) value)).find();
    if (hasSpecialChar)
    {
     JOptionPane.showMessageDialog(Core.GUIs.get(0), "This is a non-allowed character for this field", "Illegal Character", JOptionPane.ERROR_MESSAGE);
     return;
    }
   }
   System.out.println("Adding (" + value + ") at (" + row + "," + col
     + ")");
   if (row == data.size() - 1) {
    data.add(new Object[getColumnCount()]);
    for (int i = 0; i < getColumnCount(); i++) {
     if (i != 1) {
      data.get(data.size() - 1)[i] = defaultValues(Core
        .typeToInt(getColumnClass(i)));
     } else {
      data.get(data.size() - 1)[i] = nextId();
     }
    }
    fireTableRowsInserted(data.size() - 1, data.size() - 1);
   }
   data.get(row)[col] = value;
   changed.set(currentCollection, true);
   fireTableCellUpdated(row, col);
  }

  public String getColumnName(int col) {
   if (col < columnNames.length) {
    return columnNames[col];
   }
   return "";
  }

  public boolean isCellEditable(int row, int col) {
   if (col != 1) {
    return true;
   }
   return false;
  }

  public void remove(int row) {
   if (row < data.size() - 1) {
    data.remove(row);
   }
  }
 };

 public void extendedActionPerformed(ActionEvent e) {
   if(e.getSource()==newCardType){
     @SuppressWarnings("unused")
	 CardTypeUI gui = new CardTypeUI();
   }
  else if (e.getSource() == newCollectionJM) {
   // Handle menu clicks
   String newName = JOptionPane
     .showInputDialog("Please name the new collection.");
   while (newName == null || newName.equals("")
     || collections.contains(newName)) {
    if (newName == null) {
     return;
    }
    if (newName.equals("")) {
     newName = JOptionPane
       .showInputDialog("You must name the new collection.");
    } else {
     newName = JOptionPane.showInputDialog(
       "That name is already in use. "
         + "Please select a new name.", newName);
    }
   }

   // CardTypeDBM cardTypesDB = new CardTypeDBM();
   String[] possibleValues = Core.ctDBM.getEntries();
   String selectedValue = (String) JOptionPane.showInputDialog(null,
     "Choose a card type for the collection", "Input",
     JOptionPane.INFORMATION_MESSAGE, null, possibleValues,
     possibleValues[0]);

   if (selectedValue == null) {
    return;
   }

   // Store current changes
   if (collections.size() > 0) {
    ArrayList<Object[]> o = models.get(currentCollection).getData();
    o.remove(models.get(currentCollection).getData().size() - 1);
    Core.storeCollection(collections.get(currentCollection), o);
   }

   Core.createCollection(newName, selectedValue);

   collections.add(newName);
   currentCollection = collections.size() - 1;

   JScrollPane temp = tableMaker(currentCollection);

   tabbedPane.addTab(collections.get(currentCollection), null, temp,
     collections.get(currentCollection));
   // enclosure.removeAll();
//   String[] curFields = Core.getCollFields(collections
//     .get(currentCollection));
   /*
    * attributes = new JLabel[curFields.length]; for (int i = 0; i <
    * curFields.length; i++) { attributes[i] = new
    * JLabel(curFields[i]); enclosure.add(attributes[i]); }
    */
   pack();
   //
  } else if (e.getSource() == saveCollectionJM) {
   if (currentCollection >= collections.size()) {
    return;
   }
   ArrayList<Object[]> o = models.get(currentCollection).getData();
   o.remove(o.size() - 1);
   Core.saveCollection(collections.get(currentCollection), o);
   changed.set(currentCollection, false);
  } else if (e.getSource() == deleteCollectionJM) {
   if (currentCollection >= collections.size()) {
    return;
   }
   int out = JOptionPane.showConfirmDialog(null,
     "Are you sure you want to delete the collection \""
       + collections.get(currentCollection) + "\"",
     "Confirmation", JOptionPane.YES_NO_OPTION);
   if (out != 0) {
    return;
   }
   String input = (String) JOptionPane.showInputDialog(this, "Please enter the name of the collection to confirm its deletion", "Captcha", JOptionPane.INFORMATION_MESSAGE, null, null, "");
   if(!input.equals(collections.get(currentCollection))){
    return;
   }
   else{
    JOptionPane.showMessageDialog(this, "This collection will now be deleted", "Deleted", JOptionPane.INFORMATION_MESSAGE);
   }
   try {
    Core.deleteCollection(collections.get(currentCollection));
    collections.remove(currentCollection);
    tabbedPane.remove(currentCollection);
    models.remove(currentCollection);
    tables.remove(currentCollection);
    changed.remove(currentCollection);
    if (currentCollection > 0)
     currentCollection--;

   } catch (Exception e1) {
    e1.printStackTrace();
   }
  } else if (e.getSource() == importCollectionJM) {
   if (currentCollection >= collections.size()) {
    return;
   }

   int returnVal = fc.showOpenDialog(this);

   if (returnVal == JFileChooser.APPROVE_OPTION) {
    File file = fc.getSelectedFile();
    try {
     System.out.println(file.getPath());
     Core.loadCollectionFromFile(
       collections.get(currentCollection), file.getPath());
     models.get(currentCollection).setData(
       loadCollection(collections.get(currentCollection)));
     models.get(currentCollection).fireTableDataChanged();
    } catch (Exception e1) {
     // TODO Auto-generated catch block
     e1.printStackTrace();
    }
   }
  } else if (e.getSource() == exportCollectionJM) {
   if (currentCollection >= collections.size()) {
    return;
   }

   int returnVal = fc.showSaveDialog(this);

   if (returnVal == JFileChooser.APPROVE_OPTION) {
    File file = fc.getSelectedFile();
    try {
     System.out.println(file.getPath());
     Core.saveCollectionToFile(
       collections.get(currentCollection), file.getPath());
    } catch (Exception e1) {
     // TODO Auto-generated catch block
     e1.printStackTrace();
    }
   }

  }else if (e.getSource()==newDeck){
    //Add handling if no collection is open
    int idx = tabbedPane.getSelectedIndex();
    String collName = collections.get(idx);
    String fields[] = Core.getCollFields(collections.get(idx));
    dataModel model = models.get(idx);
    int len = model.getData().size();
    Object collData[][] = new Object[len][fields.length];
    DeuiThread threads[] = new DeuiThread[deuiThreadCount];
    int index = 0;
    Core.printToLog("starting");
    for (int i = 0; i<deuiThreadCount; i++){
      threads[i] = new DeuiThread(model, collData, index, 1+len/deuiThreadCount);
      index += 1+len/deuiThreadCount;
    }
    for (int i = 0; i<deuiThreadCount; i++){
      threads[i].start();
    }
    for (int i = 0; i<deuiThreadCount; i++){
      try{
        threads[i].join();
      }catch(Exception ex){
        ex.printStackTrace();
      }
    }
    Core.printToLog("done");
    /*for (int i=0; i<models.get(idx).getData().size(); i++){
      for (int j=0; j<fields.length; j++){
        collData[i][j] = (Object)models.get(idx).getValueAt(i,j);
      }
    }*/
    Core.newDeckEditorFromCEUI(collName, fields, collData);
  }else if (e.getSource()==loadDeck){
    String collName = collections.get(tabbedPane.getSelectedIndex());
    int idx = tabbedPane.getSelectedIndex();
    Vector<String> possibleValues = Core.getDeckNames();
    if(possibleValues == null){
      JOptionPane.showMessageDialog(this, "Cannot load deck, there are no saved decks", "Deck Load Failure", JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    int fdIdx = 0;
    String temp[] = new String[possibleValues.size()];
    for (int i=0; i<possibleValues.size(); i++){
      if (Core.getDeckCollection(possibleValues.get(i)).equals(collName)){
        temp[fdIdx]=possibleValues.get(i);
        fdIdx++;
      }
    }
    String finalDecks[] = new String[fdIdx];
    for (int i=0; i<fdIdx; i++){
      finalDecks[i]=temp[i];
    }
    if (finalDecks.length == 0){
      JOptionPane.showMessageDialog(this, "Cannot load deck, there are no saved decks", "Deck Load Failure", JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    String deckName = (String) JOptionPane.showInputDialog(null,
                                                           "Choose which deck to load", "Input",
                                                           JOptionPane.INFORMATION_MESSAGE, null, finalDecks,finalDecks[0]);
    if(deckName == null)
      return;
    String fields[] = Core.getCollFields(collName);
    dataModel model = models.get(idx);
    int len = model.getData().size();
    Object collData[][] = new Object[len][fields.length];
    DeuiThread threads[] = new DeuiThread[deuiThreadCount];
    int index = 0;
    Core.printToLog("starting");
    for (int i = 0; i<deuiThreadCount; i++){
      threads[i] = new DeuiThread(model, collData, index, 1+len/deuiThreadCount);
      index += 1+len/deuiThreadCount;
    }
    for (int i = 0; i<deuiThreadCount; i++){
      threads[i].start();
    }
    for (int i = 0; i<deuiThreadCount; i++){
      try{
        threads[i].join();
      }catch(Exception ex){
        ex.printStackTrace();
      }
    }
    Core.printToLog("done");
    Core.newDeckEditorFromCEUI(collName, fields, collData, deckName);
  }else if (e.getSource()==runQuery){
    new QueryUI();
  }else if (e.getSource()==openSandbox){
    new SandSplash();
  }else if (e.getSource()==importHTML){
    importHTML();
  }else if (e.getSource()==zeroQtys){
    int cont = JOptionPane.showConfirmDialog((Component) null,"This will erase all quantities in this collection. Are you sure you wish to do this?", "TCG Wizard", JOptionPane.YES_NO_OPTION);
    if (cont!=0) return;
    for (int i=0; i<models.get(currentCollection).getData().size(); i++){
      ArrayList<Object[]> objData = models.get(currentCollection).getData();
      Object[] objRow = objData.get(i);
      objRow[2] = 0;
      objData.set(i, objRow);
      models.get(currentCollection).setData(objData);
    }
    models.get(currentCollection).fireTableDataChanged();
  }else if (e.getSource()==mnuFind){
    String str = JOptionPane.showInputDialog("Look for:");
    if(str==null){return;}
    int idx = tabbedPane.getSelectedIndex();
    boolean done = false;
    ArrayList<Object[]> thedata = models.get(idx).getData();
    for (int i=0; i<thedata.size(); i++){
      for (int j=0; j<thedata.get(i).length; j++){
        if (String.valueOf(thedata.get(i)[j]).contains(str)){
          tables.get(idx).changeSelection(i,j,false,false);
          tables.get(currentCollection).setRowSelectionInterval(i, i);
          int cont = JOptionPane.showConfirmDialog((Component) null,"Keep looking?", "TCG Wizard", JOptionPane.YES_NO_OPTION);
          System.out.println(cont);
          if (cont!=0){
            done = true;
            break;
          }
        }
      }
      if (done) break;
    }       
    if (!done) JOptionPane.showMessageDialog(this, "That's all of them.", "Search Done", JOptionPane.INFORMATION_MESSAGE);
  }else if (e.getSource()==incrementQty){
    ArrayList<Object[]> objData = models.get(currentCollection).getData();
    int row = tables.get(currentCollection).getSelectedRow();
    if (row <0) return; //No row selected;
    Object[] objRow = objData.get(row);
    objRow[2] = (Integer)objRow[2]+1;
    objData.set(row, objRow);
    models.get(currentCollection).setData(objData);
    models.get(currentCollection).fireTableDataChanged();
    tables.get(currentCollection).changeSelection(row,0,false,false);
    tables.get(currentCollection).setRowSelectionInterval(row, row);
  }
 }
 private void importHTML(){
   String name = JOptionPane.showInputDialog("To use this feature, go to http://magiccards.info/search.html, select a few editions to search,\nsort by edition > collector's number, and view as list. View the page source,\n copy into a text file. Enter the name of your text file:");
   if(name==null){return;}
   int startid = Integer.parseInt(JOptionPane.showInputDialog("Enter the first card ID:"));
   try{
     FileInputStream fstream = new FileInputStream(name);
     DataInputStream in = new DataInputStream(fstream);
     BufferedReader br = new BufferedReader(new InputStreamReader(in));
     String strLine;
     boolean reading = false;
     int counter = 0;
     ArrayList<String []> mydata = new ArrayList<String[]>();
     int idx = 0;
     String [] line = new String[6];
     while ((strLine = br.readLine()) != null)   {
       if (strLine.contains("<tr class=\"even\">")){
         reading = true;
       }
       if (reading){
         if (counter==0 && strLine.contains("<td>")){ //Extract cardname from <td><a href="/gtc/en/1.html">cardname</a></td>
           String cardname = strLine.substring(0, strLine.length()-9);
           int index = cardname.lastIndexOf(">");
           cardname = cardname.substring(index+1);
           line[0]=cardname;
           counter++;
         }else if (counter>0&&counter<5){ //Extract type, cost, rarity, or artist from <td>extractThis</td>
           String item = strLine.substring(0, strLine.length()-5);
           int index = item.indexOf(">");
           item = item.substring(index+1);
           line[counter]=item;
           counter++;
         }else if (counter==5){ //Extract edition from <td><img src="http://magiccards.info/images/en.gif" alt="English" width="16" height="11" class="flag2"> Edition</td>
           String edition = strLine.substring(0, strLine.length()-5);
           int index = edition.lastIndexOf("> ");
           edition = edition.substring(index+2);
           line[counter]=edition;
           mydata.add(line);
           line = new String[6];
           counter=0;
           idx++;
         }else if (strLine.contains("</table>")){
           //We're done
           System.out.println("We stopped at "+idx);
           break;
         }
       }
     }
     //Close the input stream
     in.close();
     
     //Write the data to an importable text file.
     //The columns are: name, id, quantity, image, notes, type, cost, edition, rarity, artist
     //mydata's columns are: name, type, cost, rarity, artist, edition
     try {
       FileWriter outFile = new FileWriter(name);
       PrintWriter out = new PrintWriter(outFile);
       
       // Write text to file
       for (int i=0; i<mydata.size(); i++){
         if (i+1==mydata.size()){
           out.print(mydata.get(i)[0]+"`"+(startid+i)+"`0```"+mydata.get(i)[1]+"`"+mydata.get(i)[2]+"`"+mydata.get(i)[5]+"`"+mydata.get(i)[3]+"`"+mydata.get(i)[4]);
           break;
         }
         out.println(mydata.get(i)[0]+"`"+(startid+i)+"`0```"+mydata.get(i)[1]+"`"+mydata.get(i)[2]+"`"+mydata.get(i)[5]+"`"+mydata.get(i)[3]+"`"+mydata.get(i)[4]);
       }
       JOptionPane.showMessageDialog(this, name+" was successfully converted.", "Success", JOptionPane.INFORMATION_MESSAGE);
       out.close();
                   
     } catch (IOException e){
       e.printStackTrace();
     }
   }catch (Exception e){//Catch exception if any
     System.err.println("Error: " + e.getMessage());
   }
 }
}
