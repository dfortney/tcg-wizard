//Note: decks should not be shuffled within the MultiplayerUI when first placed. They will arrive already shuffled.

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MultiplayerUI extends JPanel implements ActionListener {
  
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private ChatClientProtocol clientPro;
//  private boolean fullScreen;
  private JTextField textField;
  private ChatTextArea textArea;
  
  @SuppressWarnings("unused")
private static SimpleServer server;
  @SuppressWarnings("unused")
private static SimpleClient client;
  private static int[] fieldSize = {5,5};
  private static String[] decks = {"Warrior Tribal", "Warrior Tribal"};
  private static int handCount, numberOfPlayers;
  
  public MultiplayerUI(int numberOfPlayers, int[] fieldSize, String[] decks, int handCount) {
    numberOfPlayers = MultiplayerUI.numberOfPlayers;
    fieldSize = MultiplayerUI.fieldSize;
    decks = MultiplayerUI.decks;
    handCount = MultiplayerUI.handCount;
  }
  
  public MultiplayerUI() {
    textField = new JTextField(20);
    textField.setEnabled(false);
    textField.addActionListener(this);
    
    textArea = new ChatTextArea(6, 20);
    textArea.setEditable(false);
    JScrollPane scrollPane = new JScrollPane(textArea);
    
    // Add Components to this panel.
    this.setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.weightx = 0;
    c.weighty = 0;
    
    c.gridx = 0;
    c.gridy = 5;
    add(scrollPane, c);
    
    c.gridx = 0;
    c.gridy = 6;
    add(textField, c);
  }
  
  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == textField) {
      String text = textField.getText();
      textField.setText("");
      clientPro.addLine(text);
      textField.selectAll();
      // Make sure the new text is visible, even if there
      // was a selection in the text area.
      textArea.setCaretPosition(textArea.getDocument().getLength());
    }
  }
  public static void main(String[] args) {
    numberOfPlayers = 2;
    handCount = 5;
    //SandboxUI ui = new SandboxUI(numberOfPlayers, fieldSize, decks, handCount);
    
    JFrame frame = new JFrame("ChatBox");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    // Add contents to the window.
    frame.add(new MultiplayerUI());
    
    // Display the window.
    frame.pack();
    frame.setSize(320, 280);
    frame.setVisible(true);
  }
}