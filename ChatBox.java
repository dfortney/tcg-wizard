import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ChatBox extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField serverPortField;
	private JButton serverPortButton;

	private JTextField clientPortField;
	private JTextField clientHostField;
	private JButton clientButton;

	private JTextField usernameField;

	private JTextField textField;
	private ChatTextArea textArea;

	private ChatClientProtocol clientPro;
	private SimpleServer server;
	@SuppressWarnings("unused")
	private SimpleClient client;

	public static void main(String[] args) {
		JFrame frame = new JFrame("ChatBox");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Add contents to the window.
		frame.add(new ChatBox());

		// Display the window.
		frame.pack();
		frame.setSize(320, 280);
		frame.setVisible(true);
	}

	public ChatBox(int port, String targetIP, int targetPort, String username){
		textField = new JTextField(20);
		textField.setEnabled(false);
		textField.addActionListener(this);

		textArea = new ChatTextArea(6, 20);
		textArea.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(textArea);

		// Add Components to this panel.
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 0;
		c.weighty = 0;

		c.gridx = 0;
		c.gridy = 6;
		add(scrollPane, c);

		c.gridx = 0;
		c.gridy = 7;
		add(textField, c);
		
		ChatServerProtocol s = new ChatServerProtocol(textArea);
		server = new SimpleServer(port, s);
		
		clientPro = new ChatClientProtocol(textArea, username);
		client = new SimpleClient(targetIP, targetPort, clientPro);
		textField.setEnabled(true);

	}
	
	public ChatBox() {
		textField = new JTextField(20);
		textField.setEnabled(false);
		textField.addActionListener(this);

		textArea = new ChatTextArea(6, 20);
		textArea.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(textArea);

		serverPortField = new JTextField(5);
		serverPortButton = new JButton("Create Server");
		serverPortButton.addActionListener(this);

		clientPortField = new JTextField(5);
		clientHostField = new JTextField("localhost", 10);
		clientButton = new JButton("Call Server");
		clientButton.addActionListener(this);

		usernameField = new JTextField(6);

		// Add Components to this panel.
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 0;
		c.weighty = 0;

		c.gridx = 0;
		c.gridy = 0;

		add(new JLabel("Server Port:"), c);

		c.gridx = 1;
		c.gridy = 0;
		add(serverPortField, c);

		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 1;
		add(serverPortButton, c);

		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 2;
		add(new JLabel("User Name:"), c);

		c.gridx = 1;
		c.gridy = 2;
		add(usernameField, c);

		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 3;
		add(new JLabel("Client Host:"), c);

		c.gridx = 1;
		c.gridy = 3;
		add(clientHostField, c);

		c.gridx = 0;
		c.gridy = 4;
		add(new JLabel("Client Port:"), c);

		c.gridx = 1;
		c.gridy = 4;
		add(clientPortField, c);

		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 5;
		add(clientButton, c);

		c.gridx = 0;
		c.gridy = 6;
		add(scrollPane, c);

		c.gridx = 0;
		c.gridy = 7;
		add(textField, c);

		// c.gridwidth = 1;
		// c.gridx = 0;
		// c.gridy = 7;
		// add(usernameField, c);
		//
		// c.gridwidth = 2;
		// c.gridx = 1;
		// c.gridy = 7;
		// add(usernameButton, c);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == textField) {
			String text = textField.getText();
			textField.setText("");
			clientPro.addLine(text);
			textField.selectAll();

			// Make sure the new text is visible, even if there
			// was a selection in the text area.
			textArea.setCaretPosition(textArea.getDocument().getLength());
		} else if (e.getSource() == serverPortButton) {
			ChatServerProtocol s = new ChatServerProtocol(textArea);

			String text = serverPortField.getText();
			try {
				int port = Integer.parseInt(text);

				server = new SimpleServer(port, s);
				JOptionPane.showMessageDialog(this, "Created the Server.");
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		} else if (e.getSource() == clientButton) {
			clientPro = new ChatClientProtocol(textArea, usernameField.getText());
			try {
				int port = Integer.parseInt(clientPortField.getText());
				String host = clientHostField.getText();
				client = new SimpleClient(host, port, clientPro);
				textField.setEnabled(true);
				JOptionPane.showMessageDialog(this, "Connected to server.");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public void close() {
		try {
			server.close();
			clientPro.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
