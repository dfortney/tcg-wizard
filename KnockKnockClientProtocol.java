import java.io.*;

public class KnockKnockClientProtocol extends Protocol {
 private BufferedReader userIn;

 public KnockKnockClientProtocol(InputStream userInput) {
  super("Bye.");
  userIn = new BufferedReader(new InputStreamReader(userInput));
 }

 public String processInput(String theInput) {
  
  System.out.println("Server: " + theInput);

  String fromUser = "";

  try {
   fromUser = userIn.readLine();
  } catch (IOException e) {
   e.printStackTrace();
  }

  if (fromUser != null) {
   System.out.println("Client: " + fromUser);
  }
  
  return fromUser;
 }
 
 public void close(){
  try {
   userIn.close();
  } catch (IOException e) {
   e.printStackTrace();
  }
 }
 
 public Protocol clone(){
  return this;
 }
}
