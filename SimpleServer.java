import java.io.*;
import java.net.*;
import java.util.ArrayList;

import javax.swing.JOptionPane;

/**
 * The SimpleServer is a basic implementation of server functionality. It
 * handles receiving user responses, and uses its protocol to interpret the data
 * and respond. An instance of the SimpleServer class can be used with a custom
 * Protocol to handle nearly any server needs.
 * 
 * @author ktheller
 * 
 */
public class SimpleServer extends Thread {
	private Boolean listening = true;
	private int port;
	private Boolean threads;
	private Protocol p;
	private ArrayList<SimpleServerThread> threadList;
	PrintWriter out = null;

	/**
	 * Shows an example use of the SimpleServer with the KnockKnockProtocol
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Going to start server");
		KnockKnockProtocol test = new KnockKnockProtocol();
		@SuppressWarnings("unused")
		SimpleServer s = new SimpleServer(4445, test);
		System.out.println("Server is Running");
	}

	/**
	 * SimpleServer's constructor; it sets local variables, then starts the
	 * server thread. It runs as a thread to avoid blocking the thread that
	 * creates it
	 * 
	 * @param port
	 *            The port for the server to listen on
	 * @param p
	 *            an instance of the protocol the server will use to interpret
	 *            client input
	 */
	public SimpleServer(int port, Protocol p) {
		this.threadList = new ArrayList<SimpleServerThread>();
		this.threads = true;
		this.port = port;
		this.p = p;
		this.start();
	}

	/**
	 * The thread run method; called automatically when the thread is running
	 */
	public void run() {
		if (port < 0 || port > 65535) {
			port = 4444;
		}
		if (threads) {
			ServerSocket serverSocket = null;
			boolean listening = true;

			try {
				serverSocket = new ServerSocket(port);
			} catch (IOException e) {
				System.err.println("Could not listen on port: " + port);
				JOptionPane.showMessageDialog(null, "Sorry, but that port is already taken.");
				return;
			}

			while (listening) {
				try {
					SimpleServerThread temp = new SimpleServerThread(serverSocket.accept(), p.clone());
					threadList.add(temp);
					temp.start();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			
			try {
				serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			ServerSocket serverSocket = null;

			try {
				serverSocket = new ServerSocket(port);
			} catch (IOException e) {
				System.err.println("Could not listen on port: " + port);
				System.exit(-1);
			}

			while (listening) {
				try {
					Socket socket = serverSocket.accept();
					out = new PrintWriter(socket.getOutputStream(),
							true);
					BufferedReader in = new BufferedReader(
							new InputStreamReader(socket.getInputStream()));

					String inputLine, outputLine;
					outputLine = p.processInput(null);
					out.println(outputLine);

					while ((inputLine = in.readLine()) != null) {
						outputLine = p.processInput(inputLine);
						 out.println(outputLine);
						if (outputLine.equals(p.getTerminator()))
							break;
					}
					out.close();
					in.close();
					socket.close();

				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			p.close();
			try {
				serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * closes the server
	 */
	public void close() {
		if(out!=null){
			out.println(p.getTerminator());
		}
		listening = false;
		for(SimpleServerThread temp : threadList){
			temp.close();
		}
	}
}