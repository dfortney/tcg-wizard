import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.swing.JTextArea;

/**
 * This class is the protocol that will control the server of a host user. It
 * receives contact from other player clients, establishes a connections, and
 * moderates the game, once it begins.
 * 
 * Messages between server and client are encoded with URL encoding (so special
 * characters are sent as %20, etc) excepting the # used to denote the beginning
 * of a command (this is used to differentiate the language the server and
 * client talk with from other text sent
 * 
 * The process for a connection is as follows (messages follow an S: for server
 * of C: for client):
 * 
 * (client connects)
 * - S: #GREETINGS<name of server user>.
 * - C: #NAME<name of client user>
 * - (server waits until user starts the game)
 * - S: #BEGIN
 * - (server state changes to GAME from GREETINGS)
 * - (client sends either a chat message or a game event):
 * 
 * - Chat:
 * - C: #CHAT<text message that was sent>
 * - S: #THANKS
 * - (server prints chat to server player's chat box, then puts the chat message
 * on the event queue. The event queue will be read by a "client" server thread)
 * 
 * - Event:
 * - C: #EVENT<description of event to be parsed>
 * - S: #THANKS
 * - (server will then enact the event and put the event message on the event
 * queue to be sent to other users by a "client" server thread)
 * 
 * 
 * - Alternately, the client can respond to the first #GREETINGS with @NAME<the
 * name> which tells the host server to act like a "client" and send new entries
 * in the EventQueue to the connected user. In that case the dialog looks like:
 * - S: #GREETINGS<name of server user>.
 * - C: @NAME<name of client user>
 * 
 * - Chat:
 * - S: #CHAT<text message that was sent>
 * - C: @THANKS
 * 
 * - Event:
 * - S: #EVENT<description of event to be parsed>
 * - C: @THANKS
 * 
 * @author ktheller
 * 
 */
public class ConnectionHostServerProtocol extends Protocol {
 private String username;
 private String hostname;
 private JTextArea tArea;

 private EventQueue Q;
 private WaitLock waitLock;
 private int pos = 0;

 private final int GREETINGS = 0;
 private final int GAME = 1;
 private int state = GREETINGS;

 private final int SERVER = 0;
 private final int CLIENT = 1;
 private int TYPE;

 /**
  * processInput handles the actions of a server after receiving data from a
  * client.
  * 
  * @param s
  *            One line of data from the client
  * @return The string to send back to the client.
  */
 public String processInput(String s) {
  System.out.println("Got: " + s);
  if (s == null) {
   String name = hostname;

   try {
    name = URLEncoder.encode(name, "UTF-8");
   } catch (UnsupportedEncodingException e) {
    e.printStackTrace();
   }

   return "#GREETINGS" + name;
  }

  if (s.contains(getTerminator())) {
   return getTerminator();
  }
  if (state == GREETINGS) {
   if (s.contains("#NAME")) {//Player Client
    s = s.replace("#NAME", "");

    try {
     s = URLDecoder.decode(s, "UTF-8");
    } catch (UnsupportedEncodingException e) {
     e.printStackTrace();
    }

    username = s;
    tArea.append(s + "\n");

    TYPE = SERVER;
    state = GAME;

    // Wait for start game event...
    waitLock.waitOnLock();
    
    
    
    return "#BEGIN";
    
   }
   if (s.contains("@GREETINGS")) {//Player Server
    s = s.replace("@GREETINGS", "");

    try {
     s = URLDecoder.decode(s, "UTF-8");
    } catch (UnsupportedEncodingException e) {
     e.printStackTrace();
    }

    username = s;
    // tArea.append("Started\n");

    TYPE = CLIENT;
    state = GAME;

    // Wait for start game event...
    waitLock.waitOnLock();
    String [] clients = tArea.getText().trim().split("\n");
       String [] full = new String[clients.length+1];
       try {
     full[0] = URLEncoder.encode(hostname, "UTF-8");
        for (int i=0; i<clients.length; i++){
          full[i+1]=URLEncoder.encode(clients[i], "UTF-8");
        }
    } catch (UnsupportedEncodingException e) {
     e.printStackTrace();
    }
       
    
       String gridSize = "3x9";
       
       
    //Send game data to players
    return "#PDATA" + Core.join(";", (Object[])full) + "#GRID" + gridSize;
   }

  }
  if (state == GAME) {
   if (TYPE == SERVER) {//connected to player Client
    if (s.contains("#EVENT")) {
     int j = s.indexOf("#NOTES");
     String evNotes = "";
     if(j!= -1){
      s = s.replace("#NOTES", "");
      evNotes = s.substring(j);
      s = s.substring(0,j);
     }
     
     int i = s.indexOf("#EVENT");
     s = s.replace("#EVENT", "");
     String evText = s.substring(i);
     String evName = s.substring(0, i);

     try {
      evText = URLDecoder.decode(evText, "UTF-8");
      evName = URLDecoder.decode(evName, "UTF-8");
      evNotes = URLDecoder.decode(evNotes, "UTF-8");
     } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
     }

     if (evName.equals(username)) {
       try{
         Core.receiveBlock(evText);
       } catch(NullPointerException e1){e1.printStackTrace();}
       catch(IndexOutOfBoundsException e2){e2.printStackTrace();}
      Q.append(evText, username, FilteredChatEntry.LOG, evNotes);
      try{
       Core.getOnlineGame().getTextArea().append(evNotes, evName, FilteredChatEntry.LOG);
      } catch (NullPointerException e){
       e.printStackTrace();
      }
     }
     
     
     
     return "#THANKS";
    } else if (s.contains("#CHAT")){
     int j = s.indexOf("#NOTES");
     if(j!= -1){
      s = s.replace("#NOTES", "");
      s = s.substring(0,j);
     }
     int i = s.indexOf("#CHAT");
     s = s.replace("#CHAT", "");
     String evText = s.substring(i);
     String evName = s.substring(0, i);

     try {
      evText = URLDecoder.decode(evText, "UTF-8");
      evName = URLDecoder.decode(evName, "UTF-8");
     } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
     }
     
     if (evName.equals(username)) {
      Q.append(evText, username, FilteredChatEntry.CHAT);
     }
     
     try{
      Core.getOnlineGame().getTextArea().append(evText, evName, FilteredChatEntry.CHAT);
     } catch(NullPointerException e){
      e.printStackTrace();
     }
      
      
     return "#THANKS";
    }
    
    
   } else if (TYPE == CLIENT) {//connected to player Server
    Event newest = Q.get(pos);
    pos++;
    
    //System.err.println("Looking at " + username + " and " + newest.getUser());
    if(newest.getUser().equals(username)){
     return "#THISISYOURS";
    }
    
    String fromUser = newest.toString();
    return fromUser;
   }
  }
  return "#SORRY";
 }

 public ConnectionHostServerProtocol(JTextArea cta, String name,
   EventQueue q, WaitLock waitLock) {
  super("#END");
  tArea = cta;
  hostname = name;
  Q = q;
  this.waitLock = waitLock;
 }

 /**
  * The close function is called after the client of server finishes using
  * the protocol. Can be used to clean up resources
  */
 public void close() {
 }

 public Protocol clone() {
  return new ConnectionHostServerProtocol(tArea, hostname, Q, waitLock);
 }
}
