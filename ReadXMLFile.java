 
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import java.io.File;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
 
public class ReadXMLFile {
 
  public static void main(String argv[]) 
  {
    try 
    {
 
      File fXmlFile = new File("CardList3.xml");
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(fXmlFile);

      Class.forName("org.sqlite.JDBC");
      Connection connection = DriverManager.getConnection("jdbc:sqlite:allcards.db");
      Statement statement = connection.createStatement();
      statement.executeUpdate("create table if not exists magic (number integer not null primary key, name string, edition string, color string, rarity string, type string)");
      
      //optional, but recommended
      //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
      doc.getDocumentElement().normalize();
      
      System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
      
      NodeList nList = doc.getElementsByTagName("Data");
      System.out.println(nList.getLength());
      
      System.out.println("----------------------------");
      String edition = "";
      String name = "";
      String color = "";
      String rarity = "";
      String type = "";
      int i = 0;
      for (int temp = 0; temp < nList.getLength(); temp++) 
      {
        
        Node nNode = nList.item(temp);
        if((temp % 5) == 0)
        {
          //System.out.println("\nCurrent Element :" + nNode.getTextContent());
          edition = nNode.getTextContent();
        }
        if((temp % 5) == 1)
        {
          //System.out.println("\nCurrent Element :" + nNode.getTextContent());
          name = nNode.getTextContent();
        }
        if((temp % 5) == 2)
        {
          color = nNode.getTextContent();
          //System.out.println("\nCurrent Element :" + nNode.getTextContent());
        }
        if((temp % 5) == 3)
        {
          //System.out.println("\nCurrent Element :" + nNode.getTextContent());
          rarity = nNode.getTextContent();
        }
        if((temp % 5) == 4)
        {
          //System.out.println("\nCurrent Element :" + nNode.getTextContent());
          type = nNode.getTextContent();
          //System.out.println(edition + " " + name + " " + color + " " + rarity + " " + type);
//          Datapacket data = new Datapacket(name, edition, color, rarity, type);
          statement = connection.createStatement();
          //System.out.println("insert into magic values('" + name + "', '" + edition + "', '" + color + "', '" + rarity + "', '" + type + "')");
          try{
          statement.executeUpdate("insert into magic values(" + i++ + ", '" + name + "', '" + edition + "', '" + color + "', '" + rarity + "', '" + type + "')");
          }
          catch(Exception e)
          {System.out.println("insert into magic values(" + i + ", '" + name + "', '" + edition + "', '" + color + "', '" + rarity + "', '" + type + "')");System.out.println(e.getMessage());}
          //PreparedStatement ps = connection.prepareStatement ("insert into magic values('?', '?', '?', '?', '?')");
          //System.out.println(ps.toString());
          //ps.setString(2,name);
          //ps.setString(3,edition);
          //ps.setString(4,color);
          //ps.setString(5,rarity);
          //ps.setString(6,type);
          //ps.executeUpdate();
          //PreparedStatement prep = connection.prepareStatement("insert into magic values('" + name + "', '" + edition + "', '" + color + "', '" + rarity + "', '" + type + "')");
          //prep.executeUpdate();
        }
        if(temp % 1000 == 0)
          System.out.println(temp);
        
        
        //System.out.println("\nCurrent Element :" + nNode.getTextContent());
        
        
      }
    }
    catch (Exception e) 
    {
      System.out.println(e.getMessage());
      e.printStackTrace();
    }
  }
  
}