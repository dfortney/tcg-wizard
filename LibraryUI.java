import java.awt.Dimension;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;

public class LibraryUI extends GenericUI{
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private JMenu tools;
  private JMenuItem mnuFind;
  private Object[][] libData;
  private JTable table;
  private JPanel subPanel;
  private JButton btnAdd, btnAddCustom;
  private JTextField txtName, txtImage;
  private JPanel container;
  private JScrollPane scroll;
  private DefaultTableModel model;
//  private String deck;
  private JLabel image;
  private int player;
  private SandboxUI sui;
  private Object emptyRow[]; //Initialized to an array of empty strings
  public LibraryUI(String deck, int player, SandboxUI sui){
    super("Card Library");
    this.player = player;
    this.sui = sui;
    container = new JPanel();
    subPanel = new JPanel();
    subPanel.setLayout(new GridBagLayout());
    table = new JTable();
    libData = new Object[32][2];
    for (int i=0; i<libData.length; i++){
      for (int j=0; j<libData[0].length; j++){
        libData[i][j] = "";
      }
    }
    model = new DefaultTableModel(libData, new String[]{"Name", "Image"}){
      /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
	@Override
      public boolean isCellEditable(int row, int column){
        return false; 
      }
      @Override
      public Object getValueAt(int row, int column){
    	  if(row >= libData.length || column >= libData[row].length){
    		  return null;
    	  }
        return libData[row][column];
      }
    };
    String collection = Core.getDeckCollection(deck);
    System.out.println(collection);
    Object temp[][] = Core.loadCollection(collection);
    int extra = 10;
    libData = new Object[temp.length+extra][2];
    libData[0]=new Object[]{"Angel","http://magiccards.info/extras/token/innistrad/angel.jpg"};
    libData[1]=new Object[]{"Beast","http://magiccards.info/extras/token/magic-2013/beast.jpg"};  
    libData[2]=new Object[]{"Cat","http://magiccards.info/extras/token/magic-2013/cat.jpg"};
    libData[3]=new Object[]{"Goblin","http://magiccards.info/extras/token/return-to-ravnica/goblin.jpg"};
    libData[4]=new Object[]{"Knight","http://magiccards.info/extras/token/league/knight.jpg"};
    libData[5]=new Object[]{"Saproling","http://magiccards.info/extras/token/return-to-ravnica/saproling.jpg"};
    libData[6]=new Object[]{"Soldier","http://magiccards.info/extras/token/magic-2013/soldier.jpg"};
    libData[7]=new Object[]{"Spirit","http://magiccards.info/extras/token/gatecrash/spirit.jpg"};
    libData[8]=new Object[]{"Wolf","http://magiccards.info/extras/token/innistrad/wolf-2.jpg"};
    libData[9]=new Object[]{"Zombie","http://magiccards.info/extras/token/magic-2013/zombie.jpg"};
    for (int i=extra; i<libData.length; i++){
      libData[i][0]=temp[i-extra][0];
      libData[i][1]=temp[i-extra][3];
    }
    emptyRow = new Object[]{"",""};
    model.fireTableDataChanged();
    int diff = libData.length-32;
    if (diff>0){
      for (int i=0; i<diff; i++){
        model.addRow(emptyRow);
      }
    }
    table = new JTable(model);
    table.setDefaultRenderer( Object.class, new BorderLessTableCellRenderer() );
    ListSelectionListener lsListener = new ListSelectionListener(){
      public void valueChanged(ListSelectionEvent e) {
        int [] selectedRow = table.getSelectedRows();
        if (selectedRow.length==0) return;
        String filePath = (String)libData[table.convertRowIndexToModel(selectedRow[0])][1];
        Core.imgMaker(filePath, image);
      }
      
    };
    table.getSelectionModel().addListSelectionListener(lsListener);
    scroll = new JScrollPane(table);
    btnAdd = new JButton("Add From Table");
    btnAdd.addActionListener(this);
    btnAddCustom = new JButton("  Add Custom  ");
    btnAddCustom.addActionListener(this);
    txtName = new JTextField("\t\t");
    txtImage = new JTextField("\t\t");
    JLabel lblName = new JLabel("Name:");
    JLabel lblImage = new JLabel("Image:");
    image = new JLabel();
    Core.imgMaker("",image);
    container.add(scroll);
    GridBagConstraints c = new GridBagConstraints();
    c.gridx=0;
    c.gridy=0;
    c.gridheight=1;
    c.gridwidth=2;
    c.insets=new Insets(10,10,10,10);
    subPanel.add(image, c);
    c.gridy=1;
    subPanel.add(btnAdd, c);
    c.gridy=2;
    c.gridwidth=1;
    subPanel.add(lblName,c);
    c.gridx=1;
    subPanel.add(txtName,c);
    c.gridy=3;
    subPanel.add(txtImage,c);
    c.gridx=0;
    subPanel.add(lblImage,c);
    c.gridwidth=2;
    c.gridy=4;
    subPanel.add(btnAddCustom,c);
    container.add(subPanel);
    this.add(container);
    tools = new JMenu("Tools");
    mnuFind = new JMenuItem("Find");
    mnuFind.addActionListener(this);
    mnuFind.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, ActionEvent.CTRL_MASK));
    tools.add(mnuFind);
    bar.add(tools);
    this.pack();
    Dimension d0 = new Dimension(txtImage.getSize().width,txtImage.getSize().height);    
    txtImage.setMinimumSize(d0);
    txtImage.setMaximumSize(d0);
    txtImage.setPreferredSize(d0);
    txtImage.setSize(d0);
    txtName.setMinimumSize(d0);
    txtName.setPreferredSize(d0);
    txtName.setMaximumSize(d0);
    txtName.setSize(d0);
    txtName.setText("");
    txtImage.setText("");
  }
  public void extendedActionPerformed(ActionEvent e){
    if (e.getSource()==btnAdd){
      int [] selectedRow = table.getSelectedRows();
      if (selectedRow.length==0) return;
      String filePath = ((String)libData[table.convertRowIndexToModel(selectedRow[0])][1]).replaceAll(" ","_");
      String name = ((String)libData[table.convertRowIndexToModel(selectedRow[0])][0]).replaceAll(" ","_"); //Can't have spaces in the names of fetched cards
      FieldCard card = new FieldCard(name, filePath);
      sui.getPlayer(player).getHand().addCard(card);
      sui.getPlayer(player).getHand().getHandPanel().refresh();
      Core.printToLog(sui.getPlayer(player).getName()+" fetched "+name+" and added it to his hand.", "Fetch "+sui.getPlayer(player).getId()+" "+name+" "+filePath+";");
    }else if (e.getSource()==btnAddCustom){
      String filePath = ((String)txtImage.getText().replaceAll(" ","_")).trim();
      String name = ((String)txtName.getText().replaceAll(" ","_")).trim();
      FieldCard card = new FieldCard(name, filePath);
      sui.getPlayer(player).getHand().addCard(card);
      sui.getPlayer(player).getHand().getHandPanel().refresh();
      Core.printToLog(sui.getPlayer(player).getName()+" fetched "+name+" and added it to his hand.", "Fetch "+sui.getPlayer(player).getId()+" "+name+" "+filePath+";");
    }else if (e.getSource()==mnuFind){
      String str = JOptionPane.showInputDialog("Look for:");
      if(str==null){return;}
      boolean done = false;
      for (int i=0; i<libData.length; i++){
        for (int j=0; j<libData[i].length; j++){
          if (String.valueOf(libData[i][j]).contains(str)){
            table.changeSelection(i,j,false,false);
            table.setRowSelectionInterval(i, i);
            int cont = JOptionPane.showConfirmDialog((Component) null,"Keep looking?", "TCG Wizard", JOptionPane.YES_NO_OPTION);
            System.out.println(cont);
            if (cont!=0){
              done = true;
              break;
            }
          }
        }
        if (done) break;
      }       
      if (!done) JOptionPane.showMessageDialog(this, "That's all of them.", "Search Done", JOptionPane.INFORMATION_MESSAGE);
    }
  }
}