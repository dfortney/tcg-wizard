import java.io.*;
import java.net.*;

public class SimpleServerThread extends Thread {
	private Socket socket = null;
	private Protocol p = null;
	private Boolean listening = true;
	private PrintWriter out;

	/**
	 * The sole constructor of SimpleServerThread. Called by the server spawning
	 * it
	 * 
	 * @param socket
	 *            The socket this thread will listen on
	 * @param p
	 *            The protocol that will be used to interpret user inputs
	 */
	public SimpleServerThread(Socket socket, Protocol p) {
		super("SimpleServerThread");
		this.socket = socket;
		this.p = p;
	}

	/**
	 * The thread run method. Called automatically once started
	 */
	public void run() {

		try {
			out = new PrintWriter(socket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));

			String inputLine, outputLine;
			outputLine = p.processInput(null);
			out.println(outputLine);

			while ((inputLine = in.readLine()) != null && listening) {
				outputLine = p.processInput(inputLine);
				out.println(outputLine);
				if (outputLine.equals(p.getTerminator()))
					break;
			}
			out.close();
			in.close();
			socket.close();
			p.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * closes the thread neatly
	 */
	public void close(){
		out.println(p.getTerminator());
		listening = false;
		/*try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
	}
}