import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class SandSplash extends GenericUI {
 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private JComboBox numOfPlayers;
 private JTextField handSize, size;
 private Vector<JComboBox> deckChoices;
 private JButton confirm;
 private JLabel hand, number, field;
 private JLabel[] decks;
 private int players;

 SandSplash() {
  super("Sandbox Setup", false);
  players = 2;
  GridBagConstraints c = new GridBagConstraints();
  numOfPlayers = new JComboBox(new Object[] {1, 2, 3, 4, 5, 6, 7, 8 });
  numOfPlayers.setSelectedIndex(1);
  numOfPlayers.addActionListener(this);
  deckChoices = new Vector<JComboBox>();
  deckChoices.add(new JComboBox((Object[]) Core.getDeckNamesArray()));
  deckChoices.add(new JComboBox((Object[]) Core.getDeckNamesArray()));
  deckChoices.add(new JComboBox((Object[]) Core.getDeckNamesArray()));
  deckChoices.add(new JComboBox((Object[]) Core.getDeckNamesArray()));
  deckChoices.add(new JComboBox((Object[]) Core.getDeckNamesArray()));
  deckChoices.add(new JComboBox((Object[]) Core.getDeckNamesArray()));
  deckChoices.add(new JComboBox((Object[]) Core.getDeckNamesArray()));
  deckChoices.add(new JComboBox((Object[]) Core.getDeckNamesArray()));
  handSize = new JTextField("7");
  hand = new JLabel("Default Hand Size:");
  number = new JLabel("Number of Players");
  field = new JLabel("Default Field Size:");
  size = new JTextField("Number of Rows x Number of Columns");
  decks = new JLabel[8];
  decks[0] = new JLabel("Player 1's deck");
  decks[1] = new JLabel("Player 2's deck");
  decks[2] = new JLabel("Player 3's deck");
  decks[3] = new JLabel("Player 4's deck");
  decks[4] = new JLabel("Player 5's deck");
  decks[5] = new JLabel("Player 6's deck");
  decks[6] = new JLabel("Player 7's deck");
  decks[7] = new JLabel("Player 8's deck");
  confirm = new JButton("Confirm");
  confirm.addActionListener(this);
  c.anchor = GridBagConstraints.LINE_START;
  c.insets = new Insets(10, 10, 0, 0);
  add(number, c);
  c.gridy = 1;
  c.anchor = GridBagConstraints.CENTER;

  c.insets = new Insets(0, 0, 0, 0);
  add(numOfPlayers, c);
  c.gridx = 1;
  c.gridy = 0;
  c.insets = new Insets(10, 10, 0, 0);
  add(hand, c);

  c.gridx = 2;
  c.ipadx = 10;
  c.insets = new Insets(10, 5, 0, 0);
  add(handSize, c);

  c.ipadx = 0;
  c.gridx = 3;
  c.insets = new Insets(10, 10, 0, 0);
  add(field, c);

  c.gridx = 4;
  c.insets = new Insets(10, 5, 0, 10);
  add(size, c);

  c.gridy = 2;
  c.gridx = 0;
  c.insets = new Insets(10, 10, 0, 0);
  add(decks[0], c);
  c.gridx = 1;
  add(decks[1], c);
  c.gridy = 3;
  c.gridx = 0;
  c.insets = new Insets(10, 10, 10, 0);
  add(deckChoices.get(0), c);

  c.gridx = 1;
  add(deckChoices.get(1), c);
  c.gridx = 4;
  c.gridy = 2;
  c.gridheight = 2;
  c.insets = new Insets(0, 10, 10, 10);
  c.ipadx = 30;
  c.ipady = 40;
  add(confirm, c);
  Dimension d = size.getPreferredSize();
  size.setMinimumSize(d);
  size.setPreferredSize(d);
  size.setMaximumSize(d);
  size.setSize(d);
  size.setText("3x9");
  pack();
 }

 public void extendedActionPerformed(ActionEvent e) 
 {
  if (e.getSource() == numOfPlayers) 
  {
   if (players != (Integer) numOfPlayers.getSelectedItem()) 
   {
    int old = players;
    GridBagConstraints c = new GridBagConstraints();
    players = (Integer) numOfPlayers.getSelectedItem();
    for(int i = 1; i < old; i++)
    {
     this.remove(decks[i]);
     this.remove(deckChoices.get(i));
    }
    c.gridx = 1;
    c.gridy = 2;
    for(int i = 1; i < players; i++)
    {
     c.insets = new Insets(10,10,0,0);
     this.add(decks[i], c);
     c.gridy++;
     c.insets = new Insets(10,10,10,10);
     this.add(deckChoices.get(i), c);
     c.gridy--;
     c.gridx++;
     if (c.gridx%4 == 0)
     {
      c.gridx = 0; 
      c.gridy+=2;
     }  
    }
      repaint();
      pack();
    }
  } 
  
  // Confirm Button is pressed
  else if (e.getSource() == confirm) 
  {
   //get number of starting cards per hand
    int handCount;
    if (!Core.canOpenConnectionUI()){
      JOptionPane.showMessageDialog(this,"You're already in the process of starting/playing a game.");
      return;
    }
   if (handSize.getText().equals("") || handSize.getText() == null) {
    JOptionPane.showMessageDialog(this,
      "Invalid hand size selected", "Invalid Hand",
      JOptionPane.ERROR_MESSAGE);
    return;
   }
   try {
    handCount = Integer.parseInt(handSize.getText());
   } catch (Exception except) {
    JOptionPane.showMessageDialog(this,
      "Invalid value for hand size selected", "Invalid Hand",
      JOptionPane.ERROR_MESSAGE);
    return;
   }
   if (handCount < 0) {
    JOptionPane.showMessageDialog(this,
      "Negative Hands aren't allowed", "Invalid Hand",
      JOptionPane.ERROR_MESSAGE);
    return;
   }
   if (size.getText().equals("") || size.getText() == null) {
    JOptionPane.showMessageDialog(this,
      "Invalid hand size selected", "Invalid Hand",
      JOptionPane.ERROR_MESSAGE);
    return;
   }
   
   //get field size
   String[] space = size.getText().split("x");
   int fieldSize[] = new int[2];
   if (space.length != 2) {
    JOptionPane
      .showMessageDialog(
        this,
        "Invalid field size format: Field size should be given as Row Number x Column Number.\nExample: 2 x 7 generates a field with 2 rows and 7 columns",
        "Invalid Field", JOptionPane.ERROR_MESSAGE);
    return;
   }
   try {
    fieldSize[0] = Integer.parseInt(space[0]);
    fieldSize[1] = Integer.parseInt(space[1]);
   } catch (Exception except) {
    JOptionPane
      .showMessageDialog(
        this,
        "Invalid field size format: Field size should be given as Row Number x Column Number.\nExample: 2 x 7 generates a field with 2 rows and 7 columns",
        "Invalid Field", JOptionPane.ERROR_MESSAGE);
    return;
   }
   
   //get decks to be used
   String[] decks = new String[(Integer) this.numOfPlayers.getSelectedItem()];
   for(int i = 0; i < decks.length; i++)
    decks[i] = (String) deckChoices.get(i).getSelectedItem();
   
   //start up sandbox and close splash screen
   new SandboxUI((Integer) this.numOfPlayers.getSelectedItem(), fieldSize, decks, handCount);
   this.dispose();

  }
 }

 public static void main(String[] args) {
  @SuppressWarnings("unused")
  SandSplash useSplashAttack = new SandSplash();
 }
}
