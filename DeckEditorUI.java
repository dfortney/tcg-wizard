import java.awt.Dimension;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.imageio.*;

/*Sorting is currently disabled, due to the number of issues with it.
 *While a TableRowSorter is used, an IndexOutOfBoundsException is thrown whenever the JTable is expanded.
 *Also, sorting a column in descending order will cause empty rows to appear at the top*/

/*The data for the tables is stored in arrays collectionData[][] and deckData[][][].

 You can modify tblMain like this:
 collectionData.get(0).get(0)="New data";
 collectionData.get(0).get(1)="More new data";
 model.fireTableDataChanged();
 However, tblMain may be out of room. To display one more row in tblMain, call:
 model.addRow(emptyRow);
 To display one fewer row, do this:
 model.removeRow(0);
 To actually remove a row i, do this:
 collectionData.get(i).get(0)=""; //flags row i for removal
 refreshCollection();
 The number of rows displayed should never go below DFLT_ROWS. Otherwise, you should display exactly the number of rows that there are.

 You can modify the deck table at index i like this:
 deckData.get(i).get(1).get(3)="New data";
 deckData.get(i).get(1).get(4)="More new data";
 deckModels.get(i).fireTableDataChanged();
 To display one more row:
 deckModels.get(i).addRow(emptyRow);
 One fewer row:
 deckModels.get(i).removeRow(0);
 The number of rows displayed follows the same rule as above, but for each deck table individually.*/

public class DeckEditorUI extends GenericUI {
        /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private String sourceColl = ""; // The card pool source (either a collection
                                                                        // or deck name)
        private String loadSource = ""; // Where the card data is stored (always a
                                                                        // collection)
        final int MAX_TABS = 10; // The max # of deck tabs allowed
        int DFLT_COLUMNS = 1; // The default/minimum # of columns
        final int DFLT_ROWS = 16; // The default/minimum # of rows
        public JTabbedPane jtp; // Contains all deck tabs
        JPanel pnl, pnl1, pnl2; // Deck Editor UI contains pnl, which contains pnl1
                                                        // for the collection table and pnl2 for the deck
                                                        // tabs
        public JTable tblMain; // the table of collection data
        Vector<JPanel> tabs; // an array of deck tabs
        Vector<JScrollPane> sPanes; // an array of scroll panes to contain the deck tables
        Vector<JTable> deckTables; // an array of deck tables
        int tabCount = 0; // the # of tabs currently open
        JScrollPane spMain; // the scroll pane for the collection table
        Object zeroCollectionData[][]; // Stores cards that are being hidden due to
                                                                        // having a quantity of 0.
        int zeroIdx = 0; // The next available index in zeroCollectionData[][].
        Object collectionData[][]; // a 2D array of collection data elements indexed
                                                                // as .get(row).get(field)
        Vector<Object[][]> deckData; // a 3D array of deck data elements indexed as
                                                        // .get(tab).get(row).get(field)
        String fields[]; // contains all the field names for the relevant card type,
                                                // used as column headers for all tables
        Object emptyRow[]; // Initialized to an array of empty strings
        boolean modified[]; // Initialized to false, set to true when the
                                                // corresponding deck (1) is set to virtual or
                                                // non-virtual, (2) has its name changed, or (3) has a
                                                // card added to or removed from it.
        boolean isVirtual[]; // Indicates for each deck whether it is virtual
        boolean virtualDeckExists = false; // True if at least one virtual deck
                                                                                // exists
        boolean handledAsVirtual = true; // True if the collection table is
                                                                                // displaying items of 0 quantity
        boolean orderTabs = false;// True if sample hand need to be recreated when
                                                                // adding a Deck
        JPanel pnlImg; // A panel containing the two card images
        JLabel cardTop, cardBot; // The top and bottom card, respectively
        Dimension dim; // the size of each table
        DefaultTableModel model; // The model for the collection table
        Vector<DefaultTableModel> deckModels; // An array of models, one for each deck
                                                                        // table
        JPopupMenu tabPaneMenu; // The context menu which appears when a deck tab is
                                                        // right-clicked
        JMenuItem toggleVirtual, subDecks, loadSub,renameDeck, newDeck, closeDeck,
                        loadDeck, saveDeck, commentNote, tool; // All additional menu items
                                                                                                        // which are used
        int index;
        Integer val = 0;
        // Integer tabVal = 0;
        JScrollPane txtPane;
        JTextArea txtArea;
        JLabel labelNote;
        Vector<String> noteS;
        Object[][] tHold;

        // Creates a new DeckEditorUI, preinitialized with a hardcoded collection
        // and an empty deck.
        public DeckEditorUI() {
                this("Deck 1", null, null, null);
        }

        // Creates a new DeckEditorUI, as below, but with the first deck empty.
        public DeckEditorUI(String deckName, String collection, String fields[],
                        Object argCollectionData[][]) {
                this(deckName, collection, fields, argCollectionData, null);
        }

        // Creates a new DeckEditorUI, as below, but non-virtual by default.
        public DeckEditorUI(String deckName, String collection, String argFields[],
                        Object argCollectionData[][], Object argDeckData[][]) {
                this(deckName, collection, argFields, argCollectionData, argDeckData,
                                false);
        }

        public DeckEditorUI(String deckName, String collection, String argFields[],
                        Object argCollectionData[][], Object argDeckData[][],
                        boolean virtual) {
                this(deckName, collection, collection, argFields, argCollectionData,
                                argDeckData, virtual);
        }

        /*
         * Creates a new DeckEditorUI with name deckName; column headers as
         * specified in argFields; collection data specified in argCollectionData,
         * and the data for the first deck specified in argDeckData; virtual
         * indicates whether the deck is to be created virtual
         */
        public DeckEditorUI(String deckName, String collection, String loadSource,
                        String argFields[], Object argCollectionData[][],
                        Object argDeckData[][], boolean virtual) {
                super("Deck Editor");
                this.sourceColl = collection;
                this.loadSource = loadSource;
                // If unselected, set argFields[] and argCollectionData[] to predefined
                // sample values.
                if (argFields == null) {
                        String fields[] = { "Card ID", "Name", "Quantity", "Color", "Type",
                                        "Power", "Toughness", "Rarity", "Notes", "Image" };
                        argFields = fields;
                }
                DFLT_COLUMNS = argFields.length;
                if (argCollectionData == null) {
                        String data2[][] = new String[90][10];
                        String temp1[] = { "1", "Act of Treason", "3", "Red", "Sorcery",
                                        "0", "0", "Common", "", "ActOfTreason.jpg" };
                        data2[0] = temp1;
                        String temp2[] = { "2", "Adaptive Snapjaw", "5", "Green",
                                        "Creature", "6", "2", "Common", "", "AdaptiveSnapjaw.jpg" };
                        data2[1] = temp2;
                        String temp3[] = { "3", "Aerial Maneuver", "2", "White", "Instant",
                                        "0", "0", "Common", "", "AerialManeuver.jpg" };
                        data2[2] = temp3;
                        String temp4[] = { "4", "AEtherize", "1", "Blue", "Instant", "0",
                                        "0", "Uncommon", "", "AEtherize.jpg" };
                        data2[3] = temp4;
                        String temp5[] = { "5", "Deathcult Rogue", "2", "Multicolored",
                                        "Creature", "2", "2", "Common", "", "DeathcultRogue.jpg" };
                        data2[4] = temp5;
                        String temp6[] = { "6", "Deathpact Angel", "1", "Multicolored",
                                        "Creature", "5", "5", "Mythic Rare", "",
                                        "DeathpactAngel.jpg" };
                        data2[5] = temp6;
                        String temp7[] = { "7", "Five-Alarm Fire", "2", "Red",
                                        "Enchantment", "0", "0", "Rare", "", "FiveAlarmFire.jpg" };
                        data2[6] = temp7;
                        String temp8[] = { "8", "Forced Adaptation", "4", "Green",
                                        "Enchantment", "0", "0", "Common", "",
                                        "ForcedAdaptation.jpg" };
                        data2[7] = temp8;
                        String temp9[] = { "9", "Nightveil Specter", "3", "Multicolored",
                                        "Creature", "0", "0", "Rare", "", "NightveilSpecter.jpg" };
                        data2[8] = temp9;
                        String temp10[] = { "10", "Rubblehulk", "2", "Multicolored",
                                        "Creature", "0", "0", "Rare", "", "Rubblehulk.jpg" };
                        data2[9] = temp10;
                        String temp11[] = { "11", "Ruination Wurm", "10", "Multicolored",
                                        "Creature", "7", "6", "Common", "", "RuinationWurm.jpg" };
                        data2[10] = temp11;
                        String temp12[] = { "12", "Skullcrack", "5", "Red", "Instant", "0",
                                        "0", "Uncommon", "", "Skullcrack.jpg" };
                        data2[11] = temp12;
                        String temp13[] = { "13", "Skyblinder Staff", "8", "Colorless",
                                        "Artifact", "0", "0", "Common", "", "SkyblinderStaff.jpg" };
                        data2[12] = temp13;
                        String temp14[] = { "14", "Treasury Thrull", "3", "Multicolored",
                                        "Creature", "4", "4", "Rare", "", "TreasuryThrull.jpg" };
                        data2[13] = temp14;
                        String temp15[] = { "15", "Truefire Paladin", "4", "Multicolored",
                                        "Creature", "2", "2", "Uncommon", "", "TruefirePaladin.jpg" };
                        data2[14] = temp15;
                        for (int i = 15; i < data2.length; i++) {
                                for (int j = 0; j < data2[0].length; j++) {
                                        data2[i][j] = data2[i % 15][j];
                                }
                                data2[i][0] = "" + (i + 1);
                                data2[i][2] = "" + (Integer.parseInt(data2[i][2]) * i) % 14;
                        }
                        argCollectionData = data2;
                }

                // Set all of the appropriate arrays.
                if (argDeckData == null) {
                        argDeckData = new Object[DFLT_ROWS][DFLT_COLUMNS];
                        for (int i = 0; i < argDeckData.length; i++) {
                                for (int j = 0; j < argDeckData[0].length; j++) {
                                        argDeckData[i][j] = "";
                                }
                        }
                }
                fields = argFields;
                deckData = new Vector<Object[][]>();
                deckData.add(argDeckData);
                emptyRow = new Object[fields.length];
                for (int i = 0; i < fields.length; i++)
                        emptyRow[i] = "";
                isVirtual = new boolean[MAX_TABS];
                setVirtual(0, virtual);
                if (!virtualDeckExists && virtual) {
                        virtualDeckExists = virtual;
                }
                modified = new boolean[MAX_TABS];
                for (int i = 0; i < MAX_TABS; i++) {
                        modified[i] = false;
                }

                // Add menu items.
                tabPaneMenu = new JPopupMenu();
                toggleVirtual = new JMenuItem("Make virtual");
                subDecks = new JMenuItem("Make a subdeck");
                loadSub = new JMenuItem("Load Subdeck");
                renameDeck = new JMenuItem("Rename");
                newDeck = new JMenuItem("New Deck");
                closeDeck = new JMenuItem("Close Deck");
                loadDeck = new JMenuItem("Load Deck");
                saveDeck = new JMenuItem("Save Deck");
                tool = new JMenuItem("Sample Hand");
                commentNote = new JMenuItem("Notes");
                toggleVirtual.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I,
                                ActionEvent.CTRL_MASK));
                subDecks.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B,
                                ActionEvent.CTRL_MASK));
                loadSub.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK));
                renameDeck.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R,
                                ActionEvent.CTRL_MASK));
                newDeck.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
                                ActionEvent.CTRL_MASK));
                closeDeck.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,
                                ActionEvent.CTRL_MASK));
                loadDeck.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L,
                                ActionEvent.CTRL_MASK));
                saveDeck.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
                                ActionEvent.CTRL_MASK));
                tabPaneMenu.add(toggleVirtual);
                tabPaneMenu.add(subDecks);
                tabPaneMenu.add(loadSub);
                tabPaneMenu.add(renameDeck);
                tabPaneMenu.add(saveDeck);
                tabPaneMenu.add(closeDeck);
                tabPaneMenu.add(tool);
                tabPaneMenu.add(commentNote);
                this.file.add(new JSeparator());
                this.file.add(newDeck);
                this.file.add(loadDeck);
                this.file.add(exit);
                toggleVirtual.addActionListener(this);
                subDecks.addActionListener(this);
                loadSub.addActionListener(this);
                renameDeck.addActionListener(this);
                newDeck.addActionListener(this);
                closeDeck.addActionListener(this);
                loadDeck.addActionListener(this);
                saveDeck.addActionListener(this);
                commentNote.addActionListener(this);
                tool.addActionListener(this);

                // Initialize the other GUI elements.
                pnl1 = new JPanel();
                pnl2 = new JPanel();
                pnl = new JPanel();
                pnlImg = new JPanel();
                tabs = new Vector<JPanel>();
                deckModels = new Vector<DefaultTableModel>();
                sPanes = new Vector<JScrollPane>();
                deckTables = new Vector<JTable>();
                pnl1.setLayout(new GridLayout(1, 1));
                pnl2.setLayout(new GridLayout(1, 1));
                pnl.setLayout(new GridLayout(2, 1));
                pnlImg.setLayout(new GridLayout(2, 1));
                noteS = new Vector<String>();

                // Ensure that the collection table appears with empty cells if
                // collectionData is small.
                Object[] temp = new Object[DFLT_COLUMNS];
                collectionData = new Object[DFLT_ROWS][DFLT_COLUMNS];
                for (int i = 0; i < collectionData.length; i++) {
                        for (int j = 0; j < collectionData[0].length; j++) {
                                collectionData[i][j] = "";
                                if (i == 0)
                                        temp[j] = "";
                        }
                }

                // Determine dim, the size we want to set our tables to.
                model = new DefaultTableModel(collectionData, temp) {
                        /**
					 * 
					 */
					private static final long serialVersionUID = 1L;

						@Override
                        public boolean isCellEditable(int row, int column) {
                                return false;
                        }
                };
                tblMain = new JTable(model);
                dim = tblMain.getPreferredSize();

                // Store the passed collection data.
                collectionData = argCollectionData;

                // Add the collection table to the JFrame.
                Object[][] temp2 = padData(collectionData);
                if (temp2 != null)
                        collectionData = temp2;
                model = new DefaultTableModel(collectionData, fields) {
                        /**
					 * 
					 */
					private static final long serialVersionUID = 1L;

						@Override
                        public boolean isCellEditable(int row, int column) {
                                return false;
                        }

                        @Override
                        public Object getValueAt(int row, int column) {
                                return collectionData[row][column];
                        }
                };
                tblMain = new JTable(model);
                tblMain.setPreferredScrollableViewportSize(dim);
                tblMain.setDefaultRenderer(Object.class,
                                new BorderLessTableCellRenderer());
                tblMain.setDragEnabled(true);

                // Implement the card transfer handler.
                CardTransferHandler cth = new CardTransferHandler();
                cth.source = -1;
                cth.dui = this;
                tblMain.setTransferHandler(cth);

                tblMain.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

                // Add mouse listener, to allow movement by double-clicking.
                MouseListener tblMainListener = new MouseAdapter() {
                        public void mouseClicked(MouseEvent e) {

                                int tblRow = tblMain.rowAtPoint(e.getPoint());
//                                int tblCol = tblMain.columnAtPoint(e.getPoint());

                                int clicks = e.getClickCount();
                                if (clicks == 2) {
                                        moveCard(-1, jtp.getSelectedIndex(), tblRow, 1);
                                }

                                // System.out.println(clicks);

                        }

                        public void mouseEntered(MouseEvent e) {
                        }

                        public void mouseExited(MouseEvent e) {
                        }

                        public void mousePressed(MouseEvent e) {
                        }

                        public void mouseReleased(MouseEvent e) {
                        }
                };
                tblMain.addMouseListener(tblMainListener);

                // Add list selection listener, to allow an image update whenever the
                // row selection changes.
                ListSelectionListener lsListener = new ListSelectionListener() {
                        public void valueChanged(ListSelectionEvent e) {
                                int[] selectedRow = tblMain.getSelectedRows();
                                if (selectedRow.length == 0)
                                        return;
                                String filePath = (String) collectionData[tblMain
                                                .convertRowIndexToModel(selectedRow[0])][fields.length - 1];
                                setImage(cardTop, filePath);
                        }

                };
                tblMain.getSelectionModel().addListSelectionListener(lsListener);

                /* ***Temporarily disabled: sorting *** */

                TableRowSorter<TableModel> sort = new TableRowSorter<TableModel>(model);
                int types[] = null;
                System.out.println(loadSource);
                if (loadSource == null || loadSource != "")
                        types = Core.getFieldTypes((String) Core
                                        .getCollCardType(loadSource));
                if (types == null)
                        types = new int[] { 2, 1, 2, 1, 1, 2, 2, 1, 1, 1 };
                for (int i = 0; i < types.length; i++) {
                        // System.out.println("Type.get("+i+")="+types.get(i));
                        if (types[i] == 2)
                                sort.setComparator(i, new IntComparator());
                        else if (types[i] == 1 || types[i] == 3)
                                sort.setComparator(i, new StringComparator());
                        else if (types[i] == 4)
                                sort.setComparator(i, new FloatComparator());
                        else
                                sort.setComparator(i, new BoolComparator());
                }

                tblMain.setRowSorter(sort);
                /* ************************************ */

                spMain = new JScrollPane(tblMain);
                spMain.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
                spMain.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
                pnl1.add(spMain);
                pnl.add(pnl1);

                // Add the tabbed pane, with one deck tab already open.
                GridBagConstraints c = new GridBagConstraints();
                c.gridx = 0;
                c.gridy = 0;
                c.gridheight = 1;
                c.gridwidth = 1;
                c.fill = GridBagConstraints.BOTH;
                jtp = new JTabbedPane();

                // Add mouse listener, to ensure that the tab context menu always
                // displays the correct option: "Make virtual" or "Make non-virtual"
                MouseListener popupListener = new MouseAdapter() {
                        public void mouseClicked(MouseEvent e) {
                        }

                        public void mouseEntered(MouseEvent e) {
                        }

                        public void mouseExited(MouseEvent e) {
                        }

                        public void mousePressed(MouseEvent e) {
                                if (e.isPopupTrigger()) {
                                        int idx = jtp.getSelectedIndex();
                                        if (isVirtual(idx))
                                                toggleVirtual.setText("Make non-virtual");
                                        else
                                                toggleVirtual.setText("Make virtual");
                                        tabPaneMenu.show(jtp, (int) e.getX(), (int) e.getY());
                                }
                        }

                        public void mouseReleased(MouseEvent e) {
                                if (e.isPopupTrigger()) {
                                        int idx = jtp.getSelectedIndex();
                                        if (isVirtual(idx))
                                                toggleVirtual.setText("Make non-virtual");
                                        else
                                                toggleVirtual.setText("Make virtual");
                                        tabPaneMenu.show(jtp, (int) e.getX(), (int) e.getY());
                                }
                        }
                };
                jtp.addMouseListener(popupListener);

                openDeck(deckName, argDeckData);
                pnl2.add(jtp);
                pnl.add(pnl2);
                this.add(pnl, c);
                this.pack();

                // Add a default image.
                Image newImg = null;
                BufferedImage rawTop = null;
//                BufferedImage rawBot = null;
                try {
                        rawTop = ImageIO.read(new File("backofamagicthegatheringcard.jpg"));
//                        rawBot = rawTop;
                        ImageIcon icon = new ImageIcon(rawTop);
                        Image img = icon.getImage();
                        newImg = img.getScaledInstance(230, 310,
                                        java.awt.Image.SCALE_SMOOTH);
                } catch (IOException ex) {
                }
                ImageIcon newIcon = null;
                if (newImg != null) {
                        try {
                                newIcon = new ImageIcon(newImg);
                        } catch (Exception ex) {
                        }
                }
                cardTop = new JLabel(newIcon);
                cardTop.revalidate();
                cardBot = new JLabel(newIcon);
                cardBot.revalidate();
                pnlImg.add(cardTop);
                pnlImg.add(cardBot);
                c.gridx = 1;
                c.gridheight = 2;
                cth.jtp = jtp;
                this.add(pnlImg, c);
                this.pack();

                // refreshCollection();
                // model.fireTableDataChanged();

                // Remove all non-virtual quantities in decks (present or not) from the
                // card pool.
                Vector<String> possibleValues = Core.getDeckNames();
                if (possibleValues != null) {
                        int fdIdx = 0;
                        String temp8[] = new String[possibleValues.size()];
                        for (int i = 0; i < possibleValues.size(); i++) {
                                if (Core.getDeckCollection(possibleValues.get(i)).equals(
                                                sourceColl)) {
                                        temp8[fdIdx] = possibleValues.get(i);
                                        fdIdx++;
                                }
                        }
                        String finalDecks[] = new String[fdIdx];
                        fdIdx = 0;
                        for (int i = 0; i < finalDecks.length; i++) {
                                if (Core.isDeckVirtual(temp8[i]) == 0)
                                        finalDecks[fdIdx++] = temp8[i];
                        }
                        for (int i = 0; i < fdIdx; i++) {
                                // Iterate through all these decks, remove their quantities from
                                // the card pool.
                                Object[][] theData = Core.loadDeck(finalDecks[i], loadSource);
                                for (int j = 0; j < theData.length; j++) {
                                        String id = Integer.toString((Integer) theData[j][1]); // loadDeck
                                                                                                                                                        // returns
                                                                                                                                                        // the
                                                                                                                                                        // id
                                                                                                                                                        // in
                                                                                                                                                        // the
                                                                                                                                                        // index
                                                                                                                                                        // 1
                                                                                                                                                        // column
                                        int qty = (Integer) theData[j][2];
                                        for (int k = 0; k < collectionData.length; k++) {
                                                if (id.equals(collectionData[k][0])) {
                                                        moveCard(-1, 0, k, qty, 2);
                                                }
                                        }
                                }
                        }
                }
                refreshCollection();
                model.fireTableDataChanged();
        }

        // Resizes zeroCollectionData, doubling its size.
        private void resizeZeroCollectionData() {
                Object temp[][] = new Object[zeroCollectionData.length * 2][fields.length];
                for (int i = 0; i < zeroCollectionData.length; i++) {
                        for (int j = 0; j < zeroCollectionData[0].length; j++) {
                                // System.out.println("i "+i+", j "+j+", temp.length "+temp.length+", temp.get(0).length "+temp.get(0).length+", zeroCollectionData.length "+zeroCollectionData.length+", zeroCollectionData.get(0).length "+zeroCollectionData.get(0).length);
                                temp[i][j] = zeroCollectionData[i][j];
                        }
                }
                zeroCollectionData = temp;
        }

        // Removes all elements of quantity zero or empty ID fields from deck idx;
        // takes any necessary related actions.
        public void refreshDeck(int idx) {
                // The block of code below was added to keep the tables from shrinking
                // down to size zero once their scrollbars appeared.
                // It does permanently resize the DeckEditorUI when the scrollbar
                // appears, however.
                for (int i = 0; i < sPanes.size(); i++) {
                        if (sPanes.get(i) == null)
                                break;
                        if (sPanes.get(i).getMinimumSize().height > 0
                                        && sPanes.get(i).getSize().height > 0) {
                                sPanes.get(i).setMinimumSize(sPanes.get(i).getSize());
                        }
                }
                this.pack();

                modified[idx] = true; // Conservative approach: assume the deck's been
                                                                // modified.
                int removeRows = 0;
                int tmpIdx = 0;
                Object temp[][] = new Object[deckData.get(idx).length][deckData.get(idx)[0].length];
                for (int i = 0; i < deckData.get(idx).length; i++) {
                        if (deckData.get(idx)[i][0] != null && !deckData.get(idx)[i][0].equals("")) {
                                if (!deckData.get(idx)[i][2].equals("0")) {
                                        for (int j = 0; j < deckData.get(idx)[0].length; j++) {
                                                temp[tmpIdx][j] = deckData.get(idx)[i][j];
                                        }
                                        tmpIdx++;
                                } else {
                                        removeRows++;
                                }
                        } else {
                                if (deckData.get(idx)[i][1] != null
                                                && !deckData.get(idx)[i][1].equals(""))
                                        removeRows++;
                        }
                }
                for (int i = tmpIdx; i < deckData.get(idx).length; i++) {
                        for (int j = 0; j < fields.length; j++) {
                                temp[i][j] = "";
                        }
                }
                try{
                        deckData.remove(idx);
                }catch(Exception e){
                        
                }
                deckData.insertElementAt(temp, idx);
                deckModels.get(idx).fireTableDataChanged();

                // Visually clean up empty rows.
                for (int i = 0; i < removeRows; i++) {
                        deckModels.get(idx).removeRow(0);
                }
        }

        // Refreshes collectionData by changing it to include or not include
        // quantity 0 elements, based on whether or not a virtual deck exists.
        // Also compresses collectionData, removing any rows with empty fields at
        // index 0.
        public void refreshCollection() {
                // The block of code below was added to keep the tables from shrinking
                // down to size zero once their scrollbars appeared.
                // It does permanently resize the DeckEditorUI when the scrollbar
                // appears, however.
                for (int i = 0; i < sPanes.size(); i++) {
                        if (sPanes == null)
                                break;
                        if (sPanes.get(i) == null)
                                break;
                        if (sPanes.get(i).getSize().height > 0) {
                                sPanes.get(i).setMinimumSize(sPanes.get(i).getSize());
                        }
                }
                if (spMain != null && spMain.getMinimumSize().height > 0
                                && spMain.getSize().height > 0) {
                        spMain.setMinimumSize(spMain.getSize());
                }
                this.pack();

                int removeRows = 0;
                int addRows = 0;
                if (handledAsVirtual && !virtualDeckExists) {
                        // Remove all elements of quantity zero and store them in
                        // zeroCollectionData[][].
                        int tmpIdx = 0;
                        zeroIdx = 0;
                        Object temp[][] = new Object[collectionData.length][collectionData[0].length];
                        zeroCollectionData = new Object[collectionData.length][collectionData[0].length];
                        for (int i = 0; i < collectionData.length; i++) {
                                if (collectionData[i][0] != null
                                                && !collectionData[i][0].equals("")) {
                                        if (collectionData[i][2].equals("0")) {
                                                for (int j = 0; j < collectionData[0].length; j++) {
                                                        zeroCollectionData[zeroIdx][j] = collectionData[i][j];
                                                }
                                                zeroIdx++;
                                                if (zeroIdx == zeroCollectionData.length)
                                                        resizeZeroCollectionData();
                                                removeRows++;
                                        } else {
                                                for (int j = 0; j < collectionData[0].length; j++) {
                                                        temp[tmpIdx][j] = collectionData[i][j];
                                                }
                                                tmpIdx++;
                                        }
                                } else if (collectionData[i][1] != null
                                                && !collectionData[i][1].equals("")) {
                                        removeRows++;
                                }
                        }
                        for (; tmpIdx < collectionData.length; tmpIdx++) {
                                for (int j = 0; j < collectionData[0].length; j++) {
                                        temp[tmpIdx][j] = "";
                                }
                        }
                        handledAsVirtual = false;
                        collectionData = temp;
                        model.fireTableDataChanged();
                } else if (!handledAsVirtual && virtualDeckExists) {
                        // Merge zeroCollectionData[][] into collectionData[][]. Also
                        // recompresses collectionData, removing any empty elements in the
                        // middle.
                        Object temp[][] = new Object[collectionData.length * 2][collectionData[0].length];
                        int tmpIdx = 0;
                        for (int i = 0; i < zeroCollectionData.length; i++) {
                                if (zeroCollectionData[i][0] != null
                                                && !zeroCollectionData[i][0].equals("")) {
                                        for (int j = 0; j < zeroCollectionData[0].length; j++) {
                                                temp[tmpIdx][j] = zeroCollectionData[i][j];
                                        }
                                        tmpIdx++;
                                        addRows++;
                                }
                        }
                        for (int i = 0; i < collectionData.length; i++) {
                                if (collectionData[i][0] != null
                                                && !collectionData[i][0].equals("")) {
                                        for (int j = 0; j < collectionData[0].length; j++) {
                                                temp[tmpIdx][j] = collectionData[i][j];
                                        }
                                        tmpIdx++;
                                } else if (collectionData[i][1] != null
                                                && !collectionData[i][1].equals("")) {
                                        removeRows++;
                                }
                        }
                        for (; tmpIdx < collectionData.length; tmpIdx++) {
                                for (int j = 0; j < collectionData[0].length; j++) {
                                        temp[tmpIdx][j] = "";
                                }
                        }
                        handledAsVirtual = true;
                        collectionData = temp;
                        model.fireTableDataChanged();
                } else {
                        // Just recompress collectionData, removing any empty elements in
                        // the middle. Also, remove items of quantity 0 if no virtual deck
                        // exists.
                        if ((collectionData == null) || (collectionData[0] == null))
                                return;
                        Object temp[][] = new Object[collectionData.length][collectionData[0].length];
                        int tmpIdx = 0;
                        for (int i = 0; i < collectionData.length; i++) {
                                if (collectionData[i][0] != null
                                                && !collectionData[i][0].equals("")
                                                && (virtualDeckExists || (!collectionData[i][2]
                                                                .equals("0")))) {
                                        for (int j = 0; j < collectionData[0].length; j++) {
                                                temp[tmpIdx][j] = collectionData[i][j];
                                        }
                                        tmpIdx++;
                                } else {
                                        if (collectionData[i][0] != null
                                                        && collectionData[i][2].equals("0")) {
                                                for (int j = 0; j < collectionData[0].length; j++) {
                                                        zeroCollectionData[zeroIdx][j] = collectionData[i][j];
                                                }
                                                zeroIdx++;
                                                if (zeroIdx == zeroCollectionData.length)
                                                        resizeZeroCollectionData();
                                        }
                                        if (collectionData[i][1] != null
                                                        && !collectionData[i][1].equals("")) {
                                                removeRows++;
                                        }
                                }
                        }
                        for (; tmpIdx < collectionData.length; tmpIdx++) {
                                for (int j = 0; j < collectionData[0].length; j++) {
                                        temp[tmpIdx][j] = "";
                                }
                        }
                        collectionData = temp;
                        model.fireTableDataChanged();
                }

                // Visually resize the table to the correct number of rows.
                for (int i = 0; i < addRows; i++) {
                        model.addRow(emptyRow);
                }
                for (int i = 0; i < removeRows; i++) {
                        model.removeRow(0);
                }
        }

        // Opens an empty (new) deck tab of the specified name.
        public int openDeck(String name) {
                return openDeck(name, null);
        }

        public int openDeck(String name, Object argDeckData[][]) {
                return openDeck(name, argDeckData, false);
        }

        // Opens a new deck tab with the specified name and data. Returns 1 if tab
        // limit is reached or deck name already exists
        public int openDeck(String name, Object argDeckData[][], boolean virtual) {
                if (tabCount < MAX_TABS) {
//                        if (tabCount != 0) {
//                                if (jtp.getTitleAt(tabCount - 1).equals("Sample Hand")) {
//                                        orderTabs = true;
//                                        tHold = (deckData.get(tabCount - 1));
//                                        jtp.remove(tabCount - 1);
//                                        tabCount--;
//                                }
//                        }
                        tabs.add(new JPanel());
                        setVirtual(tabCount, virtual);
                        if (!virtualDeckExists && virtual) {
                                virtualDeckExists = virtual;
                                refreshCollection();
                                model.fireTableDataChanged();
                        }
                        // Set the corresponding deckData entry.
                        try{
                                deckData.remove(tabCount);
                        }
                        catch(Exception e){
                                
                        }
                        deckData.insertElementAt(argDeckData, tabCount);
                        if (deckData.get(tabCount) == null) {
                                try{
                                        deckData.remove(tabCount);
                                }
                                catch(Exception e){
                                        
                                }
                                deckData.insertElementAt(new Object[DFLT_ROWS][fields.length], tabCount);
                                for (int i = 0; i < deckData.get(tabCount).length; i++) {
                                        for (int j = 0; j < deckData.get(tabCount)[0].length; j++) {
                                                deckData.get(tabCount)[i][j] = "";
                                        }
                                }
                        }

                        // Create a new table with the specified data and add it to a new
                        // tab.
                        Object[][] temp = padData(deckData.get(tabCount));
                        final int current = tabCount;
                        if (temp != null){
                                try{
                                        deckData.remove(tabCount);
                                }
                                catch(Exception e){
                                        
                                }
                                deckData.insertElementAt(temp, tabCount);
                        }
                        DefaultTableModel temp2 = new DefaultTableModel(deckData.get(tabCount),
                                        fields) {
                                /**
											 * 
											 */
											private static final long serialVersionUID = 1L;

								@Override
                                public Object getValueAt(int row, int column) {
                                        if (row < deckData.get(current).length)
                                                return deckData.get(current)[row][column];
                                        else
                                                return "";
                                }

                                @Override
                                public boolean isCellEditable(int row, int column) {
                                        return false;
                                }
                        };
                        try{
                                deckModels.remove(tabCount);
                        }
                        catch(Exception e){
                                
                        }
                        deckModels.insertElementAt(temp2, tabCount);
                        try{
                                deckTables.remove(tabCount);
                        }
                        catch(Exception e){
                                
                        }
                        deckTables.insertElementAt(new JTable(deckModels.get(tabCount)), tabCount);
                        deckTables.get(tabCount).setDragEnabled(true);

                        // Implement card transfer handler
                        CardTransferHandler cth = new CardTransferHandler();
                        cth.source = tabCount;
                        cth.jtp = jtp;
                        cth.dui = this;
                        deckTables.get(tabCount).setTransferHandler(cth);

                        deckTables.get(tabCount).setPreferredScrollableViewportSize(dim);
                        deckTables.get(tabCount)
                                        .setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                        deckTables.get(tabCount).setDefaultRenderer(Object.class,
                                        new BorderLessTableCellRenderer());

                        // Add mouse listener to allow card movement by double-clicking.
                        MouseListener deckTableListener = new MouseAdapter() {
                                public void mouseClicked(MouseEvent e) {

                                        int deckRow = deckTables.get(jtp.getSelectedIndex())
                                                        .rowAtPoint(e.getPoint());

//                                        int deckCol = deckTables.get(jtp.getSelectedIndex())
//                                                        .columnAtPoint(e.getPoint());

                                        int clicks = e.getClickCount();
                                        if (clicks == 2) {
                                                moveCard(jtp.getSelectedIndex(), -1, deckRow, 1);
                                        }
                                }

                                public void mouseEntered(MouseEvent e) {
                                }

                                public void mouseExited(MouseEvent e) {
                                }

                                public void mousePressed(MouseEvent e) {
                                }

                                public void mouseReleased(MouseEvent e) {
                                }
                        };
                        deckTables.get(tabCount).addMouseListener(deckTableListener);

                        // Add list selection listener to update image when selected row
                        // changes.
                        ListSelectionListener lsListener = new ListSelectionListener() {
                                public void valueChanged(ListSelectionEvent e) {
                                        int idx = jtp.getSelectedIndex();
                                        int[] selectedRow = deckTables.get(idx).getSelectedRows();
                                        if (selectedRow.length == 0)
                                                return;
                                        String filePath = (String) deckData.get(idx)[selectedRow[0]][fields.length - 1];
                                        setImage(cardBot, filePath);
                                }
                        };
                        deckTables.get(tabCount).getSelectionModel().addListSelectionListener(
                                        lsListener);

                        /* *** Sorting temporarily disabled *** */
                        /*
                         * TableRowSorter sort = new TableRowSorter(model);
                         * deckTables.get(tabCount).setRowSorter(sort); int types[] =
                         * Core.getFieldTypes((String)Core.getCollCardType(oadSource)); for
                         * (int i=0; i<types.length; i++){
                         * //System.out.println("Type.get("+i+")="+types.get(i)); if (types.get(i)==2)
                         * sort.setComparator(i, new IntComparator()); else if
                         * (types.get(i)==1||types.get(i)==3) sort.setComparator(i, new
                         * StringComparator()); else if (types.get(i)==4) sort.setComparator(i,
                         * new FloatComparator()); else sort.setComparator(i, new
                         * BoolComparator()); }
                         */
                        /* ************************************ */
                        try{
                                sPanes.remove(tabCount);
                        }catch(Exception e){
                                sPanes.insertElementAt(new JScrollPane(deckTables.get(tabCount)), tabCount);
                        }
                        sPanes.get(tabCount)
                                        .setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
                        sPanes.get(tabCount)
                                        .setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

                        // Somehow these two lines get the JTable to automatically resize
                        // itself.
                        deckModels.get(tabCount).addRow(emptyRow);
                        deckModels.get(tabCount).removeRow(0);

                        tabs.get(tabCount).add(sPanes.get(tabCount));
                        jtp.addTab(name, tabs.get(tabCount));
                        this.pack();
                        deckTables.get(tabCount).setMinimumSize(deckTables.get(tabCount).getSize());
                        // System.out.println(deckTables.get(tabCount).getSize());
                        Dimension d = sPanes.get(tabCount).getSize();
                        // System.out.println(d);
                        // System.out.println(tblMain.getSize());
                        sPanes.get(tabCount).setMinimumSize(d);
                        tabCount++;
                        if (orderTabs == true) {
                                orderTabs = false;
                                openDeck("Sample Hand", tHold);
                        }
                        return 0; // success
                }
                return 1; // 1 indicates that tab limit is reached
        }

        // Expands the array data[][] to fill the minimum size as defined by
        // DFLT_ROWS and DFLT_COLUMNS. New spaces are filled with empty strings.
        private Object[][] padData(Object[][] data) {
                int prevRows = data.length;
                int prevCols = data[0].length;
                int newRows = DFLT_ROWS;
                int newCols = DFLT_COLUMNS;
                if (prevRows > DFLT_ROWS)
                        newRows = prevRows;
                if (prevCols > DFLT_COLUMNS)
                        newCols = prevCols;
                if (newRows != prevRows || newCols != prevCols) {
                        Object[][] temp = new Object[newRows][newCols];
                        for (int i = 0; i < newRows; i++) {
                                for (int j = 0; j < newCols; j++) {
                                        if (i < data.length && j < data[0].length) {
                                                temp[i][j] = data[i][j];
                                        } else {
                                                temp[i][j] = "";
                                        }
                                }
                        }
                        return temp;
                }
                return data;
        }

        @SuppressWarnings("unused")
		private void printData() {
                /*
                 * System.out.println("Collection"); for (int i=0;
                 * i<collectionData.length; i++){ for (int j=0; j<fields.length; j++){
                 * System.out.print(collectionData.get(i).get(j)); } System.out.println(""); }
                 */
                for (int h = 0; h < 1; h++) {
                        System.out.println("Deck " + h);
                        for (int i = 0; i < deckData.get(h).length; i++) {
                                for (int j = 0; j < fields.length; j++) {
                                        System.out.print(deckData.get(h)[i][j]);
                                }
                                System.out.println("");
                        }
                }
        }

        // If no action is specified, let moveCard determine an action value based
        // on the arguments passed.
        public void moveCard(int source, int dest, int sourceRow, int quantity) {
                moveCard(source, dest, sourceRow, quantity, -1);
        }

        // Moves the card at source to dest, where souce and dest specify the deck
        // table index (or -1 for collection table).
        // sourceRow is the index of the card to be moved, and quantity is the
        // quantity to be moved. action specifies what type
        // of movement is made: 0 for general movement (quantity is subtracted from
        // source and added to dest), 1 for copy
        // (only add to dest), 2 for remove (only remove from source), and -1 to let
        // moveCard choose from the above.
        public void moveCard(int source, int dest, int sourceRow, int quantity,
                        int action) {
                if ((quantity == 0) || (source == dest))
                        return;
                try {
                        jtp.getTitleAt(dest);
                } catch (Exception ex) {
                        if (dest != -1)
                                return;
                }
                if (dest != -1) {
                        if (jtp.getTitleAt(dest).equals("Sample Hand")) {
                                JOptionPane.showMessageDialog(this,
                                                "Cannot move cards to a sample hand",
                                                "Invalid card movement", JOptionPane.ERROR_MESSAGE);
                                return;
                        }
                }
                // Determine the value of action
                if (action == -1) {
                        if (this.Vvirtual(source) == this.Vvirtual(dest))
                                action = 0;
                        else if (this.Vvirtual(source) && !this.Vvirtual(dest))
                                action = 2;
                        else
                                action = 1;
                }

                int temp_quantity;
                int ammount = 0;
                // if you are going from collection table to a deck table or vise versa
                if (source == -1 || dest == -1) {

                        if (source == -1) {

                                if (quantity > Integer
                                                .parseInt((String) collectionData[sourceRow][2])
                                                && action != 1) {
                                        quantity = Integer
                                                        .parseInt((String) collectionData[sourceRow][2]);
                                }

                                // converting the value in the table to an int then decrementing
                                // it
                                temp_quantity = Integer
                                                .valueOf((String) collectionData[sourceRow][2]);
                                if (action != 1)
                                        temp_quantity = temp_quantity - quantity;
                                collectionData[sourceRow][2] = Integer.toString(temp_quantity);
                                model.fireTableDataChanged();

                                if (action != 2) {
                                        // getting how many elements are in the table
                                        while (ammount != deckData.get(dest).length
                                                        && !deckData.get(dest)[ammount][2].equals("")) {
                                                ammount++;
                                        }

                                        // resize the array if there is no more room
                                        if (deckData.get(dest).length <= ammount + 1) {
                                                int temp = deckData.get(dest).length;
                                                Object tempDeck[][] = new Object[temp * 2][fields.length];
                                                for (int i = 0; i < temp; i++) {
                                                        for (int j = 0; j < fields.length; j++) {
                                                                tempDeck[i][j] = deckData.get(dest)[i][j];
                                                        }
                                                }

                                                for (int i = temp; i < temp * 2; i++) {
                                                        for (int j = 0; j < fields.length; j++) {
                                                                tempDeck[i][j] = "";
                                                        }
                                                }
                                                try{
                                                        deckData.remove(dest);
                                                }catch(Exception e){
                                                        
                                                }
                                                
                                                deckData.insertElementAt(padData(tempDeck),dest);
                                        }
                                }
                        } else {

                                if (quantity > Integer
                                                .parseInt((String) deckData.get(source)[sourceRow][2])
                                                && action != 1)
                                        quantity = Integer
                                                        .parseInt((String) deckData.get(source)[sourceRow][2]);
                                // converting the value in the table to an int then decrementing
                                // it
                                temp_quantity = Integer
                                                .valueOf((String) deckData.get(source)[sourceRow][2]);
                                if (action != 1)
                                        temp_quantity = temp_quantity - quantity;
                                deckData.get(source)[sourceRow][2] = Integer
                                                .toString(temp_quantity);
                                if ((deckModels == null) || (deckModels.get(source) == null))
                                        return;
                                deckModels.get(source).fireTableDataChanged();

                                if (action != 2) {
                                        // getting how many elements are in the table
                                        while (ammount != collectionData.length
                                                        && collectionData[ammount][0] != null
                                                        && !collectionData[ammount][0].equals("")) {
                                                ammount++;
                                        }

                                        // resize the array if there is no more room
                                        if (collectionData.length <= ammount + 1) {

                                                Object tempDeck[][] = new Object[collectionData.length * 2][fields.length];
                                                for (int i = 0; i < collectionData.length; i++) {
                                                        for (int j = 0; j < fields.length; j++) {
                                                                tempDeck[i][j] = collectionData[i][j];
                                                        }
                                                }

                                                for (int i = collectionData.length; i < collectionData.length * 2; i++) {
                                                        for (int j = 0; j < fields.length; j++) {
                                                                tempDeck[i][j] = "";
                                                        }
                                                }

                                                collectionData = padData(tempDeck);
                                                model.fireTableDataChanged();
                                        }
                                }
                        }

                        if (dest == -1 && action != 2) {

                                // checking if the element that is being moved is in the table
                                int tableRow = searchTable(-1,
                                                deckData.get(source)[sourceRow][0].toString());

                                // if it isn't, add the new card to the end of the list
                                if (tableRow == -1) {
                                        for (int i = 0; i < fields.length; i++) {
                                                if (i != 2) {
                                                        collectionData[ammount][i] = deckData.get(source)[sourceRow][i];
                                                } else {
                                                        collectionData[ammount][i] = Integer
                                                                        .toString(quantity);
                                                }
                                        }
                                        model.fireTableDataChanged();
                                        if (ammount >= DFLT_ROWS)
                                                model.addRow(emptyRow);
                                } else {

                                        // incrementing the value if the card in already in the
                                        // table
                                        temp_quantity = Integer
                                                        .valueOf((String) collectionData[tableRow][2]);
                                        temp_quantity += quantity;
                                        collectionData[tableRow][2] = Integer
                                                        .toString(temp_quantity);
                                        model.fireTableDataChanged();
                                }
                        } else if (action != 2) {

                                // checking if the element that is being moved is in the table
                                int tableRow = searchTable(dest,
                                                collectionData[sourceRow][0].toString());

                                // if it isn't, add the new card to the end of the list
                                if (tableRow == -1) {
                                        for (int i = 0; i < fields.length; i++) {
                                                if (i != 2) {
                                                        deckData.get(dest)[ammount][i] = collectionData[sourceRow][i];
                                                } else {
                                                        deckData.get(dest)[ammount][i] = Integer
                                                                        .toString(quantity);
                                                }
                                        }
                                        deckModels.get(dest).fireTableDataChanged();
                                        if (ammount >= DFLT_ROWS)
                                                deckModels.get(dest).addRow(emptyRow);
                                } else {

                                        // incrementing the value if the card in already in the
                                        // table
                                        temp_quantity = Integer
                                                        .valueOf((String) deckData.get(dest)[tableRow][2]);
                                        temp_quantity += quantity;
                                        deckData.get(dest)[tableRow][2] = Integer
                                                        .toString(temp_quantity);
                                        deckModels.get(dest).fireTableDataChanged();
                                }
                        }
                } else { // This is for if we are going from a deck table to a deck
                                        // table.

                        if (quantity > Integer
                                        .parseInt((String) deckData.get(source)[sourceRow][2])
                                        && action != 1)
                                quantity = Integer
                                                .parseInt((String) deckData.get(source)[sourceRow][2]);

                        // decrementing the value from the source
                        temp_quantity = Integer
                                        .valueOf((String) deckData.get(source)[sourceRow][2]);
                        if (action != 1)
                                temp_quantity = temp_quantity - quantity;
                        deckData.get(source)[sourceRow][2] = Integer.toString(temp_quantity);
                        deckModels.get(source).fireTableDataChanged();

                        if (action != 2) {
                                // getting the how many elements are in the table
                                while (ammount != deckData.get(dest).length
                                                && !deckData.get(dest)[ammount][2].equals("")) {
                                        ammount++;
                                }

                                // resizing if need be
                                if (deckData.get(dest).length <= ammount + 1) {
                                        int temp = deckData.get(dest).length;
                                        Object tempDeck[][] = new Object[temp * 2][fields.length];
                                        for (int i = 0; i < temp; i++) {
                                                for (int j = 0; j < fields.length; j++) {
                                                        tempDeck[i][j] = deckData.get(dest)[i][j];
                                                }
                                        }

                                        for (int i = temp; i < temp * 2; i++) {
                                                for (int j = 0; j < fields.length; j++) {
                                                        tempDeck[i][j] = "";
                                                }
                                        }
                                        try{
                                                deckData.remove(dest);
                                        }catch(Exception e){
                                        }
                                        deckData.insertElementAt(padData(tempDeck), dest);
                                }

                                // checking if the card already exists in the table
                                int tableRow = searchTable(dest,
                                                deckData.get(source)[sourceRow][0].toString());

                                if (tableRow == -1) {

                                        // if it doesn't, copy the new card to the end of the dest
                                        // table
                                        for (int i = 0; i < fields.length; i++) {
                                                if (i != 2) {
                                                        deckData.get(dest)[ammount][i] = deckData.get(source)[sourceRow][i];
                                                } else {
                                                        deckData.get(dest)[ammount][i] = "" + quantity;
                                                }
                                        }
                                        deckModels.get(dest).fireTableDataChanged();
                                        if (ammount >= DFLT_ROWS)
                                                deckModels.get(dest).addRow(emptyRow);
                                } else {

                                        // if it does, increment the value at dest table
                                        temp_quantity = Integer
                                                        .valueOf((String) deckData.get(dest)[tableRow][2]);
                                        temp_quantity += quantity;
                                        deckData.get(dest)[tableRow][2] = Integer
                                                        .toString(temp_quantity);
                                        deckModels.get(dest).fireTableDataChanged();
                                }
                        }

                }

                // Refresh the source table.
                if (source != -1) {
                        refreshDeck(source);
                } else {
                        refreshCollection();
                }
                // If a copy or move action was taken and the dest is a deck, set as
                // modified.
                if (action < 2 && dest >= 0)
                        modified[dest] = true;
                // If a move or remove action was taken and the source is a deck, set as
                // modified.
                if (action != 1 && source >= 0)
                        modified[source] = true;

                // printData();
        }

        // Searches the deck table at index table for a row whose first element is
        // name. If table is -1, search the collection
        // table instead. Return the row number of the element if found, otherwise
        // return -1.
        public int searchTable(int table, String name) {

                int i = 0;

                if (table == -1) {
                        while (i < collectionData.length) {
                                if (collectionData[i][0] == null
                                                || collectionData[i][0].equals(name)
                                                || collectionData[i][0].equals(""))
                                        break;
                                i++;
                        }

                        if (collectionData[i][0] == null || collectionData[i][0].equals("")) {
                                return -1;
                        }
                        return i;
                } else {
                        while (i < deckData.get(table).length) {
                                if (deckData.get(table)[i][0].toString().equals(name)
                                                || deckData.get(table)[i][0].toString().equals(""))
                                        break;
                                i++;
                        }

                        if (deckData.get(table)[i][0].equals("")) {
                                return -1;
                        }

                        return i;
                }
        }

        public static void main(String[] args) {
                @SuppressWarnings("unused")
				DeckEditorUI gui = new DeckEditorUI();
                // new SandboxUI(gui.collectionData);
        }

        // Indicates whether the corresponding table is virtual.
        private boolean Vvirtual(int idx) {
                if (idx < 0)
                        return false;
                return isVirtual(idx);
        }

        // This function is called by CardTransferHandler to get the quantity to be
        // moved when a drag-and-drop takes place.
        public int getMoveQuantity(int source, int dest, int sourceQty) {
                int action = 0; // 0 for move, 1 for copy, 2 for remove.
                if (this.Vvirtual(source) == this.Vvirtual(dest))
                        action = 0;
                else if (this.Vvirtual(source) && !this.Vvirtual(dest))
                        action = 2;
                else
                        action = 1;
                System.out.println("action " + action);
                String temp;
                int qty = 0;
                if (action == 0) {
                        if (sourceQty > 1) {// If moving, prompt only if the source has 2 or
                                                                // more quantity.
                                temp = JOptionPane.showInputDialog(null,
                                                "Enter quantity to move:", "", 1);
                                if (temp != null) {
                                        try {
                                                qty = Integer.parseInt(temp);
                                        } catch (Exception e) {
                                                qty = 0;
                                        }
                                }
                        } else {
                                qty = sourceQty;
                        }
                } else if (action == 1) {
                        temp = JOptionPane.showInputDialog(null, "Enter quantity to copy:",
                                        "", 1);
                        if (temp != null) {
                                try {
                                        qty = Integer.parseInt(temp);
                                } catch (Exception e) {
                                        qty = 0;
                                }
                        }
                } else {
                        if (sourceQty > 1) { // Else, we're removing. Prompt only if source
                                                                        // has 2 or more quantity.
                                temp = JOptionPane.showInputDialog(null,
                                                "Enter quantity to remove:", "", 1);
                                if (temp != null) {
                                        try {
                                                qty = Integer.parseInt(temp);
                                        } catch (Exception e) {
                                                qty = 0;
                                        }
                                }
                        } else {
                                qty = sourceQty;
                        }
                }
                if (qty < 0)
                        qty = 0;
                return qty;
        }

        // Handles the actual setting.
        private void actuallySetVirtual(int idx, boolean value) {
                // System.out.println("actuallySetVirtual("+idx+", "+value+")");
                // System.out.print("isVirtual["+idx+") becomes ");
                isVirtual[idx] = value;
                // System.out.println(isVirtual[idx));
                try {
                        String deckName = jtp.getTitleAt(idx);
                        // System.out.println("Setting DB");
                        Core.setDeckVirtual(deckName, value);
                        // System.out.println("Got "+Core.isDeckVirtual(deckName));
                } catch (Exception ex) {
                        // System.out.println("DB set failed");
                }
        }

        // Sets isVirtual[idx)=value and handles all that involves. Returns false if
        // failed.
        private boolean setVirtual(int idx, boolean value) {
                if (isVirtual(idx) == value)
                        return false;
                if (!value) { // Then make non-virtual

                        // Check to see if there is enough inventory in the collection to
                        // allow the deck to become non-virtual.
                        for (int i = 0; i < deckData.get(idx).length; i++) {
                                if (deckData.get(idx)[i][2] == null || deckData.get(idx)[i][2] == "")
                                        break;
                                int sourceRow = searchTable(-1, (String) deckData.get(idx)[i][0]);
                                int qty = Integer
                                                .parseInt((String) collectionData[sourceRow][2]);
                                if (qty < Integer.parseInt((String) deckData.get(idx)[i][2])) {
                                        JFrame frame = new JFrame("Error");
                                        JOptionPane
                                                        .showMessageDialog(
                                                                        frame,
                                                                        "You don't have enough of "
                                                                                        + (String) deckData.get(idx)[i][1]
                                                                                        + " in your collection to make this deck non-virtual. Try removing this card from your deck before proceeding.");
                                        return false;
                                }
                        }

                        actuallySetVirtual(idx, false);
                        if (virtualDeckExists) {
                                boolean temp = false;
                                for (int i = 0; i < MAX_TABS; i++) {
                                        if (isVirtual(i) == true) {
                                                temp = true;
                                                break;
                                        }
                                }
                                virtualDeckExists = temp;

                                if (!virtualDeckExists) {
                                        refreshCollection();
                                }
                        }

                        // Remove the cards contained in deckData.get(idx) from the collection
                        for (int i = 0; i < deckData.get(idx).length; i++) {
                                if (deckData.get(idx)[i][2] == null || deckData.get(idx)[i][2] == "")
                                        break;
                                int sourceRow = searchTable(-1, (String) deckData.get(idx)[i][0]);
                                moveCard(-1, idx, sourceRow,
                                                Integer.parseInt((String) deckData.get(idx)[i][2]), 2);
                        }
                } else { // Make virtual
                        actuallySetVirtual(idx, true);
                        virtualDeckExists = true;

                        refreshCollection(); // In this case, must be called before moving
                                                                        // cards, or duplicate rows will result.
                        // Add the cards contained in deckData.get(idx) back to the collection
                        for (int i = 0; i < deckData.get(idx).length; i++) {
                                if (deckData.get(idx)[i][2] == null || deckData.get(idx)[i][2] == "")
                                        break;
                                moveCard(idx, -1, i,
                                                Integer.parseInt((String) deckData.get(idx)[i][2]), 1);
                        }
                }
                return true;
        }

        public static Object[][] format(Object[][] original) {
                if (original == null)
                        return null;
                Object[][] data = new Object[original.length][original[0].length];
                for (int i = 0; i < original.length; i++) {
                        data[i][1] = original[i][0];
                        data[i][0] = original[i][1];
                        data[i][2] = original[i][2];
                        data[i][original[i].length - 2] = original[i][4];
                        data[i][original[i].length - 1] = original[i][3];
                        for (int j = 3; j < original[i].length - 2; j++) {
                                data[i][j] = original[i][j + 2];
                        }
                }
                return data;
        }

        // Handle any DeckEditorUI-specific ActionEvents.
        public void extendedActionPerformed(ActionEvent e) {
                if (e.getSource() == toggleVirtual) {
                        int idx = jtp.getSelectedIndex();
                        modified[idx] = true;
                        if (isVirtual(idx) == true) {
                                setVirtual(idx, false);
                        } else {
                                setVirtual(idx, true);
                        }
                } else if (e.getSource() == subDecks) {
                        int idx = jtp.getSelectedIndex();
                        String name = jtp.getTitleAt(idx);
                        if (name.equals("Sample Hand")) {
                                JOptionPane.showMessageDialog(this,
                                                "Cannot Create a subdeck of a sample hand",
                                                "Invalid subdeck", JOptionPane.ERROR_MESSAGE);
                                return;
                        }
                        String subName = JOptionPane.showInputDialog(null,
                                        "Enter subdeck name:", "", 1);
                        if (subName == null || subName.equals(""))
                                return;
                        if (Core.deckExists(subName) || subName.equals("Sample Hand")) {
                                JOptionPane.showMessageDialog(this,
                                                "A deck with this name already exists!",
                                                "Deck Creation Error", JOptionPane.ERROR_MESSAGE);
                        }
                        // Recall: public DeckEditorUI(String deckName, String collection,
                        // String loadSource, String argFields[], Object
                        // argCollectionData[][], Object argDeckData[][], boolean virtual){
                        @SuppressWarnings("unused")
						DeckEditorUI subDeckUI = new DeckEditorUI(subName, name,
                                        loadSource, fields, deckData.get(idx), null, false);

                } 
                else if(e.getSource()==loadSub){
                        Vector<String> possibleValues = Core.getDeckNames();
                        if (possibleValues == null) {
                                JOptionPane.showMessageDialog(this,
                                                "Cannot load deck, there are no saved decks",
                                                "Deck Load Failure", JOptionPane.INFORMATION_MESSAGE);
                                return;
                        }
                        int fdIdx = 0;
                        String temp[] = new String[possibleValues.size()];
                        for (int i = 0; i < possibleValues.size(); i++) {
                                if (Core.getDeckCollection(possibleValues.get(i)).equals(
                                                jtp.getTitleAt(jtp.getSelectedIndex()))) {
                                        temp[fdIdx] = possibleValues.get(i);
                                        fdIdx++;
                                }
                        }
                        String finalDecks[] = new String[fdIdx];
                        for (int i = 0; i < fdIdx; i++) {
                                finalDecks[i] = temp[i];
                        }

                        if (finalDecks.length == 0) {
                                JOptionPane.showMessageDialog(this,
                                                "Cannot load deck, there are no saved decks",
                                                "Deck Load Failure", JOptionPane.INFORMATION_MESSAGE);
                                return;
                        }
                        String deckName = (String) JOptionPane.showInputDialog(null,
                                        "Choose which deck to load", "Input",
                                        JOptionPane.INFORMATION_MESSAGE, null, finalDecks,
                                        finalDecks[0]);
                        if (deckName == null)
                                return;
                        Object[][] data = format(Core.loadDeck(deckName, loadSource));
                        int virt = Core.isDeckVirtual(deckName);
                        boolean val = true;
                        if (virt == 0)
                                val = false;
                        @SuppressWarnings("unused")
						DeckEditorUI subDeckUI = new DeckEditorUI(deckName, jtp.getTitleAt(jtp.getSelectedIndex()),
                                        loadSource, fields, deckData.get(jtp.getSelectedIndex()), data, val);
                }
                else if (e.getSource() == renameDeck) {
                        int idx = jtp.getSelectedIndex();
                        if (jtp.getTitleAt(idx).equals("Sample Hand")) {
                                JOptionPane.showMessageDialog(this,
                                                "The sample hands tab name cannot be renamed",
                                                "Rename Not Allowed", JOptionPane.ERROR_MESSAGE);
                                return;
                        }
                        String newName = JOptionPane.showInputDialog(null,
                                        "Enter new deck name:", "", 1);
                        if (newName != null) {
                                if (Core.deckExists(newName) || newName.equals("Sample Hand")) {
                                        JOptionPane.showMessageDialog(this,
                                                        "A deck with this name already exists!",
                                                        "Deck Creation Error", JOptionPane.ERROR_MESSAGE);
                                        return;
                                }
                                Core.renameDeck(jtp.getTitleAt(idx), newName);
                                jtp.setTitleAt(idx, newName);
                        }
                } else if (e.getSource() == newDeck) {
                        String name = JOptionPane.showInputDialog(null, "Enter deck name:",
                                        "", 1);
                        if (name == null || name.equals(""))
                                return;
                        if (Core.deckExists(name) || name.equals("Sample Hand")) {
                                JOptionPane.showMessageDialog(this,
                                                "A deck with this name already exists!",
                                                "Deck Creation Error", JOptionPane.ERROR_MESSAGE);
                        }
                        Object data[][] = new Object[1][fields.length];
                        for (int i = 0; i < fields.length; i++) {
                                data[0][i] = "";
                        }
                        openDeck(name, data, false);
                } else if (e.getSource() == closeDeck) {
                        int idx = jtp.getSelectedIndex();
//                        if (jtp.getTitleAt(idx).equals("Sample Hand")) {
//                                jtp.remove(idx);
//                                tabCount--;
//                                return;
//                        }
                        if (modified[idx] == true){
                          try{
                                Core.saveDeck(jtp.getTitleAt(idx), deckData.get(idx), noteS.get(idx), Core.getDeckCollection(jtp.getTitleAt(idx)), isVirtual[idx]);
                          }catch(Exception ex1){
                            Core.saveDeck(jtp.getTitleAt(idx), deckData.get(idx), null, Core.getDeckCollection(jtp.getTitleAt(idx)), isVirtual[idx]);
                          }
                        }
                        tabs.remove(idx);
                        sPanes.remove(idx);
                        deckTables.remove(idx);
                        tabCount--;
                        deckData.remove(idx);
                        deckModels.remove(idx);
                        jtp.remove(idx);
                        try{
                                noteS.remove(idx);
                        }catch(Exception ex1){
                                
                        }
                        Vector<Object[][]> temp = new Vector<Object[][]>();
                        String names[] = new String[tabCount-idx];
                        for(int j = idx, k = 0; j < tabCount; k++){
                                int i = idx;
                                System.out.println("Index: " + i);
                                tabs.remove(i);
                                sPanes.remove(i);
                                deckTables.remove(i);
                                tabCount--;
                                temp.add(deckData.get(i));
                                deckData.remove(i);
                                deckModels.remove(i);
                                names[k] = jtp.getTitleAt(i);
                                jtp.remove(i);
                                try{
                                        noteS.remove(i);
                                }catch(Exception ex1){
                                        
                                }
                        }
                        for(int i = 0; i < temp.size(); i++)
                                openDeck(names[i], temp.get(i), isVirtual[idx+i]);
                        
                        return;
                } else if (e.getSource() == loadDeck) {
                        Vector<String> possibleValues = Core.getDeckNames();
                        if (possibleValues == null) {
                                JOptionPane.showMessageDialog(this,
                                                "Cannot load deck, there are no saved decks",
                                                "Deck Load Failure", JOptionPane.INFORMATION_MESSAGE);
                                return;
                        }
                        int fdIdx = 0;
                        String temp[] = new String[possibleValues.size()];
                        for (int i = 0; i < possibleValues.size(); i++) {
                                if (Core.getDeckCollection(possibleValues.get(i)).equals(
                                                sourceColl)) {
                                        temp[fdIdx] = possibleValues.get(i);
                                        fdIdx++;
                                }
                        }
                        String finalDecks[] = new String[fdIdx];
                        for (int i = 0; i < fdIdx; i++) {
                                finalDecks[i] = temp[i];
                        }

                        if (finalDecks.length == 0) {
                                JOptionPane.showMessageDialog(this,
                                                "Cannot load deck, there are no saved decks",
                                                "Deck Load Failure", JOptionPane.INFORMATION_MESSAGE);
                                return;
                        }
                        String deckName = (String) JOptionPane.showInputDialog(null,
                                        "Choose which deck to load", "Input",
                                        JOptionPane.INFORMATION_MESSAGE, null, finalDecks,
                                        finalDecks[0]);
                        if (deckName == null)
                                return;
                        Object[][] data = format(Core.loadDeck(deckName, loadSource));
                        int virt = Core.isDeckVirtual(deckName);
                        boolean val = true;
                        if (virt == 0)
                                val = false;
                        int ret = openDeck(deckName, data, val);
                        if (ret == 1) {
                                JOptionPane.showMessageDialog(this,
                                                "Could not load deck too many tabs are open",
                                                "Deck Load Failure", JOptionPane.ERROR_MESSAGE);
                        }
                } else if (e.getSource() == saveDeck) {
                        int idx = jtp.getSelectedIndex();
                        if (jtp.getTitleAt(idx).equals("Sample Hand")) {
                                JOptionPane.showMessageDialog(this,
                                                "Cannot Save a sample hand", "Invalid save",
                                                JOptionPane.ERROR_MESSAGE);
                                return;
                        }
                        if (!modified[idx]) {
                                JOptionPane.showMessageDialog(this,
                                                "This deck is already up to date or is empty",
                                                "Current Deck", JOptionPane.INFORMATION_MESSAGE);
                        } else {
                                if (deckData.get(idx) == null || deckData.get(idx).length == 0) {
                                        JOptionPane.showMessageDialog(this,
                                                        "Deck contains no data! Cannot save an empty deck",
                                                        "Empty Deck", JOptionPane.ERROR_MESSAGE);
                                }
                                if(noteS.size() != 0)
                                        Core.saveDeck(jtp.getTitleAt(idx), deckData.get(idx), noteS.get(idx),
                                                        sourceColl, isVirtual[idx]);
                                else
                                        Core.saveDeck(jtp.getTitleAt(idx), deckData.get(idx), null,
                                                        sourceColl, isVirtual[idx]);
                                modified[idx] = false;
                        }
                }
                index = jtp.getSelectedIndex();
                int ran = 0;
                if (e.getSource() == commentNote) {
                        // noteS.get(index) = JOptionPane.showInputDialog("Note", noteS.get(index));
                        JTextArea inputText;
                        if(noteS.size() != 0)
                                inputText = new JTextArea(noteS.get(index), 20, 10);
                        else
                                inputText = new JTextArea("", 20, 10);
                        int okCxl = JOptionPane.showConfirmDialog(
                                        SwingUtilities.getWindowAncestor(this), inputText,
                                        "Notes and Comments", JOptionPane.OK_CANCEL_OPTION);

                        if (okCxl == JOptionPane.OK_OPTION) {
                                try{
                                        noteS.remove(index);
                                        noteS.insertElementAt(inputText.getText(),index);
                                }catch(Exception ex1){
                                        noteS.add(inputText.getText());
                                }
                        }
                } else if (e.getSource() == tool) {
                        // Request number
                        String random = JOptionPane
                                        .showInputDialog("Number of Cards", null);
                        try {
                                ran = Integer.parseInt(random);
                        } catch (NumberFormatException fail) {
                                System.out.println("Please Enter a Valid Number");
                        }
                        for(int c=0; c<tabCount;c++){
                          if(jtp.getTitleAt(c) != null){
                            if(jtp.getTitleAt(c).equals("Sample Hand")){
                              return;
                            }
                          }
                        }
                        int count = 0;
                        int cards = 0; // Total number of cards in deck
                        if (deckData.get(index)[0][0] == "") {
                                return;
                        }
                        while (count < deckData.get(index).length
                                        && deckData.get(index)[count][2] != "") {
                                // System.out.println("index "+index+", count "+count);
                                val = Integer.parseInt((String) (deckData.get(index)[count][2]));
                                cards = cards + val;
                                count++;
                        }
                        int[] shuff = new int[cards];
                        int n = 0;
                        int t = 0;
                        if (cards < ran) {
                                ran = cards;
                        }

                        // Fill shuff with indices of available cards, duplicated according
                        // to quantity.
                        while (n < cards) {
                                if ((String) deckData.get(index)[t][2] == "") {
                                        break;
                                } else {
                                        val = Integer.parseInt((String) deckData.get(index)[t][2]);
                                        // tabVal = Integer.parseInt((String)deckData.get(index).get(t).get(1));
                                        int cardNum = 1;
                                        while (cardNum <= val) {
                                                shuff[n] = t;
                                                cardNum++;
                                                n++;
                                        }
                                        t++;
                                        // tabVal++;
                                }
                        }

                        // Iterate through all cards, swapping with a randomly selected
                        // card.
                        Random rG = new Random();
                        for (int i = 0; i < shuff.length; i++) {
                                int randomPos = rG.nextInt(shuff.length);
                                int temp = shuff[i];
                                shuff[i] = shuff[randomPos];
                                shuff[randomPos] = temp;
                        }

                        int tT = 0;
                        for (int j = 0; j < ran; j++) {
                                System.out.println(shuff[j]);
                        }
                        Object sampleData[][] = new Object[ran][fields.length];
                        for (int i = 0; i < ran; i++) {
                                if (i == 0) {
                                        for (int k = 0; k < fields.length; k++) {
                                                if (k == 2) {
                                                        sampleData[i][2] = 1;
                                                } else {
                                                        sampleData[i][k] = deckData.get(index)[shuff[i]][k];
                                                }

                                        }
                                        tT++;
                                } else {
                                        int boo = 0;
                                        for (int x = 0; x < i; x++) {

                                                if (sampleData[x][0] != null
                                                                && (deckData.get(index)[shuff[i]][0]
                                                                                .equals((String) sampleData[x][0]))) {
                                                        sampleData[x][2] = (Object) ((Integer) sampleData[x][2] + 1);
                                                        boo++;
                                                }
                                        }
                                        if (boo == 0) {
                                                for (int k = 0; k < fields.length; k++) {
                                                        if (k == 2) {
                                                                sampleData[tT][2] = 1;
                                                        } else {
                                                                sampleData[tT][k] = deckData.get(index)[shuff[i]][k];
                                                        }
                                                }
                                                tT++;
                                        }

                                }
                        }
                        int rows = 0;
                        for (; rows < sampleData.length; rows++) {
                                if (sampleData[rows][0] == null) {
                                        break;
                                }
                        }
                        Object data[][] = new Object[rows][fields.length];
                        for (int i = 0; i < rows; i++) {
                                for (int j = 0; j < fields.length; j++) {
                                        data[i][j] = sampleData[i][j];
                                }
                        }
                        for (int i = 0; i < jtp.getTabCount(); i++)
                                if (jtp.getTitleAt(i).equals("Sample Hand")) {
                                        jtp.remove(i);
                                        openDeck("Sample Hand", data);
                                        return;
                                }
                        openDeck("Sample Hand", data);
                }
        }

        // Override the GenericUI method. Prompts user if a deck has been modified
        // but not saved.
        public boolean close() {
                for (int i = 0; i < MAX_TABS; i++) {
                        System.out.println(modified[i]);
                        if (modified[i] == true) {
                                int result = JOptionPane
                                                .showConfirmDialog(
                                                                (Component) null,
                                                                "One or more decks have not been saved. Save before exiting?",
                                                                "Message", JOptionPane.YES_NO_CANCEL_OPTION);
                                System.out.println(result);
                                if (result == 2)
                                        return false;
                                if (result == 1)
                                        return true;
                                if (result == 0) {
                                        // Save decks.
                                        return true;
                                }
                                return false;
                        }
                }
                return true;
        }

        private void setImage(JLabel card, String filePath) {
                // Add an image.
                Core.imgMaker(filePath, card);
                /*
                 * Image newImg = null; BufferedImage raw = null; try { raw =
                 * ImageIO.read(new File(filePath)); } catch (IOException ex) { try{ raw
                 * = ImageIO.read(new File("backofamagicthegatheringcard.jpg")); }catch
                 * (IOException ex2){ } } if (raw==null) return; ImageIcon icon = new
                 * ImageIcon(raw); Image img = icon.getImage(); newImg =
                 * img.getScaledInstance(230, 310, java.awt.Image.SCALE_SMOOTH);
                 * ImageIcon newIcon=null; if (newImg!=null){ try{ newIcon = new
                 * ImageIcon(newImg); card.setIcon(newIcon); card.revalidate(); }catch
                 * (Exception ex2){ } }
                 */
        }

        private boolean isVirtual(int i) {
                if (jtp == null) {
                        return isVirtual[i];
                }
                try {
                        if ((jtp.getTitleAt(i)) == null) {
                                return isVirtual[i];
                        }
                } catch (Exception ex) {
                        return isVirtual[i];
                }
                String deckName = jtp.getTitleAt(i);
                boolean bool = isVirtual[i];
                int temp = -1;
                try {
                        temp = Core.isDeckVirtual(deckName);
                } catch (Exception ex) {
                }
                if (temp == 0)
                        return false;
                if (temp == 1)
                        return true;
                return bool;
        }

        @SuppressWarnings("rawtypes")
		class StringComparator implements Comparator {
                public int compare(Object obj1, Object obj2) {
                        try {
                                if ((((String) obj1) == "" || ((String) obj1) == null)
                                                && (((String) obj2) == "" || ((String) obj2) == null))
                                        return 0;
                                if (((String) obj1) == "" || ((String) obj1) == null)
                                        return 0;
                                if (((String) obj2) == "" || ((String) obj2) == null)
                                        return 0;
                        } catch (Exception ex) {
                                return 0;
                        }
                        String str1 = (String) obj1;
                        String str2 = (String) obj2;
                        return str1.compareTo(str2);
                }
        }

        // int comparator
        @SuppressWarnings("rawtypes")
		class IntComparator implements Comparator {
                public int compare(Object obj1, Object obj2) {
                        try {
                                if ((((String) obj1) == "" || ((String) obj1) == null)
                                                && (((String) obj2) == "" || ((String) obj2) == null))
                                        return 0;
                                if (((String) obj1) == "" || ((String) obj1) == null)
                                        return 0;
                                if (((String) obj2) == "" || ((String) obj2) == null)
                                        return 0;
                        } catch (Exception ex) {
                                return 0;
                        }
                        int int1 = Integer.parseInt((String) obj1);
                        int int2 = Integer.parseInt((String) obj2);
                        if (int1 < int2)
                                return -1;
                        if (int1 == int2)
                                return 0;
                        return 1;
                }
        }

        @SuppressWarnings("rawtypes")
		class BoolComparator implements Comparator {
                public int compare(Object obj1, Object obj2) {
                        try {
                                if (!obj1.equals(true) && !obj1.equals(false))
                                        return 0;
                                if (!obj2.equals(true) && !obj2.equals(false))
                                        return 0;
                                if ((obj2 == null) || (obj1 == null))
                                        return 0;
                        } catch (Exception ex) {
                                return 0;
                        }
                        if (obj1.equals(obj2))
                                return 0;
                        if (obj1.equals(true))
                                return 1;
                        return -1;
                }
        }

        @SuppressWarnings("rawtypes")
		class FloatComparator implements Comparator {
				@SuppressWarnings("unused")
				public int compare(Object obj1, Object obj2) {
                        try {
                                Object obj = (Float) obj1;
                                obj = (Float) obj2;
                                if ((obj1.equals("") || (obj1) == null)
                                                || (obj1.equals("") || (obj2) == null))
                                        return 0;
                        } catch (Exception ex) {
                                return 0;
                        }
                        float int1 = (Float) (obj1);
                        float int2 = (Float) (obj2);
                        if (int1 < int2)
                                return -1;
                        if (int1 == int2)
                                return 0;
                        return 1;
                }
        }
}
