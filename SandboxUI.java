import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.*;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

public class SandboxUI extends GenericUI implements MouseMotionListener, KeyListener{
 /**
  * 
  */
 private static final long serialVersionUID = 1L;
//Variables for both modes
 private Rectangle maxBounds;
 private JMenu options;
 private JMenuItem fetchCard;
 private JMenuItem showHand;
 private JMenuItem enterCommand;
 private JMenuItem sendDeckInfo, revealCard, revealHand;
 private JDesktopPane desktop;
 private Vector<JMenuItem> sh; //Accelerator stubs
 private JTabbedPane tabs;
 private Vector<Vector<Stack>> images;
 private Vector<JPanel> page;
 private JPopupMenu context;
 private JPanel pnlChat;
 private JPanel pnlMain;
 private int contextNum;
 private int handSize=7;
 private int middle;
 private Player[] players;
 private Point originpt;
 private Vector<Stack> selected;
 private String[][] decks;
 private FilteredChatPanel fcPanel;
 private JSplitPane container;
 
 //Undecided mode
 private FilteredTextArea chatOut;
 private JTextArea chatIn;
 
 //Multiplayer mode only
 private Player me = null;
   
 public SandboxUI(int numberOfPlayers, int[] fieldSize, String[] decks, int handCount){
  this(numberOfPlayers, null, -1, fieldSize, decks, handCount);
 }
 
 public SandboxUI(int numberOfPlayers, String[] playerNames, int meIdx,int[] fieldSize, String[] decks, int handCount) {
  super(0, 0, "Sandbox");
  SandboxUI.setDefaultLookAndFeelDecorated(true);
  try{
   UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
  }catch(Exception ex){
   ex.printStackTrace();
  }
  handSize = handCount;
  players = new Player[numberOfPlayers];
  options = new JMenu("Options");
  fetchCard = new JMenuItem("Fetch Card");
  showHand = new JMenuItem("Show Hand");
  enterCommand = new JMenuItem("Enter Command");
  sendDeckInfo = new JMenuItem("Send Deck Info");
  revealCard = new JMenuItem("Reveal a card");
  revealHand = new JMenuItem("Reveal hand");
  sh = new Vector<JMenuItem>();
  for(int i = 0; i < 8; i++){
   sh.add(new JMenuItem(""));
   sh.get(i).addActionListener(this);
   switch(i){
    case 0:
     sh.get(i).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1,ActionEvent.CTRL_MASK));
    case 1:
     sh.get(i).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2,ActionEvent.CTRL_MASK));
     break;
    case 2:
     sh.get(i).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3,ActionEvent.CTRL_MASK));
     break;
    case 3:
     sh.get(i).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4,ActionEvent.CTRL_MASK));
     break;
    case 4:
     sh.get(i).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_5,ActionEvent.CTRL_MASK));
     break;
    case 5:
     sh.get(i).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_6,ActionEvent.CTRL_MASK));
     break;
    case 6:
     sh.get(i).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_7,ActionEvent.CTRL_MASK));
     break;
    case 7:
     sh.get(i).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_8,ActionEvent.CTRL_MASK));
     break;
    default:
     sh.get(i).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_9,ActionEvent.CTRL_MASK));
     break;
   }
   
  }
  file.add(exit);
  options.add(fetchCard);
  //options.add(showHand);
  options.add(enterCommand);
  options.add(sendDeckInfo);
  //options.add(revealCard);
  options.add(revealHand);
  enterCommand.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,ActionEvent.CTRL_MASK));
  fetchCard.addActionListener(this);
  showHand.addActionListener(this);
  enterCommand.addActionListener(this);
  sendDeckInfo.addActionListener(this);
  revealCard.addActionListener(this);
  revealHand.addActionListener(this);
  (this.getJMenuBar()).add(options);
  (this.getJMenuBar()).remove(games);
  this.decks = new String[10][];
  tabs = new JTabbedPane();
  pnlMain = new JPanel(new GridBagLayout());
  this.setLayout(new GridBagLayout());
  container = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
  Rectangle screenSize = getGraphicsConfiguration().getBounds();
  container.setDividerLocation(screenSize.height*2/3);
  GridBagConstraints c0 = new GridBagConstraints();
  c0.weighty = .9;
  c0.weightx = 1;
  c0.gridy = 0;
  c0.fill = GridBagConstraints.BOTH;
  container.add(pnlMain);
  desktop = new JDesktopPane();
  c0.weighty = .4;
  c0.weightx = 1;
  c0.gridy = 1;
  container.add(desktop);
  c0.gridy = 0;
  c0.weighty = .9;
  c0.weightx = 1;
  pnlMain.add(tabs,c0);
  this.add(container, c0);
  GridBagConstraints c = new GridBagConstraints();
  c.insets = new Insets(5,5,5,5);
  pnlChat = new JPanel(new GridBagLayout());
  fcPanel = new FilteredChatPanel();
  chatOut = fcPanel.getTextArea();
  c0.fill= GridBagConstraints.BOTH;
  c0.gridx = 1;
  c0.gridy = 0;
  c0.weightx = .2;
  pnlMain.add(pnlChat,c0);
  c.fill=GridBagConstraints.BOTH;
  c.gridy=0;
  c.gridx=0;
  c.weightx=1;
  c.weighty= .8;
  pnlChat.add(fcPanel,c);
  chatIn = new JTextArea();
  chatIn.setLineWrap(true);
  chatIn.setWrapStyleWord(true);
  chatIn.addKeyListener(this);
  c.gridy = 1;
  c.weighty = .2;
  pnlChat.add(chatIn,c);
  Dimension d1 = chatIn.getSize();
  chatIn.setPreferredSize(d1);
  chatIn.setMinimumSize(d1);
  chatIn.setMaximumSize(d1);
  images = new Vector<Vector<Stack>>();
  page = new Vector<JPanel>();
  selected = new Vector<Stack>();
  fieldSize[1] = fieldSize[1] * (numberOfPlayers / 2 + numberOfPlayers % 2);
  fieldSize[0] = fieldSize[0] * (2);
  if(playerNames == null)
   tabMaker(fieldSize[1], fieldSize[0], decks);
  else
   tabMaker(fieldSize[1], fieldSize[0], decks, playerNames, meIdx);
  
  //pack();
  //Multiplayer Mode Code
  if(playerNames!= null){
   for(int i = 0; i < playerNames.length; i++)
    players[i].setName(playerNames[i]);
   me = players[meIdx];
  }
  this.setResizable(false);
  this.setVisible(true);
  this.setExtendedState(this.getExtendedState() | JFrame.MAXIMIZED_BOTH);
  Dimension d = new Dimension(chatIn.getSize());
  chatIn.setMinimumSize(d);
  chatIn.setMaximumSize(d);
  chatIn.setPreferredSize(d);
  chatIn.setSize(d);
  chatIn.setText("");
 }

 public void tabMaker(int width, int height, String[] decknames){
  tabMaker(width, height, decknames, null, -1);
 }
 
 public void tabMaker(int width, int height,String[] deckNames, String[] playerNames, int meIdx){
  int numPlayers = deckNames.length;
  if(numPlayers==1)
   numPlayers = playerNames.length;
  int totalSize = width * height;// *numPlayers;
  JPanel newPanel = new JPanel();
  newPanel.addMouseMotionListener(this);
  newPanel.addMouseListener(this);
  page.add(newPanel);
  if(playerNames!=null)
   this.decks[page.size()-1] = new String[playerNames.length];
  else
   this.decks[page.size() - 1] = deckNames;
  if(playerNames!=null){
   for(int i =0; i<this.decks[page.size()-1].length;i++)
    if(i==meIdx)
     this.decks[page.size()-1][i] = deckNames[0];
    else
     this.decks[page.size()-1][i] = playerNames[i]+"'s deck";
  }
  Vector<Stack> newImages = new Vector<Stack>();
  images.add(newImages);
  newPanel.setLayout(new GridBagLayout());
  GridBagConstraints c = new GridBagConstraints();

  // go through each possible card space
  int half = deckNames.length / 2 + deckNames.length % 2;
  if(deckNames.length==1)
   half = playerNames.length/2 + playerNames.length%2;
  int secondHalf = deckNames.length - half;
  if(deckNames.length==1)
   secondHalf = playerNames.length-half;
  int personalWidth = width/half;
  int k = 0;
  for(int i = 0; i < totalSize; i++){
   //create each players opening deck
   if((i%personalWidth == 0 && i < width) || (i%personalWidth == 0 && i > (width*(height-1)-1) && i < width*(height-1) + personalWidth*secondHalf-1)){
    ArrayList<FieldCard> data = new ArrayList<FieldCard>();
    if(playerNames==null){
     Object[][] cardData;
     cardData = Core.loadDeck(deckNames[k], Core.getDeckCollection(deckNames[k]));
     for(int j = 0; j < cardData.length; j++){
      for(int l = 0; l < (Integer) cardData[j][2]; l++){
       FieldCard fc = new FieldCard((String) cardData[j][0], (String) cardData[j][3]);
       data.add(fc);
      }
     }
     Hand hand = new Hand(this, images.size()-1);
     players[k] = new Player(hand,k+1);
     Stack stack = new Stack(this, data, 0,0,players[k]);
     stack.setAsDeck();
     stack.setFaceup(false);
     stack.shuffle();
      
     players[k].addStack(stack);
     for(int j=0; j<handSize; j++){
      players[k].getStacks().get(0).draw();
     }
     players[k].getHand().initializeHandPanel();
     newImages.add(stack);
     newImages.get(i).getContainer().addMouseListener(this);
     newImages.get(i).getRotation().addMouseListener(this);
     k++;
    }
    else{
     data.add(new FieldCard((String) "empty", (String) "transparent.gif"));
     Hand hand = new Hand(this, images.size()-1);
     players[k] = new Player(hand, k+1);
     Stack stack = new Stack(this, data,0,0, players[k]);
     stack.setAsDeck();
     stack.setFaceup(false);
     players[k].addStack(stack);
     newImages.add(stack);
     newImages.get(i).getContainer().addMouseListener(this);
     newImages.get(i).getRotation().addMouseListener(this);
     k++;
    }
   }
   //all other spaces empty
   else{
    newImages.add(new Stack(this, "transparent.gif", 0, 0));
   }
   newImages.get(i).setX(i % width);
   newImages.get(i).setY(i / width);
  }
  
  c.insets = new Insets(10, 10, 10, 10);
  middle = height/2 -1;
  c.gridx = 0;
  c.gridy = 0;
  for(int i = 0; i < height; i++){
   for(int j = 0; j < width; j++){
    c.gridx = j;
    c.gridy = i;
    if(i == middle && j == 0){
     c.insets = new Insets(20, 20, 125, 20);
    }
    if(i == middle+1 && j == 0)
     c.insets = new Insets(20, 20, 20, 20);
    newPanel.add(newImages.get(width * i + j).getContainer(), c);
    newPanel.add(newImages.get(width*i+j).getRotation(),c);
   }
  }
  JScrollPane scrollpane = new JScrollPane(newPanel);
  scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
  scrollpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
  tabs.addTab("Play Field " + tabs.getComponentCount(), scrollpane);
 }
 
 @SuppressWarnings("unused")
public void extendedActionPerformed(ActionEvent e) {
  if(e.getSource() == fetchCard){
   Object[] possibilities = new Object[players.length];
   if(me == null){
     for(int i=0; i<players.length; i++)
       possibilities[i] = players[i].getName();
     String s = (String) JOptionPane.showInputDialog(this, "Fetch card for which player?", "Fetch Card", JOptionPane.PLAIN_MESSAGE, null, possibilities, "Player 1");
     int index = 0;
     for(int i = 0; i < players.length;i++){
       if(s.equals(players[i].getName())){
         index = i;
         break;
       }
     }
     String deck = decks[tabs.getSelectedIndex()][index];
     new LibraryUI(deck,index,this);
   }else{
     String deck = getDeck();
     int index = 0;
     for(int i = 0; i < players.length;i++){
       if(players[i]==me){
         index = i;
         break;
       }
     }
     new LibraryUI(deck,index,this);
   }
  }
  else if(e.getSource() == showHand){
    Object[] possibilities = new Object[players.length];
    if(me == null){
      for(int i=0; i<players.length; i++)
        possibilities[i] = players[i].getName();
      String s = (String) JOptionPane.showInputDialog(this, "Show hand for which player?", "Fetch Card", JOptionPane.PLAIN_MESSAGE,null, possibilities, "Player 1");
      for(int i = 0; i < players.length;i++){
        if(s.equals(players[i].getName())){
          players[i].getHand().initializeHandPanel();
          players[i].getHand().getHandPanel().refresh();
          break;
        }
      }
    }
    else{
      me.getHand().initializeHandPanel();
      me.getHand().getHandPanel().refresh();
    }
  }
  else if(sh.contains(e.getSource())){
   int i;
   for(i = 0; i < 8; i++){
    if(sh.get(i).equals(e.getSource())){
     break;
    }
   }
   Player p = getPlayerById(i+1);
   if(p==null)
    return;
   p.getHand().initializeHandPanel();
   p.getHand().getHandPanel().refresh();
  }
  else if(e.getSource() == enterCommand){
   String cmd = (String) JOptionPane.showInputDialog("Enter a command:");
   if(cmd!=null && cmd!="")
    Core.receiveBlock(cmd);
  }
  else if(e.getSource() == sendDeckInfo){
   Vector<String> possibleValues = Core.getDeckNames();
   if(possibleValues == null){
    JOptionPane.showMessageDialog(this, "Cannot load deck, there are no saved decks", "Deck Load Failure", JOptionPane.INFORMATION_MESSAGE);
    return;
   }
   String deckName = (String) JOptionPane.showInputDialog(null, "Choose which deck to load", "Input",
     JOptionPane.INFORMATION_MESSAGE, null, possibleValues.toArray(), possibleValues.get(0));
   if(deckName == null)
     return;
   Core.sendDeckInfo(deckName);
  }else if(e.getSource() == revealHand){
    if (me==null) {
      JOptionPane.showMessageDialog(this, "Only available in multiplayer.");
      return;
    }
    ArrayList<FieldCard> tempCards = new ArrayList<FieldCard>();
    tempCards = me.getHand().getCards();
    new FieldStack(tempCards, null, null);
    Core.sendRevealInfo(tempCards, me.getName() + " revealed their hand.");
  }
  else if(context.getComponent(0) == e.getSource()) {
    if(selected.isEmpty()){
      flip();
    }
   else{
    for(int i = 0 ; i < selected.size() ; i++){
     int stackID = selected.get(i).getId();
     flip(stackID, true);
    }   
   }
  }
  else if(context.getComponent(1) == e.getSource()) {
   if(selected.isEmpty()){
    draw();
   }
   else{
    for(int i = 0 ; i < selected.size() ; i++){
     draw(selected.get(i).getId(), true);
    }
   }
  }
  else if(context.getComponent(2) == e.getSource()){
   if(selected.isEmpty()){
    Stack stack = images.get(tabs.getSelectedIndex()).get(contextNum);
    String orig = stack.getDescriptor();
    int origId = stack.getId();
    stack.shuffle();
     if (me!=null) Core.refreshStack(origId, orig+" was shuffled.");
   }
   else{
    for(int i = 0 ; i < selected.size() ; i++){
     String orig = selected.get(i).getDescriptor();
     int origId = selected.get(i).getId();
     selected.get(i).shuffle();
     if (me!=null) Core.refreshStack(origId, orig+" was shuffled.");
    }
   }
  }
  else if(context.getComponent(3) == e.getSource()){
   // Delete
  }
  else if(context.getComponent(3) == e.getSource()){
   if(selected.isEmpty()){
    tap();
   }
   else{
    for(int i = 0 ; i < selected.size() ; i++){
     tap(selected.get(i).getId(), true);
    }
    page.get(tabs.getSelectedIndex()).repaint();
    page.get(tabs.getSelectedIndex()).revalidate();
   }
  }
  else if(context.getComponent(4) == e.getSource()){ 
   if(selected.isEmpty()){
    Stack stack = images.get(tabs.getSelectedIndex()).get(contextNum);
    new FieldStack(stack.getCards(), stack, this);
    Core.printToLog("A player is looking through "+stack.getDescriptor()+".");
   }
   else{
    for(int i = 0 ; i < selected.size() ; i++){
     new FieldStack(selected.get(i).getCards(), selected.get(i), this);
     Core.printToLog("A player is looking through "+selected.get(i).getDescriptor()+".");
    }
   }
  }
  else if(context.getComponent(5)== e.getSource()){
   String name = "abc";
   name = JOptionPane.showInputDialog(this, "How Many Cards to Draw?", "", 1);
   if(name == null)
    return;
   while(!name.matches("[0-9]+"))
    name = JOptionPane.showInputDialog(this, "Must be a number!", "", 1);
   int num = Integer.parseInt(name);
   if(num > images.get(tabs.getSelectedIndex()).get(contextNum).getCards().size()){
     JOptionPane.showMessageDialog(this, "That is more cards than there is in the stack!");
     return;
   }


   for(; num>0; num--){
    draw();
   }
  }
  else if(context.getComponent(6) == e.getSource()) {
    String name = "abc";
    name = JOptionPane.showInputDialog(this, "How Many Cards to reveal?", "", 1);
    while(name==null || name=="" || !name.matches("[0-9]+"))
      name = JOptionPane.showInputDialog(this, "Must be a number!", "", 1);
    int num = Integer.parseInt(name);
    
    if(selected.isEmpty()){
      Stack stack = images.get(tabs.getSelectedIndex()).get(contextNum);
      new FieldStack(stack.getNCards(num), null, null);
      if (me!=null)
        Core.sendRevealInfo(stack.getNCards(num),me.getName()+" revealed the top "+num+" cards of "+stack.getDescriptor());
    }
    else{
      for(int i = 0 ; i < selected.size() ; i++){
        new FieldStack(selected.get(i).getNCards(num), null, null);
        if (me!=null)
          Core.sendRevealInfo(selected.get(i).getNCards(num),me.getName()+" revealed the top "+num+" cards of "+selected.get(i).getDescriptor());
      }
    }
  }
 }
 
 public void mouseClicked(MouseEvent e){
  if(SwingUtilities.isLeftMouseButton(e)){
   int i;
   for(i = 0; i < images.get(tabs.getSelectedIndex()).size(); i++){
    if(images.get(tabs.getSelectedIndex()).get(i).getContainer().equals(e.getSource())
      || images.get(tabs.getSelectedIndex()).get(i).getRotation().equals(e.getSource()))
     break;
    else if(i + 1 == images.get(tabs.getSelectedIndex()).size())
     return;
   }
   int opId = images.get(tabs.getSelectedIndex()).get(i).getId();
   tap(opId, true);
  }
  else if(SwingUtilities.isRightMouseButton(e)){
   int i;
   for(i = 0; i < images.get(tabs.getSelectedIndex()).size(); i++){
    if(images.get(tabs.getSelectedIndex()).get(i).getContainer().equals(e.getSource())
      || images.get(tabs.getSelectedIndex()).get(i).getRotation().equals(e.getSource()))
     break;
    else if(i + 1 == images.get(tabs.getSelectedIndex()).size())
     return;
   }
   Stack op = images.get(tabs.getSelectedIndex()).get(i);
   if(op.container.isVisible()){
    op.option.show(op.getContainer(), (int) e.getX(), (int) e.getY());
   } 
   else{
    op.option.show(op.getRotation(), (int) e.getX(), (int) e.getY());
   }
   context = op.option;
   contextNum = i;
  }
 }
 
 public JPanel getSelectedPanel(){
  return page.get(tabs.getSelectedIndex());
 }

 public int spawnStack(FieldCard zygote, Point release, Player p, int sourceID){
  System.err.println("Release Point: " + release);
  int j;
  int ret = -1;
  Vector<Stack> current = images.get(tabs.getSelectedIndex());
  for(j = 0; j < current.size() && j != -1; j++){
   if(current.get(j).getContainer().contains(SwingUtilities.convertPoint(this, release, current.get(j).getContainer()))
     || current.get(j).getRotation().contains(SwingUtilities.convertPoint(this, release,current.get(j).getRotation()))){
    break;
   }
   else if(j + 1 == current.size()){
    j = -2;
   }
  }
  if(j != -1&& current.get(j).getCards().get(0).getName().equals("empty")){
   System.err.println("Origin Before Passing On");
   ret = spawner((int)current.get(j).getX(), (int)current.get(j).getY(), zygote, j, p, sourceID);
   return ret;
  }
  else if(j != -1){
   return -2;
  }
  
  release.x -=100;
  
  for(j = 0; j < current.size() && j != -1; j++) {
   if(current.get(j).getContainer().contains(SwingUtilities.convertPoint(this, release, current.get(j).getContainer()))
     || current.get(j).getRotation().contains(SwingUtilities.convertPoint(this, release,current.get(j).getRotation()))){
    break;
   }
   else if(j + 1 == current.size()){
    j = -2;
   }
  }
  int l = 0;
  if(j!=-1){
   boolean empty = true;
   for(l = 0; l < current.size()&& l > -1; l++){
    if(current.get(l).getX() == current.get(j).getX()+1 && current.get(j).getY() == current.get(l).getY() && !current.get(l).getCards().get(0).getName().equals("empty")){
     empty = false;
     break;
    }
    else if(current.get(l).getX() == current.get(j).getX()+1 && current.get(j).getY() == current.get(l).getY() && current.get(l).getCards().get(0).getName().equals("empty")){
     break;
    }
    else if(l + 1 == current.size())
     l = -2;
   }
   if(empty){
    //Spawn to Right
    System.err.println("Origin Before Passing Right");
    ret = spawner((int)current.get(j).getX()+1, (int)current.get(j).getY(), zygote, l, p, sourceID);
   }
   return ret;
  }
  release.x += 100;
  release.y -= 100;
  for(j = 0; j < current.size() && j != -1; j++){
   if(current.get(j).getContainer().contains(SwingUtilities.convertPoint(this, release, current.get(j).getContainer()))
     || current.get(j).getRotation().contains(SwingUtilities.convertPoint(this, release,current.get(j).getRotation()))){
    break;
   } 
   else if(j + 1 == current.size()){
    j = -2;
   }
  }
  if(j!=-1){
   boolean empty = true;
   for(l = 0; l < current.size()&& l > -1; l++){
    if(current.get(l).getX() == current.get(j).getX() && current.get(j).getY()+1 == current.get(l).getY() && !current.get(l).getCards().get(0).getName().equals("empty")){
     empty = false;
     break;
    }
    else if(current.get(l).getX() == current.get(j).getX() && current.get(j).getY()+1 == current.get(l).getY() && current.get(l).getCards().get(0).getName().equals("empty"))
     break;
    else if(l + 1 == current.size())
     l = -2;
   }
   if(empty){
    System.err.println("Origin Before Passing Down");
    //Spawn Below
    ret = spawner((int)current.get(j).getX(), (int)current.get(j).getY()+1, zygote, l, p, sourceID);
   }
   return ret;
  } 
  release.x += 100;
  release.y += 100;
  for(j = 0; j < current.size() && j != -1; j++){
   if(current.get(j).getContainer().contains(SwingUtilities.convertPoint(this, release, current.get(j).getContainer()))
     || current.get(j).getRotation().contains(SwingUtilities.convertPoint(this, release,current.get(j).getRotation()))){
    break;
   } 
   else if(j + 1 == current.size()){
    j = -2;
   }
  }
  if(j!=-1){ 
   boolean empty = true;
   for(l = 0; l < current.size()&& l > -1; l++){
    if(current.get(l).getX() == current.get(j).getX()-1 && current.get(j).getY() == current.get(l).getY() && !current.get(l).getCards().get(0).getName().equals("empty")){
     empty = false;
     break;
    }
    else if(current.get(l).getX() == current.get(j).getX()-1 && current.get(j).getY() == current.get(l).getY() && current.get(l).getCards().get(0).getName().equals("empty"))
     break;
    else if(l + 1 == current.size())
     l = -2; 
   }
   if(empty){
    //Spawn to Left
    System.err.println("Origin Before Passing Left");
    ret = spawner((int)current.get(j).getX()-1, (int)current.get(j).getY(), zygote, l, p, sourceID);
   }
   return ret;
  }
  release.x -= 100;
  release.y += 100;
  for(j = 0; j < current.size() && j != -1; j++){
   if(current.get(j).getContainer().contains(SwingUtilities.convertPoint(this, release, current.get(j).getContainer()))
     || current.get(j).getRotation().contains(SwingUtilities.convertPoint(this, release,current.get(j).getRotation()))){
    break;
   } 
   else if(j + 1 == current.size()){ 
    j = -2;
   }
  }
  if(j!=-1){
   boolean empty = true;
   for(l = 0; l < current.size()&& l > -1; l++){
    if(current.get(l).getX() == current.get(j).getX() && current.get(j).getY()-1 == current.get(l).getY()&& !current.get(l).getCards().get(0).getName().equals("empty")){
     empty = false;
     break;
    }
    else if(current.get(l).getX() == current.get(j).getX() && current.get(j).getY()-1 == current.get(l).getY() && current.get(l).getCards().get(0).getName().equals("empty"))
     break;
    else if(l + 1 == current.size())
     l = -2;
   }
   if(empty){
    //Spawn Above
    System.err.println("Origin Before Passing Above");
    ret = spawner((int)current.get(j).getX(), (int)current.get(j).getY()-1, zygote, l, p, sourceID);
   }
   return ret;
  }
  return ret;
 }
 
 public int spawner(int x, int y, FieldCard zygote, int j, Player p, int k){
  if(zygote == null){
   System.err.println("Debug: Cannot spawn null fieldcard");
   return -1;
  }
  if(x <0 || y<0){
   System.err.println("Debug: cannot spawn at negative coordinates");
   return -1;
  }
  int check = -1;
  if(y <= middle && p != getPlayerById(1) || y > middle && p != getPlayerById(2)){
   int safety = JOptionPane.showConfirmDialog(this, "You are about to move this card to the opponent's field are you sure?", "Dangerous move", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
     if(safety == 0){
     }
     else{
      return -1;
     }
  }
  check = JOptionPane.showConfirmDialog(this, "Create this stack face up?", "Spawn Face Up", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
  if(check == 2 || check == -1){
   return -1; 
  }
  System.err.println("ORIGIN: " + j);
  spawn(zygote, x, y, j, p, check, k);
  return 0;
 }
 
 @SuppressWarnings("unused")
public void moveToBlank(Point release, int i) {
   release.x -= 100;
   //Look Left
   int j;
   Vector<Stack> current = images.get(tabs.getSelectedIndex());
   JPanel paged = page.get(tabs.getSelectedIndex());
   System.out.println("Gridx: " + current.get(i).getX() + " Gridy: " + current.get(i).getY());
   for(j = 0; j < current.size() && j != -1; j++){
    if(current.get(j).getContainer().contains(SwingUtilities.convertPoint(this, release, current.get(j).getContainer()))
      || current.get(j).getRotation().contains(SwingUtilities.convertPoint(this, release,current.get(j).getRotation()))){
     break;
    }
    else if(j + 1 == current.size()){
     j = -2; 
    }
   }
   if(j != -1){ //asdf
     mvLeft(current.get(i), current.get(j));
     return;
   }
 //endasdf
   release.x += 100;
   release.y -= 100;
   //Look Up
   for(j = 0; j < current.size()&& j > -1; j++){
    if(current.get(j).getContainer().contains(SwingUtilities.convertPoint(this, release,current.get(j).getContainer()))
      || current.get(j).getRotation().contains(SwingUtilities.convertPoint(this,release,current.get(j).getRotation()))){
     break;
    }
    else if(j + 1 == current.size()){
     j = -2;
    } 
   }
   if(j != -1){ //asdf
     mvUp(current.get(i),current.get(j));
     return;
   }
    //Look Up
     //endasdf
   release.x += 100;
   release.y += 100;
   //Look Right 
   for(j = 0; j < current.size()&& j > -1; j++){
    if(current.get(j).getContainer().contains(SwingUtilities.convertPoint(this, release,current.get(j).getContainer()))
      || current.get(j).getRotation().contains(SwingUtilities.convertPoint(this,release,current.get(j).getRotation()))){
     break;
    } 
    else if(j + 1 == current.size()){
     j = -2;
    }
   }
   if(j != -1){ //asdf
     mvRight(current.get(i),current.get(j));
     return;
   }
     //endasdf
   release.x -= 100;
   release.y += 100;
   //Look Down
   for(j = 0; j < current.size()&& j > -1; j++){
    if(current.get(j).getContainer().contains(SwingUtilities.convertPoint(this, release,current.get(j).getContainer()))
      || current.get(j).getRotation().contains(SwingUtilities.convertPoint(this,release,current.get(j).getRotation()))){
     break;
    } 
    else if (j + 1 == current.size()){
     j = -2;
    } 
   }
   if(j!=-1){ //asdf
    mvDown(current.get(i), current.get(j));
    return;
   }//endasdf
 }
 
 public void mouseReleased(MouseEvent e){
  Vector<Stack> current = images.get(tabs.getSelectedIndex());
  JPanel paged = page.get(tabs.getSelectedIndex());
  if(SwingUtilities.isLeftMouseButton(e)){
   int i;
   //Index of the Source Stack is determined
   for(i = 0; i < current.size(); i++){
    if(current.get(i).getContainer().equals(e.getSource())
      && current.get(i).getContainer().isVisible()
      || current.get(i).getRotation().equals(e.getSource())
      && !current.get(i).getContainer().isVisible()){
     break;
    }
    else if(i + 1 == current.size())
     return;//Invalid Source location for drag
   }
   Point release = MouseInfo.getPointerInfo().getLocation();
   SwingUtilities.convertPointFromScreen(release, this);
   int j;
   //Index of the Destination Stack is determined
   for(j = 0; j < current.size(); j++){
    if(current.get(j).getContainer()
      .contains(SwingUtilities.convertPoint(this, release, current.get(j).getContainer()))
      && current.get(j).getContainer().isVisible()
      || current.get(j).getRotation()
      .contains(SwingUtilities.convertPoint(this,release,current.get(j).getRotation()))
        && !current.get(j).getContainer().isVisible()){
     break;
    } 
    else if(j + 1 == current.size()){
     //Drop Location is blank space, check if the card can be placed by reference
     Point mover = MouseInfo.getPointerInfo().getLocation();
     moveToBlank(mover, i);
     return;
    }
   }
   if(j != i){
    //We shouldn't try to swap a stack with itself
    //Check if the stack is being moved between players
    if(current.get(i).getY() <= middle){
     if(current.get(j).getY() > middle){
      int safety = JOptionPane.showConfirmDialog(this, "You are about move a card into the opponent's space, are you sure?", "Dangerous move", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        if(safety == 0){
        }
        else{
         return;
        }
     }
    }
    else{
     if(current.get(j).getY() <= middle){
      int safety = JOptionPane.showConfirmDialog(this, "You are about move a card into the opponent's space, are you sure?", "Dangerous move", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        if(safety == 0){
        }
        else{
         return;
        }
     }
    }
    
    int choice = 0;
    //Ask if we should swap or merge stacks only if the stacks are non empty and one isn't a deck.
    String[] possibleValues;
    
    if(!current.get(j).cards.get(0).getName().equals("empty") && !current.get(i).isDeck){
     possibleValues = new String[]{"Swap Stacks","Put on top", "Put on bottom", "Shuffle in", "Insert nth from top", "Insert nth from bottom", "Insert manually"};
     String selectedValue = (String) JOptionPane.showInputDialog(null,
                            "Choose an option", "Card Movement",
                            JOptionPane.INFORMATION_MESSAGE, null, possibleValues,
                            possibleValues[0]);
                    if(selectedValue == null)
                     return;
     for(int k = 0; k < possibleValues.length; k++)
      if(possibleValues[k].equals(selectedValue)){
       choice = k;
      }
    }
    if(choice == 2){
     //Merge Stacks on Bottom Doesn't use MoveSS because it is cleaner this way for this situation
     paged.remove(current.get(i).getContainer());
     paged.remove(current.get(i).getRotation());
     for(int k = current.get(i).cards.size()-1; k>=0; k--)
      current.get(j).addCardToBot(current.get(i).getCards().get(k));
     
     paged.repaint();
     paged.revalidate();
     Core.printToLog("The card stack at index "+i+" was moved to the bottom of "+current.get(j).getDescriptor()+".", "StackMerge "+current.get(i).getId()+" "+current.get(j).getId()+" " + choice +" 0 B;");
     current.remove(i);
    }
    else if(choice == 0){
     //Swap Stacks
     exchange(current.get(i), current.get(j));
    }
    else if(choice == 1){
     //Merge Stacks on Top
     paged.remove(current.get(i).getContainer());
     paged.remove(current.get(i).getRotation());
     for(int k = 0; k < current.get(i).cards.size(); k++)
      current.get(j).addCardToTop(current.get(i).getCards().get(k));
     current.get(j).refreshImg();
     
     paged.repaint();
     paged.revalidate();
     Core.printToLog("The card stack at index "+i+" was moved to the top of "+current.get(j).getDescriptor()+".", "StackMerge "+current.get(i).getId()+" "+current.get(j).getId()+" " + choice +" 0 T;");
     current.remove(i);
    }
    else if(choice == 3){
     //Shuffle
     paged.remove(current.get(i).getContainer());
     paged.remove(current.get(i).getRotation());
     for(int k = 0; k < current.get(i).cards.size(); k++)
      current.get(j).addCardToTop(current.get(i).getCards().get(k));

     current.get(j).shuffle();
     current.get(j).refreshImg();
     paged.repaint();
     paged.revalidate();
     Core.printToLog("The card stack at index "+i+" was moved to the top of "+current.get(j).getDescriptor()+".", "StackMerge "+current.get(i).getDescriptor()+" "+current.get(j).getId()+" " + choice +" 0 T;");
     if (me!=null) Core.refreshStack(current.get(j).getId(), current.get(j).getDescriptor()+" was shuffled.");
     current.remove(i);
    }
    else if(choice == 4){
     //nth from top
     int n;
     String result = JOptionPane.showInputDialog("Enter index from top. (On top is 1, below the top card is 2, etc.)");
     if(result==null) 
      return;
     try{ 
      Integer.parseInt(result); 
     }catch(NumberFormatException ex){ 
      JOptionPane.showMessageDialog(this, "Must enter an integer.", "Bad Input", JOptionPane.INFORMATION_MESSAGE);
      return;
     }
     n = Integer.parseInt(result);
     for(int k = 0; k < current.get(i).cards.size(); k++)
      current.get(j).insertCardFromTop(current.get(i).getCards().get(k), n-1);
     current.get(j).refreshImg();
     paged.remove(current.get(i).getContainer());
     paged.remove(current.get(i).getRotation());
     paged.repaint();
     paged.revalidate();
     if(n<=1) 
      Core.printToLog("The card stack at index "+i+" was moved to the top of "+current.get(j).getDescriptor()+".", "StackMerge "+current.get(i).getId()+" "+current.get(j).getId()+" " + choice +" 0 T;");
     else if(n==2)
      Core.printToLog("The card stack at index "+i+" was moved to the top of "+current.get(j).getDescriptor()+".", "StackMerge "+current.get(i).getId()+" "+current.get(j).getId()+" " + choice +" 1 T;");
     else if(n==3)
      Core.printToLog("The card stack at index "+i+" was moved to the top of "+current.get(j).getDescriptor()+".", "StackMerge "+current.get(i).getId()+" "+current.get(j).getId()+" " + choice +" 2 T;");
     else
      Core.printToLog("The card stack at index "+i+" was moved to the top of "+current.get(j).getDescriptor()+".", "StackMerge "+current.get(i).getId()+" "+current.get(j).getId()+" " + choice +" "+(n-1) + " T;");     

     current.remove(i);
    }
    else if(choice == 5){
     //nth from bottom
     int n;
     String result = JOptionPane.showInputDialog("Enter index from bottom. (On bottom is 1, above the bottom card is 2, etc.)");
     if(result==null) 
      return;
     try{ 
      Integer.parseInt(result); 
     }catch(NumberFormatException ex){ 
      JOptionPane.showMessageDialog(this, "Must enter an integer.", "Bad Input", JOptionPane.INFORMATION_MESSAGE);
      return;
     }
     n = Integer.parseInt(result);
     for(int k = 0; k < current.get(i).cards.size(); k++)
      current.get(j).insertCard(current.get(i).getCards().get(k), n-1+k);
     current.get(j).refreshImg();
     paged.remove(current.get(i).getContainer());
     paged.remove(current.get(i).getRotation());
     if(n<=1) 
      Core.printToLog("The card stack at index "+i+" was moved to the bottom of "+current.get(j).getDescriptor()+".", "StackMerge "+current.get(i).getId()+" "+current.get(j).getId()+" " + choice +" 0 B;");
     else if(n==2)
         Core.printToLog("The card stack at index "+i+" was moved to the bottom of "+current.get(j).getDescriptor()+".", "StackMerge "+current.get(i).getId()+" "+current.get(j).getId()+" " + choice +" 1 B;");
     else if(n==3)
         Core.printToLog("The card stack at index "+i+" was moved to the bottom of "+current.get(j).getDescriptor()+".", "StackMerge "+current.get(i).getId()+" "+current.get(j).getId()+" " + choice +" 2 B;");
     else
         Core.printToLog("The card stack at index "+i+" was moved to the bottom of "+current.get(j).getDescriptor()+".", "StackMerge "+current.get(i).getId()+" "+current.get(j).getId()+" " + choice +" "+(n-1) + " B;");     

     current.remove(i);
     paged.repaint();
     paged.revalidate();
    }
    else{
     //manually merge
     for(int k = 0; k < current.get(i).cards.size(); k++)
      current.get(j).addCardToTop(current.get(i).getCards().get(k));
     paged.remove(current.get(i).getContainer());
     paged.remove(current.get(i).getRotation());
     if(me != null){
      Core.printToLog("The card stack at index "+i+" was moved to the top of "+current.get(j).getDescriptor()+".", "StackMerge "+current.get(i).getId()+" "+current.get(j).getId()+" " + choice +" 0 T;");
      Core.printToLog(me.getName() +" is looking through "+current.get(j).getDescriptor()+".");
     }
     new FieldStack(current.get(j).getCards(), current.get(j), this);
     current.remove(i);
     paged.repaint();
     paged.revalidate();
    }
   }
  }
 }

 public void mousePressed(MouseEvent e){
  if(SwingUtilities.isLeftMouseButton(e)){
   originpt = MouseInfo.getPointerInfo().getLocation();
   originpt = SwingUtilities.convertPoint(this, originpt, page.get(tabs.getSelectedIndex()));
   selected.clear();
  } 
 }
 
 public void mouseDragged(MouseEvent e) {
  Point curpt = MouseInfo.getPointerInfo().getLocation();
  curpt = SwingUtilities.convertPoint(this, curpt, page.get(tabs.getSelectedIndex()));
  Rectangle rect = null;
  try{
   rect = new Rectangle(Math.min(originpt.x,curpt.x), Math.min(originpt.y,curpt.y), Math.abs(originpt.x - curpt.x), Math.abs(originpt.y - curpt.y));
  }catch(Exception ex){
   System.out.println("Could not form Rectangle rect");
  }
  
  selected.clear();
  
  if(SwingUtilities.isLeftMouseButton(e)){
   int i;
   for(i = 0; i < images.get(tabs.getSelectedIndex()).size(); i++){
    if(images.get(tabs.getSelectedIndex()).get(i).getContainer().equals(e.getSource())
      && images.get(tabs.getSelectedIndex()).get(i).getContainer().isVisible()
      || images.get(tabs.getSelectedIndex()).get(i).getRotation().equals(e.getSource())
      && !images.get(tabs.getSelectedIndex()).get(i).getContainer().isVisible()) {
     return;
    }
    try{
     Rectangle bounds = images.get(tabs.getSelectedIndex()).get(i).getContainer().getBounds();
     //System.out.println(rect.intersects(bounds));
      
     if(rect.intersects(bounds)){
      selected.add(images.get(tabs.getSelectedIndex()).get(i));
     }
    }catch(Exception ex){
     System.out.println("Invalid rectangle");
    }
   }
  } 
 }
 
 public void mouseMoved(MouseEvent e){}

 public Vector<Vector<Stack>> getImages(){
  return images;  
 }
 
 public Vector<Stack> getRelevantImages(){
  return images.get(tabs.getSelectedIndex());
 }

 public Player getPlayer(int idx){
  return players[idx];
 }
 
 public Stack getStack(int id){
  //For now, get from tab 0.
  Vector<Stack> stacks = images.get(0);
  for(int i=0; i<images.get(0).size(); i++){
   if(stacks.get(i).getId() == id)
    return stacks.get(i);
  }
  return null;
 }
 
 public Player getPlayerById(int id){
  for(int i=0; i<players.length; i++){
   if(players[i].getId() == id) return players[i];
  }
  return null;
 }
 
 private void flip(){
  flip(images.get(tabs.getSelectedIndex()).get(contextNum).getId(),true);
 }

 public void cmdFlip(int stackID){
  flip(stackID, false);
 }
 
 private void flip(int stackID, boolean printToLog){
  Stack stack = getStack(stackID);
  if(stack.faceup){
   String orig = stack.getDescriptor();
   int origId = stack.getId();
   stack.faceup = false;
   stack.refreshImg();
   stack.flipCard.setText("Flip Face Up");
   if(printToLog) 
    Core.printToLog(orig + " was flipped face-down.", "Flip "+origId+";");
  }
  else{
   String orig = stack.getDescriptor();
   int origId = stack.getId();
   stack.faceup = true;
   stack.refreshImg();
   stack.flipCard.setText("Flip Face Down");
   if(printToLog) 
    Core.printToLog(orig + " was flipped face-up, revealing "+stack.getCards().get(stack.getCards().size()-1).getName()+".", "Flip "+origId+";");
  } 
 }
 
 private void draw(){
  draw(images.get(tabs.getSelectedIndex()).get(contextNum).getId(), true);
 }
 
 public void cmdDraw(int playerID, int stackID){
  draw(stackID, false); 
 }

 @SuppressWarnings("unused")
private void draw(int stackID, boolean printToLog){
  //PlayerID is unused at this point. In future, allow drawing of other players' stacks, with permission.
  Stack stack = getStack(stackID);
  if(stack.getPlayer() == null) return;
  String orig = stack.getDescriptor();
  int origId = stack.getId();
  if(stack.cards.size() == 1){
   int i;
   for(i = 0; i < images.get(tabs.getSelectedIndex()).size(); i++){
    if(images.get(tabs.getSelectedIndex()).get(i).getId() == stack.getId()){
     break;
    }
   }
   page.get(tabs.getSelectedIndex()).remove(images.get(tabs.getSelectedIndex()).get(i).getContainer());
   page.get(tabs.getSelectedIndex()).remove(images.get(tabs.getSelectedIndex()).get(i).getRotation());
   stack.draw();
   FieldCard temp = new FieldCard((String) "empty", (String) "transparent.gif");
   Stack stack2 = new Stack(this, "empty", (int)images.get(tabs.getSelectedIndex()).get(i).getX(), (int)images.get(tabs.getSelectedIndex()).get(i).getY());
   images.get(tabs.getSelectedIndex()).remove(i);
   images.get(tabs.getSelectedIndex()).insertElementAt(stack2, i);
   GridBagConstraints c = new GridBagConstraints();
   c.gridx = (int)stack2.getX();
   c.gridy = (int)stack2.getY();
   System.out.println(c.gridx + " " + c.gridy);
   if(c.gridy == middle)
     c.insets = new Insets(20, 20, 125, 20);
   page.get(tabs.getSelectedIndex()).add(stack2.getContainer(), c);
   page.get(tabs.getSelectedIndex()).add(stack2.getRotation(), c);
   page.get(tabs.getSelectedIndex()).revalidate();
   page.get(tabs.getSelectedIndex()).repaint();
   if(printToLog)
    Core.printToLog(stack.getPlayer().getName()+" drew a card from "+orig+".", "Draw "+stack.getPlayer().getId()+" "+origId+";");

   return;
  }
  stack.draw();
  /*FieldStack fs = stack.getFieldStack();
  if (fs!=null) fs.refresh();*/
  //Store the contents of the draw in an internal buffer.
  if(printToLog)
   Core.printToLog(stack.getPlayer().getName()+" drew a card from "+orig+".", "Draw "+stack.getPlayer().getId()+" "+origId+";");
 }
 
 private void tap(){
  tap(images.get(tabs.getSelectedIndex()).get(contextNum).getId(), true);
 }
 
 public void cmdTap(int stackID){
  tap(stackID, false);
 }
 
 private void tap(int stackID, boolean printToLog){
  Stack op = getStack(stackID);
  op.tap();
  page.get(tabs.getSelectedIndex()).repaint();
  page.get(tabs.getSelectedIndex()).revalidate();
  if(printToLog)
   Core.printToLog(op.getDescriptor()+" was tapped.", "Tap "+op.getId()+";"); 
 }
 
 public void cmdHSMove(int playerID, int handIndex, int stackID, int destIndex, boolean toTop){
  Player player = getPlayerById(playerID);
  Stack dest = getStack(stackID);
  FieldCard card = player.getHand().getCards().get(handIndex);
  if(toTop){
   dest.insertCardFromTop(card, destIndex);
  }
  else{
   dest.insertCard(card, destIndex);
  }
  player.getHand().removeCard(card);
  if (player.getHand().getHandPanel()!=null)
    player.getHand().getHandPanel().refresh();
  dest.refreshImg();
 }
 
 public void cmdSSMove(int sourceID, int sourceIndex, int destID, int destIndex, boolean toTop){
  Stack source = getStack(sourceID);
  Stack dest = getStack(destID);
  FieldCard card = source.getCards().get(sourceIndex);
  if(toTop){
   dest.insertCardFromTop(card, destIndex);
  }
  else{
   dest.insertCard(card, destIndex);
  }
  source.getCards().remove(card);
  source.refreshImg();
  dest.refreshImg();
  //Update any FieldStacks corresponding to these stacks.
 }
 
 public void cmdStackMerge(int sourceID, int destID, int choice, int destIndex, boolean toTop){
  int i;
  Stack stack = getStack(sourceID);
  for(i = 0; i < images.get(tabs.getSelectedIndex()).size(); i++)
    if(images.get(tabs.getSelectedIndex()).get(i).equals(stack))
     break;
  int j;
  stack = getStack(destID);
  for(j = 0; j < images.get(tabs.getSelectedIndex()).size(); j++)
    if(images.get(tabs.getSelectedIndex()).get(j).equals(stack))
     break;
  Vector<Stack> current = images.get(tabs.getSelectedIndex());
   JPanel paged = page.get(tabs.getSelectedIndex());
  if(choice == 2){
      //Merge Stacks on Bottom Doesn't use MoveSS because it is cleaner this way for this situation
      paged.remove(current.get(i).getContainer());
      paged.remove(current.get(i).getRotation());
      for(int k = current.get(i).cards.size()-1; k>=0; k--)
       current.get(j).addCardToBot(current.get(i).getCards().get(k));
      
      paged.repaint();
      paged.revalidate();
      current.remove(i);
     }
     else if(choice == 1){
      //Merge Stacks on Top
      paged.remove(current.get(i).getContainer());
      paged.remove(current.get(i).getRotation());
      for(int k = 0; k < current.get(i).cards.size(); k++)
       current.get(j).addCardToTop(current.get(i).getCards().get(k));
      current.get(j).refreshImg();
      
      paged.repaint();
      paged.revalidate();
      current.remove(i);
     }
     else if(choice == 3){
      //Shuffle
      paged.remove(current.get(i).getContainer());
      paged.remove(current.get(i).getRotation());
      for(int k = 0; k < current.get(i).cards.size(); k++)
       current.get(j).addCardToTop(current.get(i).getCards().get(k));

      current.get(j).shuffle();
      current.get(j).refreshImg();
      paged.repaint();
      paged.revalidate();
      current.remove(i);
     }
     else if(choice == 4){
      //nth from top
      for(int k = 0; k < current.get(i).cards.size(); k++)
       current.get(j).insertCardFromTop(current.get(i).getCards().get(k), destIndex-1);
      current.get(j).refreshImg();
      paged.remove(current.get(i).getContainer());
      paged.remove(current.get(i).getRotation());
      paged.repaint();
      paged.revalidate();
      current.remove(i);
     }
     else if(choice == 5){
      //nth from bottom
      
      for(int k = 0; k < current.get(i).cards.size(); k++)
       current.get(j).insertCard(current.get(i).getCards().get(k), destIndex-1+k);
      current.get(j).refreshImg();
      paged.remove(current.get(i).getContainer());
      paged.remove(current.get(i).getRotation());
      current.remove(i);
      paged.repaint();
      paged.revalidate();
     }
     else{
      //manually merge
      for(int k = 0; k < current.get(i).cards.size(); k++)
       current.get(j).addCardToTop(current.get(i).getCards().get(k));
      paged.remove(current.get(i).getContainer());
      paged.remove(current.get(i).getRotation());
      current.remove(i);
      paged.repaint();
      paged.revalidate();
     }
 }
 
 public void cmdFetch(int playerID, String cardName, String cardImage){
  Player player = getPlayerById(playerID);
  FieldCard card = new FieldCard(cardName, cardImage);
  player.getHand().addCard(card);
  if(player.getHand().getHandPanel() != null)
   player.getHand().getHandPanel().refresh();
 } 

 public void exchange(Stack dragStack, Stack dropStack){
  exchange(dragStack, dropStack, true);
 }
 
 public void cmdExchange(int dragStackID, int dropStackID){
  System.out.println("dragStackID "+dragStackID+", dropStackID "+dropStackID);
  exchange(getStack(dragStackID), getStack(dropStackID), false);
 }
 
 public void exchange(Stack dragStack, Stack dropStack, boolean printToLog){
   System.out.println("dragStack "+dragStack+ ", dropStack "+dropStack);
   
  JPanel paged = page.get(tabs.getSelectedIndex());
  GridBagConstraints c = new GridBagConstraints();
  Point temp = new Point((int) dragStack.getX(),(int) dragStack.getY()); //Original Location of Source
  //Swap stack's locations
  dragStack.setPosition(new Point((int) dropStack.getX(), (int) dropStack.getY()));
  dropStack.setPosition(temp);
  //Set location for Destination stack
  c.gridx = temp.x;
  c.gridy = temp.y;
  if(c.gridy == middle)
   c.insets = new Insets(20, 20, 125, 20); //Inset Artifacts will work for anything that isn't on an the middle
  //Remove graphical components of stacks
  paged.remove(dragStack.getContainer());
  paged.remove(dragStack.getRotation());
  paged.remove(dropStack.getContainer());
  paged.remove(dropStack.getRotation());
  //Add the Destination Stack
  paged.add(dropStack.getContainer(), c);
  paged.add(dropStack.getRotation(), c);

  c.gridx = (int) dragStack.getX();
  c.gridy = (int) dragStack.getY();
  
  if (c.gridy == middle)
   c.insets = new Insets(20, 20, 125, 20);
  else
   c.insets = new Insets(20, 20, 20, 20);
  //Add the Source Stack
  paged.add(dragStack.getContainer(), c);
  paged.add(dragStack.getRotation(), c);
  this.repaint();
  if(printToLog)
   Core.printToLog(dragStack.getDescriptor() + " was swapped with " + dropStack.getDescriptor() + ".", "Exchange "+dragStack.getId() + " "+dropStack.getId()+";");
 }
 
 public void setStack(int stackID, ArrayList<FieldCard> cards){
  Stack stack = getStack(stackID);
  stack.setCards(cards);
  stack.refreshImg();
 }
 
 public boolean close(){
  for(int i=0; i<players.length;i++){
  if(players[i].getHand().getHandPanel() != null){
   players[i].getHand().getHandPanel().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
   players[i].getHand().getHandPanel().setVisible(false);
   players[i].getHand().getHandPanel().dispose();
  }
  }
  return true;
 }

 public Rectangle getMaximizedBounds(){
  return maxBounds;
 }
 
 public synchronized void setMaximizedBounds(Rectangle maxBounds){
  this.maxBounds = maxBounds;
  super.setMaximizedBounds(maxBounds);
 }
 
 public synchronized void setExtenededState(int state){
  if(maxBounds == null && (state & Frame.MAXIMIZED_BOTH)==Frame.MAXIMIZED_BOTH){
   Insets screenInsets = getToolkit().getScreenInsets(getGraphicsConfiguration());
   if(screenInsets.equals(new Insets(0,0,0,0))){
    screenInsets = new Insets(20,0,20,0);
   }
   Rectangle screenSize = getGraphicsConfiguration().getBounds();
   Rectangle maxBounds = new Rectangle(screenInsets.left+screenSize.x,screenInsets.top+screenSize.y,
     screenSize.x + screenSize.width - screenInsets.right - screenInsets.left,
     screenSize.y + screenSize.height - screenInsets.bottom - screenInsets.top);
   super.setMaximizedBounds(maxBounds);
  }
  super.setExtendedState(state);
 }
 
 public void printToLog(String str){
  chatOut.append(str, FilteredChatEntry.LOG);
  Core.playSound("ding2.WAV");
 }
 
 public void printToChat(String str){
  String name = "Player";
  if(me!=null)
  name = me.getName();
  chatOut.append(str, "You ("+name+")", FilteredChatEntry.CHAT);
  Core.sendChat(str);
  Core.playSound("ding1.WAV");
 }
 
 @Override
 public void keyPressed(KeyEvent e){
  int key = e.getKeyCode();
  if(key == KeyEvent.VK_ENTER){
   printToChat(chatIn.getText());
   chatIn.setText("");
//   if(chatIn.getText().trim().equals("/log")){
//    historyMode =1;
//    chatOut.setText(logHistory);
//   }
//   else if(chatIn.getText().trim().equals("/chat")){
//    historyMode=0;
//    chatOut.setText(chatHistory);
//   }
//   else if(chatIn.getText().trim().equals("/both")){
//    historyMode=2;
//    chatOut.setText(bothHistory);
//   }
//   else{
//    
//   }
  }
 }

 @Override
 public void keyReleased(KeyEvent e){
  int key = e.getKeyCode();
  if(key == KeyEvent.VK_ENTER)
   chatIn.setText("");
 }

 @Override
 public void keyTyped(KeyEvent e){
  int key = e.getKeyCode();
  if(key == KeyEvent.VK_ENTER)
   chatIn.setText("");
 }
 public void mvLeft(Stack dragStack, Stack dropStack){
   mvLeft(dragStack.getId(), dropStack.getId(), true);
 }
 public void cmdLeftMove(int dragStackID, int dropStackID){
   mvLeft(dragStackID, dropStackID, false);
 }
 public void mvLeft(int dragStackID, int dropStackID, boolean printToLog){
   /*Stack dragStack = getStack(dragStackID);
   Stack dropStack = getStack(dropStackID);
   if(dragStack.getY() <= middle && dropStack.getY()>middle||dragStack.getY() > middle && dropStack.getY()<= middle && printToLog){
     int safety = JOptionPane.showConfirmDialog(this, "You are about to move this card to the opponent's field are you sure?", "Dangerous move", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
     if(safety == 0){
     }
     else{
       return;
     }
   }
   
   JPanel paged = page.get(tabs.getSelectedIndex());
   Vector<Stack> current = images.get(tabs.getSelectedIndex());
    int x = (int) dragStack.getX();
    int y = (int) dragStack.getY();
    GridBagConstraints c = new GridBagConstraints();
    c.gridy = (int) dropStack.getY();
    if(c.gridy == middle)
     c.insets = new Insets(20, 20, 125, 20);
    else
     c.insets = new Insets(20, 20, 20, 20);
    boolean isSame = y == dropStack.getY();
    if(x > dropStack.getX() || !isSame){
     dragStack.setX((int) (dropStack.getX() + 1));
     c.gridx = (int) (dropStack.getX() + 1);

    } 
    else{
     dragStack.setX((int) (dropStack.getX()));
     c.gridx = (int) (dropStack.getX());
    }

    paged.remove(dragStack.getContainer());
    paged.remove(dragStack.getRotation());
    paged.add(dragStack.getContainer(),c);
    paged.add(dragStack.getRotation(), c);

    dragStack.setY((int) (dropStack.getY()));

    for(int k = 0; k < current.size(); k++){
     if(current.get(k) == dragStack){
     }
     else if(current.get(k).getX() < x && current.get(k).getX() > dropStack.getX()&& isSame
       && dropStack.getY() == current.get(k).getY()||!isSame
       && dropStack.getY() == current.get(k).getY()
       && current.get(k).getX() > dropStack.getX()){
      c.gridx = (int) current.get(k).getX() + 1;
      c.gridy = (int) current.get(k).getY();
      if(c.gridy == middle)
       c.insets = new Insets(20, 20, 125, 20);
      else
       c.insets = new Insets(20, 20, 20, 20);
      paged.remove(current.get(k).getContainer());
      paged.remove(current.get(k).getRotation());
      paged.add(current.get(k).getContainer(), c);
      paged.add(current.get(k).getRotation(), c);
      current.get(k).setX((int) (current.get(k).getX() + 1));
     } 
     else if(dropStack.getY() == current.get(k).getY()
       && current.get(k).getX() > x
       && current.get(k).getX() <= dropStack.getX()
       && isSame
       || current.get(k).getY() == y
       && !isSame
       && current.get(k).getX() > x){
      c.gridx = (int) current.get(k).getX() - 1;
      c.gridy = (int) current.get(k).getY();
      if(c.gridy == middle)
       c.insets = new Insets(20, 20, 125, 20);
      else
       c.insets = new Insets(20, 20, 20, 20);
      paged.remove(current.get(k).getContainer());
      paged.remove(current.get(k).getRotation());
      paged.add(current.get(k).getContainer(), c);
      paged.add(current.get(k).getRotation(), c);
      current.get(k).setX((int) (current.get(k).getX() - 1));
     }
    }
    page.get(tabs.getSelectedIndex()).repaint();
    page.get(tabs.getSelectedIndex()).revalidate();
    System.out.println("Left Reference");
    System.out.println("Position: ("+dragStack.getX()+","+dragStack.getY()+")");
    if (printToLog){
      Core.printToLog(dragStack.getDescriptor() + " was exchanged with " + dropStack.getDescriptor() + ", shifting everything to its left over a space.", "LeftMove "+dragStack.getId()+" "+dropStack.getId()+";");
    }*/
 }
 public void mvUp(Stack dragStack, Stack dropStack){
   mvUp(dragStack.getId(), dropStack.getId(), true);
 }
 public void cmdUpMove(int dragStackID, int dropStackID){
   mvUp(dragStackID, dropStackID, false);
 }
 public void mvUp(int dragStackID, int dropStackID, boolean printToLog){
   /*Stack dragStack = getStack(dragStackID);
   Stack dropStack = getStack(dropStackID);
   JPanel paged = page.get(tabs.getSelectedIndex());
   Vector<Stack> current = images.get(tabs.getSelectedIndex());
   boolean empty = true;
   for(int l = 0; l < current.size()&& l > -1; l++){
     if(current.get(l).getX() == dropStack.getX() && dropStack.getY()+1 == current.get(l).getY()){
       empty = false;
       break;
     }
    }
    GridBagConstraints c = new GridBagConstraints();
    c.gridx = (int) (dropStack.getX());
    c.gridy = (int) dropStack.getY() + 1;
    int y = (int) dragStack.getY();
    int x = (int) dragStack.getX();
    dragStack.setX((int) (dropStack.getX())); 
    dragStack.setY((int) dropStack.getY()+1);
    if(dragStack.getY() <= middle && dropStack.getY()+1>middle && printToLog){
     //Trying to move card from one player's field to another expand upper field
     int safety = JOptionPane.showConfirmDialog(this, "You are about to move this card to the opponent's field are you sure?", "Dangerous move", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
     if(safety == 0){
     }
     else{
      return;
     }
    }
    else if(dragStack.getY() > middle && dropStack.getY()+1<= middle  && printToLog){
     int safety = JOptionPane.showConfirmDialog(this, "You are about to move this card to the opponent's field are you sure?", "Dangerous move", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
     if(safety == 0){
      if(!empty)
       middle++;
     }
     else{
      return;
     }
    }
    else if(dragStack.getY() <= middle && !empty){
     middle++;
    }
    if(c.gridy == middle)
     c.insets = new Insets(20, 20, 125, 20);
    else
     c.insets = new Insets(20, 20, 20, 20);
    paged.remove(dragStack.getContainer());
    paged.remove(dragStack.getRotation());
    paged.add(dragStack.getContainer(), c);
    paged.add(dragStack.getRotation(),c);
    for(int k = 0; k < current.size(); k++){
      if(current.get(k).getY() == y && current.get(k).getX() > x && current.get(k)!=dragStack){
        c.gridx = (int) current.get(k).getX() - 1;
        c.gridy = (int) current.get(k).getY();
        if(c.gridy == middle)
          c.insets = new Insets(20, 20, 125, 20);
        else
          c.insets = new Insets(20, 20, 20, 20);
        paged.remove(current.get(k).getContainer());
        paged.remove(current.get(k).getRotation());
        paged.add(current.get(k).getContainer(), c);
        paged.add(current.get(k).getRotation(), c);
        current.get(k).setX((int) (current.get(k).getX() - 1));
      }
      if(current.get(k)==dragStack){
      }
      else if(current.get(k).getY() >= dragStack.getY() && !empty){
        current.get(k).setY((int) (current.get(k).getY()+1));
        c.gridy = (int) current.get(k).getY();
        c.gridx = (int) current.get(k).getX();
        if(c.gridy == middle)
          c.insets = new Insets(20, 20, 125, 20); 
        else
          c.insets = new Insets(20, 20, 20, 20);
        paged.remove(current.get(k).getContainer());
        paged.remove(current.get(k).getRotation());
        paged.add(current.get(k).getContainer(), c);
        paged.add(current.get(k).getRotation(),c);
      }
    }
    page.get(tabs.getSelectedIndex()).repaint();
    page.get(tabs.getSelectedIndex()).revalidate();
    System.out.println("Reference Up");
    System.out.println("Position: ("+dragStack.getX()+","+dragStack.getY()+")");
    if (printToLog){
      Core.printToLog(dragStack.getDescriptor() + " was exchanged with " + dropStack.getDescriptor() + ", shifting everything above it up a row.", "UpMove "+dragStack.getId()+" "+dropStack.getId()+";");
    }*/
 }
 public void mvRight(Stack dragStack, Stack dropStack){
   mvRight(dragStack.getId(), dropStack.getId(), true);
 }
 public void cmdRightMove(int dragStackID, int dropStackID){
   mvRight(dragStackID, dropStackID, false);
 }
 public void mvRight(int dragStackID, int dropStackID, boolean printToLog){
   /*Stack dragStack = getStack(dragStackID);
   Stack dropStack = getStack(dropStackID);
      JPanel paged = page.get(tabs.getSelectedIndex());
   Vector<Stack> current = images.get(tabs.getSelectedIndex());
   if(dragStack.getY() <= middle && dropStack.getY()>middle||dragStack.getY() > middle && dropStack.getY()<= middle && printToLog){
     int safety = JOptionPane.showConfirmDialog(this, "You are about to move this card to the opponent's field are you sure?", "Dangerous move", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
     if(safety == 0){
     }
     else{
       return;
     }
   }
   int x = (int) dragStack.getX();
   int y = (int) dragStack.getY();
   GridBagConstraints c = new GridBagConstraints();
   c.gridy = (int) dropStack.getY();
   if(c.gridy == middle)
     c.insets = new Insets(20, 20, 125, 20);
   else
     c.insets = new Insets(20, 20, 20, 20);
   if(dropStack.getX() != 0){
     dragStack.setX((int) (dropStack.getX() - 1));
     c.gridx = (int) (dropStack.getX() - 1);
   }
   else{
     dragStack.setX(0);
     c.gridx = 0;
   }
   
   
   paged.remove(dragStack.getContainer());
   paged.remove(dragStack.getRotation());
   paged.add(dragStack.getContainer(),c);
   paged.add(dragStack.getRotation(), c);
   dragStack.setY((int) (dropStack.getY()));
   boolean empty = true;
   System.out.println("Reference Right");
   System.out.println("Position: ("+dragStack.getX()+","+dragStack.getY()+")");
   for(int l = 0; l < current.size()&& l > -1; l++){
     if(current.get(l).getX() == dragStack.getX() && dragStack.getY() == current.get(l).getY() &&current.get(l)!=dragStack){
       empty = false;
       break;
     }
   }
   for(int k = 0; k < current.size(); k++){
     if(current.get(k).getY() == y && current.get(k).getX() > x && current.get(k)!=dragStack){
       c.gridx = (int) current.get(k).getX() - 1;
       c.gridy = (int) current.get(k).getY();
       if(c.gridy == middle)
         c.insets = new Insets(20, 20, 125, 20);
       else
         c.insets = new Insets(20, 20, 20, 20);
       paged.remove(current.get(k).getContainer());
       paged.remove(current.get(k).getRotation());
       paged.add(current.get(k).getContainer(), c);
       paged.add(current.get(k).getRotation(), c);
       current.get(k).setX((int) (current.get(k).getX() - 1));
     }
     if(!empty && current.get(k)!=dragStack && dragStack.getY() == current.get(k).getY()&& current.get(k).getX() >= dragStack.getX()){
       c.gridx = (int) current.get(k).getX() + 1;
       c.gridy = (int) current.get(k).getY();
       if(c.gridy == middle)
         c.insets = new Insets(20, 20, 125, 20);
       else
         c.insets = new Insets(20, 20, 20, 20);
       paged.remove(current.get(k).getContainer());
       paged.remove(current.get(k).getRotation());
       paged.add(current.get(k).getContainer(), c);
       paged.add(current.get(k).getRotation(), c);
       current.get(k).setX((int) (current.get(k).getX() + 1));
     }
   } 
   page.get(tabs.getSelectedIndex()).repaint();
   page.get(tabs.getSelectedIndex()).revalidate();
   if (printToLog){
      Core.printToLog(dragStack.getDescriptor() + " was exchanged with " + dropStack.getDescriptor() + ", shifting everything to its right over a space.", "RightMove "+dragStack.getId()+" "+dropStack.getId()+";");
   }*/
 }
 public void mvDown(Stack dragStack, Stack dropStack){
   mvDown(dragStack.getId(), dropStack.getId(), true);
 }
 public void cmdDownMove(int dragStackID, int dropStackID){
   mvDown(dragStackID, dropStackID, false);
 }
 public void mvDown(int dragStackID, int dropStackID, boolean printToLog){
   /*Stack dragStack = getStack(dragStackID);
   Stack dropStack = getStack(dropStackID);
   JPanel paged = page.get(tabs.getSelectedIndex());
   Vector<Stack> current = images.get(tabs.getSelectedIndex());
      if(dragStack.getY() <= middle && dropStack.getY()>middle||dragStack.getY() > middle && dropStack.getY()<= middle && printToLog){
     int safety = JOptionPane.showConfirmDialog(this, "You are about to move this card to the opponent's field are you sure?", "Dangerous move", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
     if(safety == 0){
     }
     else{
      return;
     }
    }
    int x = (int) dragStack.getX();
    int y = (int) dragStack.getY();
    GridBagConstraints c = new GridBagConstraints();
    c.gridx = (int) dropStack.getX();
    dragStack.setX((int) (dropStack.getX()));
    if(dropStack.getY() != 0 && dropStack.getY() != middle+1){
     dragStack.setY((int) (dropStack.getY() - 1));
     c.gridx = (int) (dropStack.getY() - 1);
    }
    else if(dropStack.getY() != 0){
     dragStack.setY(middle+1);
     c.gridy = middle+1;
    }
    else{
     dragStack.setY(0);
     c.gridy = 0;
    }
    if(c.gridy == middle)
     c.insets = new Insets(20, 20, 125, 20);
    else
     c.insets = new Insets(20, 20, 20, 20);
    System.out.println("Reference Down");
    System.out.println("Position: ("+dragStack.getX()+","+dragStack.getY()+")");
    System.out.println("GridX & Y: (" + c.gridx + "," + c.gridy + ")");
    paged.remove(dragStack.getContainer());
    paged.remove(dragStack.getRotation());
    paged.add(dragStack.getContainer(),c);
    paged.add(dragStack.getRotation(), c);
    boolean empty = true;
    
    for(int l = 0; l < current.size()&& l > -1; l++){
     if(current.get(l).getX() == dragStack.getX() && dragStack.getY() == current.get(l).getY() &&current.get(l)!=dragStack){
      empty = false;
      break;
     }
    }
    if(!empty&&dragStack.getY()==0)
     middle++;
   
    for(int k = 0; k < current.size(); k++){
     if(current.get(k).getY() == y && current.get(k).getX() >= x && current.get(k)!=dragStack){
      c.gridx = (int) current.get(k).getX() - 1;
      c.gridy = (int) current.get(k).getY();
      if(c.gridy == middle)
        c.insets = new Insets(20, 20, 125, 20);
      else
        c.insets = new Insets(20, 20, 20, 20);
      paged.remove(current.get(k).getContainer());
      paged.remove(current.get(k).getRotation());
      paged.add(current.get(k).getContainer(), c);
      paged.add(current.get(k).getRotation(), c);
      current.get(k).setX((int) (current.get(k).getX() - 1));
     }
     if(current.get(k).getY()>= dragStack.getY()&&current.get(k)!=dragStack && !empty &&(dragStack.getY()==0||dragStack.getY()==middle+1)){
       current.get(k).setY((int) (current.get(k).getY()+1));
       c.gridy = (int) current.get(k).getY();
       c.gridx = (int) current.get(k).getX();
       if(c.gridy == middle)
         c.insets = new Insets(20, 20, 125, 20);
       else
         c.insets = new Insets(20, 20, 20, 20);
       paged.remove(current.get(k).getContainer());
       paged.remove(current.get(k).getRotation());
       paged.add(current.get(k).getContainer(), c);
       paged.add(current.get(k).getRotation(),c);
     } 
    }
    page.get(tabs.getSelectedIndex()).repaint();
    page.get(tabs.getSelectedIndex()).revalidate();
    if (printToLog){
      Core.printToLog(dragStack.getDescriptor() + " was exchanged with " + dropStack.getDescriptor() + ", shifting everything below it down a row.", "DownMove "+dragStack.getId()+" "+dropStack.getId()+";");
    }*/
 }
 public Player getMe(){
   return me;
 }
 
 public FilteredTextArea getTextArea(){
  return chatOut;
 }
 public void spawn(FieldCard zygote, int x, int y, int j, Player p, int check, int sourceID){ //check==1 indicates face down, 0 face up
   spawn(zygote.getName(), zygote.getImagePath(), x, y, j, p.getId(), sourceID,check==0, true);
 }
 public void cmdSpawn(String cardName, String cardImage, int x, int y, int stackID, int playerID, int sourceID, boolean faceup){
   spawn(cardName, cardImage, x, y, stackID, playerID, sourceID, faceup, false);
 }
 public void spawn(String cardName, String cardImage, int x, int y, int stackID, int playerID, int sourceID, boolean faceup, boolean printToLog){
   System.out.println("j "+stackID);
   int origStackID = stackID;
   if(stackID != -1)
    stackID = images.get(tabs.getSelectedIndex()).get(stackID).getId();
   int check = 0;
   if (!faceup) check =1;
   Player p = getPlayerById(playerID);
   Stack stack = new Stack((GenericUI) this, new FieldCard(cardName, cardImage), x, y, p, check);
   p.addStack(stack);
   GridBagConstraints c = new GridBagConstraints();
   c.gridx = x;
   c.gridy = y;
   if(c.gridy == middle)
     c.insets = new Insets(20, 20, 125, 20);
   else
     c.insets = new Insets(20, 20, 20, 20);
   Stack source = null;
   if(stackID != -1){
     source = getStack(stackID);
     System.err.println(source.getCards().get(0).getName());
     page.get(tabs.getSelectedIndex()).remove(source.getContainer());
     page.get(tabs.getSelectedIndex()).remove(source.getRotation());
     images.get(tabs.getSelectedIndex()).remove(source);
   }
   if(sourceID!=-1){
    Stack sourced = getStack(sourceID);
    if(sourced.getCards().size()-1==0){
     page.get(tabs.getSelectedIndex()).remove(sourced.getContainer());
     page.get(tabs.getSelectedIndex()).remove(sourced.getRotation());
     images.get(tabs.getSelectedIndex()).remove(sourced);
    }
   }
   stack.getRotation().addMouseListener(this);
   stack.getContainer().addMouseListener(this);
   images.get(tabs.getSelectedIndex()).add(stack);
   page.get(tabs.getSelectedIndex()).add(stack.getContainer(),c);
   page.get(tabs.getSelectedIndex()).add(stack.getRotation(),c);
   System.err.printf("Inserting New Card At: %d, %d\n", x,y);
   page.get(tabs.getSelectedIndex()).revalidate();
   page.get(tabs.getSelectedIndex()).repaint();
   String face = "N";
   if (faceup) face = "Y";
   String name = cardName.replace(' ','`');
   if (printToLog) Core.printToLog(cardName + " was moved from this source to a new stack of its own.", "Spawn "+name+" "+cardImage+" "+x+" "+y+" "+origStackID+" "+playerID+" "+sourceID+" "+face+";");
 }
 public String getDeck(){
   return decks[0][me.getId() - 1];
 }
 public int getHandSize(){
   return handSize;
 }
 public JDesktopPane getDesktop(){
   return desktop;
 }
 public void cmdReveal(ArrayList<FieldCard> cards){
   new FieldStack(cards, null, null);
 }
}
