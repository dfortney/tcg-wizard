import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import javax.swing.event.*;
import java.util.ArrayList;
import java.awt.event.*;
import javax.swing.*;

import java.util.*;

public class HandPanel extends JInternalFrame implements MouseMotionListener, ActionListener, MouseListener, InternalFrameListener{
 private static final long serialVersionUID = 1L;
int stackSize;
 int windowWidth;
 int windowHeight;
 int playerID;
 int[] cardSize;
 int[] imageSize;
 int offset;
 private JLayeredPane layeredPane;
 private JPopupMenu rightClick;
 private JMenuItem revealCard;
 boolean oneClicked;
 private JLabel[] labels;
 private int[] origIds;
 private JLabel rightClickCard;
 JScrollPane scroll;
 int first;
 int add;
 SandboxUI sui;
 private int tab;
 Hand hand;
 int dragIdx = -1;
 //Stack sta;

 /*public void updateCards(ArrayList<FieldCard> cards) {
  if(sta == null)return;
  sta.setCards(cards);
  sta.refreshImg();
  System.out.println("closing");
 }*/

 public HandPanel(Hand hand, int playerID, SandboxUI sui, int tab) {
  //sta = s; Formerly, s was an argument of type Stack
  super("Player "+playerID+"'s Hand", true, false, true, true);
  if (sui.getMe()!=null){
    try {
      this.setMaximum(true);
    } catch (Exception e) {
      // Vetoed by internalFrame
      // ... possibly add some handling for this case
    }
  }
  add = 0;
  this.sui = sui;
  this.tab = tab;
  this.playerID = playerID;
  this.hand = hand;
  rightClick = new JPopupMenu();
  revealCard = new JMenuItem("Reveal card.");
  revealCard.addActionListener(this);
  rightClick.add(revealCard);

  this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
  refresh();
  sui.getDesktop().add(this);
  this.pack();
 }
 //public void setCards(ArrayList<FieldCard> cardIn){
   /*System.out.println("Setting cards");
   System.out.println(cardIn);
   new HandPanel(cardIn, 0);*/
   /*cards = cardIn;
   refresh();
 }*/
 private void indicateClosed(){
   this.hand.setHpOpen(false);
 }
 public void refresh(){
   JComponent newContentPane = makeFieldStack();
   this.setPreferredSize(new Dimension(windowWidth+20, (int)(395*.75 + 20)));
   newContentPane.setOpaque(true); // content panes must be opaque
   this.setContentPane(newContentPane);
   //this.setBounds(10,10,10,10);
   //sui.getDesktop().add(this);
   // Display the window.
   if (sui.getMe()!=null) {
     Core.sendHandInfo(playerID, sui.getPlayerById(playerID).getName()+"'s hand was updated accordingly.");
     try {
       this.setMaximum(true);
     } catch (Exception e) {
       // Vetoed by internalFrame
       // ... possibly add some handling for this case
     }
   }else{
     this.pack();
   }
     this.setVisible(true); 
 }

 public JComponent makeFieldStack() {
  first = -1;
  oneClicked = false;
  labels = new JLabel[hand.getCards().size()];
  origIds = new int[hand.getCards().size()];
  windowHeight = 345;
  Point origin = new Point(10, 20);
  offset = (int)(223*.75);
  if (origin.x + offset * hand.getCards().size() + 300 > (int)(223*4*.75)) {
   windowWidth = (int)(223*4*.75);
   add = 26;
  } else
   windowWidth = origin.x + offset * hand.getCards().size() + 15;
  stackSize = hand.getCards().size();
  // Create and set up the layered pane.
  layeredPane = new JLayeredPane();
  layeredPane.setPreferredSize(new Dimension(origin.x + offset * hand.getCards().size()+ 20, (int)(windowHeight*.75)));
  layeredPane.setBorder(BorderFactory.createTitledBorder("Stack"));
  // This is the origin of the first label added.

  // This is the offset for computing the origin for the next label.

  // Add overlapping labels to the layered pane
  // using absolute positioning/sizing.

  for (int i = 0; i < hand.getCards().size(); i++) {
   labels[i] = createCardLabel("Card", hand.getCards().get(i).getRaw(), origin);
   labels[i].addMouseListener(new RolloverListener());
   labels[i].addMouseMotionListener(this);
   labels[i].addMouseListener(this);
   origIds[i] = i;
   layeredPane.add(labels[i], new Integer(i));
   // origin.x += offset;
   origin.x += offset;
  }

  // add(Box.createRigidArea(new Dimension(0, 10)));
  // add(Box.createRigidArea(new Dimension(0, 10)));
  scroll = new JScrollPane(layeredPane);
  if (origin.y + 310 > 700) {
   scroll.setPreferredSize(new Dimension(263, 700));
  } else
   scroll.setPreferredSize(new Dimension(263, origin.y + (int)(330*.75)));
  return scroll;
 }

 private JLabel createCardLabel(String text, BufferedImage color,
   Point origin) {
  JLabel label = new JLabel();
  label.setVerticalAlignment(JLabel.TOP);
  label.setHorizontalAlignment(JLabel.CENTER);
  label.setOpaque(true);
  label.setIcon(new ImageIcon(((Image) color).getScaledInstance((int)(223*.75), (int)(310*.75),
    0)));
  label.setForeground(Color.black);
  label.setBorder(BorderFactory.createLineBorder(Color.black));
  label.setBounds(origin.x, origin.y, (int)(223*.75), (int)(310*.75));
  return label;
 }

 class RolloverListener extends MouseAdapter {
  public boolean clicked = false;
  public int layerNum;

  public void setClicked(Boolean b) {
   clicked = b;
   repaint();
  }

  public void mouseEntered(MouseEvent e) {
   
   if (!clicked)
    ((JLabel) e.getComponent()).setBorder(BorderFactory
      .createLineBorder(Color.green, 2));
   repaint();
  }

  public void mouseExited(MouseEvent e) {
   if (!clicked) {
    ((JLabel) e.getComponent()).setBorder(BorderFactory
      .createEmptyBorder());
    repaint();
   }
  }

  public void mouseClicked(MouseEvent e) {
    if(SwingUtilities.isLeftMouseButton(e)){
      if (clicked)
        clicked = false;
      else
        clicked = true;
      ((JLabel) e.getComponent()).setBorder(BorderFactory.createLineBorder(Color.red, 2));
      repaint();
      exchange((JLabel) e.getComponent());
    }
    else if(SwingUtilities.isRightMouseButton(e)){
      rightClickCard = (JLabel)e.getComponent();
      rightClick.show(e.getComponent(), (int) e.getX(), (int) e.getY());
    }
  }
 }
 
 public void actionPerformed(ActionEvent e){
   int j = 0;
   ArrayList<FieldCard> temp = new ArrayList<FieldCard>();
   for (int i = 0; i < labels.length; i++) {
     if (rightClickCard == labels[i])
       j = i;
   }
   temp.add(hand.getCards().get(j));
   new FieldStack(temp, null, null);
   Core.sendRevealInfo(temp, sui.getPlayer(playerID-1).getName() + " revealed "+temp.get(0).getName()+" from their hand.");
 }
 
 public void exchange(JLabel f) {
   int j = 0;
   for (int i = 0; i < labels.length; i++) {
     if (f == labels[i])
       j = i;
   }
  if (oneClicked) {
   RolloverListener e = (RolloverListener) labels[j].getMouseListeners()[0];
   int t = origIds[first];
   origIds[first] = origIds[j];
   origIds[j] = t;
   System.err.println(origIds[j] + " swapped with " + origIds[first] + " (" + j + "," + first + ")");
   FieldCard ctemp = hand.getCards().get(origIds[first]);
   hand.getCards().set(origIds[first], hand.getCards().get(origIds[j]));
   hand.getCards().set(origIds[j], ctemp);
   int temp = JLayeredPane.getLayer(labels[first]);
   layeredPane.setLayer(labels[first], JLayeredPane.getLayer(labels[j]));
   layeredPane.setLayer(labels[j], temp);
   Point temp2 = new Point(labels[first].getLocation());
   labels[first].setLocation(new Point(labels[j].getLocation()));
   labels[j].setLocation(temp2);
   labels[j].setBorder(BorderFactory.createEmptyBorder());
   labels[first].setBorder(BorderFactory.createEmptyBorder());
   e.setClicked(false);
   e = (RolloverListener) labels[first].getMouseListeners()[0];
   e.setClicked(false);
   oneClicked = false;
   
   
   //swap origIds

   
   
   first = -1;
   
   if (sui.getMe()!=null) Core.sendHandInfo(playerID, sui.getPlayerById(playerID).getName()+"'s hand was updated accordingly.");
  } else
   oneClicked = true;
  first = j;
 }
 public JLabel[] getLabels(){
   return labels;
 }
 public JLabel getDraggedLabel(){
   if (dragIdx<0) return null;
   return labels[dragIdx];
 }

 public static void main(String[] args) {
  /*ArrayList<FieldCard> j = new ArrayList<FieldCard>();
  j.add(new FieldCard("Skullcrack.jpg"));
  j.add(new FieldCard("Anduin.jpeg"));
  
  j.add(new FieldCard("AEtherize.jpg"));
  j.add(new FieldCard("AcidicSlime.jpg"));
  j.add(new FieldCard("CathedralOfWar.jpg"));
  j.add(new FieldCard("Rubblehulk.jpg"));
  j.add(new FieldCard("DeathpactAngel.jpg"));
  j.add(new FieldCard("Skullcrack.jpg"));
  j.add(new FieldCard("Anduin.jpeg"));

  j.add(new FieldCard("AEtherize.jpg"));
  j.add(new FieldCard("AcidicSlime.jpg"));
  j.add(new FieldCard("CathedralOfWar.jpg"));
  j.add(new FieldCard("Rubblehulk.jpg"));
  j.add(new FieldCard("DeathpactAngel.jpg"));
  j.add(new FieldCard("Skullcrack.jpg"));
  j.add(new FieldCard("Anduin.jpeg"));
  j.add(new FieldCard("AEtherize.jpg"));
  j.add(new FieldCard("AcidicSlime.jpg"));
  j.add(new FieldCard("CathedralOfWar.jpg"));
  j.add(new FieldCard("Rubblehulk.jpg"));
  j.add(new FieldCard("DeathpactAngel.jpg"));
  j.add(new FieldCard("Skullcrack.jpg"));

  j.add(new FieldCard("Anduin.jpeg"));
  j.add(new FieldCard("AEtherize.jpg"));
  j.add(new FieldCard("AcidicSlime.jpg"));
  j.add(new FieldCard("CathedralOfWar.jpg"));
  j.add(new FieldCard("Rubblehulk.jpg"));
  j.add(new FieldCard("DeathpactAngel.jpg"));
  */

  //HandPanel f = new HandPanel(j,1);

 }
 @SuppressWarnings("unused")
private boolean canImport(){
   
   if (tab<0) return false;
   PointerInfo pi = MouseInfo.getPointerInfo();
   Point release = pi.getLocation();
   SwingUtilities.convertPointFromScreen(release, scroll);
   int j;
   for (j = 0; j < sui.getImages().get(tab).size(); j++) {
     if (sui.getImages()
           .get(tab)
           .get(j)
           .getContainer()
           .contains(
                     SwingUtilities.convertPoint(sui, release,
                                                 sui.getImages().get(tab)
                                                   .get(j).getContainer()))
           && sui.getImages().get(tab).get(j)
           .getContainer().isVisible()
           || sui.getImages()
           .get(tab)
           .get(j)
           .getRotation()
           .contains(
                     SwingUtilities.convertPoint(
                                                 sui,
                                                 release,
                                                 sui.getImages().get(
                                                                     tab)
                                                   .get(j).getRotation()))
           && !sui.getImages().get(tab).get(j)
           .getContainer().isVisible()) {
       return true;
     } else if (j + 1 == sui.getImages().get(tab).size()) {
       Point mover = MouseInfo.getPointerInfo().getLocation();
       //Check if valid location for new stack. For now, assume false.  
       return false;
     }
   }
   return false;
 }
 public void mouseDragged(MouseEvent e){
   for (int i=0; i<labels.length; i++){
     if (e.getSource()==labels[i]){
       if (dragIdx!=i) System.out.println("dragIdx = "+i);
       dragIdx=i;
     }
   }
 }
 public void mouseMoved(MouseEvent e){
 }
 public void mousePressed(MouseEvent e){
 }
 
 public void mouseReleased(MouseEvent e){
  if(dragIdx==-1)
   return;
  Vector<Stack> images = sui.getRelevantImages();
  PointerInfo pi = MouseInfo.getPointerInfo();
  Point drop = pi.getLocation();
  SwingUtilities.convertPointFromScreen(drop, sui.getSelectedPanel()); //It may be that we want the containing scrollpane, not the pane
  int dropIdx = -1;
  int i;
  for(i=0; i<images.size() && i >= 0; i++){
   JLabel container = images.get(i).getContainer();
   if(container.contains(SwingUtilities.convertPoint(sui.getSelectedPanel(), drop, container))){
    dropIdx = i;
    break;
   }
   else if(i+1 == images.size()){
    i = -2;
   }
  }
  int origDragIdx = origIds[dragIdx]; //Temp storage of dragIdx, which we'll need to reset to -1 now.
  System.out.println("DragID = " + origDragIdx);
  dragIdx=-1;

  //Handle each of the six cases
  FieldCard card = hand.getCards().get(origDragIdx);
  boolean empty = (i == -1);
  if(i!=-1)
   empty = images.get(i).getCards().get(0).getName().equals("empty");
  if(empty){
   SwingUtilities.convertPointToScreen(drop, sui.getSelectedPanel());
   SwingUtilities.convertPointFromScreen(drop, sui);
   int check = sui.spawnStack(card,drop,hand.getPlayer(), -1);
   if(check == 0){
    hand.removeCard(hand.getCards().get(origDragIdx));
    refresh();
    return;
   }
   else if(check != -2){
    return;
   }
  }
  if(dropIdx < 0)
   return;
  String[] possibleValues = {"Put on top", "Put on bottom", "Shuffle in", "Insert nth from top", "Insert nth from bottom", "Insert manually"};
  String selectedValue = (String) JOptionPane.showInputDialog(null,
                                                                 "Choose an option", "Card Movement",
                                                                 JOptionPane.INFORMATION_MESSAGE, null, possibleValues,
                                                                 possibleValues[0]);
  if (selectedValue == null) return;
  if (selectedValue.equals("Put on top")){
   images.get(dropIdx).addCardToTop(card);
   images.get(dropIdx).refreshImg();
   Core.printToLog(hand.getPlayer().getName() + " moved a card from his hand to the top of "+images.get(dropIdx).getDescriptor()+".", "HSMove "+hand.getPlayer().getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 0 T;");
     }else if (selectedValue.equals("Put on bottom")){
       images.get(dropIdx).addCardToBot(card);
       images.get(dropIdx).refreshImg();
       Core.printToLog(hand.getPlayer().getName() + " moved a card from his hand to the bottom of "+images.get(dropIdx).getDescriptor()+".", "HSMove "+hand.getPlayer().getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 0 B;");
     }else if (selectedValue.equals("Shuffle in")){
       images.get(dropIdx).addCardToTop(card);
       images.get(dropIdx).shuffle();
       Core.printToLog(hand.getPlayer().getName() + " moved a card from his hand to the top of "+images.get(dropIdx).getDescriptor()+".", "HSMove "+hand.getPlayer().getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 0 T;");
       Core.printToLog(images.get(dropIdx).getDescriptor()+" was shuffled.", "Shuffle "+images.get(dropIdx).getId()+";");
     }else if (selectedValue.equals("Insert nth from top")){
       int n;
       String result = JOptionPane.showInputDialog("Enter index from top. (On top is 1, below the top card is 2, etc.)");
       if (result==null) return;
       try { 
         Integer.parseInt(result); 
       } catch(NumberFormatException ex) { 
         JOptionPane.showMessageDialog(this, "Must enter an integer.", "Bad Input", JOptionPane.INFORMATION_MESSAGE);
         return;
       }
       n = Integer.parseInt(result);
       images.get(dropIdx).insertCardFromTop(card, n-1);
       images.get(dropIdx).refreshImg();
       if (n<=1) 
         Core.printToLog(hand.getPlayer().getName() + " moved a card from his hand to the top of "+images.get(dropIdx).getDescriptor()+".", "HSMove "+hand.getPlayer().getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 0 T;");
       else if (n==2)
         Core.printToLog(hand.getPlayer().getName() + " moved a card from his hand to "+images.get(dropIdx).getDescriptor()+", 2nd from the top.", "HSMove "+hand.getPlayer().getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 1 T;");
       else if (n==3)
         Core.printToLog(hand.getPlayer().getName() + " moved a card from his hand to "+images.get(dropIdx).getDescriptor()+", 3rd from the top.", "HSMove "+hand.getPlayer().getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 2 T;");
       else
         Core.printToLog(hand.getPlayer().getName() + " moved a card from his hand to "+images.get(dropIdx).getDescriptor()+", "+n+"th from the top.", "HSMove "+hand.getPlayer().getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" "+(n-1)+" T;");
     }else if (selectedValue.equals("Insert nth from bottom")){
       int n;
       String result = JOptionPane.showInputDialog("Enter index from bottom. (On bottom is 1, above the bottom card is 2, etc.)");
       if (result==null) return;
       try { 
         Integer.parseInt(result); 
       } catch(NumberFormatException ex) { 
         JOptionPane.showMessageDialog(this, "Must enter an integer.", "Bad Input", JOptionPane.INFORMATION_MESSAGE);
         return;
       }
       n = Integer.parseInt(result);
       images.get(dropIdx).insertCard(card, n-1);
       images.get(dropIdx).refreshImg();
       if (n<=1) 
         Core.printToLog(hand.getPlayer().getName() + " moved a card from his hand to the bottom of "+images.get(dropIdx).getDescriptor()+".", "HSMove "+hand.getPlayer().getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 0 B;");
       else if (n==2)
         Core.printToLog(hand.getPlayer().getName() + " moved a card from his hand to "+images.get(dropIdx).getDescriptor()+", 2nd from the bottom.", "HSMove "+hand.getPlayer().getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 1 B;");
       else if (n==3)
         Core.printToLog(hand.getPlayer().getName() + " moved a card from his hand to "+images.get(dropIdx).getDescriptor()+", 3rd from the bottom.", "HSMove "+hand.getPlayer().getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 2 B;");
       else
         Core.printToLog(hand.getPlayer().getName() + " moved a card from his hand to "+images.get(dropIdx).getDescriptor()+", "+n+"th from the bottom.", "HSMove "+hand.getPlayer().getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" "+(n-1)+" B;");
     }else if (selectedValue.equals("Insert manually")){
       images.get(dropIdx).addCardToTop(card);
       new FieldStack(images.get(dropIdx).getCards(), images.get(dropIdx), sui);
       Core.printToLog(hand.getPlayer().getName() + " moved a card from his hand to the top of "+images.get(dropIdx).getDescriptor()+".", "HSMove "+hand.getPlayer().getId()+" "+origDragIdx+" "+images.get(dropIdx).getId()+" 0 T;");
       Core.printToLog(hand.getPlayer().getName() +" is looking through "+images.get(dropIdx).getDescriptor()+".");
     }
   //In any case, remove the card from the hand.
   hand.removeCard(hand.getCards().get(origDragIdx));
   refresh();
 }
 
 public void mouseClicked(MouseEvent e){
 }
 public void mouseEntered(MouseEvent e){
 }
 public void mouseExited(MouseEvent e){
 }
 public void internalFrameClosing(InternalFrameEvent e) {
   indicateClosed();
 } 
 public void internalFrameClosed(InternalFrameEvent e) {   
 } 
 public void internalFrameOpened(InternalFrameEvent e) {   
 }
 public void internalFrameIconified(InternalFrameEvent e) {   
 } 
 public void internalFrameDeiconified(InternalFrameEvent e) {
 } 
 public void internalFrameActivated(InternalFrameEvent e) {
 } 
 public void internalFrameDeactivated(InternalFrameEvent e) {
 }
}