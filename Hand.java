import java.util.*;
public class Hand {
  private ArrayList<FieldCard> cards; //the card objects in hand
  private HandPanel hp;
  private Player p;
  private SandboxUI sui;
  private int tab;
  private boolean hpOpen = false;
  public Hand(SandboxUI sui, int tab){
    this.sui = sui;
    this.tab = tab;
    cards = new ArrayList<FieldCard>();
  }
  public void setPlayer(Player p){
    //Should only be called by Player.
    this.p=p;
  }
  public Player getPlayer(){
    return p;
  }
  
  //This getCards method reserved for special cases where random selection is needed.
  public FieldCard[] getCards(int n, boolean remove){
    if (n<=0) return null;
    Random rand = new Random();
    if (n>cards.size()){
      n=cards.size();
    }
    FieldCard[] ret = new FieldCard[n];
    for (int i=0; i<n; i++){
      int randomInt = rand.nextInt(cards.size());
      ret[i] = cards.get(randomInt);
      removeCard(ret[i]);
    }
    
    //If we're not removing, put the selected cards back.
    if (!remove){
      for (int i=0; i<n; i++){        
        addCard(ret[i]);
      }
    }
    return ret;
  }
  public ArrayList<FieldCard> getCards(){
    return cards;
  }
  public void setCards(ArrayList<FieldCard> newCards){
    cards = newCards;
  }
  public void addCard(FieldCard card){
    cards.add(card);
  }
  public void removeCard(FieldCard card){
    cards.remove(card);
  }
  public int getSize(){
    return cards.size();
  }
  public void initializeHandPanel(){
    if (hpOpen){
      //Bring hand to front, somehow.
      return;
    }
    hp = new HandPanel(this, this.p.getId(), sui, tab);
    hpOpen = true;
  }
  public HandPanel getHandPanel(){
    return hp; 
  }
  public void setHpOpen(boolean b){
    hpOpen = b;
  }
  public static void main(String[] args){
    /*Hand h = new Hand();
    FieldCard[] fc = new FieldCard[20];
    for (int i=0; i<20; i++){
      fc[i] = new FieldCard(i);
    }
    h.printContents();
    System.out.println("Adding a card...");
    h.addCard(fc[6]);
    h.printContents();
    System.out.println("Adding 4 more cards...");
    for (int i=0; i<4; i++){
      h.addCard(fc[4*i]);
    }
    h.printContents();
    System.out.println("Removing number 8...");
    h.removeCard(fc[8]);
    h.printContents();
    System.out.println("Adding 3 more cards...");
    h.addCard(fc[2]);
    h.addCard(fc[19]);
    h.addCard(fc[14]);
    h.printContents();
    System.out.println("Examining 3 cards...");
    h.getCards(3, false);
    System.out.println("Contents...");
    h.printContents();
    System.out.println("Examining 4 cards...");
    h.getCards(4, false);
    System.out.println("Contents...");
    h.printContents();
    System.out.println("Examining 3 cards...");
    h.getCards(3, false);
    System.out.println("Examining 3 cards...");
    h.getCards(3, false);
    System.out.println("Examining 0 cards...");
    h.getCards(0, false);
    System.out.println("Examining 100 cards...");
    h.getCards(100, false);
    System.out.println("Removing 2 cards...");
    h.getCards(2, true);
    System.out.println("Contents...");
    h.printContents();
    System.out.println("Removing 3 cards...");
    h.getCards(3, true);
    System.out.println("Contents...");
    h.printContents();
    System.out.println("Removing 3 cards...");
    h.getCards(3, true);
    System.out.println("Contents...");
    h.printContents();
    System.out.println("Removing number 10...");
    h.removeCard(fc[10]);
    h.printContents();
    System.out.println("Adding all cards");
    for (int i=0; i<fc.length; i++){
      h.addCard(fc[i]);
    }
    h.printContents();*/
  }
}
