import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class FriendDBM {
	private Connection con;

	/**
	 * 
	 * Present for testing purposes only
	 * 
	 * @param args
	 *            default argument
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FriendDBM test = new FriendDBM();
		// test.remove("magic");
		// test.clear();
		test.remove("magic");
		test.remove("magic2");

	}

	/**
	 * 
	 * Sole constructor for the Friend Database Manager.
	 * 
	 * It will create the database "Friends.db" if it does not already exist,
	 * then create a table "friends" in that database unless that table already
	 * exists
	 */

	public FriendDBM() {
		Statement stat = null;
		try {
			// sqlite driver
			try {
				Class.forName("org.sqlite.JDBC");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// database path, if it's new database,
			// it will be created in the project folder
			con = DriverManager.getConnection("jdbc:sqlite:Friends.db");
			stat = con.createStatement();

			stat.executeUpdate("create table if not exists friends("
					+ "name varchar," + "ip varchar," + "notes varchar,"
					+ "primary key (ip));");
		}

		catch (SQLException e) {
			System.out.println("ERROR: constructor failed\n" + e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (Exception ex) {
			}
		}

	}

	/**
	 * 
	 * Drops the "friends" table from the database, then re-creates the same
	 * table.
	 */
	public void clear() {
		Statement stat = null;
		try {
			stat = con.createStatement();
			stat.executeUpdate("drop table if exists friends");
			stat.executeUpdate("create table if not exists friends("
					+ "name varchar," + "ip varchar," + "notes varchar,"
					+ "primary key (name));");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("ERROR: CardTypeDBM.clear failed\n"
					+ e.getMessage());
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (Exception ex) {
			}
		}
	}

	/**
	 * 
	 * Attempts to create the "friends" table if it does not already exist.
	 */

	public void createTable() {
		Statement stat = null;
		try {
			stat = con.createStatement();
			stat.executeUpdate("create table if not exists friends("
					+ "name varchar," + "ip varchar," + "notes varchar,"
					+ "primary key (name));");
			stat.close();
		} catch (SQLException e) {
			System.out.println("ERROR: CardTypeDBM.createTable failed\n"
					+ e.getMessage());
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (Exception ex) {
			}
		}
	}

	/**
	 * 
	 * Add a new friend to the database.
	 * 
	 * @param ip
	 *            the friend's ip address
	 */
	public void add(String ip) {
		add("", ip, "");
	}

	/**
	 * 
	 * Add a new friend to the database.
	 * 
	 * @param name
	 *            the unique name of the friend
	 * 
	 * @param ip
	 *            the friend's ip address
	 */
	public void add(String name, String ip) {
		add(name, ip, "");
	}

	/**
	 * 
	 * Add a new friend to the database. This version allows you to set the
	 * notes on a friend at creation.
	 * 
	 * @param name
	 *            the unique name of the friend
	 * 
	 * @param ip
	 *            the friend's ip address
	 * 
	 * @param notes
	 *            any notes by the user about this friend
	 */

	public void add(String name, String ip, String notes) {
		if (name == null || ip == null || notes == null) {
			System.out.println("ERROR: Friends.add: Null Input");
			return;
		}

		try {
			PreparedStatement prep = con
					.prepareStatement("insert into friends values(?,?,?);");
			prep.setString(1, name);
			prep.setString(2, ip);
			prep.setString(3, notes);
			prep.execute();
		} catch (SQLException e) {
			System.out
					.println("ERROR: FriendDBM.add failed\n" + e.getMessage());
		}
	}

	/**
	 * Sets the name field for the friend with the given IP address
	 * 
	 * @param ip
	 *            unique IP address of the friend to give a name to
	 * @param name
	 *            The name to give this friend
	 */
	public void setName(String ip, String name) {
		if (name == null || ip == null) {
			System.out.println("ERROR: FriendsDBM.setName: Null Input");
			return;
		}

		try {
			PreparedStatement prep = con
					.prepareStatement("update friends set name=? where ip=?;");
			prep.setString(1, name);
			prep.setString(2, ip);
			prep.execute();
		} catch (SQLException e) {
			System.out.println("ERROR: FriendDBM.setName failed\n"
					+ e.getMessage());
		}
	}
	
	/**
	 * Sets the notes field for the friend with the given IP address
	 * 
	 * @param ip
	 *            unique IP address of the friend to set the notes of
	 * @param notes
	 *            The notes to give this friend
	 */
	public void setNotes(String ip, String notes) {
		if (notes == null || ip == null) {
			System.out.println("ERROR: FriendsDBM.setName: Null Input");
			return;
		}

		try {
			PreparedStatement prep = con
					.prepareStatement("update friends set notes=? where ip=?;");
			prep.setString(1, notes);
			prep.setString(2, ip);
			prep.execute();
		} catch (SQLException e) {
			System.out.println("ERROR: FriendDBM.setName failed\n"
					+ e.getMessage());
		}
	}

	/**
	 * 
	 * Deletes the element with the specified name from the FriendDatabase.
	 * 
	 * @param name
	 *            the unique IP of the friend to delete
	 * 
	 * @return returns 0 if successful - other numbers indicate errors
	 */

	public int remove(String ip) {
		try {
			PreparedStatement prep = con
					.prepareStatement("delete from friends where ip=?;");
			prep.setString(1, ip);
			prep.execute();
			return 0;
		} catch (SQLException e) {
			System.out.println("ERROR: FriendsDBM.remove failed\n"
					+ e.getMessage());
		}
		return -1;
	}

	/**
	 * 
	 * Gets the unique IPs of all friends currently in the database
	 * 
	 * @return An array containing all the friends' IPs or a null if an error
	 *         occurred
	 */

	public String[] getEntries() {
		Statement stat = null;
		try {
			stat = con.createStatement();
			ResultSet res = stat.executeQuery("select * from friends");
			List<String> out = new ArrayList<String>();
			while (res.next()) {
				out.add(res.getString("ip"));
			}
			String[] s = new String[out.size()];
			out.toArray(s);
			return s;
		} catch (SQLException e) {
			System.out.println("ERROR: FriendsDBM.getEntries failed\n"
					+ e.getMessage());
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (Exception ex) {
			}
		}
		return null;
	}

	/**
	 * 
	 * Returns the name for the requested friend
	 * 
	 * @param ip
	 *            IP of the friend to get the name for
	 * 
	 * @return the name of the friend or null on an error
	 */
	public String getName(String ip) {
		/*
		 * Goes to the element with the specified name and returns the
		 * corresponding String array fields[],
		 * 
		 * specified upon initialization
		 */
		Statement stat = null;
		try {
			stat = con.createStatement();
			System.out.println(ip);
			ResultSet res = stat
					.executeQuery("select name from friends where ip='" + ip
							+ "'");
			String out = "";
			while (res.next()) {
				out = res.getString("name");
				break;
			}

			res.close();
			stat.close();

			return out;
		} catch (SQLException e) {
			System.out.println("ERROR: CardTypeDBM.getFields failed\n"
					+ e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (Exception ex) {
			}
		}
		return null;
	}

	/**
	 * 
	 * Returns the notes field for the requested friend
	 * 
	 * @param name
	 *            the unique friend IP
	 * 
	 * @return the notes string for the friend or null if an error occurred
	 */

	public String getNotes(String ip) {
		Statement stat = null;
		try {
			stat = con.createStatement();
			ResultSet res = stat
					.executeQuery("select notes from friends where ip='" + ip
							+ "'");
			String out = "";
			while (res.next()) {
				out = res.getString("notes");
				break;
			}

			res.close();
			stat.close();

			return out;

		} catch (SQLException e) {
			System.out.println("ERROR: FriendsDBM.getTypes failed\n"
					+ e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (Exception ex) {
			}
		}
		return null;
	}

	/**
	 * 
	 * Gets the unique IPs of all friends currently in the database
	 * 
	 * @return An array containing all the friends' IPs or a null if an error
	 *         occurred
	 */

	public Vector<Vector<String>> getAll() {
		Statement stat = null;
		try {
			stat = con.createStatement();
			ResultSet res = stat.executeQuery("select * from friends");
			Vector<Vector<String>> out = new Vector<Vector<String>>();
			while (res.next()) {
				out.elementAt(out.size() - 1).add(res.getString("name"));
				out.elementAt(out.size() - 1).add(res.getString("ip"));
				out.elementAt(out.size() - 1).add(res.getString("notes"));
				out.elementAt(out.size() - 1).add("false");
			}
			return out;
		} catch (SQLException e) {
			System.out.println("ERROR: FriendsDBM.getEntries failed\n"
					+ e.getMessage());
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (Exception ex) {
			}
		}
		return null;
	}
	
	/**
	 * 
	 * Prints to out all the entries in the database. Present for testing
	 * purposes
	 */
	public void printAll() {
		Statement stat = null;
		try {
			stat = con.createStatement();
			ResultSet res = stat.executeQuery("select * from friends");
			while (res.next()) {
				System.out.println(res.getString("name") + " "
						+ res.getString("ip")

						+ " " + res.getString("notes"));
			}
		} catch (SQLException e) {
			System.out.println("ERROR: FriendDBM.printAll failed\n"
					+ e.getMessage());
		} finally {
			try {
				if (stat != null)
					stat.close();
			} catch (Exception ex) {
			}
		}
	}

	public void close() {
		try {
			con.close();
		} catch (Exception ex) {
		}
	}
}
