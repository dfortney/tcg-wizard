import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.net.*;
import javax.sound.sampled.*;


import javax.imageio.ImageIO;
import javax.swing.*;

@SuppressWarnings("unused")
public class Core {
 public static ArrayList<GenericUI> GUIs;
 public static CardTypeDBM ctDBM;
 private static DeckTableDBM dtDBM;
 private static CollectionDBM cDBM;
 private static SandboxUI onlineGame = null;
 private static ArrayList<FieldCard> shuffleBuff = null;
 private static ArrayList<ArrayList<FieldCard>> decksBuff = null;
 private static ArrayList<FieldCard> deckBuff = null;
 private static int decksBuffID [] = null;
 private static int decksBuffIdx = -1;
 private static ArrayList<FieldCard> shuffBuff = null;
 private static boolean readyToReceive[] = null;
 private static EventQueue Q;
 private static WaitCountLock WCL;
 public static void main(String[] args) {
  ctDBM = new CardTypeDBM();
  dtDBM = new DeckTableDBM();
  try {
   cDBM = new CollectionDBM("sample.db");
  } catch (Exception e) {
   e.printStackTrace();
  }
  GUIs = new ArrayList<GenericUI>(); // A resizable array of all active GUIs.
  new CollectionEditorUI("Sampler");
  /*startMultiplayerGame(new String[]{"dfortney", "dfortney2"},"dfortney2","Warrior Tribal");
  receiveBlock("StartDeckOrder 2;");
  receiveBlock("ReadyToReceive 1;");
  receiveBlock("ReadyToReceive 2;");
  sendDeckInfo("Warrior Tribal",1);
  sendDeckInfo("New Alara (WUBRG)",2);
  receiveBlock("EndDeckOrder;");*/
  
  //Recall:
  /* StartDeckOrder <playerCount>; Indicates that <playerCount> decks are about to be received (including one's own?).
   * ReadyToReceive <playerID>; Indicates that the specified player is ready to receive deck data. One all players have checked in, they each send out a DeckOrder block.
   * DeckOrder <playerID>; Indicates incoming ordering data.
   * /DeckOrder <playerID>; Indicates end of ordering data.
   * Deck <cardName> <cardImage>; Indicates the next card of stackID.
   * EndDeckOrder;
   * */
 }
 
 //Do not call this function except within GenericUI.java!
 public static void addGUI(GenericUI gui) {
  if (GUIs == null) {
   GUIs = new ArrayList<GenericUI>();
  }
  GUIs.add(gui);
 }

 public static void removeGUI(GenericUI gui) {
  if (GUIs == null) {
   GUIs = new ArrayList<GenericUI>();
  }
  GUIs.remove(gui);
  if (GUIs.size() < 1)
   ctDBM.close();
 }

 public static String[] getCardTypes() {
  String[] possibleValues = ctDBM.getEntries();
  return possibleValues;
 }
 
 public static String getCollCardType(String collName){
   String ct = "";
   try{
     ct = cDBM.getCardType(collName);
   }catch(Exception ex){
   }
   return ct;
 }

 public static String[] getFields(String ctName) {
  if (ctName == null || ctName.equals(""))
   return null;
  String[] temp = ctDBM.getFields(ctName);
  String[] fields = new String[temp.length + 5];
  fields[0] = "Card ID";
  fields[1] = "Name";
  fields[2] = "Quantity";
  for (int i = 0; i < temp.length; i++) {
   fields[i + 3] = temp[i];
  }
  fields[temp.length + 3] = "Image";
  fields[temp.length + 4] = "Notes";
  return fields;
 }

 public static int[] getFieldTypes(String ctName) {
  if (ctName == null || ctName.equals(""))
   return null;
  int[] temp = ctDBM.getTypes(ctName);
  int[] fields = new int[temp.length + 5];
  fields[0] = 2;
  fields[1] = 1;
  fields[2] = 2;
  for (int i = 0; i < temp.length; i++) {
   fields[i + 3] = temp[i];
  }
  fields[temp.length + 3] = 1;
  fields[temp.length + 4] = 1;
  return fields;
 }

 public static String[] getAddedFields(String ctName) {
  if (ctName == null || ctName.equals(""))
   return null;
  String[] fields = ctDBM.getFields(ctName);
  return fields;
 }

 public static int[] getAddedFieldTypes(String ctName) {
  if (ctName == null || ctName.equals(""))
   return null;
  int[] types = ctDBM.getTypes(ctName);
  return types;
 }

 public static void addCardType(String name, String[] fields, String[] 
types) {
  int myTypes[] = new int[types.length];
  String myFields[] = new String[fields.length];
  /*
   * myTypes[0]=2; myFields[0]="Card ID"; myTypes[1]=1;
   * myFields[1]="Name"; myTypes[2]=2; myFields[2]="Quantity";
   * myTypes[myFields.length-2]=1; myFields[myFields.length-2]="Image";
   * myTypes[myFields.length-1]=2; myFields[myFields.length-1]="Notes";
   */
  for (int i = 0; i < myFields.length; i++) {
   myTypes[i] = typeToInt(types[i]);
   if (myTypes[i] == -1) {
    System.out
      .println("Core: addCardType: Invalid field type name.");
    System.exit(0);
   }
   System.out.println(fields[i]);
   myFields[i] = fields[i];
   System.out.println("addCardType: " + myFields[i]);
  }
  /*
   * System.out.println(name); for (int i=0; i<myFields.length;i++){
   * System.out.println("myFields["+i+"] = "+myFields[i]);
   * System.out.println("myTypes["+i+"] = "+myTypes[i]); }
   * System.out.println(ctDBM);
   */
  ctDBM.add(name, myFields, myTypes);
  ctDBM.save();
 }

 /**
  * Returns whether or not cDBM is null, true if not null, false if null
  * 
  * @return is cDBM not null
  */
 public static Boolean isCDBM() {
  if (cDBM == null) {
   return false;
  }
  return true;
 }

 /**
  * This method converts a java type to an int Boolean returns 0 String
  * returns 1 Integer returns 2 Character returns 3 Float returns 4 
anything
  * else returns -1
  * 
  * @param type
  *            the type of the object being parsed to be parsed as an int
  * @return the parsed int
  * 
  */
 @SuppressWarnings("rawtypes")
public static int typeToInt(Class type) {
  if (type == Boolean.class)
   return 0;
  else if (type == String.class)
   return 1;

  else if (type == Integer.class)
   return 2;

  else if (type == Character.class)
   return 3;

  else if (type == Float.class)
   return 4;

  return -1;
 }

 /**
  * This method converts a the name of a java type as a string to an int
  * Boolean returns 0, String returns 1, Integer returns 2, Character 
returns
  * 3, Float returns 4, anything else returns -1
  * 
  * @param type
  *            the String name of the object type being parsed to be 
parsed
  *            as an int
  * @return the parsed int
  * 
  */
 public static int typeToInt(String type) {
  if (type == "Boolean")
   return 0;

  else if (type == "String")
   return 1;

  else if (type == "Integer")
   return 2;

  else if (type == "Character")
   return 3;

  else if (type == "Float")
   return 4;

  return -1;
 }

 /**
  * This method converts an int to a java type 0 returns Boolean, 1 
returns
  * String, 2 returns Integer, 3 returns Character, 4 returns Float, 
anything
  * else returns String
  * 
  * @param i
  *            the int to be parsed as a type
  * @return the parsed type
  * 
  */
 @SuppressWarnings("rawtypes")
public static Class typeConversion(int i) {
  switch (i) {
  case 0:
   return Boolean.class;
  case 1:
   return String.class;
  case 2:
   return Integer.class;
  case 3:
   return Character.class;
  case 4:
   return Float.class;
  default:
   return String.class;
  }
 }

 /**
  * Returns the names of all Collections in the database as an array of
  * strings, returns null on error
  * 
  * @return the names of all collections
  */
 public static Object[] getCollNamesArray() {
  return cDBM.getCollections().toArray();
 }

 /**
  * Returns the names of all Collections in the database as an arraylist 
of
  * strings, returns null on error
  * 
  * @return the names of all collections
  */
 public static ArrayList<String> getCollNames() {
  return cDBM.getCollections();
 }

 /**
  * Returns the names of the fields of a collection in the database, 
returns
  * null on error
  * 
  * @param collName
  *            name of the collection to get the fields of
  * @return the names of the fields in a collection
  */
 public static String[] getCollFields(String collName) {
  if (collName == null || collName == "")
   return null;
  return cDBM.getFields(collName);
 }

 /**
  * Returns the types of the fields of a collectin in a database as 
integers,
  * returns null on error
  * 
  * @param collName
  *            name of the collection to get the field types of
  * @return the types of the fields parsed as ints
  */
 public static int[] getCollTypes(String collName) {
  if (collName == null || collName == "")
   return null;
  return cDBM.getTypes(collName);
 }

 /**
  * Loads a collection from the database as a 2D array of Objects
  * 
  * @param collName
  *            name of the collection being loaded
  * @return the collection data
  */
 public static Object[][] loadCollection(String collName) {
  if (collName == null || collName == "")
   return null;
  Object[][] collection = null;
  try {
    ArrayList<Object[]> temp = cDBM.getCollectionData(collName);
    collection = new Object[temp.size()][temp.get(0).length];
    for (int i=0; i<temp.size(); i++){
      for (int j=0; j<temp.get(0).length; j++){
        collection[i][j]=temp.get(i)[j];
      }
    }
  } catch (SQLException e) {
   e.printStackTrace();
  }
  return collection;
 }

 /**
  * Loads a collection from the database as an ArrayList of object[]
  * 
  * @param collName
  *            name of the collection being loaded
  * @return the collection data
  * @throws SQLException
  * 
  */
 public static ArrayList<Object[]> getCollectionData(String collName)
   throws SQLException {
  if (collName == null || collName == "")
   return null;
  return cDBM.getCollectionData(collName);
 }

 /**
  * Stores a collection in the database
  * 
  * @param collName
  *            collection name to be stored
  * @param data
  *            the data this collection contains
  */
 public static void storeCollection(String collName, ArrayList<Object[]> 
data) {
  if (collName == null || collName == "") {
   JOptionPane.showMessageDialog(GUIs.get(0),
     "Invalid collection name");
  }
  try {
   cDBM.setCollectionData(collName, data);
  } catch (Exception e) {
   JOptionPane.showMessageDialog(GUIs.get(0),
     "An error occured and this collection could not be stored");
   e.printStackTrace();
  }
 }

 /**
  * Creates a new collection entry in the database
  * 
  * @param collName
  *            the collection name of the new collection
  * @param cardType
  *            the name of the cardType of the collection
  */
 public static void createCollection(String collName, String cardType) {
  if (collName == null || collName == "") {
   JOptionPane.showMessageDialog(GUIs.get(0),
     "Invalid collection name");
  }
  try {
   cDBM.create(cardType, collName);
  } catch (Exception e) {
   JOptionPane
     .showMessageDialog(GUIs.get(0),
       "An error occured and this collection could not be created");
   e.printStackTrace();
  }
 }

 /**
  * Saves a collection entry in the database
  * 
  * @param collName
  *            name of the collection being saved
  * @param data
  *            the data this collection contains
  */
 public static void saveCollection(String collName, ArrayList<Object[]> 
data) {
  if (collName == null || collName == "") {
   JOptionPane.showMessageDialog(GUIs.get(0),
     "Invalid collection name");
  }
  try {
   storeCollection(collName, data);
   cDBM.save(collName);
  } catch (Exception e) {
   JOptionPane.showMessageDialog(GUIs.get(0),
     "An error occured and this collection could not be saved");
   e.printStackTrace();
  }
 }

 /**
  * Deletes a collection entry in the database
  * 
  * @param collName
  *            the name of the collection to be deleted
  */
 public static void deleteCollection(String collName) {
  if (collName == null || collName == "") {
   JOptionPane.showMessageDialog(GUIs.get(0),
     "Invalid collection name");
  }
  try {
   cDBM.drop(collName);
  } catch (Exception e) {
   JOptionPane
     .showMessageDialog(GUIs.get(0),
       "An error occured and this collection could not be deleted");
   e.printStackTrace();
  }
 }

 public static void loadCollectionFromFile(String collName, String 
filePath) {
  try {
   cDBM.loadFromFile(collName, filePath);
  } catch (SQLException e) {
   // TODO Auto-generated catch block
   e.printStackTrace();
  } catch (IOException e) {
   // TODO Auto-generated catch block
   e.printStackTrace();
  }
 }

 public static void saveCollectionToFile(String collName, String filePath) 
{
  try {
   cDBM.saveToFile(collName, filePath);
  } catch (SQLException e) {
   // TODO Auto-generated catch block
   e.printStackTrace();
  } catch (IOException e) {
   // TODO Auto-generated catch block
   e.printStackTrace();
  }
 }


 /**
  * Returns the names of all decks in the database as a String Vector
  * 
  * @return the names of all decks
  */
 public static Vector<String> getDeckNames() {
  dtDBM.getDecks();
  return dtDBM.getDeckNames();
 }
 public static Vector<Object> getDeckIDs(String deckName){
   return dtDBM.getDeck(deckName);
 }


 
 public static String[] getDeckNamesArray() {
   dtDBM.getDecks();
   Vector<String> vect = dtDBM.getDeckNames();
   return vect.toArray(new String[vect.size()]);
 }

 /**
  * Loads a deck from the DeckTable Database
  * 
  * @param deckName
  *            name of the deck being loaded
  * @param collName
  *            name of the collection the deck's cards belong to
  * @return returns an array of datapackets
  */
 public static Object[][] loadDeck(String deckName, String collName) {

  Vector<Object> ids = dtDBM.getDeck(deckName);
  if (ids == null) {
   return null;
  }
  int quant = 0;
  for (int i = 1; i < ids.size(); i = i + 2){
   quant ++;
  }
  System.out.println("collName is "+collName);
  String myFields[] = getFields(collName);
  Object[][] deckData = new Object[quant][myFields.length];
  int j = 0;
  for (int i = 0; i < ids.size(); i = i + 2, j++) {
   try {
     deckData[j] = cDBM.getCard(Integer.parseInt((String) ids.get(i)), 
collName).toArray();
     deckData[j][2]=ids.get(i+1); //Sets the quantity to the deck quantity (instead of collection quantity)
   } catch (Exception e) {
     System.out.println("quant "+quant);
     System.out.println("j "+j);
     System.out.println("ids.get(i+1) "+ids.get(i+1));
    e.printStackTrace();
   }

  }
  return deckData;
 }

 public static boolean deckExists(String deckName) {
  return dtDBM.deckExists(deckName);
 }

 public static void saveDeck(String deckName, Object[][] deckData, String 
notes, String collName, boolean virtual) {
  // id == 0 quant == 2
  if (dtDBM.deckExists(deckName)) {
   dtDBM.removeDeck(deckName);
  }

  if (notes == null || notes.equals(""))
   dtDBM.create(deckName, collName);
  else
   dtDBM.create(deckName, notes, collName);
  if (virtual)    
    dtDBM.setVirtual(deckName, 1);  
  else    
    dtDBM.setVirtual(deckName, 0);  

  for (int i = 0; i < deckData.length; i++) {
   if (deckData[i][0].equals("")) {
    break;
   }
   int q = 0;
   try{
     q = (Integer)deckData[i][2];
   }catch(Exception e){
     q = Integer.parseInt((String) deckData[i][2]);
   }
   try{
     dtDBM.addCard(deckName, Integer.parseInt((String) deckData[i][0]), q);
   }catch(Exception e){
     dtDBM.addCard(deckName, (Integer)deckData[i][0], q);
   }


  }
  Vector<Object> ret = dtDBM.getCardIds(deckName);
  Iterator<Object> itr = ret.iterator();
  System.out.println("Saving: ");
  while (itr.hasNext()) {
   System.out.println(itr.next());
  }
 }

 public static void deleteDeck(String deckName) {
  dtDBM.removeDeck(deckName);
 }

 public static void renameDeck(String oldName, String newName) {
  if(dtDBM.deckExists(oldName)) {
   System.err.println("Trying to create new DECK");
   dtDBM.create(newName, dtDBM.getDeckNotes(oldName),getDeckCollection(oldName), 1 == isDeckVirtual(oldName));
   Vector<Object> data = dtDBM.getCardIds(oldName);
   for (int i = 0; i < data.size(); i = i + 2)
    dtDBM.addCard(newName, Integer.parseInt((String) data.get(i)),
      (Integer) data.get(i + 1));
   deleteDeck(oldName);
  } else {
   
  }
 }

 public static Image imgMaker(String location, JLabel card){
   return imgMaker(location, card, 230, 300);
 }
 /**
  * loads an image from a file into a JLabel
  * 
  * @param location
  *            the file path of the image as a string
  * @param card
  *            the JLabel that the image will be loaded into
  */
 public static Image imgMaker(String location, JLabel card, int x, int y) {
  Image raw = null;

  // Add an image.
  Image newImg = null;
  try{
    URL url = new URL(location);
    raw=ImageIO.read(url);
  }catch(Exception ex){
    try {
      raw = ImageIO.read(new File(location));
    } catch (IOException ex1) {
      try {
        raw = ImageIO.read(new File("backofamagicthegatheringcard.jpg"));
      } catch (IOException ex2) {
    }
  }
  }
  if (raw == null)
    return null;
  ImageIcon icon = new ImageIcon(raw);
  Image img = icon.getImage();
  newImg = img.getScaledInstance(x, y, java.awt.Image.SCALE_SMOOTH);
  ImageIcon newIcon = null;
  if (newImg != null && card != null) {
   try {
    newIcon = new ImageIcon(newImg);
    card.setIcon(newIcon);
    card.revalidate();
   } catch (Exception ex) {
   }
  }
  return newImg;
 }
 public static String[][] search(String where, String tablename, String 
type, String quer, boolean all)
 {
 System.out.println(quer);
 /*if(quer.getClass().equals(String.class))
 {
    System.out.println("Its a string!!");
    Pattern p = Pattern.compile("[^a-zA-Z0-9. ]");
    boolean hasSpecialChar = p.matcher((CharSequence) quer).find();
    if (!hasSpecialChar)
    {
     System.out.println("This word will break the query");
     return null;
    }
 }*/
 quer = quer.replaceAll("  ", " ");
 String[] thequer = quer.split(" ");
 for(int i = 0; i< thequer.length; i++)
 {
  if(thequer[i].equals("does"))
  {
   if(thequer[i+2].equals("end"))
   {
    thequer[i] = "not";
    thequer[i+1] = "like";
    thequer[i+2] = "";
    thequer[i+3] = "";
    thequer[i+4] = "'%" + thequer[i+4];
    int j = i+4;
    while (!thequer[j].equals(")") && !thequer[j].equals("AND") )
     j++;
    thequer[j-1] = thequer[j-1] + "'";
   }
   if(thequer[i+2].equals("start"))
   {
    thequer[i] = "not";
    thequer[i+1] = "like";
    thequer[i+2] = "";
    thequer[i+3] = "";
    thequer[i+4] = "'" + thequer[i+4];
    int j = i+4;
    while (!thequer[j].equals(")") && !thequer[j].equals("AND") )
     j++;
    thequer[j-1] = thequer[j-1] + "%'";
   }
   if(thequer[i+2].equals("equal"))
   {
    thequer[i] = "!=";
    thequer[i+1] = "";
    thequer[i+2] = "";
    thequer[i+3] = "'" + thequer[i+3];
    int j = i+3;
    while (!thequer[j].equals(")") && !thequer[j].equals("AND") )
     j++;
    thequer[j-1] = thequer[j-1] + "'";
   }
   if(thequer[i+2].equals("contain"))
   {
    thequer[i] = "not";
    thequer[i+1] = "like";
    thequer[i+2] = "";
    thequer[i+3] = "'%" + thequer[i+3];
    int j = i+3;
    while (!thequer[j].equals(")") && !thequer[j].equals("AND") )
     j++;
    thequer[j-1] = thequer[j-1] + "%'";
   }
  }
 }
 
 for(int i = 0; i< thequer.length; i++)
 {
  if(thequer[i].equals("ends"))
  {
   thequer[i] = "like";
   thequer[i+1] = "";
   thequer[i+2] = "'%" + thequer[i+2];
   int j = i+2;
   while ( !thequer[j].equals(")") && !thequer[j].equals("AND") )
    j++;
   thequer[j-1] = thequer[j-1] + "'";
  }
  if(thequer[i].equals("starts"))
  {
   thequer[i] = "like";
   thequer[i+1] = "";
   thequer[i+2] = "'" + thequer[i+2];
   int j = i+2;
   while (!thequer[j].equals(")") && !thequer[j].equals("AND") )
    j++;
   thequer[j-1] = thequer[j-1] + "%'";
  }
  if(thequer[i].equals("equals"))
  {
   thequer[i] = "=";
   thequer[i+1] = "'" + thequer[i+1];
   int j = i+1;
   while (!thequer[j].equals(")") && !thequer[j].equals("AND") )
    j++;
   thequer[j-1] = thequer[j-1] + "'";
  }
  if(thequer[i].equals("contains"))
  {
   thequer[i] = "like";
   thequer[i+1] = "'%" + thequer[i+1];
   int j = i+1;
   while (!thequer[j].equals(")") && !thequer[j].equals("AND") )
   {
    System.out.println("j:" + j + " " + thequer[j]);
    j++;
   }
   thequer[j-1] = thequer[j-1] + "%'";
  }
  if(thequer[i].equals("is"))
  {
 thequer[i] = "=";
 if(thequer[i+1].equals("TRUE"))
  thequer[i+1] = "'true'";
 else
  thequer[i+1] = "'false'";
  }
  
 }
 quer = join(" ", (Object[])thequer);
 //quer = "(name == a)";
 System.out.println(quer);  
 if(where.equals("collection")){
   return cDBM.search(tablename, type, quer, all);
 }else{
   String[][] inter = dtDBM.search(tablename, type, quer, all);
   String[] fields = getFields(type);
   Object[][] res = new Object[inter.length][fields.length];
   for (int i=0; i<inter.length; i++){
     try{
       
res[i]=cDBM.getCard(Integer.parseInt(inter[i][1]),dtDBM.getCollection(inter[i][0])).toArray();
       res[i][2]=inter[i][2];
     }catch(Exception e){
       e.printStackTrace();
     }
   }
   String[][] finalres = new String[res.length][res[0].length+1];
   for (int i=0; i<res.length; i++){
     finalres[i][0]=inter[i][0];
     for (int j=0; j<res[0].length; j++){
       if (res[i][j]==null) res[i][j]="";
       if (res[i][j].getClass()==((Integer)1).getClass())
         res[i][j] = Integer.toString((Integer)res[i][j]);
       finalres[i][j+1]=(String)res[i][j];
     }
   }
   return finalres;
 }
 }
 public static void newDeckEditorFromCEUI(String collName, String[] 
fields, Object[][] data){

   newDeckEditorFromCEUI(collName, fields, data, null);
 }
 public static void newDeckEditorFromCEUI(String collName, String[] 
fields, Object[][] data, String deckName){
   //Format fields.
   String trueFields[] = new String[fields.length];
   trueFields[0]="Card ID";
   trueFields[1]="Name";
   trueFields[2]="Quantity";
   trueFields[fields.length-2]="Notes";
   trueFields[fields.length-1]="Image";
   for (int i=3; i<fields.length-2; i++){
     trueFields[i]=fields[i+2];
   }
   int emptiesFound = 0;
   Object temp[][] = new Object[data.length][data[0].length];
   for (int i=0; i<data.length; i++){
     if (data[i][0]==null||((String)data[i][0]).trim().equals("")){
       emptiesFound++;
       continue;
     }
     for (int j=0; j<data[0].length; j++){
       if (j==0)
         temp[i][j]=data[i][1];
       else if (j==1)
         temp[i][j]=data[i][0];
       else if (j==data[0].length-2)
         temp[i][j]=data[i][4];
       else if (j==data[0].length-1)
         temp[i][j]=data[i][3];
       else if (j==2)
         temp[i][j]=data[i][j];
       else
         temp[i][j]=data[i][j+2];
       if (temp[i][j].getClass()==((Integer)1).getClass())
         temp[i][j]=Integer.toString((Integer)temp[i][j]);
       //Add more conversions here
     }
   }
   Object trueData[][] = null;
   if (emptiesFound==0){
     trueData = temp;
   }else{
     trueData = new Object[temp.length-emptiesFound][temp[0].length];
     int tdIdx = 0;
     for (int i=0; i<temp.length; i++){
       if (temp[i][1]==null||((String)temp[i][1]).trim().equals("")){
         //Do nothing
       }else{
         for (int j=0; j<temp[0].length; j++){
           trueData[tdIdx][j]=temp[i][j];
         }
         tdIdx++;
       }
     }
   }
   if (deckName==null){
   String name = JOptionPane.showInputDialog(null, "Enter deck name:", "", 1);
      if (name==null || name.equals("")) return;
      if(Core.deckExists(name)|| name.equals("Sample Hand")){
        JOptionPane.showMessageDialog(null, "A deck with this name already exists!", "Deck Creation Error", JOptionPane.ERROR_MESSAGE);
      }
     new DeckEditorUI(name, collName, trueFields, trueData);
     return;
   }
   Object deckData[][] = loadDeck(deckName, collName);
   emptiesFound = 0;
   temp = new Object[deckData.length][deckData[0].length];
   for (int i=0; i<deckData.length; i++){
     if (((String)deckData[i][0]).trim().equals("")){
       emptiesFound++;
       continue;
     }
     for (int j=0; j<deckData[0].length; j++){
       if (j==0)
         temp[i][j]=deckData[i][1];
       else if (j==1)
         temp[i][j]=deckData[i][0];
       else if (j==deckData[0].length-2)
         temp[i][j]=deckData[i][4];
       else if (j==deckData[0].length-1)
         temp[i][j]=deckData[i][3];
       else if (j==2)
         temp[i][j]=deckData[i][j];
       else
         temp[i][j]=deckData[i][j+2];
       if (temp[i][j].getClass()==((Integer)1).getClass())
         temp[i][j]=Integer.toString((Integer)temp[i][j]);
       //Add more conversions here
     }
   }
   Object trueDeckData[][] = null;
   if (emptiesFound==0){
     trueDeckData = temp;
   }else{
     trueDeckData = new Object[temp.length-emptiesFound][temp[0].length];
     int tdIdx = 0;
     for (int i=0; i<temp.length; i++){
       if (temp[i][1]==null||((String)temp[i][1]).trim().equals("")){
         //Do nothing
       }else{
         for (int j=0; j<temp[0].length; j++){
           trueDeckData[tdIdx][j]=temp[i][j];
         }
         tdIdx++;
       }
     }
   }
   //Recall: public DeckEditorUI(String deckName, String collection, String argFields[], Object argCollectionData[][], Object argDeckData[][]){
   new DeckEditorUI(deckName, collName, trueFields, trueData, 
trueDeckData, (isDeckVirtual(deckName)==1));
 }
 
 /**
  * 
  * An internal method to join multiple objects into a single string using a
  * connecting element
  * 
  * The input objects can be in an array or passed in as separate arguments
  * 
  * @param glue
  *            a string to serve as the connective element
  * 
  * @param s
  *            the objects to join
  * 
  * @return the joined string
  */
 public static String join(String glue, Object... s) {
  int n = s.length;
  if (n == 0) {
   return null;
  }
  StringBuilder out = new StringBuilder();
  out.append(s[0].toString());
  for (int i = 1; i < n; i++) {
   out.append(glue).append(s[i].toString());
  }
  return out.toString();
 }

 /**
  * 
  * An internal method to join several ints into a single string using a
  * connecting element
  * 
  * The input ints can be in an array or passed in as separate arguments
  * 
  * @param glue
  *            a string to serve as the connective element
  * 
  * @param s
  *            the ints or array of ints to join
  * 
  * @return the joined string
  */
 public static String join(String glue, int... s) {
  int n = s.length;
  if (n == 0) {
   return null;
  }
  StringBuilder out = new StringBuilder();
  out.append("" + s[0]);
  for (int i = 1; i < n; i++) {
   out.append(glue).append("" + s[i]);
  }
  return out.toString();
 }
 
 public static String getDeckCollection(String deckName){
   return dtDBM.getCollection(deckName);
 }
 public static int isDeckVirtual (String deckName){
   int val = dtDBM.getVirtual(deckName);
   return val;
 }
 public static void setDeckVirtual (String deckName, boolean virtual){
   int val = 0;
   if (virtual) val = 1;
   dtDBM.setVirtual(deckName, val);
 }
 
 //Later, there will be more arguments, and the function will act differently.
 public static void printToLog(String logMsg){
   printToLog(logMsg, "");
 }
 public static void printToLog(String logMsg, String infoMsg){
//   DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss"); //This date should come from the server, not the clients.
//   Date date = new Date();
   if (getSui()!=null)
     getSui().printToLog(logMsg);
   //playSound("ding1.WAV");
   
   if (infoMsg!="")
     sendBlock(infoMsg, logMsg);
 }

  /**Possible infoMsg's**/
   /*
    * Flip <stackID>; Flips the stack with the specified id.
    * Draw <playerID> <stackID>; The specified player draws from the stack with the specified id.
    * Shuffle <stackID>; The stack with the specified id was shuffled. Await incoming ordering data.
    * ShufCard <stackID> <cardName> <cardImage>;
    * /Shuffle <stackID>; Indicates end of reordering data.
    * Tap <stackID>; Taps the stack with the specified id.
    * HSMove <playerID> <handIndex> <stackID> <destIndex> <relativeTo>; Moves the card at <handIndex> within <playerID>'s hand to <destIndex> within the stack <stackID>, relative to top if last arg is "T" or bottom if "B".
    *   Remove the card from the hand and add it to the stack, as specified.
    * SSMove <sourceID> <sourceIndex> <destID> <destIndex> <relativeTo>; Moves the card at <sourceIndex> within stack <sourceID> to <destIndex> within the stack <stackID>, relative to top if last arg is "T" or bottom if "B".
    *   Remove the card from the stack and add it to the stack, as specified.
    * LeftMove <dragID> <dropID>; Performs a moveToBlank via left reference.
    * RightMove <dragID> <dropID>; etc.
    * UpMove <dragID> <dropID>;
    * DownMove <dragID> <dropID>;
    * Spawn <cardName> <cardImage> <x> <y> <stackID> <playerID> <sourceID> <faceup>; Spawns the specified card at the specified location, from the stack <stackID>, creating a <faceup> stack owned by <playerID>. <faceup> is either 'Y' or 'N'.
    * Fetch <playerID> <cardName> <cardImage>; Adds a new FieldCard object to <playerID>'s hand, with the specified attributes.
    * Exchange <stackID> <stackID>; Swaps two stacks with each other.
    * StartDeckOrder <playerCount>; Indicates that <playerCount> decks are about to be received (including one's own?).
    * ReadyToReceive <playerID>; Indicates that the specified player is ready to receive deck data. One all players have checked in, they each send out a DeckOrder block.
    * DeckOrder <playerID>; Indicates incoming ordering data.
    * /DeckOrder <playerID>; Indicates end of ordering data.
    * Deck <cardName> <cardImage>; Indicates the next card of stackID.
    * HandOrder <playerID>; Indicates incoming ordering data.
    * /HandOrder <playerID>; Indicates end of ordering data.
    * Hand <cardName> <cardImage>; Indicates the next card of stackID.
    * EndDeckOrder;
    * Reveal;
    * /Reveal;
    * RevCard <cardName> <cardImage>;
    * */
 
 /**Sequence of events**/
 /*
  * Connection is established.
  * Each client records his own deck selection.
  * Each client is passed the # of players, their player names, and corresponding player ID's. The grid size also is passed (unless we assume a default value).
  * Each client creates a SandboxUI with that many players. Their own position is based on their personal player ID. A dummy Stack object (of maybe 1 item) is loaded for each player's deck.
  * The host calls the StartDeckOrder command.
  * The clients respond with the ReadyToReceive command.
  * Once all clients have signaled their readiness, they pass their deck data using the command sequence DeckOrder, Deck, Deck, ..., Deck, /DeckOrder.
  *     (Note that the deck data is passed already shuffled; the sandbox should not shuffle them, or the deck data will need to be passed a 2nd time.)
  * Once the host has received all deck data, he calls the EndDeckOrder command.
  * All decks are then loaded into the sandbox.
  */
 //sendDeckInfo should be called only after a ReadyToReceive message has been sent by all players.
 public static void sendDeckInfo(String deckName){
   sendDeckInfo(deckName, 1);
 }
 public static void sendDeckInfo(String deckName, int myPlayerId){
   Object[][] cardData = Core.loadDeck(deckName, Core.getDeckCollection(deckName));
   ArrayList<String[]> data = new ArrayList<String[]>();
   for(int j = 0; j < cardData.length; j++){
     for(int k = 0; k < (Integer) cardData[j][2]; k++){
       String name = (String) (cardData[j][0]);
       String card [] = new String[] {((String)cardData[j][0]).replace(' ','`'),(String) cardData[j][3]};
       data.add(card);
     }
   }
   Collections.shuffle(data);
   String block = "DeckOrder "+myPlayerId+";";
   for (int i=0; i<data.size(); i++){
     String name = data.get(i)[0];
     String image = data.get(i)[1];
     block+="Deck "+name+" "+image+";";
   }
   block+="/DeckOrder "+myPlayerId+";";
   sendBlock(block);
   
   receiveBlock(block); //This line may or may not be appropriate.   
 }
 public static void sendShuffleInfo(int stackID, String msg){
   if (onlineGame==null) return;
   Stack stack = onlineGame.getStack(stackID);
   ArrayList<FieldCard> cards = stack.getCards();
   String block = "Shuffle "+stackID+";";
   for (int i=0; i<cards.size(); i++){
     String name = cards.get(i).getName().replace(' ','`');
     String image = cards.get(i).getImagePath();
     block+="ShufCard "+stackID+" "+name+" "+image+";";
   }
   block+="/Shuffle "+stackID+";";
   sendBlock(block, msg);
   
   receiveBlock(block); //This line may or may not be appropriate. 
 }
 public static void sendRevealInfo(ArrayList<FieldCard> cards, String msg){
   if (onlineGame==null) return;
   String block = "Reveal;";
   for (int i=0; i<cards.size(); i++){
     String name = cards.get(i).getName().replace(' ','`');
     String image = cards.get(i).getImagePath();
     block+="RevCard "+name+" "+image+";";
   }
   block+="/Reveal;";
   sendBlock(block, msg);   
 }
 public static void sendHandInfo(int playerID, String msg){
   if (onlineGame==null) return;
   Player player = onlineGame.getPlayerById(playerID);
   ArrayList<FieldCard> cards = player.getHand().getCards();
   String block = "HandOrder "+playerID+";";
   for (int i=0; i<cards.size(); i++){
     String name = cards.get(i).getName().replace(' ','`');
     String image = cards.get(i).getImagePath();
     block+="Hand "+playerID+" "+name+" "+image+";";
   }
   block+="/HandOrder "+playerID+";";
   sendBlock(block, msg);
 }
 public static void refreshStack(int stackID, String msg){
   sendShuffleInfo(stackID, msg);
 }
 public static void sendCommand(String cmd){
   System.out.println("Command: "+cmd);
 }
 public static void sendBlock(String block, String notes){
   System.out.println("Block: "+block);
   if(Q!= null && onlineGame != null){
    Q.append(block, onlineGame.getMe().getName(), FilteredChatEntry.LOG, notes);
   }
 }
 public static void sendBlock(String block){
  sendBlock(block, "");
 }
 public static void sendChat(String chat){
   if(Q!= null && onlineGame != null){
    System.out.println("Chat: "+chat);
    Q.append(chat, onlineGame.getMe().getName(), FilteredChatEntry.CHAT);
   }
 }
 //Recall: public SandboxUI(Integer numberOfPlayers, String[] playerNames, int meIdx, int[] fieldSize, String[] decks, int handCount) {
 public static void startMultiplayerGame(String [] playerNames, String myName, String myDeckName){
   SandboxUI x;
   while((x = Core.getSui()) != null){
     x.close();
     
   }
   int meIdx = -1;
   for (int i=0; i<playerNames.length; i++){
     if (myName.equals(playerNames[i])){
       meIdx = i;
       break;
     }
   }
   int[] fieldSize = new int[]{3,9};
   int handCount = 7;
   onlineGame = new SandboxUI(playerNames.length, playerNames, meIdx, fieldSize, new String[]{myDeckName}, handCount);
 }
 //A block is a continuous string of commands, each ending in a semicolon, with no other separation.
 public static synchronized void receiveBlock(String block){
   SandboxUI sui = onlineGame;
   //block = block.substring(0, block.length()-1); //Cut off the last semicolon
   String commands[] = block.split(";");
   for (int i=0; i<commands.length; i++){
     if(!commands[i].equals("")){
       receiveCommand(commands[i]);
     }
   }
 }
 public static void receiveCommand(String cmd){
   System.err.println("COMMAND: " + cmd);
   SandboxUI sui = onlineGame;
   String args[] = cmd.split(" ");
   if (args[0].equals("Flip")){
     int stackID = Integer.parseInt(args[1]);
     sui.cmdFlip(stackID);
   }else if (args[0].equals("Draw")){
     int playerID = Integer.parseInt(args[1]);
     int stackID = Integer.parseInt(args[2]);
     sui.cmdDraw(playerID, stackID);
   }else if (args[0].equals("Shuffle")){
     int stackID = Integer.parseInt(args[1]);
     if (shuffleBuff==null){
       shuffleBuff = new ArrayList<FieldCard>();
     }else{
       System.out.println("1057: something is definitely wrong");
     }
   }else if (args[0].equals("/Shuffle")){
     int stackID = Integer.parseInt(args[1]);
     sui.setStack(stackID, shuffleBuff);
     shuffleBuff = null;
   }else if (args[0].equals("ShufCard")){
     int stackID = Integer.parseInt(args[1]);
     String cardName = args[2].replace('`',' ');
     String cardImage = args[3];
     shuffleBuff.add(new FieldCard(cardName,cardImage));
   }else if (args[0].equals("HandOrder")){
     int playerID = Integer.parseInt(args[1]);
     if (shuffleBuff==null){
       shuffleBuff = new ArrayList<FieldCard>();
     }else{
       System.out.println("1057: something is definitely wrong");
     }
   }else if (args[0].equals("/HandOrder")){
     int playerID = Integer.parseInt(args[1]);
     sui.getPlayerById(playerID).getHand().setCards(shuffleBuff);
     shuffleBuff = null;
   }else if (args[0].equals("Hand")){
     int playerID = Integer.parseInt(args[1]);
     String cardName = args[2].replace('`',' ');
     String cardImage = args[3];
     shuffleBuff.add(new FieldCard(cardName,cardImage));
   }else if (args[0].equals("Reveal")){
     if (shuffleBuff==null){
       shuffleBuff = new ArrayList<FieldCard>();
     }else{
       System.out.println("1057: something is definitely wrong");
     }
   }else if (args[0].equals("/Reveal")){
     sui.cmdReveal(shuffleBuff);
     shuffleBuff = null;
   }else if (args[0].equals("RevCard")){
     String cardName = args[1].replace('`',' ');
     String cardImage = args[2];
     shuffleBuff.add(new FieldCard(cardName,cardImage));
   }else if (args[0].equals("Tap")){
     int stackID = Integer.parseInt(args[1]);
     sui.cmdTap(stackID);
   }else if (args[0].equals("HSMove")){
     int playerID = Integer.parseInt(args[1]);
     int handIndex = Integer.parseInt(args[2]);
     int stackID = Integer.parseInt(args[3]);
     int destIndex = Integer.parseInt(args[4]);
     boolean toTop = (args[5].charAt(0) == 'T');
     sui.cmdHSMove(playerID, handIndex, stackID, destIndex, toTop);
   }else if (args[0].equals("SSMove")){
     int sourceID = Integer.parseInt(args[1]);
     int sourceIndex = Integer.parseInt(args[2]);
     int destID = Integer.parseInt(args[3]);
     int destIndex = Integer.parseInt(args[4]);
     boolean toTop = (args[5].charAt(0) == 'T');
     sui.cmdSSMove(sourceID, sourceIndex, destID, destIndex, toTop);
   }else if(args[0].equals("StackMerge")){
    int i = Integer.parseInt(args[1]);
    int j = Integer.parseInt(args[2]);
    int choice = Integer.parseInt(args[3]);
    int destIndex = Integer.parseInt(args[4]);
    boolean toTop = (args[5].charAt(0) == 'T');
    sui.cmdStackMerge(i, j, choice, destIndex, toTop);
   }
   else if (args[0].equals("Fetch")){
     int playerID = Integer.parseInt(args[1]);
     String cardName = args[2];
     String cardImage = args[3];
     sui.cmdFetch(playerID, cardName, cardImage);
   }else if (args[0].equals("Exchange")){
     int stackID1 = Integer.parseInt(args[1]);
     int stackID2 = Integer.parseInt(args[2]);
     sui.cmdExchange(stackID1, stackID2);
   }else if (args[0].equals("LeftMove")){
     int dragID = Integer.parseInt(args[1]);
     int dropID = Integer.parseInt(args[2]);
     sui.cmdLeftMove(dragID, dropID);
   }else if (args[0].equals("RightMove")){
     int dragID = Integer.parseInt(args[1]);
     int dropID = Integer.parseInt(args[2]);
     sui.cmdRightMove(dragID, dropID);
   }else if (args[0].equals("UpMove")){
     int dragID = Integer.parseInt(args[1]);
     int dropID = Integer.parseInt(args[2]);
     sui.cmdUpMove(dragID, dropID);
   }else if (args[0].equals("DownMove")){
     int dragID = Integer.parseInt(args[1]);
     int dropID = Integer.parseInt(args[2]);
     sui.cmdDownMove(dragID, dropID);
   }else if(args[0].equals("Spawn")){
     //Spawn <cardName> <cardImage> <x> <y> <stackID> <playerID> <faceup>; Spawns the specified card at the specified location, from the stack <stackID>, creating a <faceup> stack owned by <playerID>. <faceup> is either 'Y' or 'N'.
     String cardName = args[1].replace('`',' ');
     String cardImage = args[2];
     int x = Integer.parseInt(args[3]);
     int y = Integer.parseInt(args[4]);
     int stackID = Integer.parseInt(args[5]);
     int playerID = Integer.parseInt(args[6]);
     int sourceID = Integer.parseInt(args[7]);
     boolean faceup = (args[8].charAt(0)=='Y');
     sui.cmdSpawn(cardName,cardImage,x,y,stackID,playerID,sourceID,faceup);
   }else if (args[0].equals("StartDeckOrder")){
     int playerCount = Integer.parseInt(args[1]);
     decksBuffID = new int[playerCount];
     decksBuff = new ArrayList<ArrayList<FieldCard>>();
     decksBuffIdx = 0;
     int myPlayerID = 1; //Change this later.
     //sendBlock("ReadyToReceive "+myPlayerID);
   }else if (args[0].equals("ReadyToReceive")){
     /*if (readyToReceive == null){
       readyToReceive = new boolean[decksBuffID.length];
       for (int i=0;i<readyToReceive.length; i++){
         readyToReceive[i]=false;
       }
     }
     int playerID = Integer.parseInt(args[1]);
     readyToReceive[playerID-1] = true;
     for (int i=0; i<readyToReceive.length; i++){
       if (readyToReceive[i] == false) return;
     }*/
     sendDeckInfo(onlineGame.getDeck(), onlineGame.getMe().getId() - 1);
   }else if (args[0].equals("DeckOrder")){
     int playerID = Integer.parseInt(args[1]);
     decksBuffID[decksBuffIdx] = playerID;
     deckBuff = new ArrayList<FieldCard>();
   }else if (args[0].equals("/DeckOrder")){
     int playerID = Integer.parseInt(args[1]);
     decksBuff.add(deckBuff);
     deckBuff = null;
     decksBuffIdx++;
     if(WCL != null){WCL.increment();}
   }else if (args[0].equals("Deck")){
     String cardName = args[1].replace('`',' ');
     String cardImage = args[2];
     deckBuff.add(new FieldCard(cardName,cardImage));
   }else if (args[0].equals("EndDeckOrder")){
     for (int i=0; i<decksBuff.size(); i++){
       System.out.println("ID "+decksBuffID[i]);
       Player player = sui.getPlayerById(decksBuffID[i] +1);
       System.out.println("Player "+player);
       Stack deck = player.getStacks().get(0);
       sui.setStack(deck.getId(), decksBuff.get(i));
       for (int k=0; k<sui.getHandSize(); k++){
         sui.cmdDraw(player.getId(), deck.getId());
       }
       if (sui.getMe()==player){
         player.getHand().initializeHandPanel();
         player.getHand().getHandPanel().refresh();
       }
     }
     for (int i=0; i<decksBuff.size(); i++){
       System.out.println("Player "+decksBuffID[i]);
       for (int j=0; j<decksBuff.get(i).size(); j++){
         System.out.println(decksBuff.get(i).get(j).getName() + " " + decksBuff.get(i).get(j).getImagePath());
       }
     }
   }else{
     System.out.println("Invalid command! (" + args[0] + ")");
   }
 }

 public static SandboxUI getSui(){
   for (int i=0; i<GUIs.size(); i++){
     if (GUIs.get(i) instanceof SandboxUI)
       return (SandboxUI)GUIs.get(i);
   }
   return null;
 }
 public static void playSound(final String myURL) {
   URL url = null;
   try{
     url = new URL(myURL);
   }catch (Exception e){
   }
   try{
   Clip clip = AudioSystem.getClip();
   AudioInputStream ais;
   if (url!=null)
     ais = AudioSystem.getAudioInputStream(url);
   else
     ais = AudioSystem.getAudioInputStream(new File(myURL));
   clip.open(ais);
   clip.loop(0);
   }catch(Exception e){
     System.out.println("Sound error");
     return;
   }
 }
 public static boolean canOpenConnectionUI(){
   if (onlineGame!=null) return false;
   if (Core.getSui()!=null) return false;
   for (int i=0; i<GUIs.size(); i++){
     if (GUIs.get(i) instanceof ConnectionUI)
       return false;
   }
   return true;
 }
 
 public static void setEventQueue(EventQueue q){
  Q = q;
 }
 
 public static SandboxUI getOnlineGame(){
  return onlineGame;
 }
 public static void setTempWCL(WaitCountLock wcl){
   WCL = wcl;
 }


}
