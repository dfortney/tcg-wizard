import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;

public class CardTypeUI extends GenericUI implements ActionListener {
 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private JComboBox cBox;
 private JTextField tField;
 private JButton btnAdd, btnRemove, btnReset, btnConfirm;
 private JLabel lblName,lblType;
 private JScrollPane scrollpane;
 private JTable table;
 private DefaultTableModel model;
 //public int row=0; //The row in the table about to be operated on
 public int validRow;
 public CardTypeUI(){
  super("Create Card Type",false);
  //String[] test = {"", "Boolean","Character", "Float", "Integer", "String"};
  String[] test = {"", "Boolean", "Float", "Integer", "String"};
  cBox = new JComboBox(test);
  tField = new JTextField();
  btnAdd = new JButton("Add");
  btnRemove = new JButton("Remove");
  btnReset = new JButton("Reset");
  btnConfirm = new JButton("Confirm");
  lblName = new JLabel("Field Name:");
  lblType = new JLabel("Data Type:");
  //Initialize cell values. Use 10 rows as the default visible size.
  String col[]={"Field Name", "Data Type"};
  String data[][]=new String[10][2];
  final String fieldTxt[] = {"Card ID", "Name", "Quantity", "Image", "Notes"};
  final String dataTxt[] = {"Integer", "String", "Integer", "String", "String"};
  for (int i=0;i<data.length;i++){
    for (int j=0;j<data[0].length;j++){
     if(i<5){ 
      data[i][0] = fieldTxt[i];
      data[i][1] = dataTxt[i];
      
    }
     else{data[i][j]="";} 
    }
  }
  model = new DefaultTableModel(data, col){
   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

public boolean isCellEditable(int row, int column){
    return false;
   }   
  };
  table = new JTable(model);
  table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  Dimension dim = table.getPreferredSize(); //dim records the size of table when it was 10 rows. After this line, it is okay to expand the table.
  table.setPreferredScrollableViewportSize(dim); //Set the visible size to what was recorded in dim. Anything beyond this visible size must be scrolled to reach.
  scrollpane = new JScrollPane(table);

  GridBagConstraints c = new GridBagConstraints();
  c.insets = new Insets(15,10,5,0);
  c.gridwidth = 2;
  c.anchor = GridBagConstraints.FIRST_LINE_START;
  add(lblName,c);
  
  c.insets = new Insets(0,10,10,10);
  c.gridy = 1;
  c.ipadx = 175;
  c.ipady = 5;
  add(tField,c);
  
  c.insets = new Insets(0,10,5,0);
  c.ipadx = 0;
  c.ipady = 0;
  c.gridy = 2;
  add(lblType, c);
  
  c.insets = new Insets(0,10,10,10);
  c.gridy = 3;
  c.ipadx = 90;
  add(cBox, c);
  
  c.insets = new Insets(0,10,10,10);
  c.gridy = 4;
  c.ipadx = 27;
  c.ipady = 4;
  c.gridwidth = 1;
  add(btnAdd,c);
  
  c.gridy = 5;
  c.ipadx = 18;
  add(btnReset,c);
  
  c.gridx = 1;
  c.ipadx = 0;
  c.insets = new Insets(0,0,10,10);
  add(btnConfirm,c);
  
  c.gridy = 4;
  c.ipadx = 2;
  add(btnRemove, c);
  
  c.gridx = 2;
  c.gridy = 0;
  c.ipadx = 15;
  c.gridheight = 6;
  c.insets = new Insets(14,0,10,10);
  add(scrollpane, c);
  
  scrollpane.setPreferredSize(new Dimension(155,174));
  
  
  //Action
  btnAdd.addActionListener(this);
  btnRemove.addActionListener(this);
  btnReset.addActionListener(this);
  btnConfirm.addActionListener(this);
  
  this.pack();
  this.setLocation(100,100);
  scrollpane.setSize(scrollpane.getPreferredSize());
  this.pack();
 }
  public static void main (String[] args){ 
   @SuppressWarnings("unused")
CardTypeUI gui = new CardTypeUI();
  }
  public void extendedActionPerformed(ActionEvent e){
   if(e.getSource() == btnAdd){
    for(int k=0; k<20; k++){
     if(k == 10) {model.addRow(new Object[]{"",""});}
     if(table.getModel().getValueAt(k,0)== "") {
      validRow = k;
      break;
     }
    }
    
    String tText = tField.getText();
    String dText = (String) cBox.getSelectedItem();
    String tTxt = tText.toLowerCase();
    if(tText.length()  == 0 || dText == "" || tTxt.equals("card id") || tTxt.equals("name") || tTxt.equals("quantity") || tTxt.equals("image") || tTxt.equals("notes")){return;}
    if (validRow<5&&validRow>=0){
     //tText = fieldTxt[table.getSelectedRow()];
     table.getModel().setValueAt(dText, validRow, 1);
    }
    else if (validRow>=5){
     table.getModel().setValueAt(tText, validRow, 0);
     table.getModel().setValueAt(dText, validRow, 1);
    }
   }
   else if(e.getSource() == btnRemove){
    if (table.getSelectedRow()>=5){
     table.getModel().setValueAt("", table.getSelectedRow(), 0);
     table.getModel().setValueAt("", table.getSelectedRow(), 1);
     model.removeRow(table.getSelectedRow());
     if(table.getRowCount() < 10) {model.addRow(new Object[]{"",""});}
    }
   }
   else if(e.getSource() == btnReset){
    while(table.getRowCount()>10){
     model.removeRow(table.getRowCount()-1);
    }
    for(int i = 5; i<10; i++) {
     table.getModel().setValueAt("", i, 0);
     table.getModel().setValueAt("", i, 1);
    }
   }
   else if(e.getSource() == btnConfirm){
     if (table.getValueAt(5,1)==""){
       JOptionPane.showMessageDialog(this, "You must add at least one custom field.", "Invalid Submission", JOptionPane.ERROR_MESSAGE);
       return;
     }
    String ctName = JOptionPane.showInputDialog("Enter card type name:");
    
    while(ctName == null || ctName.equals("")){
     if(ctName == null){
      return;
     }
     else if(ctName.equals("")){
      JOptionPane.showMessageDialog(this, "Please enter a valid card type name", "Invalid Card Type Name", JOptionPane.ERROR_MESSAGE);
      ctName = JOptionPane.showInputDialog("Enter card type name:");
     }
    }

    int rowCount = 0;
    String types[];
    String fields[];
    for (int i=5; i<table.getRowCount(); i++){
     String elem = (String)table.getModel().getValueAt(i,0);
     if (elem=="" || elem==null){
      break;
     }
     rowCount++;
    }
    types = new String[rowCount];
    fields = new String[rowCount];
    for (int i=0; i<rowCount; i++){
     fields[i]=(String)table.getModel().getValueAt(i+5,0);
     types[i]=(String)table.getModel().getValueAt(i+5,1);
     System.out.println(fields[i]);
    }
    Core.addCardType(ctName,fields,types);
   }
  }
}
