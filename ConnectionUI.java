import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class ConnectionUI extends GenericUI implements ActionListener {
 /**
  * 
  */
 private static final long serialVersionUID = -1739401397813352399L;
 private JButton serverPortButton, clientButton, startButton;
 private JTabbedPane tabbedPane;
 private JTextArea textArea = new JTextArea(10, 10);
 private JTextField usernameField, serverPortField, clientPortField,
   clientHostField;
 private JPanel hostPanel, joinPanel;
 private JComboBox deckSelect;
 private ConnectionPlayerClientProtocol clientPro;
 private ConnectionPlayerServerProtocol clientServerPro;
 private SimpleServer server;
 private SimpleClient client;
 private SimpleClient clientServer;
 private JLabel clientStatus;
 private EventQueue Q;
 private WaitLock WL;
 private WaitCountLock WCL;

 public ConnectionUI() {
  super(400, 400, "Direct Connection", false);
  Q = new EventQueue();
  WL = new WaitLock();

  JPanel lobbyPane = new JPanel();
  lobbyPane.setLayout(new GridBagLayout());
  GridBagConstraints con = new GridBagConstraints();
  lobbyPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

  tabbedPane = new JTabbedPane();

  clientStatus = new JLabel("Waiting to connect.");

  usernameField = new JTextField(6);

  serverPortField = new JTextField("51293", 5);
  serverPortButton = new JButton("Host");
  serverPortButton.addActionListener(this);

  clientPortField = new JTextField("51293", 5);
  clientHostField = new JTextField("localhost", 10);
  clientButton = new JButton("Connect");
  clientButton.addActionListener(this);

  startButton = new JButton("Start the Game");
  startButton.addActionListener(this);
  startButton.setEnabled(false);

  textArea = new JTextArea("Press 'Host' to allow players to join", 6, 6);
  textArea.setEditable(false);
  JScrollPane scrollPane = new JScrollPane(textArea);

  try {
   deckSelect = new JComboBox((Object[]) Core.getDeckNamesArray());
  } catch (NullPointerException e) {
   deckSelect = new JComboBox(new Object[] {});
  }

  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 0;
  con.gridy = 0;
  con.gridwidth = 1;
  con.insets = new Insets(0, 0, 0, 200);
  lobbyPane.add(new JLabel("Username: "), con);

  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 1;
  con.gridy = 0;
  con.weightx = 1;
  con.gridwidth = 2;
  con.insets = new Insets(0, 0, 0, 0);
  lobbyPane.add(usernameField, con);

  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 0;
  con.gridy = 1;
  con.gridwidth = 1;
  con.insets = new Insets(0, 0, 0, 200);
  lobbyPane.add(new JLabel("Select a Deck: "), con);

  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 1;
  con.gridy = 1;
  con.weighty = 1;
  con.gridwidth = 1;
  con.insets = new Insets(0, 0, 0, 0);
  lobbyPane.add(deckSelect, con);

  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 0;
  con.gridy = 2;
  con.weighty = 1;
  con.gridwidth = 2;
  lobbyPane.add(tabbedPane, con);

  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 1;
  con.gridy = 3;
  con.weighty = 1;
  con.gridwidth = 1;
  lobbyPane.add(startButton, con);

  // ==========================================================
  //
  // Host Panel Components
  //
  // ==========================================================

  hostPanel = new JPanel();
  hostPanel.setLayout(new GridBagLayout());

  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 0;
  con.gridy = 0;
  con.weightx = 0.5;
  con.gridwidth = 1;
  hostPanel.add(new JLabel("Your IP address is: "), con);

  JTextField ipLabel = new JTextField("");
  ipLabel.setEditable(false);
  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 1;
  con.gridy = 0;
  con.weightx = 0.5;
  con.gridwidth = 2;
  try {
   ipLabel.setText(InetAddress.getLocalHost().getHostAddress());
  } catch (UnknownHostException e) {
   ipLabel.setText("Could not aquire your IP address.");
  }
  hostPanel.add(ipLabel, con);

  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 0;
  con.gridy = 1;
  con.gridwidth = 1;
  hostPanel.add(new JLabel("Port:"), con);

  con.fill = GridBagConstraints.VERTICAL;
  con.gridx = 1;
  con.gridy = 1;
  con.gridwidth = 1;
  hostPanel.add(serverPortField, con);

  con.fill = GridBagConstraints.VERTICAL;
  con.gridx = 2;
  con.gridy = 1;
  con.gridwidth = 1;
  hostPanel.add(serverPortButton, con);

  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 0;
  con.gridy = 2;
  con.gridwidth = 1;
  hostPanel.add(new JLabel("Connected Players:"), con);

  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 1;
  con.gridy = 2;
  con.gridwidth = 2;
  hostPanel.add(scrollPane, con);

  // ==========================================================
  //
  // Join Panel Components
  //
  // ==========================================================

  joinPanel = new JPanel();
  joinPanel.setLayout(new GridBagLayout());

  con.weighty = 0;
  con.weightx = 0.1;

  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 0;
  con.gridy = 0;
  con.gridwidth = 1;
  joinPanel.add(new JLabel("Host IP:"), con);

  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 1;
  con.gridy = 0;
  con.gridwidth = 2;
  joinPanel.add(clientHostField, con);

  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 0;
  con.gridy = 1;
  con.gridwidth = 1;
  joinPanel.add(new JLabel("Host Port:"), con);

  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 1;
  con.gridy = 1;
  joinPanel.add(clientPortField, con);

  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 2;
  con.gridy = 1;
  joinPanel.add(clientButton, con);

  con.fill = GridBagConstraints.HORIZONTAL;
  con.gridx = 1;
  con.gridy = 2;
  joinPanel.add(clientStatus, con);

  tabbedPane
    .addTab("Host Game", null, hostPanel, "Host an internet game");
  tabbedPane
    .addTab("Join Game", null, joinPanel, "Join an internet game");

  this.add(lobbyPane);
  this.pack();

 }

 public void actionPerformed(ActionEvent e) {
  if (e.getSource() == serverPortButton) {
   if (usernameField.getText().equals("")) {
    JOptionPane.showMessageDialog(this, "Please enter a Username.");
    return;
   }
   if (client != null) {
    client.close();
    client = null;
   }
   if (clientPro != null) {
    clientPro.close();
    clientPro = null;
   }
   ConnectionHostServerProtocol s = new ConnectionHostServerProtocol(
     textArea, usernameField.getText(), Q, WL);

   String text = serverPortField.getText();
   try {
    int port = Integer.parseInt(text);

    server = new SimpleServer(port, s);
    JOptionPane.showMessageDialog(this, "Created the Server.");
    textArea.setText("");
    startButton.setEnabled(true);
   } catch (Exception ex) {
    ex.printStackTrace();
   }

   
   // CreateGameUI.main(1);
  } else if (e.getSource() == clientButton) {
   if (usernameField.getText().equals("")) {
    JOptionPane.showMessageDialog(this, "Please enter a Username.");
    return;
   }
   startButton.setEnabled(false);
   if (server != null) {
    server.close();
    server = null;
   }
   if (client != null || clientPro != null) {
    int i = JOptionPane.showConfirmDialog(this,
      "Close Connection?",
      "You already have an open connection.\n"
        + "Are you sure you want to close that\n"
        + "connection and open a new one?",
      JOptionPane.YES_NO_OPTION);
    if (i == JOptionPane.YES_OPTION) {
     client.close();
     client = null;
     clientPro.close();
     clientPro = null;
     clientServer.close();
     clientServer = null;
     clientServerPro.close();
     clientServerPro = null;
    } else {
     return;
    }
   }
   clientPro = new ConnectionPlayerClientProtocol(
     usernameField.getText(), Q);
   clientServerPro = new ConnectionPlayerServerProtocol(null, deckSelect,
     usernameField.getText());
   try {
    int port = Integer.parseInt(clientPortField.getText());
    String host = clientHostField.getText();
    client = new SimpleClient(host, port, clientPro);
    clientServer = new SimpleClient(host, port, clientServerPro);
    clientStatus.setText("Connected to server player "
     	     + clientPro.hostname);
    
    JOptionPane.showMessageDialog(this, "Connected to server.");
   } catch (Exception ex) {
    ex.printStackTrace();
    clientPro = null;
    clientServerPro = null;
   }

   

   // CreateGameUI.main(1);
  } else if (e.getSource() == startButton) {
   // This button may be pressed only by the host of a game with at
   // least one connected player.
   // If the host presses this button while connected as a client to a
   // different host, a message will appear telling him to disconnect
   // from that connection.
   // Once pressed, this should call a Core function which opens a
   // SandboxUI instance (set to multiplayer) and sets the onlineGame
   // variable to reference
   // the SandboxUI opened.
   String[] clients = textArea.getText().trim().split("\n");
   String[] full = new String[clients.length + 1];
   full[0] = usernameField.getText();
   for (int i = 0; i < clients.length; i++) {
    full[i + 1] = clients[i];
   }
   Core.setEventQueue(Q);
   Core.startMultiplayerGame(full, usernameField.getText(),
     (String) deckSelect.getSelectedItem());
   
   WL.setCondition(true);
   WCL = new WaitCountLock(full.length);
   Core.setTempWCL(WCL);
   
   String command = "StartDeckOrder "+ full.length;
   //System.err.println(command);
   
   Core.receiveBlock(command);
   Core.sendBlock(command);
   Core.sendDeckInfo(Core.getOnlineGame().getDeck(), Core.getOnlineGame().getMe().getId() - 1);
   Core.sendBlock("ReadyToReceive");
   
   WCL.waitOnLock();
   
   Core.receiveBlock("EndDeckOrder");
   Core.sendBlock("EndDeckOrder");
   System.out.println("Host has loaded all decks, and sent them back out");
   
  }
 }

 public boolean close() {
  if (client != null) {
   client.close();
   client = null;
  }
  if (clientPro != null) {
   clientPro.close();
   clientPro = null;
  }
  if (server != null) {
   server.close();
   server = null;
  }
  return true;
 }

 public static void main(String[] args) {
  @SuppressWarnings("unused")
  ConnectionUI lui = new ConnectionUI();

 }
}