import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

@SuppressWarnings("unused")
public class WelcomeUI {
  private static String password = "mypass";
  private JTextField usernameInput;
  private JPasswordField passwordInput;
  private JButton login, help, createProfile;
  private JLabel labelUser, labelPass, background, title;
  private JFrame frame;
  
  public WelcomeUI() {
    try{
      UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
    } catch(Exception e) {
    }
    usernameInput = new JTextField(20);
    passwordInput = new JPasswordField(20);
    login = new JButton("       Login       ");
    help = new JButton("        Help        ");
    createProfile = new JButton("Create Profile");
    frame = new JFrame("Welcome");
    
    labelUser = new JLabel("User Name:");
    labelUser.setFont(new Font("Serif", Font.PLAIN, 20));
    labelUser.setForeground(Color.WHITE);
    
    labelPass = new JLabel("Password:");
    labelPass.setFont(new Font("Serif", Font.PLAIN, 20));
    labelPass.setForeground(Color.WHITE);
    
    title = new JLabel("");
    title.setFont(new Font("Serif", Font.PLAIN, 30));
    Image j = java.awt.Toolkit.getDefaultToolkit().getImage( "logo3.png" );  
    title.setIcon(new ImageIcon(j.getScaledInstance(750/2, 500/2, 0)));
    
    BackgroundPanel p = new BackgroundPanel(new GridBagLayout());  
    Image i = java.awt.Toolkit.getDefaultToolkit().getImage( "wall.jpg" );  
    p.setBackgroundImage( i ); 
    p.setBackgroundType( BackgroundPanel.SCALED );  
    frame.getContentPane().add( p );  
    
    /*
    frame.setSize(400,400);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    try {
    background = new JLabel(new ImageIcon(ImageIO.read(new File("background.jpg"))));
    } catch(IOException e) {
      e.printStackTrace();
    }
    background.setLayout(new FlowLayout());
    
    frame.add(background);
    frame.pack();
    */
    
    // Creating the grid
    /*
    JPanel panel = new JPanel(new GridBagLayout());
    frame.getContentPane().add(panel, BorderLayout.CENTER);
    */
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(1,1,1,1);
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 1;
    c.gridy = 0;
    p.add(title,c);
    
    c.insets = new Insets(1,1,1,1);
    c.gridx = 0;
    c.gridy = 1;
    p.add(labelUser,c);
    
    // Create some elements

    
    c.gridx = 1;
    c.gridy = 3;
    c.fill = GridBagConstraints.CENTER;
    login.addActionListener(new LoginButton());
    p.add(login,c);
    
    c.gridx = 1;
    c.gridy = 5;
    help.addActionListener(new LoginButton());
    p.add(help,c);
    
    c.gridx = 1;
    c.gridy = 4;
    help.addActionListener(new LoginButton());
    p.add(createProfile,c);
    
    c.gridx = 1;
    c.gridy = 1;
    c.fill = GridBagConstraints.HORIZONTAL;
    p.add(usernameInput,c);
    
    c.gridx = 0;
    c.gridy = 2;
    p.add(labelPass,c);
    
    c.gridx = 1;
    c.gridy = 2;
    p.add(passwordInput,c);
    
    frame.setDefaultCloseOperation( javax.swing.WindowConstants.DISPOSE_ON_CLOSE );  
    frame.setSize( 600, 600 );  
    frame.setVisible( true );
    frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
    //frame.pack();
  }
  
  public static void main(String[] args){
    WelcomeUI ui = new WelcomeUI();
  }
  
  class LoginButton implements ActionListener{
    
    public void actionPerformed(ActionEvent e){
      //JTextField usernameInput = (JTextField)e.getSource();
      String username = (usernameInput.getText().length()>0?usernameInput.getText():"Error");
      JOptionPane.showMessageDialog(null,"Text is : "+username);
    }
  }
  class BgPanel extends JPanel {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Image bg = new ImageIcon("background.jpg").getImage();
    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(bg, 0, 0, getWidth(), getHeight(), this);
    }
  }
}