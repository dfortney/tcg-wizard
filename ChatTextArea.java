import java.util.Collections;
import java.util.Vector;

import javax.swing.JTextArea;

public class ChatTextArea extends JTextArea {
	private static final long serialVersionUID = -3013559271462436112L;
	private Vector<ChatEntry> entries;

	public synchronized void append(String s) {
		super.append(s);
		entries.add(new ChatEntry(s, ""));
	}

	public synchronized void append(String s, String user) {

		entries.add(new ChatEntry(s, user));

		Collections.sort(entries);

		super.setText("");
		super.append(entries.get(0).toString());
		for (int i = 1; i < entries.size(); i++) {
			super.append("\n" + entries.get(i));
		}
	}
	

	public ChatTextArea() {
		super();
		entries = new Vector<ChatEntry>();
	}

	public ChatTextArea(int c, int r) {
		super(c, r);
		entries = new Vector<ChatEntry>();
	}
}
