import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import javax.swing.*;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import java.util.*;

public class Stack {
 JPopupMenu option;
 JMenuItem flipCard;
 JMenuItem draw, shuffle, delete, arrangeStack, tap, drawMultiple, revealX;
 ArrayList<FieldCard> cards;
 JLabel container;
 JLabel rotation;
 Point location;
 boolean faceup;
 boolean oc;
 BufferedImage content;
 GenericUI parent;
 Player p;
 FieldStack fs;
 int id;
 private static int globalId = 0;
 boolean isDeck = false;

 public Stack(GenericUI parent, String name, int x, int y){
  //This method is specially reserved for creating blank stacks so it doesn't need anything aside from an id
  id = globalId++;
  cards = new ArrayList<FieldCard>();
  if(name.equals("transparent.gif") || name.equals("empty"))
   cards.add(new FieldCard("empty", name));
  else
   cards.add(new FieldCard(",,,",name));//This shouldn't ever happen
  cards.add(new FieldCard("empty","transparent.gif"));
  container = new JLabel();
  container.setIcon(cards.get(cards.size() - 1).getIcon());
  rotation = new JLabel() {
   /**
  * 
  */
 private static final long serialVersionUID = 1L;

public Dimension getPreferredSize() {
    return new Dimension(246, 177);
   }

   @Override
   protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    g2.rotate(Math.PI / 2, 177.0 / 2.0, 246.0 / 2.0);
    g2.drawImage(content, -35, -35, null);
   }
  };
  rotation.setVisible(false);
  content = cards.get(cards.size() - 1).getImage(); 
  faceup = true;
  location = new Point(x, y);
 }

 public Stack(GenericUI parent, ArrayList<FieldCard> cards, int x, int y, Player p) {
  //Used primarily for spawning decks
  oc = false;
  if (p != null){
   this.p = p;
  }
  id = globalId++; 
  this.parent = parent;
  option = new JPopupMenu();
  flipCard = new JMenuItem("Flip face down");
  draw = new JMenuItem("Draw Card");
  drawMultiple = new JMenuItem("Draw Multiple Cards");
  shuffle = new JMenuItem("Shuffle Stack");
  delete = new JMenuItem("Delete Stack");
  arrangeStack = new JMenuItem("Arrange Stack of Cards");
  tap = new JMenuItem("Tap Stack");
  revealX = new JMenuItem("Reveal X cards");
  revealX.addActionListener(parent);
  arrangeStack.addActionListener(parent);
  option.add(arrangeStack);
  flipCard.addActionListener(parent);
  draw.addActionListener(parent);
  shuffle.addActionListener(parent);
  delete.addActionListener(parent);
  tap.addActionListener(parent);
  drawMultiple.addActionListener(parent);
  option.add(flipCard);
  option.add(draw);
  option.add(shuffle); 
  //option.add(delete);
  option.add(tap);
  option.add(arrangeStack);
  option.add(drawMultiple);
  option.add(revealX);
  this.cards = cards;
  container = new JLabel();
  container.setIcon(cards.get(cards.size() - 1).getIcon());
  rotation = new JLabel() {
   /**
  * 
  */
 private static final long serialVersionUID = 1L;

public Dimension getPreferredSize(){
    return new Dimension(246, 177);
   }

   @Override
   protected void paintComponent(Graphics g){
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    g2.rotate(Math.PI / 2, 177.0 / 2.0, 246.0 / 2.0);
    g2.drawImage(content, -35, -35, null);
   }
  };
  rotation.setVisible(false);
  content = cards.get(cards.size() - 1).getImage();
  faceup = true;
  location = new Point(x, y);
  if(p != null){
   this.p.addStack(this);
  }
 }

 public Stack(GenericUI genericUI, FieldCard card, int x, int y, Player player, int face){
  oc = false;
  if (player != null){
   p = player;
  }
  parent = genericUI;
  id = globalId++;
  option = new JPopupMenu();
  flipCard = new JMenuItem();
  draw = new JMenuItem("Draw Card");
  drawMultiple = new JMenuItem("Draw Multiple Cards");
  shuffle = new JMenuItem("Shuffle Stack");
  delete = new JMenuItem("Delete Stack");
  arrangeStack = new JMenuItem("Arrange Stack of Cards");
  tap = new JMenuItem("Tap Stack");
  revealX = new JMenuItem("Reveal X cards");
  revealX.addActionListener(parent);
  arrangeStack.addActionListener(parent);
  flipCard.addActionListener(parent);
  draw.addActionListener(parent);
  shuffle.addActionListener(parent);
  delete.addActionListener(parent);
  tap.addActionListener(parent);
  drawMultiple.addActionListener(parent);
  
  option.add(flipCard);
  option.add(draw);
  option.add(shuffle);
  //option.add(delete);
  option.add(tap);
  option.add(arrangeStack);
  option.add(drawMultiple);
  option.add(revealX);
  this.cards = new ArrayList<FieldCard>();
  this.cards.add(card);
  container = new JLabel();
  container.setIcon(this.cards.get(this.cards.size() - 1).getIcon());
  rotation = new JLabel(){
   /**
  * 
  */
 private static final long serialVersionUID = 1L;

public Dimension getPreferredSize(){
    return new Dimension(246, 177);
   }

   @Override
   protected void paintComponent(Graphics g){
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    g2.rotate(Math.PI / 2, 177.0 / 2.0, 246.0 / 2.0);
    g2.drawImage(content, -35, -35, null);
   }
  };
  if(face != 0){
   try{
    content = ImageIO.read(new File("backsmall.jpg"));
   }
   catch (IOException ex2){
   }
   Core.imgMaker("backsmall.jpg", container, 177, 246);
   faceup = false;
   flipCard.setText("Flip Face Up");
  } 
  else{
   faceup = true;
   content = cards.get(cards.size() - 1).getImage();
   flipCard.setText("Flip Face Down");
  }
  rotation.setVisible(false);
  location = new Point(x, y);
  if(p != null){
   this.p.addStack(this);
  }
 }

 public void setCards(ArrayList<FieldCard> j){
  cards = new ArrayList<FieldCard>(j);
 }
 
 public void draw(){
  if(cards.size() <= 0)
   return;
  p.getHand().addCard(cards.get(cards.size() - 1));
  p.addToBuffer(cards.get(cards.size()-1));
  //ArrayList<FieldCard> cardsIn = new ArrayList<FieldCard>(Arrays.asList(p.getHand().getCards(p.getHand().getSize(), false)));
//  ArrayList<FieldCard> cardsIn = p.getHand().getCards();
  Hand hand = p.getHand();
  HandPanel hp = hand.getHandPanel();
  if (hp != null) // If hp is null, it hasn't been initialized yet, in
   // which case ignore the next line.
   hp.refresh();
  cards.remove(cards.size() - 1);
  refreshImg();
  //FieldStack.updateCards(cards);
  //FieldStack fs = FieldStack.sta;
  //fs.updateCards(cards);
 }
 
 public void setFieldStack(FieldStack f){
  fs = f;
 }
 
 public FieldStack getFieldStack(){
  return fs;
 }
 
 public void shuffle(){
  Collections.shuffle(cards);
  this.refreshImg();
  System.out.println(cards.get(cards.size() - 1));
 }

 public void addCardToTop(FieldCard card){
  cards.add(card);
 }

 public void addCardToBot(FieldCard card){
  cards.add(0, card);
 }

 public void insertCard(FieldCard card, int idx){
  if(idx < 0)
   idx = 0;
  if(idx > cards.size()){
   cards.add(card);
   return;
  }
  cards.add(idx, card);
 }

 public void insertCardFromTop(FieldCard card, int idx){
  insertCard(card, cards.size() - idx);
 }

 public ArrayList<FieldCard> getCards(){
  return cards;
 }

 public ArrayList<FieldCard> getNCards(int n) {
   ArrayList<FieldCard> tempCards = new ArrayList<FieldCard>();
   int i = 0;
   if(n > cards.size()) {
     n = cards.size();
   }
   while(i < n) {
     tempCards.add(cards.get(cards.size()-1-i));
     i++;
   }
   return tempCards;
 }
 public void refreshImg(){
  // Need to handle stacks of size 0 by displaying an empty space. Don't
  // merely return.
  if(cards.size() <= 0)
   return;
  FieldCard top = null;
  if(faceup == true){
   top = cards.get(cards.size() - 1);
   content = top.getImage();
   container.setIcon(null);
   container.setIcon(top.getIcon());
  } 
  else{
   try{
    content = ImageIO.read(new File("backsmall.jpg"));
   }catch (IOException ex2){
    System.err.println("Error: Reading in back image");
   }
   Core.imgMaker("backsmall.jpg", container, 177, 246);
  }
  // img.getScaledInstance(177, 246, java.awt.Image.SCALE_SMOOTH);
  parent.repaint();
 }

 public JLabel tap(){
  if(container.isVisible()){
   container.setVisible(false);
   rotation.setVisible(true);
   return rotation;
  }
  else{
   container.setVisible(true);
   rotation.setVisible(false);
   return container;
  }
 }

 public JLabel getContainer(){
  return container;
 }

 public void setContainer(JLabel cont){
  container = cont;
 }

 public Point getPosition(){
  return location;
 }

 public double getX(){
  return location.getX();
 }

 public double getY(){
  return location.getY();
 }

 public void setPosition(Point position){
  location = position;
 }

 public void setX(int x){
  location.setLocation(x, location.getY());
 }

 public void setY(int y){
  location.setLocation(location.getX(), y);
 }

 public void setFaceup(boolean b){
  this.faceup = b;
  if(b)
   flipCard.setText("Flip Face Down");
  else
   flipCard.setText("Flip Face Up");
 }

 public JLabel getRotation(){
  return rotation;
 }

 public void setAsDeck(){
  isDeck = true;
 }

 public Player getPlayer(){
  return p;
 }

 public int getId(){
  return id;
 }

 public String getDescriptor(){
  if(isDeck){
   return (p.getName() + "'s deck");
  }
  // Other possibilities, like isGraveyard, go here as they are
  // implemented.
  else if(!faceup){ // Don't know what else to do for face-down stacks.
   return ("a face-down stack of cards");
  } else{ // By default, the descriptor is the name of the top card.
    if (cards.size() == 0) return "an empty space";
   FieldCard card = cards.get(cards.size() - 1);
   return card.getName();
  }
 }

}