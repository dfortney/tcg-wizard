import java.awt.Dimension;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;


public class LobbyUI extends GenericUI implements ActionListener {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private final static String newline = "\n";
  Object[][] lobbyData; //Table of Lobby Games and Fields
  //private JTable table;
  private DefaultTableModel model;
  int COLUMNS = 3;
  int ROWS = 4;
  String nF;
  String fF, tF;
  String fields[] = {"Name","Joined/Required", "Game Format"};
  String fillOne[] = {"Boss", "1", "3"};
  String fillTwo[] = {"Noobs", "5", "1"};
  String fillThree[] = {"Win FTW", "1", "1"};
  JTextField chat;
  private JButton create, join;
  private JTable lobby;
  
  JTextArea textArea = new JTextArea(50,50);
  
  public LobbyUI() {
    super(100,150, "Lobby", false);
    create = new JButton("Create Game");
    join = new JButton("Join Game");
    create.addActionListener(this);
    join.addActionListener(this);
    JPanel lobbyPane = new JPanel();
    lobbyPane.setLayout(new GridBagLayout());
    GridBagConstraints con = new GridBagConstraints();
    lobbyPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10)); 
    String lobbyData[][] = new String[ROWS][COLUMNS];
    lobbyData[0] = fields;
    lobbyData[1] = fillOne;
    lobbyData[2] = fillTwo;
    lobbyData[3] = fillThree;
    
    model = new DefaultTableModel(lobbyData, fields);
    lobby = new JTable();
    lobby.setModel(model);
    JScrollPane scrollLobby = new JScrollPane(lobby);
    scrollLobby.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
    //scrollLobby.setPreferredSize(new Dimension(100, 100));
    //scrollLobby.setMinimumSize(new Dimension(10,10));
    
    
    textArea.setLineWrap(true);
    textArea.setWrapStyleWord(true);
    textArea.setSize(50, 50);
    JScrollPane areaScrollPane = new JScrollPane(textArea);
    areaScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    areaScrollPane.setPreferredSize(new Dimension(50, 50));
    //areaScrollPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Lobby Chat"),BorderFactory.createEmptyBorder(5,5,5,5)),areaScrollPane.getBorder()));
    textArea.setEditable(false);
    
    chat = new JTextField(20);
    chat.addActionListener(this);
    JLabel filler = new JLabel("                                          ");
    
    
    con.fill = GridBagConstraints.HORIZONTAL;
    con.gridx = 0;
    con.gridy = 0;
    con.insets = new Insets(0,0,10,10); 
    con.anchor = GridBagConstraints.NORTHWEST;
    lobbyPane.add(lobby, con);
    
    con.fill = GridBagConstraints.HORIZONTAL;
    con.gridx = 1;
    con.gridy = 0;
    con.insets = new Insets(10,10,10,10); 
    lobbyPane.add(filler);
    
    con.fill = GridBagConstraints.VERTICAL;
    con.gridx = 2;
    con.gridy = 0;
    con.insets = new Insets(0,10,10,0); 
    lobbyPane.add(textArea, con);
    
    con.fill = GridBagConstraints.HORIZONTAL;
    con.gridx = 0;
    con.gridy = 1;
    con.gridwidth = 3;
    con.insets = new Insets(10,0,0,0); 
    lobbyPane.add(chat, con);
    
    con.fill = GridBagConstraints.HORIZONTAL;
    con.gridx = 0;
    con.gridy = 2;
    con.gridwidth = 1;
    con.insets = new Insets(0,0,0,200); 
    lobbyPane.add(join, con);
    
    con.fill = GridBagConstraints.HORIZONTAL;
    con.gridx = 1;
    con.gridy = 2;
    lobbyPane.add(create, con);
    
    this.add(lobbyPane);
    this.pack();
    
//    public void extendedActionPerformed(ActionEvent e){
//      if(e.getSource() == create){
//        for(int k=0; k<20; k++){
//          if(k == 10) {model.addRow(new Object[]{"",""});}
//          if(table.getModel().getValueAt(k,0)== "") {
//            validRow = k;
//            break;
     
    }
    
    
  //  setLayout(new GridBagLayout());
//        //Create and set up the window.
//        JFrame frame = new JFrame("Lobby");
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        //Add content to the window.
//        frame.add(lobbyPane);
//        //Display the window.
//        frame.pack();
//        frame.setVisible(true);
  

  public void actionPerformed(ActionEvent e) {
    String text = chat.getText();
    textArea.append(text + newline);
    chat.selectAll();
    textArea.setCaretPosition(textArea.getDocument().getLength());
    
    if(e.getSource() == create){
       CreateGameUI.main(1);
       if(CreateGameUI.bool ==1) {
         nF = CreateGameUI.nF;
         fF = CreateGameUI.fF;
         Object[] rowData = {"Winner", nF, fF};
         model.addRow(rowData);
         //for(int k=0; k<20; k++){
          //if(k == 10) {lobbyData.addRow(new Object[]{"",""});}
         // if(table.getModel().getValueAt(k,0)== "") {
            //validRow = k;
          //  break;
         // }
       //  }
    }
  }
  }
  public static void main(String[] args) {
        @SuppressWarnings("unused")
		LobbyUI lui = new LobbyUI();
      
    }
}
                                   
    
    
    
    
    