import java.io.*;
import java.net.*;

/**
 * The SimpleClient takes care of basic client functionality, sending data to
 * the server and waiting for a response. As with the SimpleServer, the
 * SimpleClient does not need to be chagned or extended to be used. Simply
 * create a SimpleClient and supply it a subclass of Protocol that performs the
 * operations you want the client to take upon connecting to the server.
 * 
 * @author ktheller
 * 
 */
public class SimpleClient extends Thread {
	private int port;
	private String hostName;
	private Protocol p;
	private Boolean running = true;
	private PrintWriter out = null;
	
	/**
	 * Shows an example use of the SimpleClient
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Going to start client");
		KnockKnockClientProtocol test = new KnockKnockClientProtocol(System.in);
		@SuppressWarnings("unused")
		SimpleClient s = new SimpleClient("localhost", 4445, test);
		System.out.println("client is Running");
	}

	public SimpleClient(String hostName, int port, Protocol p) {
		if(port < 0 || port > 65535){port = 4444;}
		this.p = p;
		this.port = port;
		this.hostName = hostName;
		this.start();
	}

	public void run() {
		Socket Socket = null;
		BufferedReader in = null;

		try {
			Socket = new Socket(hostName, port);
			out = new PrintWriter(Socket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(
					Socket.getInputStream()));
		} catch (UnknownHostException e) {
			System.err.println("Failed to connect to host: " + hostName);
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection to: "
					+ hostName);
			System.exit(1);
		}

		String fromServer;

		try {
			while ((fromServer = in.readLine()) != null && running) {
				if (fromServer.equals(p.getTerminator()))
					break;

				out.println(p.processInput(fromServer));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		p.close();
		out.close();

		try {
			in.close();
			Socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public void close(){
		out.println(p.getTerminator());
		running=false;
	}
}
