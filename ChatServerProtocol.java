import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class ChatServerProtocol extends Protocol {
	private String name;
	private ChatTextArea tArea;

	/**
	 * processInput handles the actions of a server after receiving data from a
	 * client.
	 * 
	 * @param s
	 *            One line of data from the client
	 * @return The string to send back to the client.
	 */
	public String processInput(String s) {
		if (s != null) {
			
			if(s.contains("#END")){
				return "#END";
			}
			
			if(s.contains("#NAME")){
				s = s.replace("#NAME", "");
				
				try {
					s = URLDecoder.decode(s, "UTF-8");
				} catch (UnsupportedEncodingException e) {e.printStackTrace();}
				
				name = s;
				return "#USERSET";
			}
			
			try {
				s  = URLDecoder.decode(s, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			if(name == null){
				tArea.append(s, "Stranger");
			} else {
				tArea.append(s, name);
			}
		} else {return "#WELCOME";}
		return "#READ";
	}

	public ChatServerProtocol(ChatTextArea cta) {
		super("#END");
		tArea = cta;
	}

	/**
	 * The close function is called after the client of server finishes using
	 * the protocol. Can be used to clean up resources
	 */
	public void close() {
	}
	
	public Protocol clone(){
		return new ChatServerProtocol(tArea);
	}

}
