import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.JLabel;
import java.net.*;


public class FieldCard{
	private String name;
	private String imagePath;
 
	FieldCard(String name, String file){
		this.name = name;
		this.imagePath = file;
	}
	
	public String getName(){
		return name;
	}
	
	public String getImagePath(){
		return imagePath;
	}
	
	public BufferedImage getRaw(){
		BufferedImage content = null;
		try{
			URL url = new URL(imagePath);
			content=ImageIO.read(url);
		}catch(Exception ex){
			try{
				content = ImageIO.read(new File(imagePath));
			}catch(IOException ex1){
				try{
					content = ImageIO.read(new File("backofamagicthegatheringcard.jpg"));
				}
				catch(IOException ex2){
					System.err.println("FieldCard: backofamgicthegatheringcard.jpg not found!");
				}
			}
		}
		return content;
	}
	
	public Icon getIcon(){
		JLabel container = new JLabel();
		Core.imgMaker(imagePath, container, 177, 246);
		return container.getIcon();
	}
 
	public BufferedImage getImage(){
		BufferedImage content = null;
		try{
			URL url = new URL(imagePath);
			content=ImageIO.read(url);
		}catch(Exception ex){
			try{
				content = ImageIO.read(new File(imagePath));
			}catch(IOException ex1){
				try{
					content = ImageIO.read(new File("backofamagicthegatheringcard.jpg"));
				}
				catch(IOException ex2){
					System.err.println("FieldCard: backofamgicthegatheringcard.jpg not found!");
				}
			}
		}
		double w = content.getWidth();
		double h = content.getHeight();
		BufferedImage after = new BufferedImage((int)w, (int)h, BufferedImage.TYPE_INT_ARGB);
		AffineTransform at = new AffineTransform();
		at.scale(177/w, 246/h);
		AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		after = scaleOp.filter(content, after);
		content = after;
		return content;	
	}
}