import javax.swing.table.*;
public class DeuiThread extends Thread{
  Object[][] collData;
  int start, length;
  AbstractTableModel model;
  public DeuiThread(AbstractTableModel model, Object[][] collData, int start, int length){
    this.model = model;
    this.collData=collData;
    this.start=start;
    this.length=length;
  }
  public void run(){
    for(int j = start; j < start+length && j < collData.length; j++){
      for(int k = 0; k < collData[0].length; k++){
        collData[j][k] = (Object)model.getValueAt(j,k);
      }
    }
  }
}