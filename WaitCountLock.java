
public class WaitCountLock {
	private int count = 0;
	private int max = 0;
	
	public synchronized void increment(){
		count++;
		notifyAll();
	}
	
	public synchronized void decrement(){
		count--;
		notifyAll();
	}
	
	public synchronized void waitOnLock(){
		while (!(count == max)) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void waitOnLock(int i){
		while (!(count == i)) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void waitOnLockL(){
		while (!(count < max)) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void waitOnLockL(int i){
		while (!(count < i)) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void waitOnLockLE(){
		while (!(count <= max)) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void waitOnLockLE(int i){
		while (!(count <= i)) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void waitOnLockG(){
		while (!(count > max)) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void waitOnLockG(int i){
		while (!(count > i)) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void waitOnLockGE(){
		while (!(count >= max)) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void waitOnLockGE(int i){
		while (!(count >= i)) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void waitOnLockNE(){
		while (count == max) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void waitOnLockNE(int i){
		while (count == i) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public WaitCountLock(){
	}
	
	public WaitCountLock(int i){
		max = i;
	}
}
