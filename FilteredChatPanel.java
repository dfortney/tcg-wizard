import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.*;
import javax.swing.event.*;

public class FilteredChatPanel extends JPanel implements ChangeListener {
 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private FilteredTextArea fta;
 private JScrollPane scrollpane;
 private JTabbedPane tabs;

 public static void main(String[] args) {
  JFrame frame = new JFrame("TestBox");
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

  // Add contents to the window.
  FilteredChatPanel panel = new FilteredChatPanel();
  frame.add(panel);

  panel.getTextArea().append("Hello", "Karl", FilteredChatEntry.CHAT);
  panel.getTextArea().append("Testing", "Other Guy",
    FilteredChatEntry.CHAT);
  panel.getTextArea().append("Moved a card", "Karl",
    FilteredChatEntry.LOG);
  panel.getTextArea().append("Winning", "Karl", FilteredChatEntry.CHAT);
  panel.getTextArea().append("Victory for Other Guy", "Karl",
    FilteredChatEntry.LOG);


  // Display the window.
  frame.pack();
  frame.setSize(320, 280);
  frame.setVisible(true);
 }

 public FilteredChatPanel() {
  fta = new FilteredTextArea(FilteredTextArea.CHAT);
  fta.setEditable(false);
  scrollpane = new JScrollPane(fta);
  scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
  tabs = new JTabbedPane();
  tabs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
  tabs.addChangeListener(this);

  tabs.addTab("Chat", null);
  tabs.addTab("Event Log", null);
  tabs.addTab("Both", null);

  this.setLayout(new GridBagLayout());
  GridBagConstraints c1 = new GridBagConstraints();
  c1.fill = GridBagConstraints.BOTH;
  c1.weightx = 1;
  c1.weighty = 0;

  c1.gridx = 0;
  c1.gridy = 1;
  add(tabs, c1);

  c1.gridx = 0;
  c1.gridy = 2;
  c1.weighty = 1;
  add(scrollpane, c1);
 }

 public FilteredTextArea getTextArea() {
  return fta;
 }

 @Override
 public void stateChanged(ChangeEvent e) {
  if (e.getSource() == tabs) {
   fta.setMode(tabs.getSelectedIndex() + 1);
  }
 }
}
