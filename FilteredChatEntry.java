import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class FilteredChatEntry extends ChatEntry {
	private final int type;
	public static final int CHAT = 1;
	public static final int LOG = 2;

	public FilteredChatEntry(String entry, String user, int type) {
		super(entry, user);
		if (type == CHAT) {
			this.type = CHAT;
		} else {
			this.type = LOG;
		}
	}

	public int getType() {
		return type;
	}

	public String toString() {
		DateFormat df = new SimpleDateFormat("HH:mm:ss");
		if(type == CHAT){
			return getUser() + " [" + df.format(getDate()) + "]: " + getEntry();
		} else {
			return "[" + df.format(getDate()) + "]: " + getEntry();
		}
	}
}
