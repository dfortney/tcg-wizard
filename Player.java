import java.util.*;
public class Player{
  private ArrayList<Stack> stacks;
  private Hand hand;
  private int id;
  private String name;
  private ArrayList<FieldCard> buffer;
  public Player(Hand hand, int playerID){
    this(hand, playerID, "Player "+playerID);
  }
  public Player(Hand hand, int playerID, String name){
    this.hand = hand;
    this.hand.setPlayer(this);
    id = playerID;
    this.name = name;
    stacks = new ArrayList<Stack>();   
    buffer = new ArrayList<FieldCard>();
  }
  public Hand getHand(){
    return hand;
  }
  public ArrayList<Stack> getStacks(){
    return stacks;
  }
  public void addStack(Stack stack){
    stacks.add(stack);
  }
  public int getId(){
    return id; 
  }
  public String getName(){
    return name;
  }
  public void setName(String name){
    this.name=name;
  }
  public void addToBuffer(FieldCard card)
  {
   buffer.add(card);
  }
  public ArrayList<FieldCard> getBuffer()
  {
   return buffer;
  }
  public void showBuffer()
  {
   new FieldStack(buffer,null,null);
  }
}