import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;

public class Event implements Comparable<Event> {
	private final Date date;
	private final String event;
	private final String user;
	private final int type;
	private final String notes;

	public static final int EVENT = 0;
	public static final int CHAT = 1;

	public Event(String event, String user, String type) {
		date = new Date();
		this.event = event;
		this.user = user;
		if(type.contains("E") || type.contains("e")){
			this.type = EVENT;
		} else {
			this.type = CHAT;
		}
		notes = "";
	}
	
	public Event(String event, String user, String type, String notes) {
		date = new Date();
		this.event = event;
		this.user = user;
		if(type.contains("E") || type.contains("e")){
			this.type = EVENT;
		} else {
			this.type = CHAT;
		}
		this.notes = notes;
	}

	public Event(String event, String user, int type) {
		date = new Date();
		this.event = event;
		this.user = user;
		this.type = type;
		notes = "";
	}
	
	public Event(String event, String user, int type, String notes) {
		date = new Date();
		this.event = event;
		this.user = user;
		this.type = type;
		this.notes = notes;
	}

	public Date getDate() {
		return date;
	}

	public String getEvent() {
		return event;
	}

	public String getUser() {
		return user;
	}
	
	public int getType(){
		return type;
	}
	
	public String getNotes() {
		return user;
	}

	public String toString() {
		//DateFormat df = new SimpleDateFormat("HH:mm:ss");
		String eUser = user;
		String eEvent = event;
		try {
			eUser = URLEncoder.encode(eUser, "UTF-8");
			eEvent = URLEncoder.encode(eEvent, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		String notesAdd = "";
		if(!notes.equals("")){
			 notesAdd =  "#NOTES" + notes;
		}
		
		if(type == CHAT){
			return eUser + "#CHAT" + eEvent + notesAdd;
			
		}
		return eUser + "#EVENT" + eEvent + notesAdd;
	}

	public int compareTo(Event c) {
		return date.compareTo(c.getDate());
	}
}
