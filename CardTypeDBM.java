import java.sql.Connection;

import java.sql.DriverManager;

import java.sql.PreparedStatement;

import java.sql.ResultSet;

import java.sql.SQLException;

import java.sql.Statement;

import java.util.ArrayList;

import java.util.List;

public class CardTypeDBM {

 private Connection con;

 private final String delimiter = "`";

 /**
  * 
  * An internal method to join multiple strings into
  * 
  * a single string using a connecting element
  * 
  * 
  * 
  * The input strings can be in an array or passed in
  * 
  * as separate arguments
  * 
  * 
  * 
  * @param glue
  *            a string to serve as the connective element
  * 
  * @param s
  *            the strings or array of strings to join
  * 
  * @return the joined string
  */

 private static String join(String glue, String... s) {
  int n = s.length;
  if (n == 0) {
   return null;
  }
  StringBuilder out = new StringBuilder();
  out.append(s[0]);
  for (int i = 1; i < n; i++) {
   out.append(glue).append(s[i]);
  }
  return out.toString();
 }

 /**
  * 
  * An internal method to join several ints into
  * 
  * a single string using a connecting element
  * 
  * 
  * 
  * The input ints can be in an array or passed in
  * 
  * as separate arguments
  * 
  * 
  * 
  * @param glue
  *            a string to serve as the connective element
  * 
  * @param s
  *            the ints or array of ints to join
  * 
  * @return the joined string
  */

 private static String join(String glue, int... s) {
  int n = s.length;

  if (n == 0) {

   return null;

  }

  StringBuilder out = new StringBuilder();

  out.append("" + s[0]);

  for (int i = 1; i < n; i++) {

   out.append(glue).append("" + s[i]);

  }

  return out.toString();

 }

 /**
  * 
  * An internal function that converts an array of
  * 
  * strings into an array of ints by running
  * 
  * Integer.parseInt on each element, and placing
  * 
  * them into a new array
  * 
  * 
  * 
  * @param s
  *            The array of strings to be converted
  * 
  * @return The array of ints
  */

 private static int[] convertArray(String[] s) {

  int n = s.length;

  if (n == 0) {

   return null;

  }

  int[] out = new int[n];

  for (int i = 0; i < n; i++) {

   out[i] = Integer.parseInt(s[i]);

  }

  return out;

 }

 /*
  * 
  * 0 boolean
  * 
  * 1 String
  * 
  * 2 int
  * 
  * 3 char
  * 
  * 4 float
  */

 /**
  * 
  * Present for testing purposes only
  * 
  * 
  * 
  * @param args
  *            default argument
  */

 public static void main(String[] args) {

  // TODO Auto-generated method stub

  CardTypeDBM test = new CardTypeDBM();

  // test.remove("magic");

  //test.clear();
  test.remove("magic");
  test.remove("magic2");
  test.remove("Sampler");

  /*test.add("magic", new String[] { "type", "text", "flavortext",
    "power", "toughness" },

  new int[] { 1, 1, 1, 2, 2 });

  test.printAll();

  System.out.println("\n");

  test.add("magic2", new String[] { "type", "text", "flavortext",
    "power", "toughness" },

  new int[] { 1, 1, 1, 2, 2 });*/

  test.save();

  /*CardTypeDBM test2 = new CardTypeDBM();

  test.add("magic3", new String[] { "type", "text", "flavortext",
    "power", "toughness" },

  new int[] { 1, 1, 1, 2, 2 });

  test2.add("magic4", new String[] { "type", "text",
    "flavortext", "power", "toughness" },

  new int[] { 1, 1, 1, 2, 2 });

  test.printAll();

  System.out.println("");

  test2.printAll();

  System.out.println("\n");

  test.revert();

  test.printAll();*/

  /*
   * System.out.println(join(";", test.getFields("magic")));
   * 
   * System.out.println(join("#", test.getTypes("magic")));
   * 
   * System.out.println(join(",", test.getEntries()));
   */

 }

 /**
  * 
  * Sole constructor for the Card Type Database Manager.
  * 
  * 
  * 
  * It will create the database "CardTypes.db" if it
  * 
  * does not already exist, then create a table
  * 
  * "cardtype" in that database unless that table
  * 
  * already exists
  * 
  * 
  * 
  * It will then create a database in memory
  * 
  * and add that same table to it, copy the data from
  * 
  * "CardTypes.db" and begin operations on the one
  * 
  * in memory
  * 
  * 
  * 
  * If one of these two does not exist, and the creation
  * 
  * fails, the other function in this class will almost
  * 
  * certainly have horrible errors.
  */

 public CardTypeDBM() {

  /*
   * Creates a new card type database with a String field for name, 50 (or
   * some high number) String
   * 
   * fields for fields, and 50 (or some high number) int fields to
   * indicate corresponding database
   * 
   * types. There may be a more efficient way to do this, but in any case,
   * there will be very few
   * 
   * entries in CardTypeDatabase, so memory wastage will be minimal
   */
  Statement stat = null;
  try {

   // sqlite driver

   try {

    Class.forName("org.sqlite.JDBC");

   } catch (ClassNotFoundException e) {

    // TODO Auto-generated catch block

    e.printStackTrace();

   }

   // database path, if it's new database,

   // it will be created in the project folder

   // con =
   // DriverManager.getConnection("jdbc:sqlite::memory:?cache=shared");

   con = DriverManager.getConnection("jdbc:sqlite:CardTypes.db");

   stat = con.createStatement();

   stat.executeUpdate("attach '' as TMP;");

   // Statement stat2 = conFile.createStatement();

   // stat.executeUpdate("drop table if exists user");

   // creating table

   stat.executeUpdate("create table if not exists cardtype("

   + "name varchar," + "fields varchar," + "types varchar,"

   + "primary key (name));");

   stat.executeUpdate("create table TMP.cardtype as select * from cardtype;");

  }

  catch (SQLException e) {

   System.out.println("ERROR: constructor failed\n" + e.getMessage());

   e.printStackTrace();

  } finally {
   try {
    if (stat != null)
     stat.close();
   } catch (Exception ex) {
   }
  }

 }

 /**
  * 
  * Drops the "cardtype" table from the database,
  * 
  * then re-creates the same table.
  */

 public void clear() {
  Statement stat = null;

  try {

   stat = con.createStatement();

   stat.executeUpdate("drop table if exists cardtype");

   stat.executeUpdate("create table cardtype("

   + "name varchar," + "fields varchar," + "types varchar,"

   + "primary key (name));");

   stat.executeUpdate("drop table if exists TMP.cardtype");

   stat.executeUpdate("create table TMP.cardtype("

   + "name varchar," + "fields varchar," + "types varchar,"

   + "primary key (name));");

  } catch (SQLException e) {

   // TODO Auto-generated catch block

   System.out.println("ERROR: CardTypeDBM.clear failed\n"
     + e.getMessage());

  } finally {
   try {
    if (stat != null)
     stat.close();
   } catch (Exception ex) {
   }
  }

 }

 /**
  * 
  * Attempts to create the "cardtype" table if it
  * 
  * does not already exist.
  */

 public void createTable() {
  Statement stat = null;

  try {

   stat = con.createStatement();

   stat.executeUpdate("create table if not exists cardtype("

   + "name varchar," + "fields varchar," + "types varchar,"

   + "primary key (name));");

   stat.executeUpdate("create table TMP.cardtype("

   + "name varchar," + "fields varchar," + "types varchar,"

   + "primary key (name));");

  } catch (SQLException e) {

   System.out.println("ERROR: CardTypeDBM.createTable failed\n"
     + e.getMessage());

  } finally {
   try {
    if (stat != null)
     stat.close();
   } catch (Exception ex) {
   }
  }

 }

 /**
  * 
  * Adds a new card type element with the specified name (a unique ID), and a
  * 
  * list of Strings (card type field names), each String associated with
  * 
  * exactly one integer (which will represent the data type of the field).
  * 
  * For any i, field[i] is associated with types[i].
  * 
  * 
  * 
  * @param name
  *            the unique name of the card type
  * 
  * @param fields
  *            the array of field names
  * 
  * @param types
  *            the corresponding types array
  */

 public void add(String name, String fields[], int types[]) {

  if (fields.length != types.length) {

   System.out
     .println("ERROR: CardTypeDBM.add: Illegal fields and types pair");

   return;

  }

  try {

   PreparedStatement prep = con

   .prepareStatement("insert into TMP.cardtype values(?,?,?);");

   prep.setString(1, name);

   prep.setString(2, join(delimiter, fields));

   prep.setString(3, join(delimiter, types));

   prep.execute();

  }

  catch (SQLException e) {

   System.out.println("ERROR: CardTypeDBM.add failed\n"
     + e.getMessage());

  }

 }

 /**
  * 
  * Deletes the element with the specified name from the CardTypeDatabase.
  * 
  * 
  * 
  * @param name
  *            the unique name of the card type to delete
  * 
  * @return returns 0 if successful - other numbers indicate errors
  */

 public int remove(String name) {

  try {

   PreparedStatement prep = con

   .prepareStatement("delete from TMP.cardtype where name=?;");

   prep.setString(1, name);

   prep.execute();

   return 0;

  }

  catch (SQLException e) {

   System.out.println("ERROR: CardTypeDBM.remove failed\n"
     + e.getMessage());

  }

  return -1;

 }

 public void save() {
  Statement stat = null;

  try {

   stat = con.createStatement();

   // stat.executeUpdate("drop table if exists user");

   // creating table

   stat.executeUpdate("drop table if exists cardtype");

   stat.executeUpdate("create table cardtype as select * from TMP.cardtype;");

  }

  catch (SQLException e) {

   System.out.println("ERROR: CardTypeDBM.save failed\n"
     + e.getMessage());

  } finally {
   try {
    if (stat != null)
     stat.close();
   } catch (Exception ex) {
   }
  }

 }

 public void revert() {
  Statement stat = null;

  try {

   stat = con.createStatement();

   // stat.executeUpdate("drop table if exists user");

   // creating table

   stat.executeUpdate("drop table if exists TMP.cardtype");

   stat.executeUpdate("create table TMP.cardtype as select * from cardtype;");

  }

  catch (SQLException e) {

   System.out.println("ERROR: CardTypeDBM.revert failed\n"
     + e.getMessage());

  } finally {
   try {
    if (stat != null)
     stat.close();
   } catch (Exception ex) {
   }
  }

 }

 /**
  * 
  * Gets the unique names of all card types currently in the database
  * 
  * 
  * 
  * @return An array containing all the card types' names or an
  * 
  *         array containing "ERROR" if an SQL error occured
  */

 public String[] getEntries() {

  Statement stat = null;

  try {

   stat = con.createStatement();

   ResultSet res = stat.executeQuery("select * from TMP.cardtype");

   List<String> out = new ArrayList<String>();

   while (res.next()) {

    out.add(res.getString("name"));

   }

   String[] s = new String[out.size()];

   out.toArray(s);

   return s;

  } catch (SQLException e) {

   System.out.println("ERROR: CardTypeDBM.getEntries failed\n"
     + e.getMessage());

  } finally {
   try {
    if (stat != null)
     stat.close();
   } catch (Exception ex) {
   }
  }

  return new String[] { "ERROR" };

 }

 /**
  * 
  * Returns the fields array for a requested card type
  * 
  * 
  * 
  * @param name
  *            name of the card type to get the fields from
  * 
  * @return array of fields or an array containing "ERROR"
  * 
  *         if an SQL error occured
  */

 public String[] getFields(String name) {

  /*
   * Goes to the element with the specified name and returns the
   * corresponding String array fields[],
   * 
   * specified upon initialization
   */
  Statement stat = null;

  try {

   stat = con.createStatement();
   System.out.println(name);
   ResultSet res = stat
     .executeQuery("select fields from TMP.cardtype where name='"
       + name + "'");
   String out = "";
   while(res.next()){
     out = res.getString("fields");
     break;
   }
   
   res.close();
   stat.close();

   return out.split(delimiter);

  }

  catch (SQLException e) {

   System.out.println("ERROR: CardTypeDBM.getFields failed\n"
     + e.getMessage());
   e.printStackTrace();

  } finally {
   try {
    if (stat != null)
     stat.close();
   } catch (Exception ex) {
   }
  }

  return new String[] { "ERROR" };

 }

 /**
  * 
  * Returns the int array of types belonging to the given
  * 
  * card type. These correspond 1-to-1 with the fields
  * 
  * belonging to the same card type
  * 
  * 
  * 
  * @param name
  *            the unique card type name
  * 
  * @return array of types or an array containing -1
  * 
  *         if an SQL error occured
  */

 public int[] getTypes(String name) {

  /*
   * Goes to the element with the specified name and returns the
   * corresponding int array types[],
   * 
   * specified upon initialization
   */
  Statement stat = null;

  try {

   stat = con.createStatement();

   ResultSet res = stat
     .executeQuery("select types from TMP.cardtype where name='"
       + name + "'");
   String out = "";
   while(res.next()){
     out = res.getString("types");
     break;
   }
   
   res.close();
   stat.close();

   if (out=="") return new int[0];
   return convertArray(out.split(delimiter));

  }

  catch (SQLException e) {

   System.out.println("ERROR: CardTypeDBM.getTypes failed\n"
     + e.getMessage());
   e.printStackTrace();

  } finally {
   try {
    if (stat != null)
     stat.close();
   } catch (Exception ex) {
   }
  }

  return new int[] { -1 };

 }

 /**
  * 
  * Prints to out all the entries in the database.
  * 
  * Present for testing purposes
  */

 public void printAll() {
  Statement stat = null;

  try {

   stat = con.createStatement();

   ResultSet res = stat.executeQuery("select * from TMP.cardtype");

   while (res.next()) {

    System.out.println(res.getString("name") + " "
      + res.getString("fields")

      + " " + res.getString("types"));

   }

  } catch (SQLException e) {

   System.out.println("ERROR: CardTypeDBM.printAll failed\n"
     + e.getMessage());

  } finally {
   try {
    if (stat != null)
     stat.close();
   } catch (Exception ex) {
   }
  }

 }

 public void close() {
  try {
   con.close();
  } catch (Exception ex) {
  }
 }

 /*
  * Elements in CardTypeDatabase are read-only and may not be modified. There
  * will only be one
  * 
  * CardTypeDatabase, so it is not necessary to worry about which database is
  * being called.
  */

}
