import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

public class DeckTableDBM {

 private Connection connection;

 // create a new DeckTableDBM object.
 public DeckTableDBM() {
  // open the database connection
  try {
   Class.forName("org.sqlite.JDBC");
   connection = DriverManager.getConnection("jdbc:sqlite:alldecks.db");
   Statement statement = connection.createStatement();

   // if first time, run initial setup
   statement
     .executeUpdate("create table if not exists decks (name string not null primary key, notes string, collection string, virtual boolean)");
  } catch (Exception e) {
   System.out.println("Error 9: " + e.getMessage());
  }
 }

 // close the database connection
 public void close() {
  try {
   connection.close();
  } catch (Exception e) {
   {
    System.out.println("Error 11: " + e.getMessage());
   }
  }
 }

 // create a new deck without any notes attatched to it
 public int create(String name, String collName) {
   return create(name, "", collName);
 }

 // create a new deck with notes
 public int create(String name, String notes, String collName) {
   return create(name, notes, collName, false);
 }
 public int create(String name, String notes, String collName, boolean virtual){
  try {
   // check if the deck name already exists
   int i = 0;
   Statement statement = connection.createStatement();
   ResultSet rs = statement
     .executeQuery("select 1 from decks where name= '" + name
       + "'");
   while (rs.next()) {
    i++;
   }
   if (i != 0) {
    System.err.println("Deck Name Already Exists!");
    return -1;
   }

   // create deck if it doesnt already exist
   statement.executeUpdate("insert into decks values('" + name
     + "', '" + notes + "', '" + collName + "', '"+virtual+"')");
   statement.executeUpdate("create table '" + name
     + "' (id integer not null primary key, quantity integer)");
   return 1;
  } catch (Exception e) {
   System.out.println("Error 1: " + e.getMessage());
   e.printStackTrace();
   return -1;
  }
 }

 public boolean deckExists(String deckName) {
  try {
   // check if the deck name already exists
   int i = 0;
   Statement statement = connection.createStatement();
   ResultSet rs = statement
     .executeQuery("select 1 from decks where name='"
       + deckName + "'");
   while (rs.next()) {
    i++;
   }
   if (i != 0) {
    return true;
   }
   return false;
  } catch (Exception e) {
   System.out.println("Error 1: " + e.getMessage());
   e.printStackTrace();
   return true;
  }
 }

 // set quantity of card in deck
 public void setQty(String deck, int cardID, int qty) {
  try {
   // check if card exists in deck
   int i = 0;
   Statement statement = connection.createStatement();
   ResultSet rs = statement.executeQuery("select 1 from '" + deck
     + "' where id= '" + cardID + "'");
   while (rs.next()) {
    i++;
   }
   if (i == 0) {
    System.out.println("Card Doesnt Exist. Cant update quantity");
    return;
   }

   // if card exists, update quantity
   statement.executeUpdate("update '" + deck + "' set quantity = "
     + qty + " where id = " + cardID);
  } catch (Exception e) {
   System.out.println("Error 2: " + e.getMessage());
  }
 }

 // print out all card ids and quantity of each in deck
 public Vector<Object> getCardIds(String deckName) {
  Vector<Object> ret = new Vector<Object>();
  try {
   Statement statement = connection.createStatement();
   ResultSet rs = statement.executeQuery("select * from '" + deckName
     + "'");
   while (rs.next()) {
    ret.add(rs.getString("id"));
    ret.add(rs.getInt("quantity"));
   }
   return ret;
  } catch (Exception e) {
   System.out.println("Error 3: " + e.getMessage());
  }
  return null;
 }
 
 public String getCollection(String deckName){
   String ret = null;
   try{
     Statement statement = connection.createStatement();
     ResultSet rs = statement.executeQuery("select collection from decks where name = '"
       + deckName + "'");
     while (rs.next()){
       ret = rs.getString("collection");
       return ret;
     }
   } catch (Exception e){
     System.out.println("Error 3.1: " + e.getMessage());
     e.printStackTrace();
   }
   return null;
 }
 
 public int getVirtual(String deckName){
   boolean ret = false;
   int found = 0;
   ResultSet rs = null;
   Statement statement = null;
   try{
   statement = connection.createStatement();
   rs = statement.executeQuery("select virtual from decks where name = '" + deckName + "'");
   while(rs.next()){
     ret = rs.getBoolean("virtual");
     found=1;
     break;
   }
   }catch (Exception e){
     System.out.println("Error 3.2: " + e.getMessage());
     e.printStackTrace();
   }
   try{
     if (statement!=null) statement.close();
     if (rs!=null) rs.close();
   }catch(Exception ex){
   }
   if (found==0) return -1; //Return -1 if deckName not found
   if (ret) return 1;
   return 0;
 }

 public Vector<Object> getDeck(String deckName) {
  Vector<Object> data = new Vector<Object>();
  try {
   Statement statement = connection.createStatement();
   ResultSet rs = statement.executeQuery("select * from '" + deckName
     + "'");
   while (rs.next()) {
    // read the result set
    data.add(rs.getString("id"));
    data.add(rs.getInt("quantity"));
   }
  } catch (Exception e) {
   System.out.println("Error 3: " + e.getMessage());
   return null;
  }
  return data;
 }

 // add a card to the deck
 public void addCard(String name, int cardnum, int quant) {
  try {
   Statement statement = connection.createStatement();
   statement.executeUpdate("insert into '" + name + "' values("
     + cardnum + ", " + quant + ")");
  }

  // will catch error from card already being in the deck
  catch (Exception e) {
   if (e.getMessage().equals("PRIMARY KEY must be unique"))
    System.out.println("Card already exists in deck!");
   else
    System.out.println("Error 4: " + e.getMessage());
  }
 }

 public void removeCard(String name, int cardnum) {
  try {
   Statement statement = connection.createStatement();
   int i = 0;
   // check if card exists
   ResultSet rs = statement.executeQuery("select id from '" + name
     + "' where id= '" + cardnum + "'");
   while (rs.next()) {
    i++;
   }
   if (i == 0) {
    System.out.println("Card Doesnt Exist. Cant remove card");
    return;
   }

   // remove card if it exists
   statement.executeUpdate("delete from '" + name + "' Where id = "
     + cardnum);
  } catch (Exception e) {
   System.out.println("Error 8: " + e.getMessage());
  }
 }

 public void removeDeck(String name) {
  try {
   // remove deck if it exists
   Statement statement = connection.createStatement();
   statement.executeUpdate("drop table if exists '" + name + "'");
   statement.executeUpdate("delete from decks where name = '" + name
     + "'");
  } catch (Exception e) {
   System.out.println("Error 8: " + e.getMessage());
  }
 }

 // read out all deck names
 public void getDecks() {
  try {
   // get all deck names from the decks table
   Statement statement = connection.createStatement();
   ResultSet rs = statement.executeQuery("select * from decks");
   while (rs.next()) {
    // print the result set
    System.out.println("deck name = " + rs.getString("name"));
   }
  } catch (Exception e) {
   System.out.println("Error 10: " + e.getMessage());
  }
 }

 public Vector<String> getDeckNames() {
  Vector<String> names = new Vector<String>();
  try {
   // get all deck names from the decks table
   Statement statement = connection.createStatement();
   ResultSet rs = statement.executeQuery("select * from decks");
   while (rs.next()) {
    // print the result set
    String temp = rs.getString("name");
    names.add(temp);
   }
   return names;
  } catch (Exception e) {
   System.out.println("Error 10: " + e.getMessage());
  }
  return null;
 }

 // get notes a specific deck
 public void setDeckNotes(String deckName, String notes) {
  try {
   Statement statement = connection.createStatement();
   statement.executeUpdate("update decks set notes = '" + notes
     + "' where name = '" + deckName + "'");
  } catch (Exception e) {
   System.out.println("Error 12: " + e.getMessage());
  }
 }
 
 public void setVirtual(String deckName, int virtual){
   Statement statement = null;
   try{
     statement = connection.createStatement();
     System.out.println(deckName);
     statement.executeUpdate("update decks set virtual = '" + virtual + "' where name = '" + deckName + "'");
   }catch (Exception e){
     System.out.println("Error 12.1: " + e.getMessage());
   }
   try{
     if (statement!=null) statement.close();
   }catch(Exception ex){
   }
 }

 public String getDeckNotes(String name) {
  try {
   // get all deck names from the decks table
   Statement statement = connection.createStatement();
   ResultSet rs = statement
     .executeQuery("select notes from decks where name = '"
       + name + "'");
   while (rs.next()) {
    // print the result set
    return rs.getString("notes");
   }
  } catch (Exception e) {
   System.out.println("Error 10: " + e.getMessage());
   return null;
  }
  return null;
 }

 public String grabDeckNotes(String name) {
  try {
   // get all deck names from the decks table
   Statement statement = connection.createStatement();
   ResultSet rs = statement
     .executeQuery("select notes from decks where name = '"
       + name + "'");
   while (rs.next()) {
    // print the result set
    return rs.getString("notes");
   }
  } catch (Exception e) {
   System.out.println("Error 10: " + e.getMessage());
  }
  return null;
 }

 public static void main(String[] args) {

  DeckTableDBM deck = new DeckTableDBM();
  deck.removeDeck("Black Deck");
  deck.removeDeck("BlackDeck");
  deck.removeDeck("Clubs");
  deck.removeDeck("Spades");
  deck.removeDeck("Warrior Tribal (RG)");
  deck.removeDeck("New Alara (WUBRG).1");
  deck.removeDeck("New Alara (WUBRG).2");
  deck.removeDeck("asdf");
  deck.removeDeck("asert");
  deck.removeDeck("qwerty");
  deck.removeDeck("qwdfqweu");
  
  
  //deck.removeDeck("Warrior Tribal (RG)");
  /*deck.create("Deck Number 1", "My First Deck");
  deck.addCard("Deck Number 1", 1, 1);
  deck.addCard("Deck Number 1", 2, 1);
  deck.addCard("Deck Number 1", 3, 1);
  deck.addCard("Deck Number 1", 4, 1);
  deck.addCard("Deck Number 1", 5, 1);
  deck.addCard("Deck Number 1", 6, 1);
  deck.addCard("Deck Number 1", 1347, 3);
  deck.setQty("Deck Number 1", 3, 5);
  deck.setQty("Deck Number 1", 45, 5);
  deck.removeCard("Deck Number 1", 87);
  deck.getCardIds("Deck Number 1");
  deck.getDecks();
  deck.removeDeck("Deck Number 1");
  deck.getDeckNotes("Deck Number 1");
  deck.setDeckNotes("Deck Number 2", "Hello");
  deck.setDeckNotes("Deck Number 1", "Hello");
  deck.getDeckNotes("Deck Number 1");*/
  deck.close();
  try {
   Class.forName("org.sqlite.JDBC");
   Connection connection = null;
   connection = DriverManager.getConnection("jdbc:sqlite:alldecks.db");
   Statement statement = connection.createStatement();
   ResultSet rs = statement
     .executeQuery("select * from sqlite_master where type='table'");
   while (rs.next()) {
    // read the result set
    System.out.println("table name = " + rs.getString("name"));
   }

   connection.close();
  } catch (Exception e) {
   System.out.println("Error 6: " + e.getMessage());
  }

 }

 public String[][] search(String which, String type, String quer, boolean all)
 {
   quer = quer.replaceAll("Card ID", "id");
   if(all)//if checking all decks of type type
   {
    //get all decks of cardtype type, go through collections
    String s = "Select * from decks";
    try
    {
     Statement st = connection.createStatement();
     ResultSet rs = st.executeQuery(s);
     int i = 0;
     while(rs.next())i++;//get number of decks
     boolean[] b = new boolean[i];
     rs = st.executeQuery(s);
     Statement ss = connection.createStatement();
     ResultSet r;
     i = 0;
     while (rs.next())//go through list of decks
     {
      //check if current deck's collection's type equals asking type
       String coll = rs.getString("collection");
       String ct = Core.getCollCardType(coll);
      //r = ss.executeQuery("select type from m_coll where collection = '" + rs.getString("collection") +"'") ;
      //while(r.next())
      //{
       //set boolean array of current deck index
       if(ct.equals(type))
        b[i] = true;
       else b[i] = false;
      //}
      i++;
     }
     int j = 0;
     int total = 0;
     //get decks again
     rs = st.executeQuery(s);
     while(rs.next())
     {
      if(b[j++])//table type matches asking type
      {
       r = ss.executeQuery("select * from " + rs.getString("name") + " where (" + quer + ")");
       while(r.next())total++;
      }
        
     }
     //get actual query results
     String[][] array = new String[total][3];
     j=0;
     i = 0;
     rs = st.executeQuery(s);
     while(rs.next())
     {
      if(b[j++])//table type matches asking type
      {
       //get results from current deck against query string
       r = ss.executeQuery("select * from " + rs.getString("name") + " where (" + quer + ")");
       while(r.next())
       {
        array[i][0] = rs.getString("name");
        array[i][1] = r.getString("id");
        array[i++][2] = r.getString("quantity");
       }
      }
        
     }
     return array;
    }
    catch(Exception e){System.out.println(e.getMessage());return null;}
   
   }
   else // checking only one deck, String which
   {
    try{
     String s = "select * from " + which + " where (" + quer + ")";
     Statement statement = connection.createStatement();
     ResultSet rs = statement.executeQuery(s);
     int i = 0;
     while(rs.next())
      i++;
     rs = statement.executeQuery(s);
     String array[][] = new String[i][3];
     i=0;
     while(rs.next())
     {
      array[i][0] = which;
      array[i][1] = rs.getString("id");
      array[i][2] = rs.getString("quantity");
      i++;
     }
     return array;
    }catch(Exception e){System.out.println("Deck Query Failed:\n" + e.getMessage() + "\n" + e.getStackTrace());}
    return null;
   }
 }
}
